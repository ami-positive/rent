package com.sixt.android.email;

import android.content.Context;
import android.content.Intent;

import com.sixt.android.app.util.AppContext;

public class EmailSender {

	private EmailSender() {}

	public static void send(String addr, String subject, String body) {
		body = body==null ? "" : body;
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, addr);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, body);

		Context c = AppContext.appContext;
		c.startActivity(Intent.createChooser(intent, "Send Email"));
	}

}
