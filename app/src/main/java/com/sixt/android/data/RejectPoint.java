package com.sixt.android.data;

import com.sixt.android.ui.Point;

import com.sixt.android.app.json.response.ImageRejects;
import com.sixt.android.app.json.response.ImageRejects.RejectValue;

import static com.sixt.android.activities.ImageCarActivity.MARKER_NOTSET_CODE;

public class RejectPoint {
	public final String Reject_Type; // "B" "S" "T" or "W"
    public final String Car_Type;
    public final String Reject_Desc;
    public final String Reject_Code;
    
    public int reject_code_position = MARKER_NOTSET_CODE;
    

    public Point screenPosition;
    
	private static final boolean _DEBUG = true;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof RejectPoint)) {
			return false;
		}
		RejectPoint other = (RejectPoint) o;
		return Reject_Code.equals(other.Reject_Code);
	}
	
	
	public void clear() {
		reject_code_position = MARKER_NOTSET_CODE;
	}

    public RejectPoint(String Reject_Type, String Car_Type, String Reject_Desc, String Reject_Code, Point position) {
        this(Reject_Type, Car_Type, Reject_Desc, Reject_Code);
        this.screenPosition = position;
    }

    public RejectPoint(String Reject_Type, String Car_Type, String Reject_Desc, String Reject_Code) {
        this.Reject_Type = Reject_Type;
        this.Car_Type = Car_Type;
        this.Reject_Desc = Reject_Desc;
        this.Reject_Code = Reject_Code;
        if (_DEBUG) {
        	ImageRejects.get_for_type(Reject_Type); // make sure no exception
        }
    }
    
    public RejectValue[] get_possible_rejects() { 
    	return ImageRejects.get_for_type(Reject_Type); 
    }

	public Object[] get_possible_reject_descriptions() { 
		return ImageRejects.get_descriptions_for_type(Reject_Type);
	}
    
    
    public void setScreenPosition(Point position){
        this.screenPosition = position;
    }

    public RejectPoint setScreenPosition(float x, float y){
        this.screenPosition = new Point(x,y);
        return this;
    }


	public int get_value() {
		int val = ImageRejects.position_to_value(reject_code_position, Reject_Type);
		return val;
	}


	public int get_position_from_value(int value) {
		int position = ImageRejects.value_to_position(value, Reject_Type);
		return position;
	}


	public boolean is_lower_right() {
		boolean is_lower = screenPosition.x == ImageRejectContainer.LOWER_RIGHT_POS;
		return is_lower;
	}


	public static int lower_right_selected_value() {
		return 1;
	}



}
