package com.sixt.android.data;

import com.sixt.android.ui.Point;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ImageRejectContainer {
	
    public static final int LOWER_RIGHT_POS = -100; 
    
	// content for points containter
    //unused rejects
    public static RejectPoint _001 = new RejectPoint("S","PM","חשש לנזילת שמן","001");
    public static RejectPoint _002 = new RejectPoint("S","PM","חשש לפגיעה במרכב תחתון","002");

    public static RejectPoint AL = new RejectPoint("B","PM","פגוש קדמי שמאל","AL");
    public static RejectPoint AM = new RejectPoint("B","PM","פגוש קדמי אמצע","AM");
    public static RejectPoint AR = new RejectPoint("B","PM","פגוש קדמי ימין","AR"); // גדכד
    public static RejectPoint BL = new RejectPoint("B","PM","פגוש אחורי שמאל","BL");
    public static RejectPoint BM = new RejectPoint("B","PM","פגוש אחורי אמצע","BM");
    public static RejectPoint BR = new RejectPoint("B","PM","פגוש אחורי ימין","BR"); //d גדכ
    public static RejectPoint CLB = new RejectPoint("B","PM","מכסה מנוע אחורי שמאל","CLB");
    public static RejectPoint CLF = new RejectPoint("B","PM","מכסה מנוע קדמי שמאל","CLF");
    public static RejectPoint CRB = new RejectPoint("B","PM","מכסה מנוע אחורי ימין","CRB");
    public static RejectPoint CRF = new RejectPoint("B","PM","מכסה מנוע קדמי ימין","CRF");
    public static RejectPoint ELB = new RejectPoint("B","PM","כנף קדמי אחורי שמאל","ELB");  //גדכ
    public static RejectPoint ELF = new RejectPoint("B","PM","כנף קדמי קדמי שמאל","ELF");
    public static RejectPoint ERB = new RejectPoint("B","PM","כנף קדמי אחורי ימין","ERB");
    public static RejectPoint ERF = new RejectPoint("B","PM","כנף קדמי קדמי ימין","ERF");
    public static RejectPoint FLB = new RejectPoint("B","PM","דלת קדמית תחתון אחורי ימין","FLB");
    public static RejectPoint FLF = new RejectPoint("B","PM","דלת קדמית תחתון קדמי ימין","FLF");
    public static RejectPoint FUB = new RejectPoint("B","PM","דלת קדמית עליון אחורי ימין","FUB");
    public static RejectPoint FUF = new RejectPoint("B","PM","דלת קדמית עליון קדמי ימין","FUF");
    public static RejectPoint GB = new RejectPoint("B","PM","סף דלת קדמית אחורי ימין","GB");
    public static RejectPoint GF = new RejectPoint("B","PM","סף דלת קדמית קדמי ימין","GF");
    public static RejectPoint GRILL = new RejectPoint("T","PM","גריל", "GRILL",new Point(364,139));
    public static RejectPoint HLB = new RejectPoint("B","PM","דלת קדמית תחתון אחורי שמאל","HLB");
    public static RejectPoint HLF = new RejectPoint("B","PM","דלת קדמית תחתון קדמי שמאל","HLF");
    public static RejectPoint HUB = new RejectPoint("B","PM","דלת קדמית עליון אחורי שמאל","HUB");
    public static RejectPoint HUF = new RejectPoint("B","PM","דלת קדמית עליון קדמי שמאל","HUF");
    public static RejectPoint IB = new RejectPoint("B","PM","סף דלת קדמית אחורי שמאל","IB");
    public static RejectPoint IF = new RejectPoint("B","PM","סף דלת קדמית קדמי שמאל","IF");
    public static RejectPoint JLB = new RejectPoint("B","PM","דלת אחורית תחתון אחורי ימין","JLB");
    public static RejectPoint JLF = new RejectPoint("B","PM","דלת אחורית תחתון קדמי ימין","JLF");
    public static RejectPoint JUB = new RejectPoint("B","PM","דלת אחורית עליון אחורי ימין","JUB");
    public static RejectPoint JUF = new RejectPoint("B","PM","דלת אחורית עליון קדמי ימין","JUF");
    public static RejectPoint KB = new RejectPoint("B","PM","סף דלת אחורית אחורי ימין","KB");
    public static RejectPoint KESHETGALGS = new RejectPoint("B","PM","קשת גלגל שמאל","KESHETGALGS");
    public static RejectPoint KESHETGALGY = new RejectPoint("B","PM","קשת גלגל ימין","KESHETGALGY");
    public static RejectPoint KF = new RejectPoint("B","PM","סף דלת אחורית קדמי ימין","KF");
    public static RejectPoint LLB = new RejectPoint("B","PM","דלת אחורית תחתון אחורי שמאל","LLB");
    public static RejectPoint LLF = new RejectPoint("B","PM","דלת אחורית תחתון קדמי שמאל","LLF");
    public static RejectPoint LMIRRORH1 = new RejectPoint("B","PM","בית מראה שמאל","LMIRRORH1");
    public static RejectPoint LUB = new RejectPoint("B","PM","דלת אחורית עליון אחורי שמאל","LUB");
    public static RejectPoint LUF = new RejectPoint("B","PM","דלת אחורית עליון קדמי שמאל","LUF");
    public static RejectPoint MB = new RejectPoint("B","PM","סף דלת אחורית אחורי שמאל","MB");
    public static RejectPoint MDASA = new RejectPoint("B","M","משקוף דלת אחורית אחורי שמאל","MDASA");
    public static RejectPoint MDASE = new RejectPoint("B","PM","משקוף דלת אחורית עליון שמאל","MDASE");
    public static RejectPoint MDASK = new RejectPoint("B","PM","משקוף דלת אחורית קדמי שמאל","MDASK");
    public static RejectPoint MDAYA = new RejectPoint("B","M","משקוף דלת אחורית אחורי ימין","MDAYA");
    public static RejectPoint MDAYE = new RejectPoint("B","PM","כנף אחורי עליון ימין","MDAYE");
    public static RejectPoint MDAYK = new RejectPoint("B","PM","משקוף דלת אחורית קדמי ימין","MDAYK");
    public static RejectPoint MDKSA = new RejectPoint("B","PM","משקוף דלת קדמית אחורי שמאל","MDKSA");
    public static RejectPoint MDKSE = new RejectPoint("B","PM","משקוף דלת קדמית עליון שמאל","MDKSE");
    public static RejectPoint MDKSK = new RejectPoint("B","PM","משקוף דלת קדמית קדמי שמאל","MDKSK");
    public static RejectPoint MDKYA = new RejectPoint("B","PM","משקוף דלת קדמית אחורי ימין","MDKYA");
    public static RejectPoint MDKYE = new RejectPoint("B","PM","משקוף דלת קדמית עליון ימין","MDKYE");
    public static RejectPoint MDKYK = new RejectPoint("B","PM","משקוף דלת קדמית קדמי ימין","MDKYK");
    public static RejectPoint MF = new RejectPoint("B","PM","סף דלת אחורית קדמי שמאל","MF");
    public static RejectPoint MKSE = new RejectPoint("B","M","משקוף כנף עליון שמאל","MKSE");
    public static RejectPoint MKSK = new RejectPoint("B","M","משקוף כנף קדמי שמאל","MKSK");
    public static RejectPoint MKYE = new RejectPoint("B","M","משקוף כנף עליון ימין","MKYE");
    public static RejectPoint MKYK = new RejectPoint("B","M","משקוף כנף קדמי ימין","MKYK");
    public static RejectPoint NLB = new RejectPoint("B","PM","גג אחורי שמאל","NLB");
    public static RejectPoint NLF = new RejectPoint("B","PM","גג קדמי שמאל","NLF");
    public static RejectPoint NRB = new RejectPoint("B","PM","גג אחורי ימין","NRB");
    public static RejectPoint NRF = new RejectPoint("B","PM","גג קדמי ימין","NRF");
    public static RejectPoint OLB = new RejectPoint("B","PM","כנף אחורי אחורי שמאל","OLB");
    public static RejectPoint OLF = new RejectPoint("B","PM","כנף אחורי קדמי שמאל","OLF");
    public static RejectPoint OLU = new RejectPoint("B","PM","כנף אחורי עליון שמאל","OLU");
    public static RejectPoint ORB = new RejectPoint("B","PM","כנף אחורי אחורי","ORB");
    public static RejectPoint ORF = new RejectPoint("B","PM","כנף אחורי קדמי ימין","ORF");
    public static RejectPoint ORU = new RejectPoint("B","PM","כנף אחורי עליון ימין","ORU");
    public static RejectPoint PANASAS = new RejectPoint("T","PM","פנס אחורי שמאל","PANASAS");
    public static RejectPoint PANASAY = new RejectPoint("T","PM","פנס אחורי ימין","PANASAY");
    public static RejectPoint PANASKS = new RejectPoint("T","PM","פנס קדמי שמאל","PANASKS");
    public static RejectPoint PANASKY = new RejectPoint("T","PM","פנס קדמי ימין","PANASKY");
    public static RejectPoint PANASVS = new RejectPoint("T","PM","פנס איתות שמאל","PANASVS");
    public static RejectPoint PANASVY = new RejectPoint("T","PM","פנס איתות ימין","PANASVY");
    public static RejectPoint PLB = new RejectPoint("B","PM","מכסה תא מטען אחורי (אופקי) שמאל","PLB");
    public static RejectPoint PLF = new RejectPoint("B","PM","מכסה תא מטען קדמי (אופקי) שמאל","PLF");
    public static RejectPoint PRB = new RejectPoint("B","PM","מכסה תא מטען אחורי (אופקי) ימין","PRB");
    public static RejectPoint PRF = new RejectPoint("B","PM","מכסה תא מטען קדמי (אופקי) ימין","PRF");
    public static RejectPoint QL = new RejectPoint("B","P","דלת תא מטען (אנכי) שמאל","QL");
    public static RejectPoint QR = new RejectPoint("B","P","דלת תא מטען (אנכי) ימין","QR");
    public static RejectPoint RMIRRORH1 = new RejectPoint("B","PM","בית מראה ימני","RMIRRORH1");
    public static RejectPoint TB = new RejectPoint("W","PM","חלון אחורי ראשי","TB");
    public static RejectPoint TF = new RejectPoint("W","PM","חלון קדמי ראשי","TF");
    public static RejectPoint ULB = new RejectPoint("W","PM","חלון אחורי שמאל","ULB");
    public static RejectPoint ULB3 = new RejectPoint("W","M","חלון כנף אחרית קדמית שמאל","ULB3");
    public static RejectPoint ULB4 = new RejectPoint("W","M","חלון כנף אחרית אחרי שמאל","ULB4");
    public static RejectPoint ULF = new RejectPoint("W","PM","חלון קדמי שמאל","ULF");
    public static RejectPoint URB = new RejectPoint("W","PM","חלון אחורי ימין","URB");
    public static RejectPoint URB3 = new RejectPoint("W","M","חלון כנף אחרית קדמית ימין","URB3");
    public static RejectPoint URB4 = new RejectPoint("W","M","חלון כנף אחרית אחורי","URB4");
    public static RejectPoint URF = new RejectPoint("W","PM","חלון קדמי ימין","URF");
    public static RejectPoint VLB = new RejectPoint("W","P","משולש אחורי שמאל","VLB");
    public static RejectPoint VLB1 = new RejectPoint("W","M","חלון אחורי שמאל","VLB1");
    public static RejectPoint VRB = new RejectPoint("W","P","משולש אחורי ימין","VRB");
    public static RejectPoint VRB1 = new RejectPoint("W","M","חלון אחורי ימין","VRB1");

    
    public static RejectPoint [] getRejectPoints(String imageType){
        if (imageType.equals("p")){
            // add coordinates for p image
            RejectPoint [] rejectPoints = {
            		_001.setScreenPosition( LOWER_RIGHT_POS, LOWER_RIGHT_POS), 
            		_002.setScreenPosition( LOWER_RIGHT_POS, LOWER_RIGHT_POS),
                    AL.setScreenPosition(257,34),
                    AM.setScreenPosition(364,34),
                    AR.setScreenPosition(468,34),
                    GRILL.setScreenPosition(364,139),
                    ELF.setScreenPosition(116,201),
                    PANASVS.setScreenPosition(234,219),
                    CLF.setScreenPosition(292,211),
                    CRF.setScreenPosition(430,211),
                    PANASVY.setScreenPosition(490,219),
                    ERF.setScreenPosition(607,201),
                    ELB.setScreenPosition(147,306),
                    LMIRRORH1.setScreenPosition(215,383),
                    CLB.setScreenPosition(292,325),
                    CRB.setScreenPosition(430,325),
                    RMIRRORH1.setScreenPosition(515,383),
                    ERB.setScreenPosition(577,305),
                    IF.setScreenPosition(33,452),
                    HLF.setScreenPosition(100,455),
                    HUF.setScreenPosition(156,455),
                    TF.setScreenPosition(362,448),
                    FUF.setScreenPosition(567,455),
                    FLF.setScreenPosition(623,455),
                    GF.setScreenPosition(692,452),
                    IB.setScreenPosition(33,581),
                    HLB.setScreenPosition(100,581),
                    HUB.setScreenPosition(155,581),
                    ULF.setScreenPosition(200,523),
                    MDKSA.setScreenPosition(213,604),
                    MDKSE.setScreenPosition(247,553),
                    NLF.setScreenPosition(324,581),
                    NRF.setScreenPosition(400,581),
                    MDKYE.setScreenPosition(476,553),
                    URF.setScreenPosition(523,523),
                    MDKYA.setScreenPosition(511,604),
                    FUB.setScreenPosition(567,581),
                    FLB.setScreenPosition(623,581),
                    GB.setScreenPosition(692,581),
                    MB.setScreenPosition(33,697),
                    LLF.setScreenPosition(100,685),
                    LUF.setScreenPosition(156,685),
                    MDASK.setScreenPosition(211,672),
                    NLB.setScreenPosition(325,693),
                    NRB.setScreenPosition(401, 693),
                    MDAYK.setScreenPosition(511,672),
                    JUF.setScreenPosition(568,685),
                    JLF.setScreenPosition(623,685),
//                    JLB.setScreenPosition(623,727),
                    JLB.setScreenPosition(624,736), 
                    KB.setScreenPosition(692,697),
                    ULB.setScreenPosition(200,725),
                    MDASE.setScreenPosition(252,725),
                    MDAYE.setScreenPosition(471,725),
                    URB.setScreenPosition(522,725),
                    LLB.setScreenPosition(100,734),
                    LUB.setScreenPosition(156,762),
                    JUB.setScreenPosition(567,762),
                    VLB.setScreenPosition(195,791),
                    OLU.setScreenPosition(221,814),
                    TB.setScreenPosition(363,829),
                    ORU.setScreenPosition(502,814),
                    VRB.setScreenPosition(528,791),
                    OLF.setScreenPosition(156,883),
                    ORF.setScreenPosition(568,883),
                    OLB.setScreenPosition(118,957),
                    PLF.setScreenPosition(301,939),
                    PLB.setScreenPosition(301,992),
                    PRF.setScreenPosition(424,939),
                    PRB.setScreenPosition(424,992),
                    ORB.setScreenPosition(605,957),
                    QL.setScreenPosition(300,1067),
                    QR.setScreenPosition(422,1067),
                    BL.setScreenPosition(286,1247),
                    BM.setScreenPosition(363,1247),
                    PANASAS.setScreenPosition(285,1180),
                    PANASAY.setScreenPosition(446,1180),
                    PANASKS.setScreenPosition(288,140),
                    PANASKY.setScreenPosition(440,140),
                    MF.setScreenPosition(31,640),
                    KF.setScreenPosition(690,640),
                    MDKSK.setScreenPosition(218, 477),
                    MDKYK.setScreenPosition(504, 477),
                    KESHETGALGS.setScreenPosition(111,787),
                    KESHETGALGY.setScreenPosition(613,787),
                    BR.setScreenPosition(447,1247)
            };
            return rejectPoints;
        } else {
            //add coordinates for m image
            RejectPoint [] rejectPoints = {
            		_001.setScreenPosition( LOWER_RIGHT_POS, LOWER_RIGHT_POS), 
            		_002.setScreenPosition( LOWER_RIGHT_POS, LOWER_RIGHT_POS),
                    AL.setScreenPosition(261,29),
                    AM.setScreenPosition(361,29),
                    AR.setScreenPosition(458,29),
                    GRILL.setScreenPosition(361,136),
                    ELF.setScreenPosition(113,193),
                    PANASVS.setScreenPosition(232,207),
                    CLF.setScreenPosition(294,203),
                    CRF.setScreenPosition(423,203),
                    PANASVY.setScreenPosition(492,206),
                    ERF.setScreenPosition(609,193),
                    ELB.setScreenPosition(129,322),
                    LMIRRORH1.setScreenPosition(213,367),
                    CLB.setScreenPosition(294,309),
                    CRB.setScreenPosition(423,309),
                    RMIRRORH1.setScreenPosition(510,367),
                    ERB.setScreenPosition(593,321),
                    IF.setScreenPosition(29,454),
                    HLF.setScreenPosition(91,438),
                    HUF.setScreenPosition(144,438),
                    MDKSK.setScreenPosition(207,470),
                    TF.setScreenPosition(360,438),
                    MDKYK.setScreenPosition(517,470),
                    FUF.setScreenPosition(580,438),
                    FLF.setScreenPosition(632,438),
                    GF.setScreenPosition(693,455),
                    IB.setScreenPosition(30,577),
                    HLB.setScreenPosition(91,575),
                    HUB.setScreenPosition(143,575),
                    ULF.setScreenPosition(187,533),
                    MDKSA.setScreenPosition(197,597),
                    MDKSE.setScreenPosition(228,548),
                    NLF.setScreenPosition(324,572),
                    NRF.setScreenPosition(395,572),
                    MDKYE.setScreenPosition(494,549),
                    URF.setScreenPosition(536,533),
                    MDKYA.setScreenPosition(527,597),
                    FUB.setScreenPosition(580,575),
                    FLB.setScreenPosition(632,575),
                    GB.setScreenPosition(693,577),
                    MB.setScreenPosition(30,698),
                    LLF.setScreenPosition(91,673),
                    LUF.setScreenPosition(143,673),
                    MDASK.setScreenPosition(195,661),
                    NLB.setScreenPosition(325,693),
                    NRB.setScreenPosition(401, 693),
                    MDAYK.setScreenPosition(528,661),
                    JUF.setScreenPosition(580,673),
                    JLF.setScreenPosition(632,673),
                    KB.setScreenPosition(693,699),
                    LLB.setScreenPosition(100,727),
                    ULB.setScreenPosition(189,720),
                    URB.setScreenPosition(534,720),
                    JLB.setScreenPosition(623,727),
                    LUB.setScreenPosition(139,772),
                    VLB1.setScreenPosition(190,801),
                    MDASE.setScreenPosition(228,760),
                    TB.setScreenPosition(360,827),
                    MDAYE.setScreenPosition(494,760),
                    VRB1.setScreenPosition(534,801),
                    JUB.setScreenPosition(584,772),
                    KESHETGALGS.setScreenPosition(109,831),
                    MDASA.setScreenPosition(194,854),
                    MDAYA.setScreenPosition(532,854),
                    KESHETGALGY.setScreenPosition(613,831),
                    OLF.setScreenPosition(115,940),
                    MKSK.setScreenPosition(192,915),
                    MKYK.setScreenPosition(531,915),
                    ORF.setScreenPosition(608,940),
                    ULB3.setScreenPosition(188,970),
                    MKSE.setScreenPosition(233,1011),
                    PLF.setScreenPosition(297,970),
                    PRF.setScreenPosition(427,970),
                    MKYE.setScreenPosition(490,1011),
                    URB3.setScreenPosition(536,970),
                    OLB.setScreenPosition(120,1076),
                    ULB4.setScreenPosition(190,1051),
                    OLU.setScreenPosition(193,1104),
                    PLB.setScreenPosition(297,1077),
                    PRB.setScreenPosition(426,1077),
                    URB4.setScreenPosition(533,1051),
                    ORU.setScreenPosition(529,1104),
                    ORB.setScreenPosition(603,1076),
                    BL.setScreenPosition(289,1248),
                    BM.setScreenPosition(370,1248),
                    BR.setScreenPosition(454,1248),
                    PANASAS.setScreenPosition(290,1177),
                    PANASAY.setScreenPosition(453,1177),
                    PANASKS.setScreenPosition(273,135),
                    PANASKY.setScreenPosition(454,135),
                    MF.setScreenPosition(30,644),
                    KF.setScreenPosition(693,644)
            };
            return rejectPoints;
        }
    }


    
	public static void reset_all_rejects() { 
//		BR.reject_code_value = 324;
//		ORB.reject_code_value = 22;
//		FUB.reject_code_value = 2;
		
		Field[] declaredFields = ImageRejectContainer.class.getDeclaredFields();
		Method clear_method;
		int ctr = 0;
		try {
			clear_method = RejectPoint.class.getDeclaredMethod("clear", null);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();
		}
		for (Field field : declaredFields) {
		    if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
		    	if (field.getType().equals(RejectPoint.class)) {
		    		Object obj;
					try {
						obj = field.get(null); 
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						throw new RuntimeException();
					} 
					
		    		try {
						clear_method.invoke(obj, null);
						ctr++;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new RuntimeException();
					} 		    	
		    	}
		    }
		}

	}
}
