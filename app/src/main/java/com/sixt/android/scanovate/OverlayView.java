package com.sixt.android.scanovate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.sixt.android.MyApp;
import com.sixt.rent.R;

public class OverlayView extends View {

	float ratio;
	float marginVER;
	float marginHOR;
	
	@SuppressLint("NewApi")
	public OverlayView(Context context)//, Float ratio, Size marginSize)
	{
		super(context);
		// TODO Auto-generated constructor stub
		if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
		
		this.ratio = 2.0f;
        this.marginVER = 200.0f;
        this.marginHOR = 100.0f;
	}

	@SuppressLint("NewApi")
	public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //In versions > 3.0 need to define layer Type
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        
        this.ratio = 2.0f;
        this.marginVER = 200.0f;
        this.marginHOR = 100.0f;
    }
	

	public void setWidthHeightRatioAndMargin(float ratio, float marginHOR, float marginVER)
	{
		this.ratio = ratio;
		this.marginHOR = marginHOR;
        this.marginVER = marginVER;  
	}


	@SuppressLint("DrawAllocation")
	@Override
	    public void onDraw(Canvas canvas) {
	        super.onDraw(canvas);
	        
	     Paint paint = new Paint();
	     paint.setColor(MyApp.appContext.getResources().getColor(R.color.dimBlack));

	     paint.setStyle(Paint.Style.FILL);
         canvas.drawPaint(paint);
	     
         float holeWidth;
         float holeHeight;
         
         float maxWidth = canvas.getWidth() - (2 * marginHOR);
         float maxHeight = canvas.getHeight() - (2 * marginVER);
         
         holeWidth = maxWidth;
         holeHeight = holeWidth / ratio;
         
         if (holeHeight > maxHeight)
         {
        	 holeHeight = maxHeight;
        	 holeWidth = ratio * holeHeight ;
         }
         
         paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
         
         float left = (canvas.getWidth() - holeWidth) / 2;
         float top = (canvas.getHeight() - holeHeight) / 2;
         float right = left + holeWidth;
         float bottom = top + holeHeight;
         
         RectF holeRect = new RectF(left, top, right, bottom);
         
         canvas.drawRoundRect(holeRect, 85.0f, 85.0f, paint);
         
	     Paint borderPaint = new Paint();
	     borderPaint.setARGB(255, 255, 255, 255);
	     borderPaint.setStyle(Paint.Style.STROKE);
	     borderPaint.setStrokeWidth(4);

	     canvas.drawRoundRect(holeRect, 85.0f, 85.0f, borderPaint);
	    }
}
