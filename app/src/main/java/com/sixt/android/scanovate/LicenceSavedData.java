package com.sixt.android.scanovate;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.sixt.android.MyApp;

import java.util.ConcurrentModificationException;

/**
 * Created by Izakos on 06/09/2016.
 */
public class LicenceSavedData {

    public static final String CLASS_NAME = "LicenceSavedData";
    private static LicenceSavedData instance;
    private static SharedPreferences sharedPref;

    private String strFirstNameHeb = "";
    private String strFirstNameEng = "";
    private String strLastNameHeb = "";
    private String strLastNameEng = "";
    private String strDateOfBirth = "";
    private String strDateOfExpiry = "";
    private String strIdNumber = "";
    private String strAddress = "";
    private String strDateOfIssue = "";
    private String strDrivinLicenseNumber = "";
    private String faceImageBase64 = "";
    public String path = "";
    public String uri = "";

    public String getFaceImageBase64() {
        return faceImageBase64;
    }

    public void setFaceImageBase64(String faceImageBase64) {
        this.faceImageBase64 = faceImageBase64;
    }

    public String getStrFirstNameHeb() {
        return strFirstNameHeb;
    }

    public void setStrFirstNameHeb(String strFirstNameHeb) {
        this.strFirstNameHeb = strFirstNameHeb;
    }

    public String getStrFirstNameEng() {
        return strFirstNameEng;
    }

    public void setStrFirstNameEng(String strFirstNameEng) {
        this.strFirstNameEng = strFirstNameEng;
    }

    public String getStrLastNameHeb() {
        return strLastNameHeb;
    }

    public void setStrLastNameHeb(String strLastNameHeb) {
        this.strLastNameHeb = strLastNameHeb;
    }

    public String getStrLastNameEng() {
        return strLastNameEng;
    }

    public void setStrLastNameEng(String strLastNameEng) {
        this.strLastNameEng = strLastNameEng;
    }

    public String getStrDateOfBirth() {
        return strDateOfBirth;
    }

    public void setStrDateOfBirth(String strDateOfBirth) {
        this.strDateOfBirth = strDateOfBirth;
    }

    public String getStrDateOfExpiry() {
        return strDateOfExpiry;
    }

    public void setStrDateOfExpiry(String strDateOfExpiry) {
        this.strDateOfExpiry = strDateOfExpiry;
    }

    public String getStrIdNumber() {
        return strIdNumber;
    }

    public void setStrIdNumber(String strIdNumber) {
        this.strIdNumber = strIdNumber;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }

    public String getStrDateOfIssue() {
        return strDateOfIssue;
    }

    public void setStrDateOfIssue(String strDateOfIssue) {
        this.strDateOfIssue = strDateOfIssue;
    }

    public String getStrDrivinLicenseNumber() {
        return strDrivinLicenseNumber;
    }

    public void setStrDrivinLicenseNumber(String strDrivinLicenseNumber) {
        this.strDrivinLicenseNumber = strDrivinLicenseNumber;
    }

    public static LicenceSavedData getInstance() {
        if (instance == null) {
            Gson gson = new Gson();
            sharedPref = MyApp.appContext.getSharedPreferences(CLASS_NAME, Context.MODE_PRIVATE);
            LicenceSavedData LicenceSavedData = gson.fromJson(sharedPref.getString(CLASS_NAME, ""), LicenceSavedData.class);
            instance = LicenceSavedData == null ? new LicenceSavedData() : LicenceSavedData;
        }
        return instance;
    }

    public void save() {
        Gson gson = new Gson();

        try {
            String json = gson.toJson(LicenceSavedData.this);
            Log.i(CLASS_NAME, "save: " + json.toString());
            sharedPref.edit().putString(CLASS_NAME, json).commit();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        }
    }
}
