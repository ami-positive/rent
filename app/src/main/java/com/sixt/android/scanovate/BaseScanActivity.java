package com.sixt.android.scanovate;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sixt.rent.R;

public class BaseScanActivity extends Activity{

	TextView sightText;
	ImageView logoView;
	ImageView debugView;
	FrameLayout cameraPreviewHolder;
	ImageButton btnCancel;	
	ImageButton btnTorch;
	OverlayView overlayView;
	boolean Finished = false;	
	Object finishSynchronizer=new Object();
	String clickNotificationKey="";
	boolean notifyOnclick=false;
	TranslateAnimation trans;
	Animation to_middle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Finished = false;
		
		notifyOnclick= getIntent().hasExtra(AppData.CLICK_EVENTS_NOTIFICATION_KEY);

		if(notifyOnclick)
		{
			clickNotificationKey = getIntent().getStringExtra(AppData.CLICK_EVENTS_NOTIFICATION_KEY);
		}	
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
				WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

		android.os.Process
		.setThreadPriority(android.os.Process.THREAD_PRIORITY_DISPLAY);

		setContentView(R.layout.activity_ocr_scan);
		btnCancel 			= (ImageButton) findViewById(R.id.btnCancel);	
		btnTorch			= (ImageButton) findViewById(R.id.btnTorch);
//		sightLine 			= (ImageView)   findViewById(R.id.sightLine);
		sightText			= (TextView)    findViewById(R.id.sightText);
		logoView			= (ImageView)   findViewById(R.id.logo);
		debugView 			= (ImageView)   findViewById(R.id.ivDebug);
		cameraPreviewHolder = (FrameLayout) findViewById(R.id.camera_preview);
		overlayView 		= (OverlayView) findViewById(R.id.overlayView);
		
		to_middle = AnimationUtils.loadAnimation(this, R.anim.shrink_to_middle);
	}
	
	@Override
	   public void onConfigurationChanged(Configuration newConfig) {
	        super.onConfigurationChanged(newConfig);
	 
	        // Checks the orientation of the screen for landscape and portrait and set portrait mode always
	        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        }
	    }
	
	void startLogoSpin()
	{
		logoView.setAnimation(to_middle);
		to_middle.start();
	}
	
	void stopLogoSpin()
	{
		to_middle.cancel();
	}
	
	BroadcastReceiver cancelReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Return();

		}
	};
	
	protected void Return() {
		try {
			freeOCR();
		}
		catch (Exception e)
		{
		}	
		if (getParent() == null) {
			setResult(Activity.RESULT_CANCELED, null);
		} else {

			getParent().setResult(Activity.RESULT_CANCELED, null);
		}
		finish();
	}
	
	void NotifyOnClick()
	{
		if(notifyOnclick)
		{

			try {
				Intent local = new Intent();
				local.setAction(clickNotificationKey);
				sendBroadcast(local);
			} catch (Exception e) {
			}
		}
	}

	protected void freeOCR() {
		// TODO Auto-generated method stub
		
	}
	
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	protected void AllowRetry() {
		stopLogoSpin();
//		trans.cancel();
//		sightLine.setVisibility(View.INVISIBLE);
//		sightLine.clearAnimation();	
	}
	
	protected void SightLooper() {


		startLogoSpin();
//		float totalWidth  = cameraPreviewHolder.getMeasuredWidth();
//		//		float totalHeight = cameraPreviewHolder.getMeasuredHeight();
//		float overlayLeft=ocrScanOverlayView.overlayRect.left;
//		float overlayRight=ocrScanOverlayView.overlayRect.right;
//
//		float leftStart=(overlayLeft+15.0f)/totalWidth;
//		float rightEnd=(overlayRight-15.0f)/totalWidth;
//
//		if(android.os.Build.VERSION.SDK_INT<11)
//		{
//			sightLine.setVisibility(View.VISIBLE);
//			sightLine.getLayoutParams().height = ocrScanOverlayView.overlayRect.bottom-ocrScanOverlayView.overlayRect.top;
//			sightLine.requestLayout();
//			trans = new TranslateAnimation(
//					TranslateAnimation.ABSOLUTE, leftStart*totalWidth,
//					TranslateAnimation.ABSOLUTE,rightEnd*totalWidth,
//					TranslateAnimation.ABSOLUTE, 0.00f, TranslateAnimation.ABSOLUTE,
//					0.00f);
//			trans.setFillAfter(true);
//			trans.setDuration(1800);
//			trans.setRepeatCount(-1);
//			trans.setRepeatMode(Animation.REVERSE);
//			trans.setZAdjustment(1);
//			sightLine.startAnimation(trans);
//		}
//		else
//		{
//
//			sightLine.setVisibility(View.VISIBLE);
//			sightLine.getLayoutParams().height = ocrScanOverlayView.overlayRect.bottom-ocrScanOverlayView.overlayRect.top;
//			sightLine.requestLayout();
//			if (trans==null) {
//				trans = new TranslateAnimation(
//						TranslateAnimation.RELATIVE_TO_PARENT, leftStart,
//						TranslateAnimation.RELATIVE_TO_PARENT, rightEnd,
//						TranslateAnimation.RELATIVE_TO_PARENT, 0.00f,
//						TranslateAnimation.RELATIVE_TO_PARENT, 0.00f);
//				trans.setFillAfter(true);
//				trans.setDuration(1800);
//				trans.setRepeatCount(-1);
//				trans.setRepeatMode(Animation.REVERSE);
//				trans.setZAdjustment(1);
//			}
//			sightLine.startAnimation(trans);
//		}
	}
	
	@Override
	protected void onDestroy() {

		try {
			unregisterReceiver(cancelReceiver);
		} catch (Exception ex) {
		}
		;

		super.onDestroy();

	}

	@Override
	protected void onStop() {
		Return();
		super.onStop();

	}
	
	public static float convertDpToPixel(float dp, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
	    return px;
	}
}
