package com.sixt.android.scanovate;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

public class AppData 
{	

	public static final String CLICK_EVENTS_NOTIFICATION_KEY = "CLICK_EVENTS_NOTIFICATION_KEY";
// 

	public static final int GLOBAL_ACTIVITY_REQUEST=1;
	private static AppData instance = new AppData();	  
	
	public static HashMap<String,Object> processResult;
	
	public static void clearProcessResult()
	{
		if(processResult!=null)
		{
		for (Map.Entry<String, Object> entry : processResult.entrySet()) {
		    Object value = entry.getValue();
		   
		    if(value!=null&&value instanceof Bitmap)
		    {
		    	try {
					((Bitmap)value).recycle();
					 value=null;
					 entry.setValue(null);
				} catch (Exception e) {
					
				}
		    }
		}
		}
	}
	
	private AppData() 
	{
		
	}
	
	public static synchronized AppData getInstance() 
	{
		return instance;
	}
	   
}

