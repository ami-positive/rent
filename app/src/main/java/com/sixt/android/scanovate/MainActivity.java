package com.sixt.android.scanovate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.sixt.rent.R;

import scanovate.ocr.israeldriverslicense.IsraelDriversLicenseOCRManager;

public class MainActivity extends Activity {

    public static final int REQUEST_CAMERA_TAG = 1;

    Button btnLaunchIDL;
    ImageButton btnInfo;
    private boolean isTapped;
    private OnClickListener btnInfoListener = new OnClickListener() {

        public void onClick(View v) {

            String message = String.format("%s\n%s", IsraelDriversLicenseOCRManager.getInfo(), IsraelDriversLicenseOCRManager.getVersion());

            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Info");
            alertDialog.setMessage(message);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

        }
    };
    private OnClickListener btnLaunchIDLOCRListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (!isTapped) {
                isTapped = true;
            } else {
                Intent myIntent;
                myIntent = new Intent(MainActivity.this, IsraelDriversLicenseScanActivity.class);

                MainActivity.this.startActivityForResult(myIntent, AppData.GLOBAL_ACTIVITY_REQUEST);
            }
            isTapped = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        setContentView(R.layout.activity_main);
        isTapped = false;

        btnInfo = (ImageButton) findViewById(R.id.infoImageButton);
        btnInfo.setOnClickListener(btnInfoListener);
        btnLaunchIDL = (Button) findViewById(R.id.btnLaunchIDLOCR);
        btnLaunchIDL.setOnClickListener(btnLaunchIDLOCRListener);


    }

}
