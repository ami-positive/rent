package com.sixt.android.scanovate;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.sixt.android.MyApp;
import com.sixt.rent.R;

import java.util.HashMap;

import scanovate.ocr.common.ScanDebugListener;
import scanovate.ocr.common.ScanListener;
import scanovate.ocr.israeldriverslicense.IsraelDriversLicenseOCRManager;

public class IsraelDriversLicenseScanActivity extends BaseScanActivity {
    public static final String OCRScanActivityCloseAction = "IsraelDriversLicenseScanActivity.close.action";
    private static final float DRIVING_LICENCE_ASPECT_RATIO = 85.6f / 53.98f;
    IsraelDriversLicenseOCRManager israelDriversLicenseOCRManager;
    private OnClickListener btnCancelListener = new OnClickListener() {

        public void onClick(View v) {

            israelDriversLicenseOCRManager.cancelScan();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);

        IntentFilter filter = new IntentFilter();
        filter.addAction(OCRScanActivityCloseAction);
        registerReceiver(cancelReceiver, filter);

        btnCancel.setOnClickListener(btnCancelListener);

        sightText.setText("Place Drivers License Here");

        overlayView.setWidthHeightRatioAndMargin(DRIVING_LICENCE_ASPECT_RATIO, convertDpToPixel(60.0f, this), convertDpToPixel(0.0f, this));

        israelDriversLicenseOCRManager = new IsraelDriversLicenseOCRManager();

        israelDriversLicenseOCRManager.scanListener = new ScanListener() {
            @Override
            public void ScanSucceeded(final HashMap<String, Object> resultValues) {
                synchronized (finishSynchronizer) {
                    if (Finished) {
                        return;
                    }
                }

                AppData.processResult = resultValues;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        AllowRetry();

                        ResultDialog successDialog = new ResultDialog(IsraelDriversLicenseScanActivity.this, R.style.PauseDialog, ScanType.ISRAEL_DRIVERS_LICENSE);
                        successDialog.setResultValues(resultValues);

                        successDialog.RetryListener = new ResultDialog.SuccessDialogButtonClickListener() {
                            @Override
                            public void onClick(View v) {
                                RunOCR();
                                SightLooper();
                            }
                        };


                        successDialog.CancelListener = new ResultDialog.SuccessDialogButtonClickListener() {
                            @Override
                            public void onClick(View v) {
                                returnPhotoUri(Uri.parse(MyApp.lsdManager.uri));
                                Return();
                            }
                        };

                        successDialog.show();
                    }
                });
            }

            @Override
            public void ScanFailed(final HashMap<String, Object> resultValues) {
                AppData.processResult = resultValues;

                if (Finished)
                    return;

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        AllowRetry();

                        ResultDialog failDialog = new ResultDialog(IsraelDriversLicenseScanActivity.this, R.style.PauseDialog, ScanType.FAILED);

                        failDialog.setResultValues(resultValues);

                        failDialog.RetryListener = new ResultDialog.SuccessDialogButtonClickListener() {
                            @Override
                            public void onClick(View v) {
                                RunOCR();
                                SightLooper();
                            }
                        };


                        failDialog.CancelListener = new ResultDialog.SuccessDialogButtonClickListener() {
                            @Override
                            public void onClick(View v) {
                                Return();
                            }
                        };

                        failDialog.show();

                    }
                });
            }

            @Override
            public void ScanCanceled(final HashMap<String, Object> resultValues) {

                NotifyOnClick();
                Return();

            }

            @Override
            public void ScanLog(final String text) {
                Log.i("LOG__LOG__LOG", text);
            }
        };

        try {
            israelDriversLicenseOCRManager.init(cameraPreviewHolder, this);
        } catch (Exception e1) {

            israelDriversLicenseOCRManager.free();
            finish();
        }

        israelDriversLicenseOCRManager.ocrDebugListener = new ScanDebugListener() {
            @Override
            public void OCRShowDebug(Bitmap debugBitmap) {
                debugView.setImageBitmap(debugBitmap);
            }
        };
        RunOCR();
        SightLooper();
    }

    private void RunOCR() {
        israelDriversLicenseOCRManager.startScan();

    }

    @Override
    protected void freeOCR() {

        synchronized (finishSynchronizer) {
            if (Finished == false) {
                Finished = true;
                israelDriversLicenseOCRManager.free();
            }
        }
    }

    @Override
    public void onBackPressed() {
        israelDriversLicenseOCRManager.cancelScan();
    }

    public void returnPhotoUri(Uri uri) {
        Intent data = new Intent();
        data.setData(uri);

        if (getParent() == null) {
            setResult(RESULT_OK, data);
        } else {
            getParent().setResult(RESULT_OK, data);
        }

        finish();
    }
}