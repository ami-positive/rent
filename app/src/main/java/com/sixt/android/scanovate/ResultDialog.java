package com.sixt.android.scanovate;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.ocr.ImageUtility;
import com.sixt.android.activities.customerYard.utils.BitmapUtil;
import com.sixt.rent.R;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import scanovate.ocr.israeldriverslicense.IsraelDriversLicenseOCRManager;


public class ResultDialog extends Dialog implements
View.OnClickListener
{
	public Activity context;
	public Button btnCancel;
	public Button btnRetry;
	public SuccessDialogButtonClickListener CancelListener;
	public SuccessDialogButtonClickListener RetryListener;
	ScanType _scanType;
	static final Map<ScanType, Integer> resultLauouts;
	static {
		Map<ScanType, Integer> tmp =new HashMap<ScanType, Integer>();
		tmp.put(ScanType.FAILED, R.layout.failed_result_dialog);
		tmp.put(ScanType.ISRAEL_DRIVERS_LICENSE, R.layout.idl_result_dialog);
		
		resultLauouts = Collections.unmodifiableMap(tmp);
	}
	HashMap<String,Object> _resultValues;
	public ResultDialog(Activity a,int theme,ScanType scanType) {
		super(a,theme);
		this.context = a;
		_scanType=scanType;
	}

	public ResultDialog(Activity a,int theme) {
		super(a,theme);
		// TODO Auto-generated constructor stub
		this.context = a;
	}

	public ResultDialog(Activity a) {
		super(a);
		// TODO Auto-generated constructor stub
		this.context = a;

	}

	public void setResultValues(HashMap<String,Object> values)
	{
		_resultValues=values;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(resultLauouts.get(_scanType));
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setCancelable(false);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnRetry = (Button) findViewById(R.id.btnRetry);
		btnCancel.setOnClickListener(this);
		btnRetry.setOnClickListener(this);
		displayResult();

	}
	void displayResult()
	{
		switch (_scanType) {
		case FAILED:
			displayFailedResult();
			break;
		case ISRAEL_DRIVERS_LICENSE:
			displayIDLResult();
			break;
		default:
			break;
		}
	}

	void displayFailedResult()
	{
		ImageView imgFailed =(ImageView) findViewById(R.id.imgFailed);
		
		
		TextView txtFailed = (TextView) findViewById(R.id.txtFailed);

		txtFailed.setText(_resultValues.toString());
		txtFailed.setMovementMethod(new ScrollingMovementMethod());

		for (Object obj : _resultValues.values())
		{
			if (obj instanceof Bitmap)
			{
				final Bitmap bm = (Bitmap)obj;
				imgFailed.setImageBitmap(bm);
			}
		}
	}
		
	void displayIDLResult()
	{
		ImageView imgFace=(ImageView) findViewById(R.id.ivFace);
		Bitmap faceImage=(Bitmap)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FACE_IMAGE);
		imgFace.setImageBitmap(faceImage);
		
		String strFirstNameHeb = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FIRST_NAME_HEBREW);
		String strFirstNameEng = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FIRST_NAME_ENGLISH);
		String strLastNameHeb = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LAST_NAME_HEBREW);
		String strLastNameEng = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LAST_NAME_ENGLISH);
		String strDateOfBirth = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_BIRTH);
		String strDateOfExpiry = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_EXPIRY);
		String strIdNumber = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_ID);
		String strAddress = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_ADDRESS);
		String strDateOfIssue = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_ISSUE);
		String strDrivinLicenseNumber = (String)_resultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LICENSE_NUMBER);

		LicenceSavedData lsd = MyApp.lsdManager;
		lsd.setStrFirstNameHeb(strFirstNameHeb);
		lsd.setStrFirstNameEng(strFirstNameEng);
		lsd.setStrLastNameHeb(strLastNameHeb);
		lsd.setStrLastNameEng(strLastNameEng);
		lsd.setStrDateOfBirth(strDateOfBirth);
		lsd.setStrDateOfExpiry(strDateOfExpiry);
		lsd.setStrIdNumber(strIdNumber);
		lsd.setStrAddress(strAddress);
		lsd.setStrDateOfIssue(strDateOfIssue);
		lsd.setStrDrivinLicenseNumber(strDrivinLicenseNumber);
		lsd.setFaceImageBase64(BitmapUtil.getImageAsStringEncodedBase64(faceImage));
		Uri photoUri = ImageUtility.savePicture(MyApp.appContext, faceImage, lsd.path);
		lsd.uri = photoUri.toString();
		lsd.save();



		TextView txtFirstNameHeb =  (TextView) findViewById(R.id.txtIDLFirstNameHEB);
		TextView txtFirstNameEng =  (TextView) findViewById(R.id.txtIDLFirstNameENG);
		TextView txtLastNameHeb =  (TextView) findViewById(R.id.txtIDLLastNameHEB);
		TextView txtLastNameEng =  (TextView) findViewById(R.id.txtIDLLastNameENG);
		TextView txtDateOfBirth =  (TextView) findViewById(R.id.txtIDLDateOfBirth);
		TextView txtDateOfExpiry =  (TextView) findViewById(R.id.txtIDLDateOfExpiry);
		TextView txtIdNumber = (TextView) findViewById(R.id.txtIDLidNumber);
		TextView txtAddress =  (TextView) findViewById(R.id.txtIDLAddress);
		TextView txtDateOfIssue = (TextView) findViewById(R.id.txtIDLDateOfIssue);
		TextView txtDrivinLicenseNumber =  (TextView) findViewById(R.id.txtIDLNumber);
		
		txtFirstNameHeb.setText(strFirstNameHeb);
		txtLastNameHeb.setText(strLastNameHeb);
		txtFirstNameEng.setText(strFirstNameEng);
		txtLastNameEng.setText(strLastNameEng);
		txtDateOfBirth.setText(strDateOfBirth);
		txtDateOfExpiry.setText(strDateOfExpiry);
		txtIdNumber.setText(strIdNumber);
		txtAddress.setText(strAddress);
		txtDateOfIssue.setText(strDateOfIssue);
		txtDrivinLicenseNumber.setText(strDrivinLicenseNumber);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCancel: 
			CancelListener.onClick(v);
			break;
		case R.id.btnRetry: 
			RetryListener.onClick(v);
			break;
		default:
			break;
		}
		dismiss();
	}
	
	static public class SuccessDialogButtonClickListener
	{
		public void onClick(View v) {}
	}
}