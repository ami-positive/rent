package com.sixt.android;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.sixt.android.activities.ActivityLifecycle;
import com.sixt.android.activities.AppSetting;
import com.sixt.android.activities.customerYard.database.tables.TableWordsReport;
import com.sixt.android.activities.customerYard.network.NetworkManager;
import com.sixt.android.activities.customerYard.storage.AppPreference;
import com.sixt.android.activities.customerYard.storage.StorageManager;
import com.sixt.android.scanovate.LicenceSavedData;

import io.fabric.sdk.android.Fabric;


/**
 * Created by natiapplications on 17/11/15.
 */
public class MyApp extends Application implements Application.ActivityLifecycleCallbacks{


    public static boolean VALIDATION = true;

    public static Context appContext;
    //public static TableWordsReport wordsReportManager;


    public static TableWordsReport wordsReportManager;
    public static StorageManager storageManager;
    public static NetworkManager networkManager;
    public static String serverEndPoint;
    public static String appVersion;
    public static AppSetting appSetting;


    public static LicenceSavedData lsdManager;
    public static AppPreference appPreference;
    public static AppPreference.PreferenceUserSettings userSettings;
    public static AppPreference.PreferenceGeneralSettings generalSettings;
    public static AppPreference.PreferenceUserProfil userProfile;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        Fabric.with(this, new Crashlytics(), new Answers());
        appVersion = getApplicationVersion(appContext);
        serverEndPoint = NetworkManager.DEV;
        wordsReportManager = TableWordsReport.getInstance(appContext);
        loadPreferences();
        networkManager = NetworkManager.getInstance(appContext);
        ActivityLifecycle.activate(this); //all activities lifecycle. ex:ActivityLifecycle.mCurrentActivity

        appSetting = AppSetting.getInstance();
        lsdManager = LicenceSavedData.getInstance();
    }

    private void loadPreferences() {
        appPreference = AppPreference.getInstans(appContext);
        userSettings = appPreference.getUserSettings();
        userProfile = appPreference.getUserProfil();
        generalSettings = appPreference.getGeneralSettings();
        storageManager = StorageManager.getInstance();
    }

    /**
     * get the application version name
     *
     * @param context - context of current application
     * @return the application version name
     */
    public  static String getApplicationVersion(Context context) {
        String appVersion = "";
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    context.getPackageName(), 0);
            appVersion =  info.versionName;
        } catch (Exception e) {
        }
        return appVersion;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
