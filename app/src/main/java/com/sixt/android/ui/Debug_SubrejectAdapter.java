package com.sixt.android.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.rent.R;

public class Debug_SubrejectAdapter extends ArrayAdapter<Debug_SubrejectRecord> {

	private static final int DIALOG_ENTRY_LAYOUT = R.layout.subreject_entry;



    public Debug_SubrejectAdapter(Context context, Debug_SubrejectRecord[] values) {
        super(context, R.layout.subreject_entry, values);
    }

    class DialogViewHolder {
    	public TextView caption = null;
    	public ToggleButton leftmost = null;
    	public ToggleButton pre_leftmost = null;
    	public ToggleButton center1 = null;
    	public ToggleButton center2 = null;
    	public ToggleButton center3 = null;
    	public ToggleButton rightmost = null;
    	
//        ToggleButton getCurrentRightMost() {
//            ToggleButton toggles[] = {rightmost, center1, center2, center3, leftmost};
//
//            for (ToggleButton toggleButton : toggles) {
//                if (toggleButton!=null &&toggleButton.getText() != null && toggleButton.getText().toString().length() > 0 && !toggleButton.getText().toString().equals("OFF"))
//                    return toggleButton;
//            }
//            return null;
//        }

        void uncheckButtons() {
            ToggleButton toggles[] = {rightmost, center1, center2, center3, pre_leftmost, leftmost};
            for (ToggleButton toggleButton : toggles) {
                toggleButton.setChecked(false);
            }
        }

        void setClickListener(View.OnClickListener onClickListener) {
            ToggleButton toggles[] = {rightmost, center1, center2, center3, pre_leftmost, leftmost};
            for (ToggleButton toggleButton : toggles) {
                toggleButton.setOnClickListener(onClickListener);
            }
        }
        
        

		void mark_selected(View v, Debug_SubrejectRecord current) {
			// 0==rightmost   1 = center3 , 2 = center2 , 3 = center1 , 4 = leftmost
			if (v==rightmost) {
				current.selected = 0;
			}
			else if (v==center3) {
				current.selected = 1; 
			}
			else if (v==center2) {
				current.selected = 2; 
			}
			else if (v==center1) {
				current.selected = 3; 
			}
			else if (v==pre_leftmost) {
				current.selected = 4; 
			}			
			else if (v==leftmost) {
				current.selected = 5; 
			}
			else {
				throw new RuntimeException();
			}
			
		}


		public void set_selected_btn(int selected) {
			// 0==rightmost   1 = center3 , 2 = center2 , 3 = center1 , 4 = leftmost
			uncheckButtons();
			if (selected==0) {
				rightmost.setChecked(true);
			}
			else if (selected==1) {
				center3.setChecked(true);
			}
			else if (selected==2) {
				center2.setChecked(true); 
			}
			else if (selected==3) {
				center1.setChecked(true); 
			}
			else if (selected==4) {
				pre_leftmost.setChecked(true); 
			}
			else if (selected==5) {
				leftmost.setChecked(true); 
			}			
			else {
				throw new RuntimeException();
			}			
		}
        
    };




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ToggleButton currentRightMost = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(DIALOG_ENTRY_LAYOUT, parent, false);
            DialogViewHolder holder = new DialogViewHolder();
            holder.caption = (TextView) convertView.findViewById(R.id.caption);
            holder.rightmost = (ToggleButton) convertView.findViewById(R.id.rightmost);
            holder.center1 = (ToggleButton) convertView.findViewById(R.id.center1);
            holder.center2 = (ToggleButton) convertView.findViewById(R.id.center2);
            holder.center3 = (ToggleButton) convertView.findViewById(R.id.center3);
            holder.pre_leftmost = (ToggleButton) convertView.findViewById(R.id.pre_leftmost);
            holder.leftmost = (ToggleButton) convertView.findViewById(R.id.leftmost);
            convertView.setTag(holder);
        }
        final DialogViewHolder holder = (DialogViewHolder) convertView.getTag(); 

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.uncheckButtons();
                ((ToggleButton) v).setChecked(true);
                Debug_SubrejectRecord current = getItem(position);
                holder.mark_selected(v, current);
            }
        };

        final Debug_SubrejectRecord current = getItem(position);
        
        holder.caption.setText(current.caption);
        
        if (current.rightmost != null) {
        	set_all_texts(holder.rightmost, current.rightmost);
        } else {
            holder.rightmost.setVisibility(View.GONE);
        }

        if (current.center1 != null) {
        	set_all_texts(holder.center1, current.center1);
        } else {
            holder.center1.setVisibility(View.GONE);
        }

        if (current.center2 != null) {
        	set_all_texts(holder.center2, current.center2);
        } else {
            holder.center2.setVisibility(View.GONE);
        }

        if (current.center3 != null) {
        	set_all_texts(holder.center3, current.center3);
        } else {
            holder.center3.setVisibility(View.GONE);
        }
        if (current.pre_leftmost != null) {
        	set_all_texts(holder.pre_leftmost, current.pre_leftmost);
        } else {
            holder.pre_leftmost.setVisibility(View.GONE);
        }

        if (current.leftmost != null) {
        	set_all_texts(holder.leftmost, current.leftmost);
        } else {
            holder.leftmost.setVisibility(View.GONE);
        }
        
//        currentRightMost = holder.getCurrentRightMost();
//        if (currentRightMost != null) {
//            currentRightMost.setChecked(true);
//        }
        holder.set_selected_btn(current.selected);
        
        holder.setClickListener(onClickListener);

        return convertView;
    }




	private static void set_all_texts(ToggleButton btn, String str) {
		btn.setText(str);
		btn.setTextOn(str);
		btn.setTextOff(str);
		btn.setVisibility(View.VISIBLE);
	}
}


