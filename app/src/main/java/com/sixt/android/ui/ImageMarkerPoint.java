package com.sixt.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.android.activities.ImageCarActivity;
import com.sixt.android.app.util.TEST;
import com.sixt.android.data.ImageRejectContainer;
import com.sixt.android.data.RejectPoint;
import com.sixt.rent.R;

import static com.sixt.android.activities.ImageCarActivity.MARKER_NOTSET_CODE;

 
public class ImageMarkerPoint extends ToggleButton {

	
	// selection from menu: ON_NEW_VALUE_SELECTED
	
	private static final boolean _DEBUG = TEST.debug_mode(false);	 			
	
	private static int WIDTH = 50; 
	private static int HEIGHT = 60;
	private AlertDialog markerOptionsDialog;
	RejectPoint rejectPoint;
//	private RejectValue[] reject_arr;
	private ImageCarActivity activity = null;

	public ImageMarkerPoint(Context context) {
		super(context);
	}

	public ImageMarkerPoint(Context context, RejectPoint rejectPoint, ViewGroup container) {
		super(context);
		
		
		if (rejectPoint == ImageRejectContainer.KESHETGALGY) {
			int jj=324;
			jj++;
		}
		
		if (context instanceof ImageCarActivity) {
			this.activity = (ImageCarActivity) context;
		}
		this.rejectPoint = rejectPoint;
		this.setBackgroundResource(R.drawable.toggle_btn_bg); //RED color inside (see @drawable/toggle_checked)


		this.setMinimumHeight(0);
		this.setMinimumWidth(0);
		this.setWidth(WIDTH);
		this.setHeight(HEIGHT);

		double x_val = rejectPoint.screenPosition.x - WIDTH / 2;
		double y_val = rejectPoint.screenPosition.y - HEIGHT / 2;
		
		this.setX((float) x_val);
		this.setY((float) y_val);
		this.setText("");
		this.setTextOff("");
		this.setTextOn("");
		this.setTextSize(0);

		if (TEST.SHOW_IMAGE_MARKER_CODE_ON_SCREEN) { 
			debug_show_marker_text_on_screen(context, container);
		}

		createMarkerOptions();
	}


	private int set_selection_color() {
		final int which = rejectPoint.reject_code_position;
		String reject_type = rejectPoint==null ? null : rejectPoint.Reject_Type; // "B" "S" "T" or "W"
		
//		B – פחחות  pachachut
//		W – חלונות chalonnot
//		T - פנסים pannasim
		 
		
		if (reject_type != null) {
			reject_type = reject_type.toUpperCase();
		}		
		
		int resid = -1;
		if (reject_type==null) {
			resid = standard_set_selection_color(which);
			return resid;
		}
		else if (reject_type.contains("B")) {
			resid = pachachut_set_selection_color(which);
			return resid;
//			return B_set_selection_color(which); 
		}
		else if (reject_type.contains("S")) {
			resid = standard_set_selection_color(which);
			return resid;
//			return S_set_selection_color(which);
		}
		else if (reject_type.contains("T")) {
			resid = pannasim_set_selection_color(which);
			return resid;
//			return T_set_selection_color(which);
		}
		else if (reject_type.contains("W")) {
			resid = chalonnot_set_selection_color(which);
			return resid;
//			return W_set_selection_color(which);
		}
		else {
			resid = standard_set_selection_color(which);
			return resid;
		}
	}


	private static final int[] stdrd_res_arr = {
		R.drawable.toggle_normal,
		R.drawable.toggle_checked_1,
		R.drawable.toggle_checked_2,
		R.drawable.toggle_checked_3,
		R.drawable.toggle_checked_4,
	};
	
	private static int standard_set_selection_color(int which) {		
		if (which < 0) {
			which = 0;
		}
		if (which >= stdrd_res_arr.length) {
			return R.drawable.toggle_checked;
		}
		return stdrd_res_arr[which];
	}

	////////////////
	
	private static final int[] pachachut_res_arr = {
		R.drawable.toggle_normal,
		R.drawable.toggle_checked_sritta,
		R.drawable.toggle_checked_shifshuf, 
		R.drawable.toggle_checked_lechitza,
		R.drawable.toggle_checked_makka,
		R.drawable.toggle_checked_makka_kasha,
	};
	
	
	private static int pachachut_set_selection_color(int which) {		
		if (which < 0) {
			which = 0;
		}
		if (which >= pachachut_res_arr.length) {
			return R.drawable.toggle_checked;
		}
		return pachachut_res_arr[which];
	}
	

	///////////////////
	
	private static final int[] chalonnot_res_arr = {
		R.drawable.toggle_normal,
		R.drawable.toggle_checked_nekudda,
		R.drawable.toggle_checked_sritta, // ==seddek,
		R.drawable.toggle_checked_makka, // == mennupatz,
		R.drawable.toggle_checked_makka_kasha, // == shavvur,
	};
	
	
	private static int chalonnot_set_selection_color(int which) {		
		if (which < 0) {
			which = 0;
		}
		if (which >= chalonnot_res_arr.length) {
			return R.drawable.toggle_checked;
		}
		return chalonnot_res_arr[which];
	}
	

	///////////////////
	
	private static final int[] pannasim_res_arr = {
		R.drawable.toggle_normal,
		R.drawable.toggle_checked_sritta, // ==seddek,
		R.drawable.toggle_checked_makka_kasha, // == shavvur,
	};
	
	
	private static int pannasim_set_selection_color(int which) {		
		if (which < 0) {
			which = 0;
		}
		if (which >= pannasim_res_arr.length) {
			return R.drawable.toggle_checked;
		}
		return pannasim_res_arr[which];
	}
	
	
	

	// orig
//	private int set_selection_color() {
//		int which = rejectPoint.reject_code_position;
//		if (which <= 0) {
//			return R.drawable.toggle_normal;
//		}
//		if (which == 1) {
//			return R.drawable.toggle_checked_1;
//		}
//		if (which == 2) {
//			return R.drawable.toggle_checked_2;
//		}
//		if (which == 3) {
//			return R.drawable.toggle_checked_3;
//		}
//		if (which == 4) {
//			return R.drawable.toggle_checked_4;
//		}
//		return R.drawable.toggle_checked_5; 
//	}
	
	@Override
	public boolean onTouchEvent(MotionEvent motionEvent){
		markerOptionsDialog.show();
		ListView items = markerOptionsDialog.getListView();
		return true;
	}

	private void createMarkerOptions() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		Object[] option_arr;
		option_arr = rejectPoint.get_possible_reject_descriptions();
		 
		// create context menu for each marker
//		final ListAdapter itemlist = new ArrayAdapter(getContext(), R.layout.dialog_right_aligned_item, option_arr);
//		        builder.setAdapter(itemlist, new DialogInterface.OnClickListener() {
//		            @Override
//		            public void onClick(DialogInterface dialog, int which) {
//		            	// ON_NEW_VALUE_SELECTED
//		            	String code = rejectPoint.Reject_Code;
//		            	if (!activity.is_legal_reject_level(code, which)) {
//		            		activity.onClick_IllegalRejectLevel();
//		            		return;
//		            	}
//		                rejectPoint.reject_code_position = which; 
//		                boolean is_checked = (which > MARKER_NOTSET_CODE);
//		                setChecked(is_checked);
//		                activity.onClick_MarkerPoint(rejectPoint, is_checked);
//		            }
//		        })
//		                .setTitle(rejectPoint.Reject_Desc)
//		                .setNegativeButton("סגור", new DialogInterface.OnClickListener() {
//		                    @Override
//		                    public void onClick(DialogInterface dialog, int id) {
//		                    	// no op
//		                    }
//		                });

		final ListAdapter itemlist = new MyArrayAdapter(getContext(), option_arr, this);
		builder.setAdapter(itemlist, new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
			        	// ON_NEW_VALUE_SELECTED
			        	String code = rejectPoint.Reject_Code;
			        	if (!activity.is_legal_reject_level(code, which)) { //ggggfix
			        		activity.onClick_IllegalRejectLevel();
			        		return;
			        	}
		                rejectPoint.reject_code_position = which; 
		                boolean is_checked = (which > MARKER_NOTSET_CODE);
		                setChecked(is_checked);
		                activity.onClick_MarkerPoint(rejectPoint, is_checked);
		            }
		        })
		                .setTitle(rejectPoint.Reject_Desc)
		                .setNegativeButton("סגור", new DialogInterface.OnClickListener() {
		                    @Override
		                    public void onClick(DialogInterface dialog, int id) {
		                    	// no op
		                    }
		                });
		
		markerOptionsDialog = builder.create();

        ListView listView = markerOptionsDialog.getListView();
		if (_DEBUG) {
			disabled_before_pos(listView, 3);
		}
		else {
			//
		}

        markerOptionsDialog.setOnShowListener(new DialogInterface.OnShowListener() {  //
            @Override
            public void onShow(DialogInterface dialog) {
                ((ArrayAdapter) itemlist).notifyDataSetChanged();
                ListView listView = ((AlertDialog) dialog).getListView();
                listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listView.setItemChecked(rejectPoint.reject_code_position, true);
                if (rejectPoint.reject_code_position > -1) {
                	View cur = listView.getChildAt(rejectPoint.reject_code_position);
                    cur.setBackgroundResource(R.color.selected_dialog_option);
                }                
            }
        });
        markerOptionsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                ListView listView = ((AlertDialog)dialog).getListView();
                int childCount = listView.getChildCount();
                for (int i=0; i < childCount; i++) {
                    listView.getChildAt(i).setBackgroundResource(R.color.unselected_dialog_option);
                }
            }
        });
	}

	
	private void disabled_before_pos(ListView listView, int start_enabled_pos) {
        int total_num = listView.getChildCount();
        int num = Math.min(total_num, start_enabled_pos);
        // disable loop
        for (int i = 0; i < num; i++) {
        	View cur = listView.getChildAt(rejectPoint.reject_code_position);
        	cur.setEnabled(false); 
//	        cur.setBackgroundResource(R.color.selected_dialog_option);
        } 
//        // enable loop
//        for (int i = num; i < total_num; i++) {
//        	View cur = listView.getChildAt(rejectPoint.reject_code_position);
//        	cur.setEnabled(true);  
////	        cur.setBackgroundResource(R.color.selected_dialog_option);
//        }
	}
	

	public void clear(){
		setScaleX(1);
		setScaleY(1);
		setChecked(false);
		rejectPoint.clear();
	}
	
	
	@Override
	public void setChecked(boolean checked) { // 		 
		super.setChecked(checked); 
		if (!checked) {
			this.setScaleX(1f);
			this.setScaleY(1f);
			setBackgroundResource(R.drawable.toggle_normal);
		}  
		else {
			this.setScaleX(1.3f); 
			this.setScaleY(1.3f);
			int sel_resid = set_selection_color();
			setBackgroundResource(R.drawable.toggle_normal); // clear prior! 
			setBackgroundResource(sel_resid);
		}

	}



	private void debug_show_marker_text_on_screen(Context context, ViewGroup container) {
		TextView marker_code = new TextView(context);
		marker_code.setText(rejectPoint.Reject_Code);
		marker_code.setMinimumWidth(0);
		marker_code.setMinimumWidth(0);
		marker_code.setTextColor(getResources().getColor(R.color.black));
		marker_code.setTextSize(10);
		marker_code.setX((float) (rejectPoint.screenPosition.x - 30));
		marker_code.setY((float) rejectPoint.screenPosition.y);
		container.addView(marker_code);
	}

	
	public static int getRejectIconForDefect(int which, String reject_type) {
			
//			B – פחחות  pachachut
//			W – חלונות chalonnot
//			T - פנסים pannasim
			 
			
			if (reject_type != null) {
				reject_type = reject_type.toUpperCase();
			}		
			
			if (reject_type==null) {
				return standard_set_selection_color(which);
			}
			else if (reject_type.contains("B")) {
				return pachachut_set_selection_color(which);
//				return B_set_selection_color(which); 
			}
			else if (reject_type.contains("S")) {
				return standard_set_selection_color(which);
//				return S_set_selection_color(which);
			}
			else if (reject_type.contains("T")) {
				return pannasim_set_selection_color(which);
//				return T_set_selection_color(which);
			}
			else if (reject_type.contains("W")) {
				return chalonnot_set_selection_color(which);
//				return W_set_selection_color(which);
			}
			else {
				return standard_set_selection_color(which);
			}
		}
	}
	


class MyArrayAdapter extends ArrayAdapter {
	final Context context;
	final ImageMarkerPoint thisMarkerPoint;
	public MyArrayAdapter(Context _context, Object[] option_arr, ImageMarkerPoint markerPoint) {
		super(_context, R.layout.image_reject_context_item, option_arr);
		context = _context;
		thisMarkerPoint = markerPoint;
	}
	 
	//gggg change image_reject_context_item
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { //ggg reject menu
		LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View row = inflater.inflate(R.layout.image_reject_context_item, parent, false); // gggg dup image_reject_context_item 
        ImageView imgIcon = (ImageView)row.findViewById(R.id.reject_icon); //ggg see usage of imgIcon
        TextView txtView = (TextView)row.findViewById(R.id.text1);        
        String txt = (String) getItem(position);
        
         
        int iconResId; 
//        if (thisMarkerPoint.isChecked()) {
    		int which = position;
    		if (which==0) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					imgIcon.setBackground(null);
				}else{
					imgIcon.setBackgroundDrawable(null);
				}
			}
    		else {
    			String reject_type = thisMarkerPoint.rejectPoint==null ? null : thisMarkerPoint.rejectPoint.Reject_Type; // "B" "S" "T" or "W"
    			iconResId = ImageMarkerPoint.getRejectIconForDefect(which, reject_type);
    	        imgIcon.setBackgroundResource(iconResId);
    		}
//        }
//        else {
//        	iconResId = R.drawable.toggle_normal;
//        }                
        txtView.setText(txt);
        return row;
	}
	
};


