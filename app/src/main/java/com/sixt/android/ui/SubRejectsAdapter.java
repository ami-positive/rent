package com.sixt.android.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.android.activities.SubRejectsActivity;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.TEST;
import com.sixt.rent.R;

public class SubRejectsAdapter extends ArrayAdapter<SubReject> {

	private static final int DIALOG_ENTRY_LAYOUT = R.layout.subreject_entry;

    public SubRejectsAdapter(Context context, SubReject[] values) {
        super(context, R.layout.subreject_entry, values);
        for (SubReject subrej: values) {
//        	if (subrej.value_was_set_by_user()) {
        		// keep user value as is
        	int curval = subrej.value ;
        	boolean nodef = subrej.no_default_value();
//        	if (subrej.value < 1) {
//        		subrej.value = nodef ? -1 : 0;
//        	}
//        	if (curval < 0 && !nodef) {
//        		subrej.value = 0;
//        	}
        	int jj=234;
        	jj++;
//        	subrej.value = 0;
//        	if (subrej.no_default_value()) {
//        		int jj=234;
//        		jj++;
//        		subrej.value = -1;
//        	}
        }
    }
    

    class DialogViewHolder {
    	public TextView caption = null;
    	public CheckBox left_checkbox = null;
    	public TextView txt_simmun; // ggg and usage
    	public ToggleButton leftmost = null; 
    	public ToggleButton pre_leftmost = null;
    	public ToggleButton center1 = null;
    	public ToggleButton center2 = null;
    	public ToggleButton center3 = null;
    	public ToggleButton rightmost = null;

    	
        void uncheckButtons() {
            ToggleButton toggles[] = {rightmost, center3, center2, center1, pre_leftmost, leftmost};
            for (ToggleButton toggleButton : toggles) {
                toggleButton.setChecked(false);
            }
        }

        void setClickListener(View.OnClickListener onClickListener) {
        	ToggleButton toggles[] = {rightmost, center3, center2, center1, pre_leftmost, leftmost};
            for (ToggleButton toggleButton : toggles) {
                toggleButton.setOnClickListener(onClickListener);
            }
            left_checkbox.setOnClickListener(onClickListener);
        }
        
        

		private int get_btn_ind(View v) {
			// 0==rightmost   1 = center3 , 2 = center2 , 3 = center1 , 4 = leftmost
			int selected_val = -1;
			
//			leftmost center1 center2  center3 rightmost
			if (v==rightmost) {
				selected_val = 0;
			}
			else if (v==center3) {
				selected_val = 1; 
			}
			else if (v==center2) {
				selected_val = 2; 
			}
			else if (v==center1) {
				selected_val = 3; 
			}
			else if (v==pre_leftmost) {
				selected_val = 4; 
			}
			else if (v==leftmost) {
				selected_val = 5; 
			}
			else {
				throw new RuntimeException();
			}
			return selected_val;
		}

//		>> subRejects:  keep set value + handle leftmost CheckBox;
		

		public void set_selected_btn(int selected) {
			// 0==rightmost   1 = center3 , 2 = center2 , 3 = center1 , 4 = leftmost
			uncheckButtons();
			if (selected==0) {
				rightmost.setChecked(true);
			}
			else if (selected==1) {
				center3.setChecked(true);
			}
			else if (selected==2) {
				center2.setChecked(true); 
			}
			else if (selected==3) {
				center1.setChecked(true); 
			}
			else if (selected==4) {
				pre_leftmost.setChecked(true); 
			}
			else if (selected==5) {
				leftmost.setChecked(true); 
			}			
			else {
//				throw new RuntimeException(); -- ok; no default
			}			
		}

		public boolean get_is_checked() {
			boolean visible = (left_checkbox.getVisibility() == View.VISIBLE);
			boolean checked = left_checkbox.isChecked();
			return visible && checked;
		}
        
    };




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ToggleButton currentRightMost = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(DIALOG_ENTRY_LAYOUT, parent, false);
            DialogViewHolder holder = new DialogViewHolder();
            holder.caption = (TextView) convertView.findViewById(R.id.caption);
            holder.rightmost = (ToggleButton) convertView.findViewById(R.id.rightmost);
            holder.center1 = (ToggleButton) convertView.findViewById(R.id.center1);
            holder.center2 = (ToggleButton) convertView.findViewById(R.id.center2);
            holder.center3 = (ToggleButton) convertView.findViewById(R.id.center3);
            holder.pre_leftmost = (ToggleButton) convertView.findViewById(R.id.pre_leftmost);
            holder.leftmost = (ToggleButton) convertView.findViewById(R.id.leftmost);
            holder.left_checkbox = (CheckBox) convertView.findViewById(R.id.left_checkbox);
            holder.txt_simmun = (TextView) convertView.findViewById(R.id.txt_simmun);
            convertView.setTag(holder);
        }
        final DialogViewHolder holder = (DialogViewHolder) convertView.getTag();
        
        

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) { // ok onClick send
            	
            	ToggleButton selected_btn = null;
            	CheckBox left_checkBox = null; 
            	int new_val = -1;
            	if (v instanceof ToggleButton) { // or: CheckBox
            		selected_btn = (ToggleButton)v;
                    new_val = holder.get_btn_ind(v);
            	}
            	else {
            		left_checkBox = (CheckBox) v;
            	}
                final SubReject the_current = getItem(position);
                final String _code = the_current.SubRejectCode;
                final boolean new_checked = holder.get_is_checked();
        		final int orig_val = GetCarDetails_Response.get_orig_car_reject_val(_code);
        		if (selected_btn != null && new_val < orig_val) {
        			v.setSelected(false);
        			SubRejectsActivity.handle_lesser_reject_set();
        			return;
        		}
            	
                
        		boolean is_first_option = false;
            	final int old_val = the_current.value; 
            	final boolean old_checked = the_current.left_check_selected;
            	 
                if (selected_btn != null) {
                    holder.uncheckButtons();
                	selected_btn.setChecked(true);
                	the_current.value = new_val;
    				is_first_option = (selected_btn == holder.rightmost); // should not open subrejects if rightmost option selected!
    			}
                else {
                	the_current.left_check_selected = left_checkBox.isChecked();
                	int jj=234;
                	jj++;
                }
                SubRejectsActivity.onClick_ListItem(the_current, is_first_option, old_val, old_checked);                
            }
        };

        final SubReject current_item = getItem(position);
        int kk = current_item.value;
        int jj=234;
        jj++;
        
        if (TEST.SHOW_REJECT_CODES_ON_SCREEN) { 
        	holder.caption.setText(current_item.SubRejectCode);
        }
        else {
        	holder.caption.setText(current_item.SubRejectDesc);
        }
        
        
        int eol_visible = current_item.show_eol_checkbox() ? View.VISIBLE : View.INVISIBLE;
        if (current_item.show_eol_checkbox()) {
        	int jjj=234;
        	jjj++;
        } 
        final boolean _sel_checkbox = current_item.left_check_selected;
        holder.left_checkbox.setVisibility(eol_visible);
        holder.left_checkbox.setChecked(_sel_checkbox);
        

        holder.txt_simmun.setVisibility(View.INVISIBLE);
        if (eol_visible == View.VISIBLE) { //ggggggggg  
        	holder.txt_simmun.setVisibility(View.VISIBLE);
        }
        
        if (current_item.first() != null) {
        	set_all_texts(holder.rightmost, current_item.first(), current_item);
        } else {
            holder.rightmost.setVisibility(View.GONE);
        }

        if (current_item.place_1() != null) {
        	set_all_texts(holder.center3, current_item.place_1(), current_item); 
        } else {
            holder.center3.setVisibility(View.GONE);
        }

        if (current_item.place_2() != null) {
        	set_all_texts(holder.center2, current_item.place_2(), current_item);
        } else {
            holder.center2.setVisibility(View.GONE);
        }

        if (current_item.place_3() != null) {
        	set_all_texts(holder.center1, current_item.place_3(), current_item);
        } else {
            holder.center1.setVisibility(View.GONE);
        }

        if (current_item.place_4() != null) {
        	set_all_texts(holder.pre_leftmost, current_item.place_4(), current_item);
        } else {
            holder.pre_leftmost.setVisibility(View.GONE);
        }
        
        if (current_item.last() != null) {
        	set_all_texts(holder.leftmost, current_item.last(), current_item);
        } else {
            holder.leftmost.setVisibility(View.GONE);
        }
        
        holder.set_selected_btn(current_item.value);
        
        holder.setClickListener(onClickListener);

        return convertView;
    }


	private static void set_all_texts(ToggleButton btn, String str, SubReject current_item) {
		btn.setText(str);
		btn.setTextOn(str);
		btn.setTextOff(str);
		btn.setVisibility(View.VISIBLE);
	}
}


