package com.sixt.android.ui;

import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.PnimmiValue;

public class DisplayLogic {
	
	private DisplayLogic() {}

	public static boolean should_show_hearot_screen() {
		boolean is_hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();
		boolean pnimmi_sgirra =  PnimmiValue.Pnimmi() && ActionValue.Sgirra();
		if (is_hachzarra || pnimmi_sgirra) {
			return true;
		}
		return false;
	}

	public static boolean mok_is_active() {
		return reject_mode_mesirra(); // identical logic
	}

	
	public static boolean reject_mode_mesirra() {
		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra(); 
		boolean pnimmi_pticha =  PnimmiValue.Pnimmi() && ActionValue.Pticha();
				
		return pnimmi_pticha || mode_mesirra;
	}

}
