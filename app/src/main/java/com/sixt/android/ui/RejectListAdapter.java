package com.sixt.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.android.activities.RejectListActivity;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.TEST;
import com.sixt.android.ui.RejectListAdapter.ViewHolder;
import com.sixt.rent.R;

public class RejectListAdapter extends ArrayAdapter<GetRejects_Reject> {

    private static final int LIST_ELEMENT_LAYOUT = R.layout.reject_entry;
    
	private static AlertDialog alertDialog;

//	>> disablw list reject elems in menu
	
	public RejectListAdapter(Context context, GetRejects_Reject[] values) {
        super(context, R.layout.reject_entry, values);
//        for (GetRejects_Reject rec: values) {
//        	boolean nodef = rec.no_default_value();
//        	if (rec.value < 1) {
//        		rec.value = nodef ? -1 : 0;
//        	}
//        	if (rec.no_default_value()) {
//        		rec.value = -1;
//        	}        	
//        }
	}

	 

	public class ViewHolder {
		TextView caption;
        ToggleButton left;
        ToggleButton center;
        ToggleButton right;

        void uncheckButtons(){
            left.setChecked(false);
            center.setChecked(false);
            right.setChecked(false);
        }

		public void set_listeners(OnClickListener onClickListener) {
	        left.setOnClickListener(onClickListener);
	        center.setOnClickListener(onClickListener);
	        right.setOnClickListener(onClickListener);
		}

		public void set_btn_texts(GetRejects_Reject current) {
			if (TEST.SHOW_REJECT_CODES_ON_SCREEN) {
				caption.setText(current.RejectCode);
			}
			else {
				caption.setText(current.RejectDesc);
			}
			final String _left = current.left_str();
			final String _center = current.center_str();
			final String _right = current.right_str();
			if (_notEmpty(_left)) {
		        left.setTextOn(_left);
		        left.setTextOff(_left);
		        left.setText(_left);
		        left.setVisibility(View.VISIBLE);
			}
			else {
				left.setVisibility(View.INVISIBLE);
			}
	        center.setTextOn(_center);
	        center.setTextOff(_center);
	        center.setText(_center);
	        right.setText(_right);
	        right.setTextOn(_right);
	        right.setTextOff(_right);
		}
		
		void mark_selected(View v, GetRejects_Reject current) {
			//gilad: 0==right, 1==center, 2==left
			current.value = -1;
			if (v==right) {
				current.value = 0;
			}
			else if (v==center) {
				current.value = 1; 
			}
			else if (v==left) {
				current.value = 2; 
			}
			else {
				int jj = 345;
				jj++;
//				throw new RuntimeException();
			}
			
		}

		public void set_selected_btn(int selected) {
			//gilad: 0==right, 1==center, 2==left
			uncheckButtons();
			if (selected==0) {
				right.setChecked(true);
			}
			else if (selected==1) {
				center.setChecked(true);
			}
			else if (_visible(left) && selected==2) {
				left.setChecked(true); 
			}
			else {
				int jj=234;
				jj++;
//				throw new RuntimeException(); -- it is ok - has no selection				
			}
			
		}

		public int get_selected_btn_ind() {
			if (right.isChecked()) {
				return 0;
			}
			else if (center.isChecked()) {
				return 1; 
			}
			else if (left.isChecked()) {
				return 2; 
			}
			else {
				int jj = 345;
				jj++;
				return -1;
//				throw new RuntimeException();
			}
		}
    };
    

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView==null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(LIST_ELEMENT_LAYOUT, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.caption = (TextView) convertView.findViewById(R.id.caption);  
            holder.left = (ToggleButton) convertView.findViewById(R.id.left);
            holder.center = (ToggleButton) convertView.findViewById(R.id.center);
            holder.right = (ToggleButton) convertView.findViewById(R.id.right);
            convertView.setTag(holder);
        }

        final GetRejects_Reject current = getItem(position);
        String __code = current.RejectCode.toUpperCase();
        if (__code.contains("WHEEL")) {
        	int jj=234;
        	jj++;
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	on_item_selected(v, holder, position);
            }
        };
        
        holder.set_listeners(onClickListener);


        holder.set_btn_texts(current);
        
        String _code = current.RejectCode;
        
        holder.set_selected_btn(current.value); 

        return convertView;
    }

    

	public static boolean _notEmpty(String _left) {
		if (_left==null) {
			return false;
		}
		_left = _left.trim();
		if (_left.length()==0) {
			return false;
		}
		return true;
	}



	public static boolean _visible(ToggleButton btn) {
		return btn.getVisibility() == View.VISIBLE;
	}



	public void on_item_selected(View v, ViewHolder holder, final int position) {
        final ToggleButton selected_btn = (ToggleButton)v; 
        final GetRejects_Reject current = getItem(position);
        final String _code = current.RejectCode;
        final int new_val = get_btn_ind(selected_btn, holder);
		int orig_val = GetCarDetails_Response.get_orig_car_reject_val(_code);
		 
		if (new_val < orig_val) {
			selected_btn.setSelected(false);
			RejectListActivity.handle_lesser_reject_set();
			return;
		}
        holder.uncheckButtons();
        selected_btn.setChecked(true);
        final boolean is_first_option = (selected_btn == holder.right); // should not open subrejects if rightmost option selected!  
        holder.mark_selected(v, current);
        if (new_val==0 && orig_val < 0) {
        	orig_val = 0;
        }
        RejectListActivity.onClick_ListItem(current, orig_val, new_val, is_first_option, holder, position);
	}
	
	public void set_to_noerror(ViewHolder clicked_reject_holder) {
		if (clicked_reject_holder != null) {
			clicked_reject_holder.uncheckButtons();
			clicked_reject_holder.right.setChecked(true); //
		}
	}
	
	public void set_selected(ViewHolder clicked_reject_holder, int pos) {
		if (clicked_reject_holder != null) {
			clicked_reject_holder.set_selected_btn(pos);
		}
	}

	
	public int get_selected_btn_ind(ViewHolder clicked_reject_holder) {
		if (clicked_reject_holder != null) {
			return clicked_reject_holder.get_selected_btn_ind();
		}
		return -1;
	}

	protected static int get_btn_ind(ToggleButton selected_btn, ViewHolder holder) {
		if (selected_btn == holder.right) {
			return 0;
		}
		else if (selected_btn == holder.center) {
			return 1;
		}
		else if (selected_btn == holder.left) {
			return 2;
		}
		throw new RuntimeException();
	}

}


