package com.sixt.android.ui;

/**
 * Created by Izakos on 07/03/2016.
 */
public class Point {

    public float x;
    public float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
