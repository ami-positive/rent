package com.sixt.android.ui;

public class Debug_SubrejectRecord {
	public String caption;
	public String leftmost;
	public String pre_leftmost;
	public String center1;
	public String center2;
	public String center3;
	public String rightmost;
	public int selected = 0; // 0==rightmost 1 = center3 , 2 = center2 , 3 = center1 , 4 = leftmost  

	public Debug_SubrejectRecord(String caption, String leftmost, String pre_leftmost, String center1, String center2, String center3, String rightmost) {
    	this.caption = _null_if_empty(caption);
        this.leftmost = _null_if_empty(leftmost);
        this.pre_leftmost = _null_if_empty(pre_leftmost);
        this.center1 = _null_if_empty(center1);
        this.center2 = _null_if_empty(center2);
        this.center3 = _null_if_empty(center3);
        this.rightmost = _null_if_empty(rightmost);
    }

	private static String _null_if_empty(String s) {
		if (s==null) {
			return null;
		}
		return s.length()==0 ? null : s;
	}
}

