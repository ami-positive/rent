package com.sixt.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sixt.rent.R;

public class FuelSlide extends LinearLayout {
	
	public static final int FUEL_LEVEL_NOTSET = -1;
	
	private Context appContext;
	private Point start = new Point();
	private ImageView thumb;
	private ImageView ruller;
	private int PADDING=20;
	private int VIEW_WIDTH;
	private int VIEW_HEIGHT;
	private int RULLER_WIDTH;
	private int RULLER_HEIGHT;
	private int blockSize;
	private int fuelParts = 9;
	private int current_fuel_level = FUEL_LEVEL_NOTSET; // 0..8

	public FuelSlide(Context context, AttributeSet attrs) {
		super(context, attrs);
		appContext = context;
		this.setOrientation(VERTICAL);
				
		thumb = new ImageView(context);
		thumb.setImageResource(R.drawable.fuel_indicator);
		ruller = new ImageView(context);
		ruller.setImageResource(R.drawable.fuel_bg);  

		ruller.setY(thumb.getMeasuredHeight());

		this.setPadding(PADDING,PADDING,PADDING,PADDING);
		ruller.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		addView(thumb);
		addView(ruller);
		
//		ruller.setBackgroundColor(getResources().getColor(R.color.green)); 

		set_fuel_level(FUEL_LEVEL_NOTSET);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int count = getChildCount();

		// Measurement will ultimately be computing these values.
		int maxHeight = 0;
		int maxWidth = 0;
		int childState = 0;
		int summedHeight = 0;

		// Iterate through all children, measuring them and computing our dimensions
		// from their size.
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() != GONE) {
				// Measure the child.
				measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);

				// Update our size information based on the layout params.  Children
				// that asked to be positioned on the left or right go in those gutters.
				final LayoutParams lp = (LayoutParams) child.getLayoutParams();

				maxWidth = Math.max(maxWidth,
						child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
				maxHeight = Math.max(maxHeight,
						child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin);
				summedHeight +=child.getMeasuredHeight();
				childState = combineMeasuredStates(childState, child.getMeasuredState());
			}
		}
		WindowManager wm = (WindowManager) appContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		// calculate the view width
		if (maxWidth + 2*PADDING < size.x){
			VIEW_WIDTH = maxWidth+ 2*PADDING;
		} else {
			VIEW_WIDTH = size.x;
		}
		VIEW_HEIGHT = summedHeight+2*PADDING;
		RULLER_HEIGHT = VIEW_HEIGHT - 2*PADDING;
		RULLER_WIDTH = VIEW_WIDTH - 2*PADDING;
		blockSize = RULLER_WIDTH / fuelParts;
		if (current_fuel_level > FUEL_LEVEL_NOTSET){
            thumb.setX(current_fuel_level*blockSize + blockSize/2 - thumb.getWidth()/2 + PADDING);
        } else {
            thumb.setX(PADDING + blockSize/2 - getChildAt(0).getMeasuredWidth()/2);
        }
		setMeasuredDimension(VIEW_WIDTH, VIEW_HEIGHT);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction();
		int[] viewCoords = new int[2];
		Point eventCoords = new Point();
		ruller.getLocationOnScreen(viewCoords);
		
		thumb.setVisibility(View.VISIBLE); //!

		switch (action) {
		case MotionEvent.ACTION_DOWN:
			start.x = (int) event.getX();
			start.y = (int) event.getY();
			eventCoords.x = start.x - viewCoords[0];
			eventCoords.y = start.y - viewCoords[1];
			if (eventCoords.x >0 && eventCoords.x < VIEW_WIDTH -2*PADDING) {
				current_fuel_level =  Math.round((eventCoords.x / blockSize));
				validate_value();
			}
			thumb.setX(current_fuel_level*blockSize + blockSize/2 - thumb.getWidth()/2 + PADDING);
			break;
		case MotionEvent.ACTION_MOVE:
			start.x = (int) event.getX();
			start.y = (int) event.getY();
			eventCoords.x = start.x - viewCoords[0];
			eventCoords.y = start.y - viewCoords[1];
			if (eventCoords.x >0 && eventCoords.x < RULLER_WIDTH -2*PADDING) {
				current_fuel_level =  Math.round((eventCoords.x / blockSize));
				validate_value();
			}
			thumb.setX(current_fuel_level*blockSize + blockSize/2 - thumb.getWidth()/2 + PADDING);
			break;
		}

		return true;

	}

	public void clear() {
		set_fuel_level(FUEL_LEVEL_NOTSET);		
	}

	public  int get_fuel_level(){
		validate_value();
		return this.current_fuel_level;
	}

	public void set_fuel_level(int fuel_level){
		this.current_fuel_level = fuel_level;
		if (current_fuel_level < 0) {
			thumb.setVisibility(View.INVISIBLE);
		}
		else {
			thumb.setX(fuel_level*blockSize + blockSize/2 - thumb.getWidth()/2 + PADDING);
			thumb.setVisibility(View.VISIBLE);
		}
		validate_value();
		invalidate();
	}

	private void validate_value() {
		// -1 ==> value not-set
//		if (current_fuel_level < -1 || current_fuel_level > 8) {
//			throw new RuntimeException("Illegal fuel value: " + current_fuel_level);
//		}
		
	}


}
