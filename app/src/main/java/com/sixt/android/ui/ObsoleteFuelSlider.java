//package com.sixt.android.ui;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.util.AttributeSet;
//import android.widget.SeekBar;
//
//import com.sixt.rent.R;
//
//public class ObsoleteFuelSlider extends SeekBar {
//    private int stepSize = 1;
//    private int max = 9;
//    private TypedArray attributes;
//
//    public ObsoleteFuelSlider(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        attributes = context.getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.FuelSlider,
//                0, 0);
//        try {
//            max = attributes.getInteger(R.styleable.FuelSlider_max, 9);
//            stepSize = attributes.getInteger(R.styleable.FuelSlider_stepSize, 1);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            attributes.recycle();
//        }
//        this.setMax(max);
//    }
//
//    @Override
//    public void setProgress(int progress) {
//        if (stepSize > 0)
//            progress = ((int) Math.round(progress / stepSize)) * stepSize;
//        super.setProgress(progress);
//    }
//
//
//}
