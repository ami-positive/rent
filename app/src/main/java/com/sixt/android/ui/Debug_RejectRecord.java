package com.sixt.android.ui;

public class Debug_RejectRecord {
    String caption;
    String right;
    String center;
    String left;
    boolean show_dialog = true;
    int selected = 0; //gilad: 0==right, 1==center, 2==left
    
    
    public Debug_RejectRecord(String caption, String left, String center, String right, boolean show_dialog){
    	this.caption = caption;
        this.right = right;
        this.center = center;
        this.left = left;
        this.show_dialog = show_dialog;
    }
}
