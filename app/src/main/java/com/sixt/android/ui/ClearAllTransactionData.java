package com.sixt.android.ui;

import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.activities.HearotLehachzarraActivity;
import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.activities.SignatureActivity;
import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.FuelValues;

public class ClearAllTransactionData {
	private ClearAllTransactionData() {}

	public static void clear() {
		CarNumberActivity.lastForm = null; 
		UniqueId.clear();
		BreadCrumbs.set("");
		MenuMischarriActivity.reset();
		MenuPnimmiActivity.reset();
		HearotLehachzarraActivity.clear_hearrot();
		FuelValues.clear();
		CarNumberValue.clear();
		SignatureActivity.list_reject_arr = null;
		SignatureActivity.image_reject_arr = null;
	}

}
