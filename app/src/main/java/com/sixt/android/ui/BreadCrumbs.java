package com.sixt.android.ui;

public class BreadCrumbs {
	
	public static final String SEP = " > ";

	private static String _txt = "";

	
	private BreadCrumbs() {} 
	
	
	public static void set(String txt) {
		_txt = txt;
	}
	
	public static void append(String txt) {
		_txt += (BreadCrumbs.SEP + txt);
	}
	
	
	public static String get() {
		return _txt;
	}
	
	

}
