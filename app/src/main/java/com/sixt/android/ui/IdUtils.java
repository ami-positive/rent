package com.sixt.android.ui;


public class IdUtils { 

	private IdUtils() {}

	
	public static boolean validate(String idnum) {
		// number MUST INCLUDE sifrat bikoret!
		try { 
			return inner_validate(idnum);
		}
		catch (Exception e) {
			return false;
		}
	}
	
	private static boolean inner_validate(String idnum) {
				
		idnum = idnum.trim();

		if (idnum.length() > 9) {
			return false;
		}
		if (idnum.length() < 6) {
			return false;
		}

		while (idnum.length() < 9){
			idnum="0"+idnum;
		}

	
		int idnum1=_tonum(idnum.substring(0,1))*1;
		int idnum2=_tonum(idnum.substring(1,2))*2;
		int idnum3=_tonum(idnum.substring(2,3))*1;
		int idnum4=_tonum(idnum.substring(3,4))*2;
		int idnum5=_tonum(idnum.substring(4,5))*1;
		int idnum6=_tonum(idnum.substring(5,6))*2;
		int idnum7=_tonum(idnum.substring(6,7))*1;
		int idnum8=_tonum(idnum.substring(7,8))*2;
		int idnum9=_tonum(idnum.substring(8,9))*1;

		if (idnum1>9) idnum1=(idnum1%10)+1;
		if (idnum2>9) idnum2=(idnum2%10)+1;
		if (idnum3>9) idnum3=(idnum3%10)+1;
		if (idnum4>9) idnum4=(idnum4%10)+1;
		if (idnum5>9) idnum5=(idnum5%10)+1;
		if (idnum6>9) idnum6=(idnum6%10)+1;
		if (idnum7>9) idnum7=(idnum7%10)+1;
		if (idnum8>9) idnum8=(idnum8%10)+1;
		if (idnum9>9) idnum9=(idnum9%10)+1;

		int sumval = idnum1+idnum2+idnum3+idnum4+idnum5+idnum6+idnum7+idnum8+idnum9;

		sumval = sumval%10;
		
		if (sumval>0){
//			alert("תעודת הזהות שגוייה");
			return false;
		} 

		return true;
	}


	private static int _tonum(String s) {
		try { 
			int val = Integer.parseInt(s);
			return val;
		}
		catch (Exception e) {
			int jj=234;
			jj++;
			throw new RuntimeException("bad num: " + s);
		}
	}
}
