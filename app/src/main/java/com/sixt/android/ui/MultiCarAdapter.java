//package com.sixt.android.ui;
//
//import java.util.ArrayList;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener; 
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.sixt.android.app.json.objects.CarModelNumber;
//import com.sixt.rent.R;
//
//public class MultiCarAdapter extends ArrayAdapter<CarModelNumber> {
//    public MultiCarAdapter(Context context, ArrayList<CarModelNumber>  values) {
//        super(context, R.layout.merukezzet_row, values);
//    }
//
//    class ViewHolder {
//        TextView car_number;
//        TextView car_model;
//        ImageView delete_icon;
//
//        void setContent(CarModelNumber carDeggem){
//            car_number.setText(carDeggem.number);
//            car_model.setText(carDeggem.getModel());
//        }
//    };
//
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        if (convertView==null) {
//            LayoutInflater inflater = (LayoutInflater) getContext()
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = inflater.inflate(R.layout.merukezzet_row, parent, false);
//            ViewHolder holder = new ViewHolder();
//            holder.car_number = (TextView) convertView.findViewById(R.id.number);
//            holder.car_model = (TextView) convertView.findViewById(R.id.daggem);
//            holder.delete_icon = (ImageView) convertView.findViewById(R.id.delete_icon);
//            convertView.setTag(holder);
//        }
//
//        final ViewHolder holder = (ViewHolder) convertView.getTag();
//
//        View.OnClickListener onClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        };
//
//        CarModelNumber current = getItem(position);
//
//        holder.setContent(current);
//
//        holder.delete_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CarModelNumber car_rec = getItem(position);
//                remove(car_rec);
//                notifyDataSetChanged();
////                MesiraMerukezzetActivity.remove_from_list(car_rec);
//            }
//        });
//
//        return convertView;
//    }
//}
//
//
