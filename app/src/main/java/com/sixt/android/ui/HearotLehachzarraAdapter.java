package com.sixt.android.ui;


import java.util.ArrayList;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.android.activities.HearotLehachzarraActivity;
import com.sixt.android.app.json.objects.HearotLehachzarraRecord;
import com.sixt.android.app.json.objects.RejectListInCiRecord;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.TEST;
import com.sixt.rent.R;

public class HearotLehachzarraAdapter extends ArrayAdapter<HearotLehachzarraRecord> {
	
	public static final int ENTRY_LAYOUT = R.layout.hearot_lehachzarra_entry;
	
	private static final boolean _DEBUG = TEST.debug_mode(false);
 

//    public HearotLehachzarraAdapter(Context context) {
//        super(context, ENTRY_LAYOUT, getFirstRec());
//    }
	
	private String last_record_txt = "";

    public HearotLehachzarraAdapter(Context c, int e_layout, ArrayList<HearotLehachzarraRecord> initial_content) { 
    	super(c, e_layout, initial_content); 
	}
   

	class DialogViewHolder {
		public TextView upper_caption;
		public ImageView delete_icon;
		public Spinner takalla_spinner2;  //WITH first empty line; NO default 
		public EditText txt_description;
		public Button add_new_btn;    	
    }; 


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	final HearotLehachzarraActivity a = HearotLehachzarraActivity.inst; 
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(ENTRY_LAYOUT, parent, false);
            DialogViewHolder holder = new DialogViewHolder();
            holder.upper_caption = (TextView) convertView.findViewById(R.id.upper_caption); 
            holder.delete_icon = (ImageView) convertView.findViewById(R.id.delete_icon);
            holder.takalla_spinner2 = (Spinner) convertView.findViewById(R.id.takalla_spinner2);            
            if (DeviceAndroidId.is_lab_device(a) && holder.takalla_spinner2==null) {  
            	throw new RuntimeException();
            }
            holder.txt_description = (EditText) convertView.findViewById(R.id.txt_description);
            holder.add_new_btn = (Button) convertView.findViewById(R.id.add_new_btn);
            convertView.setTag(holder);
        }
        final DialogViewHolder holder = (DialogViewHolder) convertView.getTag(); 
        final HearotLehachzarraRecord current = getItem(position);
        
        ArrayAdapter<String> spinner_adapter = null;
        if (_DEBUG) {
        	spinner_adapter = debug_spinner_adapter();
        }
        else {
        	spinner_adapter = populate_spinner_adapter();
        }
        if (DeviceAndroidId.is_lab_device(a) && spinner_adapter.getCount() < 2) {
        	throw new RuntimeException();
        }
		holder.takalla_spinner2.setAdapter(spinner_adapter);

//        if (position == getCount()-1) {
//        	last_record_txt =holder.txt_description;
//        }

        
        final boolean is_first_record = (position==0);
        final boolean is_last_record = (position==getCount()-1);
        
        holder.upper_caption.setVisibility(is_first_record ? View.VISIBLE : View.GONE); 
        
        holder.add_new_btn.setVisibility(is_last_record ? View.VISIBLE : View.INVISIBLE);
        
        int _sel = current.selected_takalla >= 0 ? current.selected_takalla : 0; 
        holder.takalla_spinner2.setSelection(_sel);
        holder.txt_description.setText(current.txt_description);
        
        holder.txt_description.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (is_last_record) {
					last_record_txt = s.toString();
				}
			}
		});
        	

        
        holder.txt_description.setOnFocusChangeListener(new View.OnFocusChangeListener() {			
			@Override
			public void onFocusChange(View edit_text, boolean hasFocus) {
				if (hasFocus) {
					return;
				}
				final HearotLehachzarraAdapter record_adapter = HearotLehachzarraAdapter.this;
				final int all_count = record_adapter.getCount();
				debug_position = position;
				if (position < 0 || position >= all_count) {  
					return;
				}
				final HearotLehachzarraRecord cur_record = record_adapter.getItem(position);
//				if (position >= record_adapter.getCount()) {
//					return;
//				} 
//				if (holder != null) {
					EditText et = (EditText) edit_text;
					String desc = et.getText().toString();
//					HearotLehachzarraRecord cur = a.getItem(position);
//					cur.txt_description = s;
					cur_record.txt_description = desc; 
					int jj=234;
					jj++;
//				} 
			}
		}); 
        
                        
        holder.takalla_spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> the_spinner, View arg1,
					int sel_value, long arg3) {
				final HearotLehachzarraAdapter record_adapter = HearotLehachzarraAdapter.this;
				final HearotLehachzarraRecord cur_record = record_adapter.getItem(position);
				
				int jj=234;  
				jj++;
				cur_record.selected_takalla = -1;
				cur_record.actual_reject_code = -1;
				
//				final int num_records = record_adapter.getCount();
				ArrayAdapter<String> spinner_adapter = (ArrayAdapter<String>) the_spinner.getAdapter();
				final int num_in_spinner = spinner_adapter.getCount();
				debug_position = sel_value; 
				if (sel_value >= num_in_spinner) {
					int jjj=2342; 
					jjj++;
					return;
				}
				if (sel_value < 0 || sel_value >= num_in_spinner) {
					int jjj=2342; 
					jjj++;
					return;
				}
				final String sel_txt = spinner_adapter.getItem(sel_value);
				final int sel_code = AllDataRepository.get_hearrot_code(sel_txt);
				if (sel_code < 0) {
					int jj2=234;
					jj2++;
					return;
				}
				cur_record.selected_takalla = sel_value; /// move up
				cur_record.actual_reject_code = sel_code;
//				String caption = cur.caption;
//				cur.reject_code = AllDataRepository.get_hearrot_code(caption);
				int jjjj=234;
				jjjj++;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// no op
				
			}
		});
        
        holder.add_new_btn.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) { 
				// add a new line
				update_struct_from_ui();
				HearotLehachzarraAdapter a = HearotLehachzarraAdapter.this;
				a.add(new HearotLehachzarraRecord());  
//				HearotLehachzarraActivity.add_line(HearotLehachzarraAdapter.this);
				a.notifyDataSetChanged();
			}
		});
        
        holder.delete_icon.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// delete line
				update_struct_from_ui();
				HearotLehachzarraAdapter a = HearotLehachzarraAdapter.this;
				int num_lines = a.getCount();
				if (num_lines==1) {
					HearotLehachzarraActivity.cannot_delete_last_toast();
					return;
				}
				HearotLehachzarraRecord cur = a.getItem(position);
				a.remove(cur);
				a.notifyDataSetChanged(); 
			}
		});
        
        return convertView;
    }

	public void update_struct_from_ui() {
		final int last_pos = getCount() - 1; 
		if (last_pos < 0) {
			return;
		}
		HearotLehachzarraRecord lastItem = getItem(last_pos);
		if (lastItem==null) { 
			return;
		}
		String txt = last_record_txt;
		lastItem.txt_description = txt; 		
	}
	

	protected DialogViewHolder viewToHolder(View vv) {
		ViewParent parent;
		while ((parent=vv.getParent()) != null) {
			if (!(parent instanceof View)) {
				int jj=234;
				jj++;
				break;
			}
			Object tag = ((View)parent).getTag();
			if (tag != null && tag instanceof DialogViewHolder) {
				return (DialogViewHolder)tag;
			}			
		}
		return null;
	}

	private ArrayAdapter<String> debug_spinner_adapter() {
		if (_DEBUG) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String> (getContext(), android.R.layout.simple_spinner_item );
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			adapter.add("");
			adapter.add("הגה רועד");					 
			adapter.add("גלגלים חסרים");
			adapter.add("שמן ברקס");
			adapter.add("גג חסר");
			adapter.add("שמשה שבורה");
			adapter.add("שברים בפח");
			return adapter;
		}
		return null;
	}
	
	private ArrayAdapter<String> populate_spinner_adapter() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String> (getContext(), android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		RejectListInCiRecord[] rejects = AllDataRepository.getRejectList();
		adapter.add("");
		if (rejects != null) {
			for (RejectListInCiRecord r: rejects) {		
				adapter.add(r.RejectName);
			}
		}
		return adapter;		
	}


	private static int debug_position;
	
//	private static void set_all_texts(ToggleButton btn, String str) {
//		btn.setText(str);
//		btn.setTextOn(str);
//		btn.setTextOff(str);
//		btn.setVisibility(View.VISIBLE);
//	}
}


