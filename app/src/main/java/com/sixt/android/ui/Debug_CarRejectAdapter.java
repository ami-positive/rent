package com.sixt.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sixt.android.activities.SubRejectsActivity_;
import com.sixt.rent.R;

public class Debug_CarRejectAdapter extends ArrayAdapter<Debug_RejectRecord> {

    private static final int LIST_ELEMENT_LAYOUT = R.layout.reject_entry;
    
	private static AlertDialog alertDialog;
	
    public Debug_CarRejectAdapter(Context context, Debug_RejectRecord[] values) {
        super(context, R.layout.reject_entry, values);
        mark_right_btns_as_default(); 
    } 
 
    private void mark_right_btns_as_default() { //gilad
//		int num = getCount();
//		for (int  i = 0; i < num; i++) {
//			get
//			ListRecord cur = getItem(i);
//			cur..getT right.setChecked(true);
//		}		
	}
    

	class ViewHolder {
		TextView caption;
        ToggleButton left;
        ToggleButton center;
        ToggleButton right;

        void uncheckButtons(){
            left.setChecked(false);
            center.setChecked(false);
            right.setChecked(false);
        }

		public void set_listeners(OnClickListener onClickListener) {
	        left.setOnClickListener(onClickListener);
	        center.setOnClickListener(onClickListener);
	        right.setOnClickListener(onClickListener);
		}

		public void set_btn_texts(Debug_RejectRecord current) {
			caption.setText(current.caption);
	        left.setTextOn(current.left);
	        left.setTextOff(current.left);
	        left.setText(current.left);
	        center.setTextOn(current.center);
	        center.setTextOff(current.center);
	        center.setText(current.center);
	        right.setText(current.right);
	        right.setTextOn(current.right);
	        right.setTextOff(current.right);
		}

		
		void mark_selected(View v, Debug_RejectRecord current) {
			//gilad: 0==right, 1==center, 2==left
			if (v==right) {
				current.selected = 0;
			}
			else if (v==center) {
				current.selected = 1; 
			}
			else if (v==left) {
				current.selected = 2; 
			}
			else {
				throw new RuntimeException();
			}
			
		}

		public void set_selected_btn(int selected) {
			//gilad: 0==right, 1==center, 2==left
			uncheckButtons();
			if (selected==0) {
				right.setChecked(true);
			}
			else if (selected==1) {
				center.setChecked(true);
			}
			else if (selected==2) {
				left.setChecked(true); 
			}
			else {
				throw new RuntimeException();
			}
			
		}
    };
    

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView==null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(LIST_ELEMENT_LAYOUT, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.caption = (TextView) convertView.findViewById(R.id.caption);  
            holder.left = (ToggleButton) convertView.findViewById(R.id.left);
            holder.center = (ToggleButton) convertView.findViewById(R.id.center);
            holder.right = (ToggleButton) convertView.findViewById(R.id.right);
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.uncheckButtons();
                ((ToggleButton) v).setChecked(true);
                Debug_RejectRecord current = getItem(position);
                holder.mark_selected(v, current);
                if (current.show_dialog) { //gilad 
                	SubRejectsActivity_.intent(getContext()).start();   
                }
            }
        };
        
        holder.set_listeners(onClickListener); 

        Debug_RejectRecord current = getItem(position);

        holder.set_btn_texts(current);
        
        holder.set_selected_btn(current.selected); 

//        holder.right.setChecked(true); gilad: not here

        return convertView;
    }
}

