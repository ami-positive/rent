package com.sixt.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.sixt.android.MyApp;
import com.sixt.rent.R;

public class RejectView extends View {
    private int sourceImage;

    protected TypedArray attributes;
    
    public RejectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RejectView,
                0, 0);

        try {
            sourceImage = attributes.getResourceId(R.styleable.RejectView_source,0);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            attributes.recycle();
        }
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if (sourceImage == 0) {
            return;
        }
        Paint p=new Paint();
        int Height = MyApp.generalSettings.getScreenHight();
        int Width = MyApp.generalSettings.getScreenWidth();
        Bitmap b= BitmapFactory.decodeResource(getResources(), sourceImage);
        Log.i("Car Display", "image before width|hight: " + b.getWidth() + "|" + b.getHeight());
        if(Height != 0 && Width != 0){b  = b.createScaledBitmap(b, Width,Height,false );}
        Log.i("Car Display", "image after width|hight: " + b.getWidth() + "|" + b.getHeight());
        p.setColor(Color.RED);
        canvas.drawBitmap(b,0,0,p);
    }


    public void setSourceImage(int resourceID){
        this.sourceImage = resourceID;
        invalidate();
    }

}
