package com.sixt.android.httpClient;
import android.util.Log;

import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.CommonFunctions;
import com.sixt.android.app.util.JsonDebug;
import com.sixt.android.app.util.TEST;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class JsonTransmitter { 
	
	private JsonTransmitter() {}
 
	////////////////////// 
  
	private static final String GILADD = // TEC == reasonably stable 
			"http:// 212.150.71.199:57772/csp/carsmartphonetecprod/AUTENTICATION.csp"; 
	
	private static final String DEVV = // ==for sharon ;  not stable 
//			"http://212.150.71.199:57772/csp/carsmartphonedevprod/AUTENTICATION.csp";
			"http://212.150.71.199:2015";
	
	private static final String PROD_HTTP = "http://212.150.71.199:2013"; 
	
//	private static final String PROD_CSP = // production url; old one
//			"http://212.150.71.199:57772/csp/carsmartphoneprod/AUTENTICATION.csp"; 
	 
	public static volatile String MAIN_SERVER_URL = DEVV;

	
	 
	public static void setUrl(int val) { // 0 > dev ; 1 > PROD_CSP ; 2 > PROD_HTTP 
		if (val==0) {
			MAIN_SERVER_URL = DEVV; 
		}
//		else if (val==1) {
//			MAIN_SERVER_URL = PROD_CSP;
//		}
		else if (val==2) {
			MAIN_SERVER_URL = PROD_HTTP;
		}
		else {
			throw new RuntimeException();
		}
	}
	public static String getRequestUrl()
	{
		return MAIN_SERVER_URL;
	}
	public static int getRequestTimeout()
	{
		return DEFAULT_JSON_TIMEOUT;
	}
	
 
	public static final String FTP_SERVER_URL = "212.150.71.199";
 	
	public static final String LPR_URL = "http://212.150.71.199:8088/PhotoService/photos/uploadPhoto";
		
	
	//////////////////////
	

	
	
//	private static final int CONNECTION_TIMEOUT = 60*1000;   
    private static final int DEFAULT_JSON_TIMEOUT = 600*1000;
    
    private static final Executor http_writer = Executors.newSingleThreadExecutor();
	
	public static void send(final String json) {
		http_writer.execute(new Runnable() {			
			@Override
			public void run() {
				try {
					do_send(json, false, DEFAULT_JSON_TIMEOUT); 
				} catch (ActionFailedError e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	} 

	public static Base_Response_JsonMsg send_blocking(final String json) 
			throws ConnectionError , ActionFailedError {
		return send_blocking(json, DEFAULT_JSON_TIMEOUT); 
	}

	public static Base_Response_JsonMsg send_blocking(final String json, int timeout) 
			throws ConnectionError , ActionFailedError{
		return send_blocking(json, false, timeout);
	}

	
	public static Base_Response_JsonMsg send_blocking(final String json, 
			final boolean use_debug_url) throws ConnectionError, ActionFailedError  {
		return send_blocking(json, use_debug_url, DEFAULT_JSON_TIMEOUT);
	}
	

	public static Base_Response_JsonMsg send_blocking(final String json, 
			final boolean use_debug_url, int timeout) throws ConnectionError, ActionFailedError  {
		try { 
			Base_Response_JsonMsg response = do_send(json, use_debug_url, timeout);
			return response;
		}
		catch (ActionFailedError err) {
			throw err;
		}
		catch (Exception e) {
			throw new ConnectionError(e);
		}
		
	}
 		 			
	static String cur_response;
 		 		
	
	private static Base_Response_JsonMsg do_send(String json, final boolean use_debug_url, int timeout) throws ActionFailedError {
		JsonDebug.set_request(json);
		

		
//		String json2 = "{\"Authentication_req\":{\"App\":\"L\",\"Debug\":\"אבגדהוזחטי\",\"Login\":\"29435555\",\"Pass\":\"29435555\",\"msg_type\":\"Authentication_Request\"}}";
		
		final String url = get_server_url(use_debug_url);

		Log.e("Retrofit", "Request: --> url: " + url + "\ncontent: " + json + "\nis debug ? " + use_debug_url);
		
		ActionFailedError err = null;
        try { 
        	
//        	HttpPost request = get_post_obj(json, url); // -- original works NO heb!        
//        	HttpPost request = new_get_post_obj(json, url); 
        	HttpPost request = CommonFunctions.getPostObjHebrew(json, url); //giladnew 
        	
        	
            HttpParams connParams = new BasicHttpParams();
//            HttpConnectionParams.setConnectionTimeout(connParams, CONNECTION_TIMEOUT);
//            HttpConnectionParams.setSoTimeout(connParams, SOCKET_TIMEOUT);
            HttpConnectionParams.setConnectionTimeout(connParams, timeout);
            HttpConnectionParams.setSoTimeout(connParams, timeout);
             
            DefaultHttpClient httpClient = new DefaultHttpClient(connParams);
            
            HttpResponse _response = null; 
            try {
                _response = httpClient.execute(request); 
            } catch (Exception e) {
//                throw new RTException(); 
            	Log.e("SIXTJSON", "\n\n\n execute error:" + e + " \n\n\n");
            	throw new RuntimeException(); 
            }
            
            HttpEntity response_entity = _response.getEntity();

                                    
            //InputStream is = response_entity.getContent();
            try {
            	// EntityUtils.toString:  default character will be used if none is found in the entity!
            	String response = EntityUtils.toString(response_entity, "UTF-8"); //gggg


				Log.i("Retrofit", "<-- Response content: " + response );

            	int jjj=234;
            	jjj++;
            
            /*    String response = "";
                int len = 0;                
                byte[] data1 = new byte[1024];
                while (-1 != (len = is.read(data1))) {
                    response += (new String(data1, 0, len));
                }*/
                final int _len = response.length(); 
 
                 
                Log.i("SIXTJSON", "\n\n\n received msg len = " + _len + " \n\n\n");
                
                String orig_request = json;
                cur_response = response;
                 
//              debug___testt_str_for_bad_chars(response); 
        		
        		JsonDebug.set_response(response);
        		
        		Base_Response_JsonMsg response_obj;
        		try {
        			response_obj = Base_Response_JsonMsg.load(response);
        			int jj=234;
        			jj++; 
        		}
        		catch (ActionFailedError e) {
        			Log.e("SIXTJSON", "\n\n\n ActionFailedError = " + e + " \n\n\n");
        			err = e;
        			throw err;
        		}
        		 
        		int jj=234;
        		jj++; 
        		 
        		 
        		if (response_obj != null) {
        			String type = response_obj.getClass().getName(); 
        			Log.i("SIXTJSON", "\n\n\n preLoad type = " + type + " \n\n\n");
        			
        			try { 
        				response_obj.postLoad(); //!
        			}
        			catch (Exception ee) {
        				Log.e("SIXTJSON", "\n\n\n Error while response_obj.postLoad() " + ee + " \n\n\n");
        				throw ee;
        			}
        		}
        		else {
            		Log.e("SIXTJSON", "\n\n\n response_obj==null ! \n\n\n");
        		}
        		
        		return response_obj;
                                 
            } finally { 
                //is.close();
            }
              
        } catch (Exception e) {
        	Log.e("SIXTJSON", "\n\n\n Error while do_send() " + e + " \n\n\n");
            String errmsg = "Error attempting to send Http request to server: " + e;
            Log.e("Http", errmsg);
            if (err != null) {
            	throw err;
            }
            else {
            	throw new RuntimeException("<" + e + ">"); 
            }
        }		
	}

		
	private static void debug___testt_str_for_bad_chars(String response) {
		if (!TEST.MODE) {
			return;
		}
	
		if (response==null) {
			return;
		}
		int len = response.length();
		for (int i = 0; i < len; i++) {
			char c = response.charAt(i);
			boolean not_Ascii = (c > 0x7F); 
			if (not_Ascii && c != ' ' && c != '_'  && c != '\'' && (c > 'ת' || c < 'א') && 
					(c > 'z' || c < 'a') && (c > 'Z' || c < 'A')) { 
				
				//U+FFFD � replacement character used to replace an unknown or unrepresentable character
				int jjj=234;
				jjj++;
				if (c == '\uFFFD') {
					int jj=2342;
					jj++;
				}
			}
		}
	}

	private static String get_server_url(boolean use_debug_url) {
//		if (use_debug_url) {
//			return DEBUG_SERVER_URL; 	
//		}
		return MAIN_SERVER_URL;
	}
	

	
	private static HttpPost get_post_obj(String json, String url) { // original; works fine, no hebrew!
		// gilad orig:
    	BasicHeader hdr;
        HttpPost request = new HttpPost(url);
        StringEntity entity;
		try {
			entity = new StringEntity(json);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Log.e("SIXTJSON", "\n\n\n get_post_obj error:" + e + " \n\n\n");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
        entity.setContentType("application/json;charset=UTF-8");//text/plain;charset=UTF-8
        hdr = new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8");
        entity.setContentEncoding(hdr);
        request.setEntity(entity);
        return request;
	}

	
	
	
	private static HttpPost new2_get_post_obj(String json, String url)  {
        HttpPost request = new HttpPost(url);
//        request.setHeader("Content-type", "application/x-www-form-urlencoded"); //giladnew
        request.setHeader("Content-type", "application/x-www-form-urlencoded"); //giladnew
        StringEntity entity;
		try {
			entity = new StringEntity(json, "UTF8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} //giladnew
//        entity.setContentType("application/json;charset=UTF-8");//text/plain;charset=UTF-8
//        BasicHeader hdr = new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8");
//        entity.setContentEncoding(hdr);
        request.setEntity(entity);
        return request;
	}

	
	private static HttpPost new_get_post_obj(String json, String url) { //testedNO  
        HttpPost request = new HttpPost(url);
        request.setHeader("Content-type", "application/x-www-form-urlencoded"); //giladnew
        StringEntity entity;
		try {
			entity = new StringEntity(json, "UTF8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} //giladnew 
//        entity.setContentType("application/json;charset=UTF-8");//text/plain;charset=UTF-8 giladnew comment out
//        BasicHeader hdr = new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"); //giladnew comment out
//        entity.setContentEncoding(hdr); //giladnew comment out
        request.setEntity(entity);
        return request;
	}
	
	
}


