package com.sixt.android.httpClient;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.CommonFunctions;
import com.sixt.android.httpClient.IHttpRequestsClient;
import com.sixt.android.httpClient.IWebServiceClientFactory;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpRequestsClient  implements IHttpRequestsClient {

	public final ExecutorService httpExecutor = Executors.newCachedThreadPool();

	private String getRequestURL(){
		
		return JsonTransmitter.getRequestUrl();
	}
	private int getRequestTimeout()
	{
		return JsonTransmitter.getRequestTimeout();
	}
	@Override
	public void request(final String jsonObj,final int flag, final Handler handler) {

		httpExecutor.execute(new Runnable() {
			@Override
			public void run() {
				String strResponse = "";
				DefaultHttpClient httpClient;
				HttpResponse response=null;
				try {
					HttpPost request = CommonFunctions.getPostObjHebrew(jsonObj, getRequestURL());

					HttpParams connParams = new BasicHttpParams();
		            HttpConnectionParams.setConnectionTimeout(connParams, getRequestTimeout());
		            HttpConnectionParams.setSoTimeout(connParams, getRequestTimeout());
					httpClient=new DefaultHttpClient(connParams);
					response =httpClient.execute(request);
					HttpEntity responseEntity = response.getEntity();
					strResponse = EntityUtils.toString(responseEntity, "UTF-8");
					Base_Response_JsonMsg responseObj;
					responseObj = Base_Response_JsonMsg.load(strResponse);
					Bundle bundle=new Bundle();
					bundle.putSerializable(IHttpRequestsClient.TAG_HTTP_RESPONSE_OBJ, responseObj);
					sendCallBackSuccessMessage(bundle,flag,handler);
				}catch (ActionFailedError e) {
        			Log.e("SIXTJSON", "\n\n\n ActionFailedError = " + e + " \n\n\n");
        			sendCallBackFailMessage(e.getMessage()==null?" EXCEPTION-":e.getMessage(), IWebServiceClientFactory.Response.RUNTIME_ACTION_FAILURE.getCode(), handler);
        		}
				catch (Exception e) {
					Log.d("SIXTJSON webservice", e.getMessage()==null?" EXCEPTION-":e.getMessage());
					sendCallBackFailMessage(e.getMessage()==null?" EXCEPTION-":e.getMessage(), IWebServiceClientFactory.Response.UNKNOWN.getCode(), handler);
					e.printStackTrace();
				}
				
				finally {
					if (response != null) {
						try {
							response.getEntity().consumeContent();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

	public void sendCallBackSuccessMessage(int arg1, String message, Handler handler) {
		Message msg = new Message();
		msg.what = IWebServiceClientFactory.Response.RESPONSE_OK.getCode();
		msg.arg1 = arg1;
		Bundle bundle = new Bundle();
		bundle.putString(IWebServiceClientFactory.TAG_MESSAGE, message);
		msg.setData(bundle);
		handler.sendMessage(msg);
	}
	
	public void sendCallBackSuccessMessage(Bundle bundle, int arg1, Handler handler) {
		Message msg = new Message();
		msg.what = IWebServiceClientFactory.Response.RESPONSE_OK.getCode();
		msg.arg1 = arg1;
		msg.setData(bundle);
		handler.sendMessage(msg);
	}
	
	public void sendCallBackFailMessage(String message, int what, Handler handler){
		Message msg = new Message();
		Bundle bundle = new Bundle();
		bundle.putCharSequence(IWebServiceClientFactory.TAG_MESSAGE, message);
		msg.what = what;
		msg.setData(bundle);
		handler.sendMessage(msg);
	}
}
