package com.sixt.android.httpClient;

import android.util.SparseArray;


public interface IWebServiceClientFactory {

	public String TAG_MESSAGE = "message";
	public String TAG_ERRORS = "errors";
	
	enum Response {
		UNKNOWN(-1, "Unknown Error"), 
		NOT_CONNECTED(0, "Not Connected Error"), 
		RESPONSE_OK(200, "OK"), 
		RESPONSE_CREATED(201, "Created"), 
		RUNTIME_EXCEPTION(301, "Runtime Exception"), 
		RUNTIME_ACTION_FAILURE(302, "Action Failure Exception"), 
		MISSING_API_TOKEN(400, "Missing Api Token"), 
		RESPONSE_UNAUTHORIZED(401, "Unauthorized: The username and password combination is wrong"), 
		USER_IS_UNAUTHORIZED(403, "Unauthorized User"), 
		NOT_FOUND(404, "Not Found: There is no user in the system with given id");
		
		private int code;
		private String information;
		
		private static SparseArray<Response> codeToResponseMapping;
		
		private Response(int c, String info) {
			code = c;
			information=info;
		}

		public int getCode() {
			return code;
		}
		
		public String getInfo(){
			return information;
		}
		
		public static Response getResponse(int i) {
	        if (codeToResponseMapping == null) {
	            initMapping();
	        }
	        return codeToResponseMapping.get(i, UNKNOWN);
	    }
	 
	    private static void initMapping() {
	    	codeToResponseMapping = new SparseArray<Response>();
	        for (Response s : values()) {
	        	codeToResponseMapping.put(s.code, s);
	        }
	    }
		
	}

	IHttpRequestsClient getHttpRequestsClient(boolean createNew);
	
}
