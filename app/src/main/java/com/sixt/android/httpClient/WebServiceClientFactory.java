package com.sixt.android.httpClient;

public class WebServiceClientFactory implements IWebServiceClientFactory {

	private HttpRequestsClient httpRequestsClient;

	private WebServiceClientFactory() {
	}

	public static class WSHolder {
		public static WebServiceClientFactory webServiceClientFactory = new WebServiceClientFactory();
	}

	public static WebServiceClientFactory getInstance() {
		return WSHolder.webServiceClientFactory;
	}

	

	@Override
	public IHttpRequestsClient getHttpRequestsClient(boolean createNew) {
		if (httpRequestsClient == null || createNew) {
			httpRequestsClient = new HttpRequestsClient();
		}
		return httpRequestsClient;
	}

	

}
