package com.sixt.android.httpClient;

@SuppressWarnings("serial")
public class ConnectionError extends Exception {

	public ConnectionError(Exception e) {
		super(e);
	}

	public ConnectionError(String s) {
		super(s);
	}
	
}
