package com.sixt.android.httpClient;

import android.os.Handler;


public interface IHttpRequestsClient {

	String TAG_HTTP_REQUEST = "http_request";
	String TAG_HTTP_RESPONSE_OBJ = "response_object";
	
	int FLAG_FEEDBACK = 1400;
			
	void request(String jsonObj,int flag, Handler handler);
	
}
