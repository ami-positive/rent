package com.sixt.android.webservices;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class WebServices {

	private static final int SOAP_VERSION = SoapEnvelope.VER11;
	private static final String WSDL_URL = "http://175.157.143.117:8085/TestProperties/services/TestPropts?wsdl"; //  The URL of WSDL file. In my case it is " http://175.157.143.117:8085/TestProperties/services/TestPropts?wsdl"
	// to access WSDL: <WS-url> + "?wsdl"
	
	private static final String NAMESPACE = "http://ws.testprops.com"; // targetNamespace in the WSDL.
	private static final String METHOD_NAME = "testMyProps"; // something like  <wsdl:operation name="............."> in your WSDL.
	private static final String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME; //  "NAMESPACE/METHOD_NAME"

		
	private WebServices() {}

	
    private static final Executor _writer = Executors.newSingleThreadExecutor();
	
	public static void activate(final String json) {		
		_writer.execute(new Runnable() {			
			@Override
			public void run() {
				do_activate();
			}
		});
	} 
		
	

	private static String do_activate() {
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME); 

//		String firstName = "Android";
//		String lastName = "Program";

//		//Pass value for fname variable of the web service
//		PropertyInfo fnameProp = new PropertyInfo();
//		fnameProp.setName("fname");//Define the variable name in the web service method
//		fnameProp.setValue(firstName);//Define value for fname variable
//		fnameProp.setType(String.class);//Define the type of the variable
//		request.addProperty(fnameProp);//Pass properties to the variable
		
		// properties or parameters comes (in WSDL) 'inside' each method
		request.addProperty("propName", "propValue");
		request.addProperty("propName-2", "propValue-2");
//		add_string_prop(request, "propName", "propValue");
//		add_string_prop(request, "propName-2", "propValue-2");
		
		
//
//		//Pass value for lname variable of the web service
//		PropertyInfo lnameProp =new PropertyInfo();
//		lnameProp.setName("lname");
//		lnameProp.setValue(lastName);
//		lnameProp.setType(String.class);
//		request.addProperty(lnameProp);
		
		String response = get_response_for(request);
		return response;

	}



	private static String get_response_for(SoapObject request) {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SOAP_VERSION);
		envelope.setOutputSoapObject(request);
//		envelope.dotNet = true;
		HttpTransportSE httpTransport = new HttpTransportSE(WSDL_URL); // J2SE based HttpTransport layer.
		try {
			httpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive)envelope.getResponse();
			
			 // Get the SoapResult from the envelope body.
//            SoapObject result = (SoapObject)envelope.bodyIn;
//			  if(result != null) {
//                  //Get the first property 
//                  String p1 = result.getProperty(0).toString();
//              }
			
			if (response == null) {
				throw new RuntimeException("No response from server!");
			}

			String response_str = response.toString();

			return response_str;

		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		

	}
	


//	private static void add_string_prop(SoapObject request, String propName, String propValue) {
//		PropertyInfo fnameProp = new PropertyInfo();
//		fnameProp.setName(propName);
//		fnameProp.setValue(propValue);
//		fnameProp.setType(String.class);
//		request.addProperty(fnameProp);
//	}



}
