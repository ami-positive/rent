package com.sixt.android.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class XmlParser {

	private static final String _NAMESPACE = null;
	private static final String DATA_NAME = "data";
	private static final String RELEVANT_ENTRY = "json_obj";

	private XmlParser() {}



	public static class XMLEntry {
		public final String title;
		public final String link;
		public final String summary;

		private XMLEntry(String title, String summary, String link) {
			this.title = title;
			this.summary = summary;
			this.link = link;
		}
	}

	//http://developer.android.com/training/basics/network-ops/xml.html



//	public static String parse_test() {
//		final String TEST_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n <data> \n <json_obj>content of json inner msg here</json_obj> \n </data>";
//		InputStream is = new ByteArrayInputStream(TEST_XML.getBytes());		
//		return parse(is);
//	}	


	public static String parse(InputStream in) {
		try { 
			XmlPullParser parser = create_parser(in);
			try {
				return readFeed(parser);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			} 
		}
		finally {
			try { in.close(); } catch (Exception e) {}
		}
	}

	private static String readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
		List<XMLEntry> entries = new ArrayList<XMLEntry>();

		parser.require(XmlPullParser.START_TAG, _NAMESPACE, DATA_NAME );
		int eventType = parser.getEventType();
		String last_tag = "";
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_DOCUMENT) {
				System.out.println("Start document");
			} 
			else if(eventType == XmlPullParser.START_TAG) {
				last_tag = parser.getName();
				int jj=2342;
				jj++;
			} 
			else if(eventType == XmlPullParser.END_TAG) {
				String ss = parser.getName();
				int jj=2342;
				jj++;
			} 
			else if(eventType == XmlPullParser.TEXT) {
				if (RELEVANT_ENTRY.equals(last_tag)) {
					String text = parser.getText();
					return text;
				}
				int jj=2342;
				jj++;
			}
			eventType = parser.next(); 
			int jj=2342;
			jj++; 
		}
		return null;

		//		while (parser.next() != XmlPullParser.END_TAG) { 
		//			if (parser.getEventType() != XmlPullParser.START_TAG) {
		//				continue;
		//			}
		//			String name = parser.getName();
		//			String ddv1 = parser.getText(); 
		//			String v1 = parser.getAttributeValue(0);
		//			parser.getAttributeValue(0);
		//			// Starts by looking for the entry tag
		//			if (name.equals(RELEVANT_ENTRY)) { 
		//				entries.add(readEntry(parser));
		//			} else {
		//				skip(parser);
		//			}
		//		}  
//		return entries;
		
	}


	private static XmlPullParser create_parser(InputStream in) {	
		XmlPullParser parser = Xml.newPullParser();
		try {
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return parser;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e); 
		}
	}

	private static XMLEntry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, _NAMESPACE, RELEVANT_ENTRY);
		String title = null;
		String summary = null;
		String link = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			int etype = parser.getEventType();
			if (etype != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("title")) {
				title = readTitle(parser);
			} else if (name.equals("summary")) {
				summary = readSummary(parser);
			} else if (name.equals("link")) {
				link = readLink(parser);
			} else {
				skip(parser);
			}
		}
		return new XMLEntry(title, summary, link);
	}

	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}	

	// Processes title tags in the feed.
	private static String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, _NAMESPACE, "title");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, _NAMESPACE, "title");
		return title;
	}

	// Processes link tags in the feed.
	private static String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
		String link = "";
		parser.require(XmlPullParser.START_TAG, _NAMESPACE, "link");
		String tag = parser.getName();
		String relType = parser.getAttributeValue(null, "rel");  
		if (tag.equals("link")) {
			if (relType.equals("alternate")){
				link = parser.getAttributeValue(null, "href");
				parser.nextTag();
			} 
		}
		parser.require(XmlPullParser.END_TAG, _NAMESPACE, "link");
		return link;
	}

	// Processes summary tags in the feed.
	private static String readSummary(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, _NAMESPACE, "summary");
		String summary = readText(parser);
		parser.require(XmlPullParser.END_TAG, _NAMESPACE, "summary");
		return summary;
	}

	// For the tags title and summary, extracts their text values.
	private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

}


