package com.sixt.android.ftp;

import android.os.Environment;
import android.util.Log;

import com.sixt.android.MyApp;
import com.sixt.android.activities.SignatureActivity;
import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.CameraUtils;
import com.sixt.android.app.util.FileUtils;
import com.sixt.android.httpClient.JsonTransmitter;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.sauronsoftware.ftp4j.FTPClient;


public class FtpUploader {

	private static final int FTP_PORT = 21; 
	private static final String FTP_USERNAME = "Pidionote";
	private static final String FTP_PASSWORD = "P<nhsfi.!!";
	private static final long DEFAULT_FTP_TIMEOUT = 30*1000;

	private static FTPClient mFTPClient ; 

	private static final Executor exec = Executors.newSingleThreadExecutor();

	private static final int MAX_IMG_SIZE = 250*1000; 	
	public static final int SIGNATURE_MAX_SIZE = 8000;


	public static String upload_blocking(String file_name) {
		return upload_blocking(file_name, UniqueId.current_transaction_id);
	}

	public static String upload_blocking(String file_name, String unique_id) {
		return upload_blocking(file_name, MAX_IMG_SIZE, unique_id);
	}

	private static String debug_remoteName;
	
	public static String upload_blocking(String file_name, int max_size, String unique_id) {
		File _sdcard  = Environment.getExternalStorageDirectory();
		String sdcard = _sdcard.getAbsolutePath();
		if (!UniqueId.id_exists(unique_id)) {
			throw new RuntimeException();
		}		
		String dir = sdcard + "/" + UniqueId.idToFolderName(unique_id);
		//		new File(dir).mkdirs(); 
		if (!new File(dir).exists()) {
			throw new RuntimeException();
		}		
		File f = new File(dir + "/" + file_name);//
		String res = upload_blocking(f, max_size);		
		if (res != null) { // succeee
			int jj=324;
			jj++;
		}
		else {
			throw new RuntimeException("FTP Error");
		}
		debug_remoteName = res;
		int jj=234;
		jj++;
		return res;
	}


	public static String upload_blocking(final File local_file) {
		return upload_blocking(local_file, MAX_IMG_SIZE); 
	}


	public static String upload_blocking(final File local_file, int max_size) {
		if (local_file==null) {
			int jj=324;
			jj++;
			return null;
		}
		String local = local_file.getName();
		String pref = find_prefix(local);
		String extension = "";
		int i = local.lastIndexOf('.');		
		if (i > 0) {
			extension = "." + local.substring(i+1);
		}
		else {
			return null;
		}
		final String remote_Filename = FileUtils.get_randon_filename(pref, extension);

		//		File resized_local = resize_photo(local_file, remote_Filename, max_size);
		File resized_local = CameraUtils.create_resized_photo(local_file, remote_Filename, max_size);

		int jj=234;
		jj++;
		do_upload(resized_local, remote_Filename);
		return remote_Filename;
	}


	private static String find_prefix(String local) {
		int i = local.indexOf('_');		
		if (i > 0 && i < 10) {
			String pref = local.substring(0, i);
			return pref;
		}
		else {
			return "unknown";
		}
	}


	private static void do_upload(File local_file, final String remote_Filename) {
		if (local_file==null || !local_file.exists()) {
			throw new RuntimeException("File not found!"); 
		}		

		File local_dir = local_file.getParentFile();
		File new_file = new File(local_dir, remote_Filename);

		local_file.renameTo(new_file);

		local_file = new_file; 
		System.setProperty ("ftp4j.activeDataTransfer.acceptTimeout", ""+DEFAULT_FTP_TIMEOUT); 
		FTPClient client = ftp_connect();	
		
		try {
			ftp_login(client);
			ftp_upload(client, local_file);
		}
		finally {	    			
			try { client.disconnect(true); } catch (Exception e) {}
		}
	}


	private static FTPClient ftp_connect() {
		FTPClient client = new FTPClient();
		try {
			client.connect(JsonTransmitter.FTP_SERVER_URL, FTP_PORT);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} 
		return client;
	}

	private static void ftp_login(FTPClient client) {
		try {
			client.login(FTP_USERNAME, FTP_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} 
	}

	private static void ftp_upload(FTPClient client, File local_file) {
		try {				
			client.upload(local_file);
			int jj=2342;
			jj++;
		} catch (Exception e) {
			e.printStackTrace();
			SignatureActivity.imageCounterB--;
			logBuilder("failed to upload: " + local_file.getPath() + " " + e.getMessage());
			MyApp.appSetting.showImageLoadingProgressBar = false;
			throw new RuntimeException(e);
		}finally {
			logBuilder(local_file.getName() + " ⇝ UPLOADED! (Renamed & Resized)\n");
		}
	}

	public static void logBuilder(String log) {
		Log.i("Image_log", "" + log);
		MyApp.appSetting.appendImageEvent(log);
	}

	public static File download_blocking(String remote_filename, String locaFilePath) {
		delete_old_local_copy(locaFilePath);		
		FTPClient client = ftp_connect();
		File locaFile = new File(locaFilePath);
		try {
			ftp_login(client);
			try {
				client.download(remote_filename, locaFile);
				if (locaFile.exists() && locaFile.length() > 10) {
					// all is ok
					return locaFile;
				}
				int jj=243;
				jj++;
				return null;
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			} 
		}
		finally {	    			
			try { ftp_delete_remote(client, remote_filename);  } catch (Exception e) {}
			try { client.disconnect(true); } catch (Exception e) {}
		}
	}


	private static void ftp_delete_remote(FTPClient client, String remote_filename) {
		try {
			client.deleteFile(remote_filename);
		} 
		catch (Exception e) {
			int jj=234;
			jj++;
			e.printStackTrace();
		} 	
	}

	private static void delete_old_local_copy(String locaFilePath) {
		try { 
			new File(locaFilePath).delete();
		}
		catch (Exception e) {
			int jj=234;
			jj++;
		}
	}



}
