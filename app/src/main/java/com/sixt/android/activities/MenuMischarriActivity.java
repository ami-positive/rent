package com.sixt.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.CarSortUtils;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.mischarri_mesirra)
@NoTitle
public class MenuMischarriActivity extends Activity { //abc

	private static MenuMischarriActivity inst;

	public static String caption_elem = "";
		 	
	@ViewById
	Button btn_mesirra;
	@ViewById
	Button btn_hachzarra;
	@ViewById
    TextView txt_hello_username;
	@ViewById
	TextView txt_branch;

	
	
	@ViewById
	TextView caption;


	public static int mode = -1;
	
	public static boolean Mesirra() { return mode==0; }
	public static boolean Hachzarra() { return mode==1; }
	
	public static void reset() { mode = -1; }
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inst = this;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		reset();
		BreadCrumbs.set("מסחרי");
		MainMenuActivity.set_username_branch(txt_hello_username, txt_branch);
//		MainMenuActivity.caption = MainMenuActivity.caption_elem; 
//		caption.setText(MainMenuActivity.caption);
	}
	

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;
	}

	@Click
	void btn_mesirra() {
		mode = 0;
		ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
		BreadCrumbs.append("מסירה");
//		MainMenuActivity.rechev_kavua = true;
//		caption_elem = " קבוע";
//		MainMenuActivity.caption += caption_elem;
//		CarSortUtils.set("C"); 
//		LakoachSapakSelectorActivity_.intent(this).start();
		CarNumberMischarriMesirraActivity_.intent(this).start();
	}
	
	
	@Click
	void btn_hachzarra() {
		ActionValue.set(ActionValue.Values.Hachzarra);
		BreadCrumbs.append("החזרה");
		mode = 1;
		CarNumberMischarriHachzarraActivity_.intent(this).start();
	}
	

	@Click
	void btn_mode_chalufi() {
		MainMenuActivity.rechev_kavua = false;
//		MainMenuActivity.caption = "חלופי";
		caption_elem = " חלופי";
		MainMenuActivity.caption += caption_elem; 
		CarSortUtils.set("R"); 
		LakoachSapakSelectorActivity_.intent(this).start();
	}


	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}

	}

}
