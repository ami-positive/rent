package com.sixt.android.activities.customerYard.network;



import com.sixt.android.activities.customerYard.network.requests.CreditGuardQueryRequest;
import com.sixt.android.activities.customerYard.network.requests.GetCompaniesRequest;
import com.sixt.android.activities.customerYard.network.requests.GetContactDocRequest;
import com.sixt.android.activities.customerYard.network.requests.GetContractDataRequest;
import com.sixt.android.activities.customerYard.network.requests.GetCreditGuardLink;
import com.sixt.android.activities.customerYard.network.requests.SendContractDocRequest;
import com.sixt.android.activities.customerYard.network.requests.SendCreditCardRequest;
import com.sixt.android.activities.customerYard.network.requests.SendDriverDetailsRequest;
import com.sixt.android.activities.customerYard.network.requests.SendExtrasRequest;
import com.sixt.android.activities.customerYard.network.requests.SendPhotoRequest;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.network.response.ContractDocResponse;
import com.sixt.android.activities.customerYard.network.response.CreditGuardQueryResponse;
import com.sixt.android.activities.customerYard.network.response.GetCompaniesResponse;
import com.sixt.android.activities.customerYard.network.response.GetCreditGuardLinkResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by natiapplications on 16/08/15.
 */
public interface ApiInterface {

    //REQUESTS

    public static final String GET_COMPANIES = "/GetCompanies";
    public static final String GET_CONTRACT_DETAILS = "/GetContractDetails";
    public static final String SEND_CREDIT_CARD = "/SendJ5";
    public static final String SEND_EXTRAS = "/SendExtras";
    public static final String SEND_DRIVER_DETAILS = "/SendDriver";
    public static final String SEND_PHOTO = "/SendPhoto";
    public static final String GET_CONTRACT_DOC = "/GetContractDoc";
    public static final String GET_CREDIT_GUARD_LINK = "/GetCreditGuardUrl";
    public static final String GET_CREDIT_GUARD_QUERY = "/CreditGuardQuery";
    public static final String SEND_CONTRACT_DOC = "/SendContractDoc";


    @POST(GET_COMPANIES)
    public void getCompanies(@Body GetCompaniesRequest.GetCompanies_Req getCompanies_req,
                             Callback<GetCompaniesResponse> retroCallback);

    @POST(GET_CONTRACT_DETAILS)
    public void getContractDetails(@Body GetContractDataRequest.ContractDetailsRequestInner contractDetailsRequestInner,
                             Callback<ContractDetailsResponse> retroCallback);

    @POST(SEND_CREDIT_CARD)
    public void sendCreditCard(@Body SendCreditCardRequest.CreditCardRequestInner creditCardRequestInner,
                                   Callback<BaseResponseObject> retroCallback);


    @POST(SEND_EXTRAS)
    public void sendExtras(@Body SendExtrasRequest.ExtrasRequestInner extrasRequestInner,
                               Callback<BaseResponseObject> retroCallback);

    @POST(SEND_DRIVER_DETAILS)
    public void sendDriverDetails(@Body SendDriverDetailsRequest.DriverDetailsRequestInner driverDetailsRequestInner,
                           Callback<BaseResponseObject> retroCallback);

    @POST(SEND_PHOTO)
    public void sendPhoto(@Body SendPhotoRequest.PhotoReqeustInner photoReqeustInner,
                                  Callback<BaseResponseObject> retroCallback);


    @POST(GET_CONTRACT_DOC)
    public void getContractDoc(@Body GetContactDocRequest.ContractDocRequestInner contractDocRequestInner,
                          Callback<ContractDocResponse> retroCallback);



    @POST(SEND_CONTRACT_DOC)
    public void sendContractDoc(@Body SendContractDocRequest.SendContractDocRequestInner sendContractDocRequestInner,
                               Callback<BaseResponseObject> retroCallback);


    @POST(GET_CREDIT_GUARD_QUERY)
    public void getCreditGuardQuery(@Body CreditGuardQueryRequest.CreditGuardQueryRequestInner CreditGuardQueryRequestInner,
                                    Callback<CreditGuardQueryResponse> retroCallback);

    @POST(GET_CREDIT_GUARD_LINK)
    public void getCreditGuardLink(@Body GetCreditGuardLink.CreditGuardLinkReqeustInner creditGuardLinkReqeustInner,
                          Callback<GetCreditGuardLinkResponse> retroCallback);
}
