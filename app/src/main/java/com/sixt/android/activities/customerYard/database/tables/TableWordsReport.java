package com.sixt.android.activities.customerYard.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;


import com.sixt.android.activities.customerYard.database.BaseContentProvider;
import com.sixt.android.activities.customerYard.database.ContentProviderDataSourceAdapter;
import com.sixt.android.activities.customerYard.database.SelectionBuilder;
import com.sixt.android.activities.customerYard.database.providers.WordsReportContetnProvieder;
import com.sixt.android.activities.customerYard.models.WordReport;

import java.util.ArrayList;


/**
 * Created by natiapplications on 21/08/15.
 */
public class TableWordsReport extends ContentProviderDataSourceAdapter<WordReport> {

    // table name
    public static final String TABLE_NAME = "words_reports";


    // columns names
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ORIGINAL_WORD = "COLUMN_ORIGINAL_WORD";
    public static final String COLUMN_CORRECT_WORD = "COLUMN_CORRECT_WORD";
    public static final String COLUMN_REPORTED_AT = "COLUMN_REPORTED_AT";
    public static final String COLUMN_TYPE = "COLUMN_LEVEL";


    public static final String[] ALL_COLUMNS = {
            COLUMN_ID, COLUMN_TYPE,COLUMN_ORIGINAL_WORD,COLUMN_CORRECT_WORD
            ,COLUMN_REPORTED_AT
    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TYPE + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + COLUMN_REPORTED_AT + " integer DEFAULT " + DEFAULT_INT_VALUE + ", "
            + COLUMN_CORRECT_WORD + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + COLUMN_ORIGINAL_WORD + " text not null DEFAULT " + DEFAULT_TEXT_VALUE
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }




    public static TableWordsReport instance;



    public static TableWordsReport getInstance (Context context){
        if (instance == null){
            instance = new TableWordsReport(context);
        }
        return instance;
    }


    private TableWordsReport(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return WordsReportContetnProvieder.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }



    @Override
    public WordReport cursorToEntity(Cursor cursor) {

        WordReport item = new WordReport();

        item.setDBID(cursor.getLong(cursor.getColumnIndexOrThrow(TableWordsReport.COLUMN_ID)));
        item.setOriginalWord(cursor.getString(cursor.getColumnIndexOrThrow(TableWordsReport.COLUMN_ORIGINAL_WORD)));
        item.setCorrectWord(cursor.getString(cursor.getColumnIndexOrThrow(TableWordsReport.COLUMN_CORRECT_WORD)));
        item.setType(cursor.getString(cursor.getColumnIndexOrThrow(TableWordsReport.COLUMN_TYPE)));
        item.setReportedAd(cursor.getLong(cursor.getColumnIndexOrThrow(TableWordsReport.COLUMN_REPORTED_AT)));

        return item;
    }

    @Override
    public ContentValues entityToContentValue(WordReport entity) {
        ContentValues values = new ContentValues();

        values.put(TableWordsReport.COLUMN_TYPE, entity.getDBID());
        values.put(TableWordsReport.COLUMN_CORRECT_WORD, entity.getCorrectWord());
        values.put(TableWordsReport.COLUMN_ORIGINAL_WORD, entity.getOriginalWord());
        values.put(TableWordsReport.COLUMN_REPORTED_AT, entity.getReportedAd());


        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new WordsReportContetnProvieder();
    }


    @Override
    public synchronized WordReport insert(WordReport entity) {
        return super.insert(entity);

    }

    public WordReport isEntryAllradyExsit (WordReport toCheck){

        try {
            WordReport msg = readEntityByID(toCheck.getDBID());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public WordReport insertOrUpdate (WordReport toAdd) {
        WordReport check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getDBID(), toAdd);
        }else{
            return insert(toAdd);
        }
    }


    public ArrayList<WordReport> getByOriginalWord (String originalWord){
        String selection = new SelectionBuilder().where(COLUMN_ORIGINAL_WORD)
                .like(originalWord).build();

        try{
            ArrayList<WordReport> result = read(selection,COLUMN_REPORTED_AT);
            return result;
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error r){
            r.printStackTrace();
        }

        return new ArrayList<WordReport>();


    }






}