package com.sixt.android.activities.customerYard.pdf;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

/**
 * Created by natiapplications on 24/12/15.
 */
public class FileConverterTask extends AsyncTask<String, Boolean, Boolean> {


    private int[] flags = {Base64.URL_SAFE,Base64.DEFAULT,Base64.CRLF,Base64.NO_CLOSE,Base64.NO_PADDING};

    private String filePath;
    private String errDesc;
    private FileConverterCallback callback;

    private int flagIndex;

    public FileConverterTask(FileConverterCallback callback){
        this.callback = callback;
    }


    @Override
    protected Boolean doInBackground(String... strings) {

        boolean state = false;

        final String fileAsBase64 = strings[0];

        try{
            this.filePath = PDFUtil.writePDFAsBase64ToFile(fileAsBase64,flags[flagIndex]);
            state = true;
        }catch (Exception e){
            if (flagIndex >= flags.length-1){
                e.printStackTrace();
                this.errDesc = e.getMessage();
            }else {
                flagIndex++;
                Log.e("ConvertPdfLog", "try again: " + flagIndex);
                return doInBackground(fileAsBase64);
            }
        }

        return state;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (callback != null){
            callback.onConverted(aBoolean.booleanValue(), filePath, errDesc);
        }

    }

    public interface FileConverterCallback {
        public void onConverted(boolean success, String filePath, String errDesc);
    }

}
