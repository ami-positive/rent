package com.sixt.android.activities.customerYard.models;

import android.graphics.Bitmap;
import android.util.Log;


import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.ocr.OCRUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by natiapplications on 17/11/15.
 */
public class LicenseDrive implements Serializable {


    private final String DEFAULT = "Default";


    private String firstNameEngOCR = "";
    private String lastNameEngOCR = "";
    private String firsNameOCR = "";
    private String lastNameOCR = "";
    private String licenseNumberOCR = "";
    private String userIdOCR;
    private String addressOCR;
    private String cityOCR = "";

    private String firstNameEng = "";
    private String lastNameEng = "";
    private String firsName = "";
    private String lastName = "";
    private String licenseNumber = "";
    private String city = "";
    private String userID;
    private String address;
    private String licenseType;
    private String LicenseEndDate;
    private String licenseStartDate;
    private String driverBirthDate;

    //private Bitmap userImage;
    //private Bitmap cardImage;


    private String ocrString;
    public String cleanLines = "";
    private String ocrStringEng = "";
    public String cleanLinesEng = "";

    private boolean isUserIdCorrected;
    private boolean isAddressCorrected;
    private boolean isFirstNameCorrected;
    private boolean isLastNameCorrected;
    private boolean isLicenseNumberCorrected;
    private boolean isFirstNameEngCorrected;
    private boolean isLastNameEngCorrected;
    private boolean isCityCorrected;


    public LicenseDrive() {

    }

    public LicenseDrive(String ocrString) {
        buildByOcrStringHeb(ocrString);
    }

    private void buildByOcrStringHeb(String ocrString) {


        this.ocrString = ocrString;


        String lines[] = ocrString.split("\\r?\\n");
        ArrayList<String> textLines = new ArrayList<String>();

        for (int i = 0; i < lines.length; i++) {
            String tempLine = OCRUtils.cleanText(lines[i]);
            if (!tempLine.isEmpty()) {
                textLines.add(tempLine);
                cleanLines += tempLine;
                cleanLines += "\n";
            }
        }


        for (int i = 0; i < textLines.size(); i++) {

            String tempLine = textLines.get(i);

            Log.e("CleanLinesLog", "temp line at " + i + " : " + tempLine);

            //last name
            if (i == 0) {
                String tempLastName = OCRUtils.cleanNumber(tempLine);
                String r = tempLastName.replaceAll(" ", "");

                ErrorCorrection errorCorrection = new ErrorCorrection(r);

                this.lastNameOCR = errorCorrection.findCorrection();

                if (errorCorrection.isCorrected()) {
                    this.setIsLastNameCorrected(true);
                }
            }

            //first name
            if (i == 2) {
                if (!tempLine.isEmpty()) {
                    String r = OCRUtils.cleanNumber(tempLine);

                    ErrorCorrection errorCorrection = new ErrorCorrection(r);

                    this.firsNameOCR = errorCorrection.findCorrection();

                    if (errorCorrection.isCorrected()) {
                        this.setIsFirstNameCorrected(true);
                    }
                }
            }


            if (i == 4) {
                if (!tempLine.isEmpty()) {
                    String[] birthDateAndLicenseEndDate = OCRUtils.getBirthDateAndLicenseStartDate(tempLine.replaceAll(" ", ""));
                    if (birthDateAndLicenseEndDate != null) {
                        this.driverBirthDate = birthDateAndLicenseEndDate[1];
                        this.licenseStartDate = birthDateAndLicenseEndDate[0];

                        Log.e("ADVANCED_OCR_LOG", "birthDate: " + driverBirthDate + " startDate: " + licenseStartDate);
                    }
                }
            }

            //license number

            if (i == 5) {
                if (!tempLine.isEmpty()) {
                    String first7digits = OCRUtils.getFistSevenNumbersFromString(tempLine, true);
                    this.licenseNumberOCR = first7digits;

                    String licenseEnd = OCRUtils.getLicenseEndDate(tempLine);
                    this.LicenseEndDate = licenseEnd;

                    Log.e("ADVANCED_OCR_LOG", "EndDate: " + LicenseEndDate);
                }
            }


            //user id
            if (i == 6) {
                if (!tempLine.isEmpty()) {
                    String first9digits = OCRUtils.getFistNineNumbersFromString(tempLine, true);
                    this.userIdOCR = first9digits;
                }
            }

            //address
            if (i == 7) {
                if (!tempLine.isEmpty()) {

                    String[] addressAndCity = OCRUtils.findAddressAndCity(tempLine);


                    ErrorCorrection addressErrorCorrection = new ErrorCorrection(addressAndCity[0]);
                    String correctedAddress = addressErrorCorrection.findCorrection();
                    if (addressErrorCorrection.isCorrected()) {
                        this.setIsAddressCorrected(true);
                    }
                    this.addressOCR = correctedAddress;


                    ErrorCorrection cityErrorCorrection = new ErrorCorrection(addressAndCity[1]);
                    String correctedCity = cityErrorCorrection.findCorrection();
                    if (cityErrorCorrection.isCorrected()) {
                        this.setIsCityCorrected(true);
                    }

                    this.cityOCR = correctedCity;


                    Log.e("ADVANCED_OCR_LOG", "adress: " + this.addressOCR + " city: " + this.cityOCR);
                }
            }


        }
    }


    public void buildByOcrStringEng(String ocr) {


        this.ocrStringEng = ocr;

        String lines[] = ocrStringEng.split("\\r?\\n");
        ArrayList<String> textLines = new ArrayList<String>();

        for (int i = 0; i < lines.length; i++) {
            String tempLine = OCRUtils.cleanTextEng(lines[i]);
            if (!tempLine.isEmpty()) {
                textLines.add(tempLine);
                cleanLinesEng += tempLine;
                cleanLinesEng += "\n";
            }
        }


        for (int i = 0; i < textLines.size(); i++) {

            String tempLine = textLines.get(i);

            Log.e("CleanLinesLog", "temp line at " + i + " : " + tempLine);

            //last name
            if (i == 1) {
                String tempLastName = OCRUtils.cleanNumberEng(tempLine);
                String r = tempLastName.replaceAll(" ", "");

                ErrorCorrection errorCorrection = new ErrorCorrection(r);

                this.lastNameEngOCR = errorCorrection.findCorrection();

                if (errorCorrection.isCorrected()) {
                    this.setIsLastNameEngCorrected(true);
                }
            }

            //first name
            if (i == 3) {
                if (!tempLine.isEmpty()) {
                    String r = OCRUtils.cleanNumberEng(tempLine);

                    ErrorCorrection errorCorrection = new ErrorCorrection(r);

                    this.firstNameEngOCR = errorCorrection.findCorrection();

                    if (errorCorrection.isCorrected()) {
                        this.setIsFirstNameEngCorrected(true);
                    }
                }
            }

            if (i == 4) {
                if (!tempLine.isEmpty()) {

                    if (this.driverBirthDate == null || this.LicenseEndDate == null) {
                        String[] birthDateAndLicenseEndDate = OCRUtils.getBirthDateAndLicenseStartDate(tempLine.replaceAll(" ", ""));
                        if (birthDateAndLicenseEndDate != null) {
                            if (driverBirthDate == null) {
                                this.driverBirthDate = birthDateAndLicenseEndDate[1];
                            }

                            if (licenseStartDate == null) {
                                this.licenseStartDate = birthDateAndLicenseEndDate[0];
                            }

                            Log.e("ADVANCED_OCR_LOG", "birthDate: " + driverBirthDate + " EndDate: " + licenseStartDate);
                        }
                    }

                }
            }


            //license number

            if (i == 5) {
                if (this.licenseNumberOCR.length() < 7) {
                    if (!tempLine.isEmpty()) {
                        String first7digits = OCRUtils.getFistSevenNumbersFromString(tempLine, true);
                        if (first7digits.length() > this.licenseNumberOCR.length()) {
                            Log.e("bestOcrLog", "LicenseNumber - heb: " + this.licenseNumberOCR + " eng: " + first7digits);
                            this.licenseNumberOCR = first7digits;

                        }

                        if (this.LicenseEndDate == null) {
                            String licenseEnd = OCRUtils.getLicenseEndDate(tempLine);
                            this.LicenseEndDate = licenseEnd;

                            Log.e("ADVANCED_OCR_LOG", "EndDate: " + LicenseEndDate);
                        }
                    }
                }

            }


            //user id
            if (i == 6) {
                Log.e("bestOcrLog", "userIdOCR: " + this.userIdOCR);
                if (this.userIdOCR != null) {
                    if (this.userIdOCR.length() < 9) {
                        if (!tempLine.isEmpty()) {
                            String first9digits = OCRUtils.getFistNineNumbersFromString(tempLine, true);
                            if (first9digits.length() > this.userIdOCR.length()) {
                                Log.e("bestOcrLog", "ID - heb: " + this.userIdOCR + " eng: " + first9digits);
                                this.userIdOCR = first9digits;
                            }

                        }
                    }
                }
            }

        }

    }

    private void createUserImage(Bitmap cardImage) {
        /*if (cardImage != null){
            try{
                this.userImage = Bitmap.createBitmap(cardImage,cardImage.getWidth()/15,cardImage.getHeight()/5,cardImage.getWidth()/5,
                        cardImage.getHeight()/2);
            }catch (Exception e){e.printStackTrace();}catch (Error r){r.printStackTrace();}
        }*/
    }


    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getFirsNameOCR() {
        return firsNameOCR;
    }

    public void setFirsNameOCR(String firsNameOCR) {
        this.firsNameOCR = firsNameOCR;
    }

    public String getLicenseNumberOCR() {
        return licenseNumberOCR;
    }

    public void setLicenseNumberOCR(String licenseNumberOCR) {
        this.licenseNumberOCR = licenseNumberOCR;
    }

    public String getLastNameOCR() {
        return lastNameOCR;
    }

    public void setLastNameOCR(String lastNameOCR) {
        this.lastNameOCR = lastNameOCR;
    }

    public Bitmap getCardImage() {
        //return cardImage;
        return null;
    }

    public void setCardImage(Bitmap cardImage) {
        // this.cardImage = cardImage;
        // createUserImage(cardImage);
    }

    public String getOcrString() {
        return ocrString;
    }

    public void setOcrString(String ocrString) {
        this.ocrString = ocrString;
    }

    public Bitmap getUserImage() {
        // return userImage;
        return null;
    }

    public void setUserImage(Bitmap userImage) {
        // this.userImage = userImage;
    }


    public boolean isFirstNameCorrected() {
        return isFirstNameCorrected;
    }

    public void setIsFirstNameCorrected(boolean isFirstNameCorrected) {
        this.isFirstNameCorrected = isFirstNameCorrected;
    }

    public boolean isLastNameCorrected() {
        return isLastNameCorrected;
    }

    public void setIsLastNameCorrected(boolean isLastNameCorrected) {
        this.isLastNameCorrected = isLastNameCorrected;
    }

    public boolean isLicenseNumberCorrected() {
        return isLicenseNumberCorrected;
    }

    public void setIsLicenseNumberCorrected(boolean isLicenseNumberCorrected) {
        this.isLicenseNumberCorrected = isLicenseNumberCorrected;
    }

    public boolean isAddressCorrected() {
        return isAddressCorrected;
    }

    public void setIsAddressCorrected(boolean isAddressCorrected) {
        this.isAddressCorrected = isAddressCorrected;
    }

    public boolean isUserIdCorrected() {
        return isUserIdCorrected;
    }

    public void setIsUserIdCorrected(boolean isUserIdCorrected) {
        this.isUserIdCorrected = isUserIdCorrected;
    }

    public String getCleanLines() {
        return cleanLines;
    }

    public void setCleanLines(String cleanLines) {
        this.cleanLines = cleanLines;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAddressOCR() {
        return addressOCR;
    }

    public void setAddressOCR(String addressOCR) {
        this.addressOCR = addressOCR;
    }

    public String getUserIdOCR() {
        return userIdOCR;
    }

    public void setUserIdOCR(String userIdOCR) {
        this.userIdOCR = userIdOCR;
    }

    public String getFirstNameEngOCR() {
        return firstNameEngOCR.toUpperCase();
    }

    public void setFirstNameEngOCR(String firstNameEngOCR) {
        this.firstNameEngOCR = firstNameEngOCR;
    }

    public String getLastNameEngOCR() {
        return lastNameEngOCR.toUpperCase();
    }

    public void setLastNameEngOCR(String lastNameEngOCR) {
        this.lastNameEngOCR = lastNameEngOCR;
    }

    public String getFirstNameEng() {
        return firstNameEng.toUpperCase();
    }

    public void setFirstNameEng(String firstNameEng) {
        this.firstNameEng = firstNameEng;
    }

    public String getLastNameEng() {
        return lastNameEng.toUpperCase();
    }

    public void setLastNameEng(String lastNameEng) {
        this.lastNameEng = lastNameEng;
    }

    public boolean isFirstNameEngCorrected() {
        return isFirstNameEngCorrected;
    }

    public void setIsFirstNameEngCorrected(boolean isFirstNameEngCorrected) {
        this.isFirstNameEngCorrected = isFirstNameEngCorrected;
    }

    public boolean isLastNameEngCorrected() {
        return isLastNameEngCorrected;
    }

    public void setIsLastNameEngCorrected(boolean isLastNameEngCorrected) {
        this.isLastNameEngCorrected = isLastNameEngCorrected;
    }

    public String getOcrStringEng() {
        return ocrStringEng;
    }

    public void setOcrStringEng(String ocrStringEng) {
        this.ocrStringEng = ocrStringEng;
    }

    public String getCleanLinesEng() {
        return cleanLinesEng;
    }

    public void setCleanLinesEng(String cleanLinesEng) {
        this.cleanLinesEng = cleanLinesEng;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {

        if (licenseType != null && !licenseType.isEmpty()) {
            if (licenseType.length() == 1 || licenseType.length() == 2) {
                this.licenseType = licenseType;
            } else {
                if (licenseType.contains(".")) {
                    int pointIndex = licenseType.indexOf(".");
                    for (int i = pointIndex; i < licenseType.length(); i++) {
                        char temp = licenseType.charAt(i);
                        if (temp >= 'a' && temp <= 'z') {
                            this.licenseType = temp + "";
                            break;
                        } else if (temp >= 'A' && temp <= 'Z') {
                            this.licenseType = temp + "";
                        }
                    }
                } else {
                    licenseType = licenseType.substring(licenseType.length() - 1);
                }
            }
        }

    }

    public String getLicenseEndDate() {
        return LicenseEndDate;
    }

    public void setLicenseEndDate(String licenseEndDate) {
        LicenseEndDate = licenseEndDate;
    }

    public String getLicenseStartDate() {
        return licenseStartDate;
    }

    public void setLicenseStartDate(String licenseStartDate) {
        this.licenseStartDate = licenseStartDate;
    }

    public String getDriverBirthDate() {
        return driverBirthDate;
    }

    public void setDriverBirthDate(String driverBirthDate) {
        this.driverBirthDate = driverBirthDate;
    }

    public String getCityOCR() {
        return cityOCR;
    }

    public void setCityOCR(String cityOCR) {
        this.cityOCR = cityOCR;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public boolean isCityCorrected() {
        return isCityCorrected;
    }

    public void setIsCityCorrected(boolean isCityCorrected) {
        this.isCityCorrected = isCityCorrected;
    }

    public void addWordReport() {
        addWordReport(getFirsName(), getFirsNameOCR());
        addWordReport(getLastName(), getLastNameOCR());
        addWordReport(getFirstNameEng(), getFirstNameEngOCR());
        addWordReport(getLastNameEng(), getLastNameEngOCR());
        addWordReport(getAddress(), getAddressOCR());
        addWordReport(getCity(), getCityOCR());
    }

    private void addWordReport(String realWord, String ocrWord) {
        WordReport wordReport = new WordReport();
        wordReport.setOriginalWord(ocrWord);
        wordReport.setCorrectWord(realWord);
        wordReport.setReportedAd(System.currentTimeMillis());
        wordReport.setType("LicenseDrive");

        MyApp.wordsReportManager.insert(wordReport);
    }

    public boolean isValidResult() {

        int emptyCount = 0;
        if (!isValidFiled(firsNameOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(lastNameOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(firstNameEngOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(lastNameEngOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(licenseNumberOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(userIdOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(addressOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(cityOCR)) {
            emptyCount++;
        }
        if (!isValidFiled(driverBirthDate)) {
            emptyCount++;
        }
        if (!isValidFiled(licenseStartDate)) {
            emptyCount++;
        }
        if (!isValidFiled(LicenseEndDate)) {
            emptyCount++;
        }
        if (!isValidFiled(licenseType)) {
            emptyCount++;
        }

        return emptyCount < 3;
    }

    private boolean isValidFiled(String filed) {
        return filed != null && !filed.isEmpty();
    }
}
