package com.sixt.android.activities.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.utils.ProgressDialogUtil;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.dialog.DialogError;
import com.sixt.android.app.util.LastUserAction;
import com.sixt.android.ui.DialogUtils;

public class BaseActivity extends FragmentActivity {//

	protected ProgressDialog progress_dialog;
	protected ProgressDialog progressDialog;

	protected void open_progress_dialog(final String txt) { 
		close_progress_dialog();
		progress_dialog = ProgressDialog.show(this, DialogUtils.CAPTION, txt, true, true, 
				new DialogInterface.OnCancelListener() {					
			@Override
			public void onCancel(DialogInterface dialog) {
				open_progress_dialog(txt); // disable cancellation
				// no op
			}
		});

	}

	
	public void toast(String text, boolean success) {
		if (success) {
			MyToast.makeText_success(this, text, Toast.LENGTH_LONG).show(); 
		}
		else {
			toast(text);
		}
	}

	
	public void toast(String text) { // 'error' type of toast
//		MyToast.makeText(this, text, Toast.LENGTH_LONG).show();
		DialogError.show(this, text); //ggg show 'error' type toasts in popup
		MyApp.appSetting.setCameraError("");
	}
	
	

	@Override
	protected void onResume() {
		super.onResume();
		LastUserAction.update(this); 
	}


	@Override
	protected void onPause() {
		super.onPause();
		close_progress_dialog();
	}



	protected void close_progress_dialog() {
		if (progress_dialog != null && progress_dialog.isShowing()) {
			progress_dialog.dismiss();
			progress_dialog = null;
		}
	}


	public void showProgressDialog(String message) {
		this.progressDialog =
				ProgressDialogUtil.showProgressDialog(this, message);
	}

	public void showProgressDialog() {
		try {
			this.progressDialog =
					ProgressDialogUtil.showProgressDialog(this,"אנא המתן...");

		} catch (Exception e) {}
	}

	public void dismissProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}
}
