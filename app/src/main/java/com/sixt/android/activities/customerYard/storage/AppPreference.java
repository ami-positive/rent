/**
 * 
 */
package com.sixt.android.activities.customerYard.storage;


import android.content.Context;
import android.content.SharedPreferences;

import com.sixt.android.MyApp;


/**
 * @author Nati Gabay
 *
 */
public class AppPreference {

	
	public static final int UNITS_TYPE_KM = 1;
	public static final int UNITS_TYPE_MILES = 2;
	public static final int SKI_PASS_TYPE_AREA = 1;
	public static final int SKI_PASS_TYPE_RESORT = 2;
	
	
	private static AppPreference instance;
	private static Context context = MyApp.appContext;
	
	private static PreferenceUserProfil userProfil;
	private static PreferenceUserSettings userSettings;
	private static PreferenceGeneralSettings generalSettings;

	private AppPreference (Context context){
		userSettings = PreferenceUserSettings.getUserSettingsInstance();
		userProfil = PreferenceUserProfil.getUserProfileInstance();
		generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
	}
	
	public static AppPreference getInstans(Context context){
		if(instance == null){
			instance = new AppPreference(context);
		}
		setInstancContext(context);
		return instance;
	}
	
	public static void removeInstance(){
		userSettings.removeInstance();
		userProfil.removeInstance();
		generalSettings.removeInstance();
		instance = null;
	}
	
	private static void setInstancContext (Context context){
		AppPreference.context = context;
	}
	
	
	
	/**
	 * @return the userProfil
	 */
	public PreferenceUserProfil getUserProfil() {
		return userProfil;
	}

	/**
	 * @param userProfil the userProfil to set
	 */
	public void setUserProfil(PreferenceUserProfil userProfil) {
		this.userProfil = userProfil;
	}

	/**
	 * @return the userSettings
	 */
	public PreferenceUserSettings getUserSettings() {
		return userSettings;
	}

	/**
	 * @param userSettings the userSettings to set
	 */
	public void setUserSettings(PreferenceUserSettings userSettings) {
		this.userSettings = userSettings;
	}
	
	/**
	 * @return the generalSettings
	 */
	public PreferenceGeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	/**
	 * @param generalSettings the generalSettings to set
	 */
	public void setGeneralSettings(PreferenceUserSettings generalSettings) {
		this.userSettings = generalSettings;
	}


	




	public static class PreferenceGeneralSettings {

		public final String GENERAL_SETTINGS = "GeneralSettings";

		public static final String LAT = "Lat";
		public static final String LON = "Lon";
		public static final String SCREEN_WIDTH = "ScreenWidth";
		public static final String SCREEN_HIGHT = "ScreenHight";
		public static final String IN_CUSTOMER_YARD_MODE = "IN_CUSTOMER_YARD_MODE";


		private SharedPreferences generalSettings;
		public static PreferenceGeneralSettings generalSettingInstance;

		private PreferenceGeneralSettings() {
			generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
					Context.MODE_PRIVATE);
		}

		public static PreferenceGeneralSettings getGeneralSettingsInstance() {
			if (generalSettingInstance == null) {
				generalSettingInstance = new PreferenceGeneralSettings();
			}
			return generalSettingInstance;
		}
		
		public void removeInstance(){
			generalSettings.edit().clear().commit();
			generalSettingInstance = null;
		}

		
		public void setLat(String toSet) {
			generalSettings.edit().putString(LAT, toSet).commit();
		}

		public void setLon(String toSet) {
			generalSettings.edit().putString(LON, toSet).commit();
		}
		

		
		public void setScreenWidth(int width) {
			generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
		}
		
		public void setScreenHight(int hight) {
			generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
		}


		
		public String getLat() {
			return generalSettings.getString(LAT, "0.0");
		}

		public String getLon() {
			return generalSettings.getString(LON, "0.0");
		}
		
		
		public String getLocation () {
			return getLat()+","+getLon();
		}
		

		
		public int getScreenWidth () {
			return generalSettings.getInt(SCREEN_WIDTH, 0);
		}
		
		public int getScreenHight () {
			return generalSettings.getInt(SCREEN_HIGHT, 0);
		}


		public void setPDASignatureID (int set){
			generalSettings.edit().putInt("PDASignatureID",set).commit();
		}

		public int getPDASignatureID (){
			return generalSettings.getInt("PDASignatureID", 0);
		}

		public void setInCustomerYardMode (boolean set){
			generalSettings.edit().putBoolean(IN_CUSTOMER_YARD_MODE,set).commit();
		}

		public boolean isInCustomerYardMode (){
			return generalSettings.getBoolean(IN_CUSTOMER_YARD_MODE,false);
		}


	}



	public static class PreferenceUserProfil {
		
		public static final String USER_PROFILE = "UserProfile";




		private SharedPreferences userProfile;
		private static PreferenceUserProfil userProfileInstance;


		
		private  PreferenceUserProfil (){
			userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
		}
		
		public  static PreferenceUserProfil getUserProfileInstance (){
			if (userProfileInstance == null){
				userProfileInstance = new PreferenceUserProfil();
			}
			return userProfileInstance;
		}
		
		public  void removeInstance(){
			userProfile.edit().clear().commit();
			userProfileInstance = null;
		}

		public void removeAll() {
			userProfile.edit().clear().commit();
		}


	}







	public static class PreferenceUserSettings {
		
		public  final String USER_SETTINGS = "UserSettings";

		private static final String UNIT_DISTANCE = "UnitDistance";


		private SharedPreferences userSettings;
		public static  PreferenceUserSettings userSettingInstance;
		
		private PreferenceUserSettings() {
			userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
		}
		
		public static PreferenceUserSettings getUserSettingsInstance() {
			if (userSettingInstance == null){
				userSettingInstance = new PreferenceUserSettings();
			}
			return userSettingInstance;
		}
		
		public void removeInstance(){
			userSettings.edit().clear().commit();
			userSettingInstance = null;
		}

		public boolean getDistanceUnit() {
			return userSettings.getBoolean(UNIT_DISTANCE, false);
		}
		public String getDistanceUnitName() {
			if (getDistanceUnit()){
				return "Mile";
			}
			return "Km";
		}
		public double getDistanceUnitFactor() {
			if (getDistanceUnit()){
				return 1609.34;
				
			}
			return 1000;
		}

		

		public void removeAllSettings (){
			userSettings.edit().clear().commit();
		}
	}
	
	
	
	

	
	
	
	
	

}
