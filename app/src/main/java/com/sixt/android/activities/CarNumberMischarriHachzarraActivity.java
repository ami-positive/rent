package com.sixt.android.activities;


import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.ReturnReasonRecord;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActionValue.Values;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CarNumberUtils;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.NetworkState;
import com.sixt.android.app.util.TEST;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.mischarri_hachzarra_number)
@NoTitle
public class CarNumberMischarriHachzarraActivity extends BaseActivity {
 
    public static volatile SendForm_Request lastForm; // to be used at Mesirrat_Rechev
 
    
	private static final boolean _DEBUG = TEST.debug_mode(false);
	
	static final String DEBUG_NO = "2669873"; //TEST.NUM_MISCHARRI; 
	

	private static CarNumberMischarriHachzarraActivity inst;
	 
//	public static String number;


	public static boolean car_is_private = true;
	
	
	@ViewById
	TextView caption;
	
	@ViewById 
	Spinner return_reason_spinner; //WITH first empty line; NO default 

     
    @ViewById
    EditText car_number; //==login
    
    @ViewById
    Button btn_ok; // send  void btn_ok() 
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	inst = this;
    	lastForm = null;
    	car_is_private = true;
    	GetCarDetails_Response.carDetails_response = null;
//    	number = ""; // clear old value
    }
     
	@AfterViews
	void post_onCreate() {
		CarNumberValue.clear();
		if (DeviceAndroidId.is_lab_device(this)) {
			car_number.setText(DEBUG_NO);
//			number = CarNumberUtils.format_car_number(DEBUG_NO);
		}
		
		ArrayAdapter <String> adapter =
				new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		if (_DEBUG) {
			adapter.add( "תאונה");					 
			adapter.add( "תקלה");
			adapter.add( "איחור");
		}
		else {
			populate_return_reason_adapter(adapter);
		}
		return_reason_spinner.setAdapter(adapter);

	}	

	
    private static void populate_return_reason_adapter(ArrayAdapter<String> adapter) {
    	adapter.add(""); // start with empty line
    	ReturnReasonRecord[] rason_arr = AllDataRepository.getReturnReasons();
    	if (rason_arr==null) {
    		return;
    	}
    	for (ReturnReasonRecord r: rason_arr) {
    		adapter.add(r.ReasonName);
    	}
	}
    

	@Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	inst = null;
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart(); 
//    	caption.setText(MainMenuActivity.caption);
    	caption.setText(BreadCrumbs.get());    	
    }
    

    @Click
    void btn_ok() {
    	GetCarDetails_Response.carDetails_response = null;
//    	number = "";
    	String num = car_number.getText().toString();

		if (num == null || num.length() != 7) {
			toast("מספר הרכב אינו חוקי!");
			return;    			
		}
		if (return_reason_spinner.getSelectedItemPosition() < 1) {
			toast("יש לציין סיבת החזרה");
			return;    						
		}
		num = CarNumberUtils.format_car_number(num);

		String return_reason_str = (String) return_reason_spinner.getSelectedItem();
		int return_reason = AllDataRepository.getReturnReasonFromName(return_reason_str); 
    	CarNumberValue.set_with_return_reason(num, return_reason);

    	
    	if (_DEBUG) {
    		open_progress_dialog("מעביר נתונים לשרת...");
    		new Timer().schedule(new TimerTask() {				
				@Override
				public void run() {
					open_next_activity();
				}
			}, 800);
    	}
    	else {
    		if (!NetworkState.is_connected(this)) {
    			toast("אין חיבור רשת פעיל!");
    			return;
    		}
    		open_progress_dialog("מתחבר לשרת. אנא המתן...");
    		bg_send_car_number(num); 
    	}    	    	
    }

    
	@Background
	void bg_send_car_number(final String car_no) {				
		final String action = ActionValue.get();
//		boolean goto_next_activity = true;
				
		try  {
//			boolean skip_server = false;
			final String MESIRRA = Values.Mesirrat_Rechev.value;

//			if (MESIRRA.equals(action)) { //!!
//				LastFormStruct res = SignatureActivity.get_last_form_for_number(car_no); //use last stored form
//				lastForm = res.last_form;
//				boolean match = res.is_match;
//				boolean match = false;
//				if (!match) {
//					do_toast("הפריט לא תואם את האיסוף. אנא בדוק את התפריט" );
//					// and allow user to continue
//				}

//				if (lastForm != null) {
//					car_is_private = lastForm.is_p_or_pm();
//					skip_server = true;
//				}
//			}

//			if (!skip_server) {
				boolean success = get_car_details_from_server(car_no, action);		
//			}
			
			if (success) {
				open_next_activity(); // move to next activity even if error
			}
		}
		finally {
			close_progress_dialog();	
		}

	}



	private boolean get_car_details_from_server(String car_no, String action) {
		GetCarDetails_Response.clear();
		GetCarDetails_Request req = new GetCarDetails_Request(car_no); 
		String req_str = GGson.toJson(req);
    	try {
			Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
			// if success - keep "privateness" of car
			if (res instanceof GetCarDetails_Response) {
				car_is_private = GetCarDetails_Response.is_p_or_pm();
				return true; // success
			}			
		} 
    	catch (ConnectionError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
			e.printStackTrace();
//			do_toast("ארעה תקלה בחיבור  " );
			do_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
    	catch (ActionFailedError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
			do_toast(e.getReasonDesc());
			boolean stay_in_screen = !e.is_recoverable_error(); // == 99
//			goto_next_activity = !stay_in_screen;  
		} 
    	catch (Exception e) {
    		// do not propagate
    	}    	
    	return false;
	}
	

	@UiThread
	void do_toast(String msg) {
		toast(msg);
	}

	@UiThread
	void open_next_activity() {
//		FuelmesirraHachzarra_.intent(this).start();
		
//		SignatureActivity.show_decline_option = true;
//		SignatureActivity_.intent(this).start();
		
//		HearotLehachzarraActivity_.intent(this).start();
		FuelmesirraHachzarraActivity_.intent(this).start();
	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}

	}
	
}

