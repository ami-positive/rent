package com.sixt.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


import com.sixt.android.app.util.SignerTypeValue;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.car_lakoach_sapak_activity)
@NoTitle
public class LakoachSapakSelectorActivity extends Activity { 

	private static LakoachSapakSelectorActivity inst;
	
	public static final String LAKOACH_SignerType = "C";
	public static final String SAPAK_SignerType = "S";
	
	public static String caption_elem = "";
	
	public static String signer_type = LAKOACH_SignerType;

	@ViewById
	Button btn_mode_lakoach;
	@ViewById
	Button btn_mode_sapak;
	

	@ViewById
	TextView caption;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inst = this;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		caption_elem = "";
		MainMenuActivity.caption = MainMenuActivity.caption_elem + KavuaChalufiActivity.caption_elem; 
		caption.setText(MainMenuActivity.caption);
	}
	

	@Click
	void btn_mode_lakoach() {
		caption_append(true);
		signer_type = LAKOACH_SignerType; 
		SignerTypeValue.set(LAKOACH_SignerType);
		MainMenuActivity.tofrom_lakoach = true;
		CarNumberActivity_.intent(this).start();
	}
 
	@Click
	void btn_mode_sapak() {
		caption_append(false);
		signer_type = SAPAK_SignerType; 
		SignerTypeValue.set(SAPAK_SignerType);
		MainMenuActivity.tofrom_lakoach = false;
		CarNumberActivity_.intent(this).start();
	}
	

	private void caption_append(boolean is_lakoach) {
		String pref = MainMenuActivity.is_isuf ? " מ" : " ל"; 
		caption_elem = is_lakoach ? (pref+"לקוח") : (pref+"ספק");  
		MainMenuActivity.caption += caption_elem;
	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}

}


