package com.sixt.android.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.dialog.DialogOkCancel;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.BranchesRecord;
import com.sixt.android.app.json.objects.DriverRecord;
import com.sixt.android.app.json.objects.GarageRecord;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.request.SendForm_Request_Inner;
import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response.FuelAlert;
import com.sixt.android.app.json.response.GetCarDetails_Response_Inner;
import com.sixt.android.app.json.response.SendForm_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.FuelValues;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.app.util.TEST;
import com.sixt.android.app.util.TxtUtils;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.android.ui.FuelSlide;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import static com.sixt.android.ui.FuelSlide.FUEL_LEVEL_NOTSET;

@EActivity(R.layout.fuel_pticha_layout)
@NoTitle
public class FuelPtichaActivity extends BaseActivity { // car_isuff_mesirra_layout
 
	private static final boolean _DEBUG = TEST.debug_mode(false);

	private static final int DLG_CANCEL = 323;
	private static final int DLG_CLEAR = 442; 
	


	public static FuelPtichaActivity inst;

	@ViewById
	TextView caption;

	//	@ViewById
	//    TextView txt_caption;

	private boolean data_already_sent = false;

	private boolean fuel_was_alerted;
	private boolean fuel_alert_stop_mode;

	@ViewById
	Button btn_send;// bg_perform_send_commands
	@ViewById
	Button btn_clear;
	@ViewById
	Button btn_cancel;


	@ViewById
	TextView license_no;	
	@ViewById
	TextView contract_type;	
	@ViewById
	TextView car_model;
	@ViewById
	EditText mileage; // Mad Mud Utzz
	@ViewById
	FuelSlide fuel_slide;	
	@ViewById
	Spinner driver_name_list; //no first empty line; use default == DriverNo from Auth service + match against Drivers[] AllData
	@ViewById
	Spinner musach_list; //HAS first empty line; NO default 
	@ViewById
	TextView txt_musach_label;
	@ViewById
	Spinner snif_chazarra_list;	 //no first empty line; use default == current branch 
	@ViewById
	EditText txt_comment;

//	private int show_musach = -1; // old: show_musach
	
	private boolean musach_is_mandatory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		data_already_sent = false;
		inst = this; 
		int garage_def = AllDataRepository.getGarageDefault();
//		show_musach = (garage_def == 1) ? View.VISIBLE : View.INVISIBLE;
		musach_is_mandatory = (garage_def == 1);
//		if (_DEBUG) {
//			show_musach = View.INVISIBLE; 
//		}
	}

	@AfterViews
	void post_onCreate() {
		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);

		FuelValues.clear();

		set_caption_txt();

		if (!_DEBUG) { 
//			if (show_musach == View.VISIBLE) {
				show_musach(true);
//			}
//			else {
//				show_musach(false);
//			}
		}
		else {
			show_musach(false);
		}

//		int CiCoIndex = calcCiCoIndex();		
//		if (CiCoIndex==1) {
		if (GetCarDetails_Response.show_next_button()) { 
			//HABBA haba
			btn_send.setText("הבא");
		}
		else {
			//SHLACHH
			btn_send.setText("שלח");
		}




		//		if (CurrentAction.get().equals(CurrentAction.Values.Isuf_Rechev.value)) {
		//			collected_container.setVisibility(View.GONE);
		//			rechev_neesaf_metuchnan_container.setVisibility(View.GONE);
		//		}

		//		BreadCrumbs.append("פרטי חוזה");
		final String cur_bcrums = BreadCrumbs.get() + BreadCrumbs.SEP + "חוזה";
		caption.setText(cur_bcrums);


		if (!_DEBUG) {
			populate_screen();
			populate_all_spinners();
			set_default_snif_chazarra();
			set_default_driver_name();
		}
		else {
			debug_populate_spinners();
		}

		if (DeviceAndroidId.is_lab_device(this)) {
			setText_id_empty(mileage, "53897");
			//			setText_id_empty(car_code,"1");
			//			setText_id_empty(editText_neesaf_bafoall, "3333333"); 
			//			setText_id_empty(num_of_keys, "2");
			//			setText_id_empty(num_of_controllers, "3");
		}
	}

 
	private void show_musach(boolean show) {
		musach_list.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
		txt_musach_label.setVisibility(show ? View.VISIBLE : View.INVISIBLE);   
	}
	

	private void set_caption_txt() {
		//		CaptionSetter.set(txt_caption); 
		//		caption.setText("");
		//		String txt = "";
		//		if (MainMenuActivity.Mischari()) {
		//			if (MischarriMenuActivity.Mesirra()) {
		//				txt = "מסחרי - מסירה";
		//			}
		//			else if (MischarriMenuActivity.Hachzarra()) {
		//				txt = "מסחרי - החזרה";
		//			}
		//			else {
		//				txt = "מסחרי  ";
		//			}
		//		}
		//		else if (MainMenuActivity.Pnimmi()) {
		//			if (PnimmiMenuActivity.pticha()) {
		//				txt = "פנימי - פתיחה";				
		//			}
		//			else if (PnimmiMenuActivity.sgirra()) {
		//				txt = "פנימי - סגירה";
		//			}
		//			else if (PnimmiMenuActivity.mesirra()) {
		//				txt = "פנימי - מסירה";
		//			}
		//			else if (PnimmiMenuActivity.hachzarra()) {
		//				txt = "פנימי - החזרה";
		//			}
		//			else {
		//				txt = "פנימי ";
		//			}
		//		}
		//		else if (MainMenuActivity.ChatzarLakoach()) {
		//			txt = "חצר - ";
		//		}		
		//		caption.setText(txt);		
	}


	private void debug_populate_spinners() {
		if (_DEBUG) {
			ArrayAdapter<String> adapter =
					new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			adapter.add("אבי בן מנשה");					 
			adapter.add("מנשה בן יצחק");
			adapter.add("אילנית בנדורי");
			driver_name_list.setAdapter(adapter);

			adapter = new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			adapter.add("");
			adapter.add("צמרת");					 
			adapter.add("המזרח");
			adapter.add("החורשים");
			musach_list.setAdapter(adapter);

			adapter = new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			adapter.add("רעננה");					 
			adapter.add("עפולה");
			adapter.add("תל אביב");
			snif_chazarra_list.setAdapter(adapter);						
		}
	}

	private void populate_all_spinners() {
		ArrayAdapter <String> dname_adapter =
				new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		dname_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		DriverRecord[] all_drivers = AllDataRepository.getAllDrivers();
		if (all_drivers != null) {
			for (DriverRecord drec: all_drivers) {
				dname_adapter.add(drec.DriverName);
			}
		}		
		driver_name_list.setAdapter(dname_adapter);
		

		ArrayAdapter<String>  m_adapter = new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		m_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_adapter.add("            "); // add empty line!
//		if (show_musach == View.VISIBLE) {
			GarageRecord[] all_garages = AllDataRepository.getAllGarages();
			if (all_garages != null) {
				for (GarageRecord rec: all_garages) {
					m_adapter.add(rec.GarageName);					 				
				}
			}
//		}
		musach_list.setAdapter(m_adapter);
		

		ArrayAdapter<String> sn_adapter = new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		sn_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		BranchesRecord[] all_branches = AllDataRepository.getAllBranches();
		if (all_branches != null) {
			for (BranchesRecord rec: all_branches) {
				sn_adapter.add(rec.Branch_Name);
			}
		}
		snif_chazarra_list.setAdapter(sn_adapter);
	}


	//	private void set_default_snif() {
	//		final String cur_branch_name = BranchNoValue.getName();		
	//		if (cur_branch_name != null) {
	//			int pos = sn_adapter.getPosition(cur_branch_name);
	//			if (pos > -1) {
	//				snif_chazarra_list.setSelection(pos);
	//			}
	//		}
	//	}

	private static void setText_id_empty(EditText txt, String s) {
		if (txt.getText().toString().trim().length() == 0) {
			txt.setText(s);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//		caption.setText(MainMenuActivity.caption);
		//		driver_name_list.setF;
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		//		InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		//		inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}


	@Click
	void btn_clear() {
		//		do_clear();
		showDialog(DLG_CLEAR); //do_clear
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DLG_CANCEL) {
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {				
				@Override
				public void run() {
					do_cancel();
				}
			});
		}
		else if (id == DLG_CLEAR) { 
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {  				
				@Override
				public void run() {
					do_clear();
				}  
			});
		}
		else {
			return super.onCreateDialog(id); 
		}
	}

	@UiThread
	void do_cancel() { 
		this.finish();
		ActivityUtils.close_all();
	}


	private void do_clear() {
		mileage.setText(""); 
		//		car_code.setText("");
		//		editText_neesaf_bafoall.setText("");
		//		num_of_keys.setText("");
		//		num_of_controllers.setText("");
		fuel_slide.clear();
		musach_list.setSelection(0);
		txt_comment.setText("");
		set_default_snif_chazarra();
		set_default_driver_name();
	}


	private void populate_screen() {
		clear_textViews();

		license_no.setText(CarNumberValue.get());

		//		SendForm_Request last_form = CarNumberActivity.lastForm;
		//		if (last_form != null) {
		//			populate_from_last_form(last_form);
		//		}
		//		else {
		populate_from_last_details_response();
		//		}
	}


	private void clear_textViews() {
		license_no.setText("");
		car_model.setText("");

		contract_type.setText("");
		mileage.setText("");
		txt_comment.setText("");

		set_default_driver_name();
//		if (show_musach == View.VISIBLE) {
			musach_list.setSelection(0);
//		}
		set_default_snif_chazarra();

		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);
	}



	private void populate_from_last_form(SendForm_Request last_form) {
		SendForm_Request_Inner inner = last_form.SendRentForm_req;
		if (inner==null) {
			return;
		}
		car_model.setText(inner.CarModel);
		//		company_name.setText(TxtUtils.limit(12, inner.CompanyName));
		//		license_expiration_date.setText(inner.DateLicense);
		//		insurance_expiration_date.setText(inner.DateInsurance);
	}


	@SuppressWarnings("unchecked")
	private void populate_from_last_details_response() {
		set_default_driver_name();
		set_default_snif_chazarra();
				
		GetCarDetails_Response details = GetCarDetails_Response.carDetails_response;
		if (details==null) {
			return;
		}
		GetCarDetails_Response_Inner _cur = details.GetRentCarDetails_res;
		if (_cur==null) {
			return;
		}				
		car_model.setText(TxtUtils.limit(22,_cur.CarModel));
		set_contractType(_cur); 
	}



	private void set_contractType(GetCarDetails_Response_Inner _cur) {
		String _ContractType;
		boolean pnimmi_pticha = PnimmiValue.Pnimmi() && MenuPnimmiActivity.Pticha();
		if (pnimmi_pticha) {
			int contract_code = CarNumberValue.get_contract_type(); 
			_ContractType = AllDataRepository.getContractName(contract_code);
		}
		else {
//			contract_type.setText("" + _cur.ContractType);
			_ContractType = _cur.ContractType;
		}
		contract_type.setText(_ContractType);
	}

	private void set_default_snif_chazarra() {
		if (snif_chazarra_list==null || snif_chazarra_list.getAdapter() == null) {
			int jj=234;
			jj++;
			return;
		}
		snif_chazarra_list.setSelection(0);

		int return_branch_no = GetCarDetails_Response.get_ReturnBranchNo();
		if (return_branch_no > -1) {
			String cur_branch_name = AllDataRepository.getBranchName(return_branch_no);
			if (cur_branch_name != null && cur_branch_name.length() > 0) {
				int pos = ((ArrayAdapter<String>)snif_chazarra_list.getAdapter()).getPosition(cur_branch_name);
				if (pos > -1) {
					snif_chazarra_list.setSelection(pos);
				}				
			}
		}				
		
		if (GetCarDetails_Response.can_change_branch()) { // using ReturnBranchOpt
			// allow change
			snif_chazarra_list.setEnabled(true);
		}
		else {
//			read only;
			snif_chazarra_list.setEnabled(false);
		}
	}


	private void set_default_driver_name() {
		if (driver_name_list==null || driver_name_list.getAdapter()==null) {
			return;
		}
		driver_name_list.setSelection(0); 
		String dname = Authentication_Response.get_this_driver_name();
		if (dname != null && dname.length() > 0) {
			int pos = ((ArrayAdapter<String>)driver_name_list.getAdapter()).getPosition(dname);
			if (pos > -1) {
				driver_name_list.setSelection(pos);
			}
		}
	}

	ActionValue.Values orig_action;
	
	private String txt_mileage;
	private int fuel_level;

	@Click 
	void btn_send() {		
		//		CarCode = car_code.getText().toString().trim(); 
		//		Keys = num_of_keys.getText().toString().trim();
		//		Controls = num_of_controllers.getText().toString().trim();
		//		Mileage = mileage.getText().toString().trim();
		//		CollectedCar = editText_neesaf_bafoall.getText().toString().trim();
		//		String ncontrols = num_of_controllers.getText().toString().trim();
		//		String nkeys = num_of_keys.getText().toString().trim();

		txt_mileage = mileage.getText().toString().trim();
		fuel_level = fuel_slide.get_fuel_level();
		//		CurFuel = "" + fuel_level; // valid = 0..8 

//		int garage_def = AllDataRepository.getGarageDefault();
//		final boolean show_musach = (garage_def == 1); 


		//		if (!_DEBUG) {
		if (txt_mileage.isEmpty()) { 
			toast("יש להזין ערך מד אוץ");
			return;				
		}
		if (txt_mileage.length() > 6) {
			toast(" קילומטראז לא חוקי");
			return;
		} 
		if (fuel_level < 0) {
			toast("יש לציין ערך דלק");
			return;								
		}
		
//		FuelAlert f_alert;
//		if ((f_alert=GetCarDetails_Response.fuel_alert_needed(fuel_level)) != null) {
//			toast(f_alert.errmsg);
//			if (f_alert.stop) {
//				return;
//			}
//		}
		
		if (!fuel_was_alerted || (fuel_was_alerted && fuel_alert_stop_mode)) {
			FuelAlert f_alert;
		    if ((f_alert=GetCarDetails_Response.fuel_alert_needed(fuel_level)) != null) {
		    	toast(f_alert.errmsg);
		    	fuel_was_alerted = true;
		    	fuel_alert_stop_mode = f_alert.stop;
	    		return;
		    }
		}
		
		if (driver_name_list.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
			toast("יש לציין שם נהג");
			return;									    	
		}  
//		if (show_musach == View.VISIBLE) {
		if (musach_is_mandatory) {
			if (musach_list.getSelectedItemPosition() == Spinner.INVALID_POSITION ||
					musach_list.getSelectedItemPosition() == 0) {
				toast("יש לציין מוסך");
				return;									    	
			}
		}
		if (snif_chazarra_list.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
			toast("יש לציין סניף החזרה");
			return;									    	
		}

		if (DeviceAndroidId.is_lab_device(this)) { 
			perform_actual_action();
		}
		else {
			DialogOkCancel.handle_mad_utz(this, txt_mileage, mileage, new Runnable() {				
				@Override
				public void run() {
					perform_actual_action();
				}
			});
//			if (!GetCarDetails_Response.mad_utz_ok(txt_mileage)) {
//				toast(GetCarDetails_Response.error_msg());
//				return;
//			}
		}
		
		
	}


	private void perform_actual_action() {
		MyApp.appSetting.setFuel(fuel_level);
		MyApp.appSetting.setKm(txt_mileage);
		FuelValues.comments = txt_comment.getText().toString().trim();
		
		FuelValues.driver_name_ind = get_driver_value();
		FuelValues.musach_ind = get_musach_value();
		MyApp.appSetting.setReturnBranch(get_snif_chazarra_value());

		final boolean habba_mode =  GetCarDetails_Response.show_next_button();

		open_progress_dialog("מתחבר לשרת");
		orig_action = ActionValue.getObj();
		bg_perform_send_commands(habba_mode);
	}
	

//	private int calcCiCoIndex() {
//		int CiCoIndex;
//		
//		boolean pnimmi_pticha = MainMenuActivity.Pnimmi() && MenuPnimmiActivity.Pticha();
//		if (pnimmi_pticha) {
//			int contract_type = CarNumberValue.get_contract_type();
//			CiCoIndex = AllDataRepository.getCiCoIndex(contract_type);
//		}
//		else {
//			CiCoIndex = AllDataRepository.getCiCoIndex();
//		}
//		return CiCoIndex; 
//	}

	
	@Background
	void bg_perform_send_commands(boolean habba_mode) {
		try {  

			int jj=23423;
			jj++;
			if (data_already_sent) {
				handle_send_success(habba_mode);
				return;
			}			
			ActionValue.set(ActionValue.Values.Pticha); 				
			boolean success = block_sendForm();
			if (!success) {
				int jjj=234;
				jjj++;
				return;
			}
			if (habba_mode) {
				ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
				success = block_getCarDetails();
				if (!success) {
					int jjj=234;
					jjj++;
					return;
				}
			}
			data_already_sent = true;
			handle_send_success(habba_mode); 			
		}
		finally {
			close_progress_dialog();
			ActionValue.set(orig_action);
		}
	}

	private void handle_send_success(boolean habba_mode) {
		int jj=234;
		jj++;
		if (habba_mode) {
			ui_open_image_activity();
		}
		else {			
			ActivityUtils.close_all();
		}
	}

	private boolean block_getCarDetails() {
		try { 
			String car_no = CarNumberValue.get();
			GetCarDetails_Request req = new GetCarDetails_Request(car_no);
			String json = GGson.toJson(req);
			Base_Response_JsonMsg response;
			try { 
				response = JsonTransmitter.send_blocking(json);
			}
			catch (ActionFailedError failed_err) {
				call_toast("<" + failed_err.getReasonDesc() + ">");
				return false;
			}
			if (response==null || !(response instanceof GetCarDetails_Response)) {
				call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
				return false;
			}
//			GetCarDetails_Response res = (GetCarDetails_Response) response;
			call_toast("פרטי רכב התקבלו בהצלחה", true); 
			return true;
		}
		catch (ConnectionError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//				String msg = Authentication_Response.get_error_msg();
			//				toast("ú÷ìä áçéáåø: " + msg);
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		catch (Exception e) { 
			e.printStackTrace();
			call_toast("תקלה בהתחברות לשרת: " + e);
		} 
		finally {
			//?
		}
		return false;
	}

	private boolean block_sendForm() {
//		SendForm_Request_Inner.Image_remote_filename = null;
		SendForm_Request_Inner.reset_photo_arr();
		try { 
			SendForm_Request req = new SendForm_Request(this, true, null, null);
			if (!req.is_valid_form()) {
				call_toast("חסרים נתונים לבקשה!");
				return false;
			}
			String json = GGson.toJson(req);
			Base_Response_JsonMsg response;
			try { 
				response = JsonTransmitter.send_blocking(json);
			}
			catch (ActionFailedError failed_err) {
				call_toast("<" + failed_err.getReasonDesc() + ">");
				return false;
			}
			if (response==null || !(response instanceof SendForm_Response)) {
//				call_toast("תקלה בחיבור לשרת");
				call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
				return false;
			} 
			SendForm_Response res = (SendForm_Response) response;
//			call_toast("הטופס  הועבר לשרת בהצלחה", true);
			//call_toast(SignatureActivity.getSuccessMsg(res), true);
			MainMenuActivity.success_mst_to_show = SignatureActivity.getSuccessMsg(res);
			
			
			
			return true;
		}
		catch (ConnectionError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		finally {
			//?
		}
		return false;
	}

	@UiThread
	void call_toast(String string, boolean success) {
		toast(string, success);
	}

	@UiThread
	void call_toast(String string) {
		toast(string);
	}
	
	@UiThread
	void ui_open_image_activity() {
		ImageCarActivity_.intent(this).start();
	}

	private int get_snif_chazarra_value() {
		String name = (String) snif_chazarra_list.getSelectedItem();
		if (name==null || name.length()==0) {
			return -1;
		}
		int val = AllDataRepository.getSnifChazarraValue(name);
		return val;
	}

	private int get_driver_value() {
		String driver_name = (String) driver_name_list.getSelectedItem();
		if (driver_name==null || driver_name.length()==0) {
			return -1;
		}
		int val = AllDataRepository.getDriverValue(driver_name);
		return val;
	}

	private int get_musach_value() {
		String name = (String) musach_list.getSelectedItem();
		if (name==null || name.length()==0) {
			return -1;
		}
		int val = AllDataRepository.getMusachValue(name);
		return val;
	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}


	@Click 
	void btn_cancel() {
		showDialog(DLG_CANCEL); //do_clear
		//		this.finish();
	}

}
