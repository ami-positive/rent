package com.sixt.android.activities.customerYard.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.sixt.android.activities.customerYard.database.BaseContentProvider;
import com.sixt.android.activities.customerYard.database.DatabaseHelper;
import com.sixt.android.activities.customerYard.database.tables.TableWordsReport;


/**
 * Created by natiapplications on 21/08/15.
 */
public class WordsReportContetnProvieder extends BaseContentProvider {


    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "dev.positiveapps.rateapp.android.db.wordsreport.contentprovider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "wordsreport";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return  TableWordsReport.TABLE_NAME;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableWordsReport.COLUMN_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableWordsReport.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}

