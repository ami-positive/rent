package com.sixt.android.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.sixt.android.MyApp;

import java.util.ConcurrentModificationException;

/**
 * Created by Izakos on 19/07/2016.
 */
public class AppSetting {

    public static final String CLASS_NAME = "AppSetting";

    private static AppSetting instance;
    private static SharedPreferences sharedPref;

    private int returnReason = -1;
    private int contractType = -1;
    private String carNumber;
    private String km = "0";
    private int fuel = -1;
    private int returnBranch = -1;
    private StringBuilder imageEvent;
    private String activity;
    private String cameraError;
    public boolean showImageLoadingProgressBar;

    public String getCameraError() {
        if(cameraError == null){
            cameraError = "";
        }
        return cameraError;
    }

    public void setCameraError(String cameraError) {
        this.cameraError = cameraError;
    }

    public int getReturnBranch() {
        return returnBranch;
    }

    public void setReturnBranch(int returnBranch) {
        this.returnBranch = returnBranch;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(int returnReason) {
        this.returnReason = returnReason;
    }

    public int getContractType() {
        return contractType;
    }

    public void setContractType(int contractType) {
        this.contractType = contractType;
    }

    public StringBuilder getImageEvent() {
        if(imageEvent == null){
            imageEvent = new StringBuilder();
        }
        return imageEvent;
    }

    public void setImageEvent(StringBuilder imageEvent) {
        this.imageEvent = imageEvent;
    }

    public void appendImageEvent(String log) {
        getImageEvent().append(log+"\n");
    }

    public void restImageEvent() {
        this.imageEvent = new StringBuilder();
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        if(km == null){
            this.km = "0";
        }
        this.km = km;
    }

    public String getCarNumber() {
        if(carNumber == null){
            carNumber = "00-000-00";
        }
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public static AppSetting getInstance() {
        if (instance == null) {
            Gson gson = new Gson();
            sharedPref = MyApp.appContext.getSharedPreferences(CLASS_NAME, Context.MODE_PRIVATE);
            AppSetting appSetting = gson.fromJson(sharedPref.getString(CLASS_NAME, ""), AppSetting.class);
            instance = appSetting == null ? new AppSetting() : appSetting;
        }
        return instance;
    }

    public void save() {
        Gson gson = new Gson();

        try {
            String json = gson.toJson(AppSetting.this);
            Log.i(CLASS_NAME, "save: " + json.toString());
            sharedPref.edit().putString(CLASS_NAME, json).commit();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        }
    }

}
