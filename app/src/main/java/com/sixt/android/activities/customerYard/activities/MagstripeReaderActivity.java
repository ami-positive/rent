package com.sixt.android.activities.customerYard.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.android.activities.customerYard.models.CardDataParser;
import com.sixt.android.activities.customerYard.models.ParseResult;
import com.sixt.rent.R;

import java.io.ByteArrayOutputStream;

public class MagstripeReaderActivity extends FragmentActivity {
    private static final int _frequency = 44100;
    private static final int _channelConfig = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private static final int _audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    private int _bufferSize;
    private AudioRecord _audioRecord;
    private AsyncTask<Void, Void, ParseResult> _task;
    private boolean _good;
    private String _data;
    private String _cardNum;
    private MusicIntentReceiver myReceiver;
    private TextView header;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Make us non-modal, so that others can receive touch events.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.main);

        myReceiver = new MusicIntentReceiver();
//        _data = getString(R.string.transfer_credit_card);
        _data = "";
        _SetText(false, _data);

        try {
            _bufferSize = AudioRecord.getMinBufferSize(_frequency, _channelConfig, _audioEncoding) * 8;
            _audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, _frequency,
                    _channelConfig, _audioEncoding, _bufferSize);
            System.out.println("_audioRecord " + _audioRecord);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        header = (TextView) findViewById(R.id.header);
        ((Button) findViewById(R.id.continuation)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "close");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }


    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        header.setText(getString(R.string.please_attche_swipe));
//                        Toast.makeText(MagstripeReaderActivity.this, "Headset is unplugged", Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        header.setText(getString(R.string.please_swipe_card));
//                        Toast.makeText(MagstripeReaderActivity.this, "Headset is plugged", Toast.LENGTH_LONG).show();

                        break;
                    default:
                        header.setText(getString(R.string.error_in_reader));
//                        Toast.makeText(MagstripeReaderActivity.this, "I have no idea what the headset state is", Toast.LENGTH_LONG).show();

                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);

        _SetText(_good, _data);

        _audioRecord.startRecording();
        _task = new MonitorAudioTask();
        _task.execute(null, null, null);

//		DialogManager.showCreditGuardDialog(getSupportFragmentManager(),
//				"https://cgmpiuat.creditguard.co.il//CGMPI_Server/PerformTransaction?txId=c02a40a5-ca1e-4aa7-a46c-bb8fc6a445e3", new CreditGuardDialog.onLinkChanges() {
//					@Override
//					public void linkChanges(CreditGuardDialog dialog, String url) {
//						System.out.println("url - > " + url);
//						if (url.contains("merchantPages")) {
//						}
//					}
//				});
    }

    @Override
    public void onPause() {
        super.onPause();

        _task.cancel(true);
        _audioRecord.stop();
        unregisterReceiver(myReceiver);
    }

    private void _SetText(boolean good, String text) {
        _good = good;
        _data = text;
        TextView t = (TextView) findViewById(R.id.text);
        t.setText(_data);

        if (good)
            t.setTextColor(Color.GREEN);
        else
            t.setTextColor(Color.RED);
    }

    private class MonitorAudioTask extends AsyncTask<Void, Void, ParseResult> {

        @Override
        protected ParseResult doInBackground(Void... params) {
            final double QUIET_THRESHOLD = 32768.0 * 0.02; //anything higher than 0.02% is considered non-silence
            final double QUIET_WAIT_TIME_SAMPLES = _frequency * 0.25; //~0.25 seconds of quiet time before parsing
            short[] buffer = new short[_bufferSize];
            Long bufferReadResult = null;
            boolean nonSilence = false;
            ParseResult result = null;

            while (!nonSilence) {
                if (isCancelled())
                    break;

                bufferReadResult = new Long(_audioRecord.read(buffer, 0, _bufferSize));
                if (bufferReadResult > 0) {
                    for (int i = 0; i < bufferReadResult; i++)
                        if (buffer[i] >= QUIET_THRESHOLD) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            long silentSamples = 0;

                            //Save this data so far
                            for (int j = i; j < bufferReadResult; j++) {
                                stream.write(buffer[j] & 0xFF);
                                stream.write(buffer[j] >> 8);
                            }

                            //Keep reading until we've reached a certain amount of silence
                            boolean continueLoop = true;
                            while (continueLoop) {
                                bufferReadResult = new Long(_audioRecord.read(buffer, 0, _bufferSize));
                                if (bufferReadResult < 0)
                                    continueLoop = false;

                                for (int k = 0; k < bufferReadResult; k++) {
                                    stream.write(buffer[k] & 0xFF);
                                    stream.write(buffer[k] >> 8);
                                    if (buffer[k] >= QUIET_THRESHOLD || buffer[k] <= -QUIET_THRESHOLD)
                                        silentSamples = 0;
                                    else
                                        silentSamples++;
                                }

                                if (silentSamples >= QUIET_WAIT_TIME_SAMPLES)
                                    continueLoop = false;
                            }

                            //Convert to array of 16-bit shorts
                            byte[] array = stream.toByteArray();
                            short[] samples = new short[array.length / 2];
                            for (int k = 0; k < samples.length; k++)
                                samples[k] = (short) ((short) (array[k * 2 + 0] & 0xFF) | (short) (array[k * 2 + 1] << 8));

                            //Try parsing the data now!
                            result = CardDataParser.Parse(samples);
                            if (result.errorCode != 0) {
                                //Reverse the array and try again (maybe it was swiped backwards)
                                for (int k = 0; k < samples.length / 2; k++) {
                                    short temp = samples[k];
                                    samples[k] = samples[samples.length - k - 1];
                                    samples[samples.length - k - 1] = temp;
                                }
                                result = CardDataParser.Parse(samples);
                            }

                            nonSilence = true;
                            break;
                        }
                } else
                    break;
            }

            return result;
        }

        @Override
        protected void onPostExecute(ParseResult result) {
            if (result != null) {
                _cardNum = result.data;
//                String str = "Data:\r\n" + result.data + "\r\n\r\n";
                String str = "(" + result.data + ")\r\n";

                if (result.errorCode == 0) {
                    str += "Success";

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result",_cardNum);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();

                } else {
                    String err = Integer.toString(result.errorCode);
                    switch (result.errorCode) {
                        case -1: {
                            err = "NOT_ENOUGH_PEAKS";
                            break;
                        }
                        case -2: {
                            err = "START_SENTINEL_NOT_FOUND";
                            break;
                        }
                        case -3: {
                            err = "PARITY_BIT_CHECK_FAILED";
                            break;
                        }
                        case -4: {
                            err = "LRC_PARITY_BIT_CHECK_FAILED";
                            break;
                        }
                        case -5: {
                            err = "LRC_INVALID";
                            break;
                        }
                        case -6: {
                            err = "NOT_ENOUGH_DATA_FOR_LRC_CHECK";
                            break;
                        }
                    }
                    str += getString(R.string.swipe_error);
//                    str += "Error: " + err;
                }

                _SetText(result.errorCode == 0, str);
            } else
                _SetText(false, "[Parse Error]");

            //Now start the task again
            _task = new MonitorAudioTask();
            _task.execute(null, null, null);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // If we've received a touch notification that the user has touched
        // outside the app, finish the activity.
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            finish();
            return true;
        }

        // Delegate everything else to Activity.
        return super.onTouchEvent(event);
    }

}
