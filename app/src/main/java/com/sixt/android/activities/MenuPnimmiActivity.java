package com.sixt.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;


import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.pnimmi_menu_activity)
@NoTitle
public class MenuPnimmiActivity extends Activity {

	private static MenuPnimmiActivity inst;

//	public static String old_caption = "";
	public static String caption_elem = "";

	@ViewById
    TextView caption;
	@ViewById
	Button btn_pticha;
	@ViewById
	Button btn_sgirra;
	@ViewById
	Button btn_mesirra;
	@ViewById
	Button btn_hachzarra;
	@ViewById
    TextView txt_hello_username;
	@ViewById
	TextView txt_branch;

	
	private static int mode = -1;
	
	public static boolean Sgirra() { return mode==0; }
	public static boolean Hachzarra() { return mode==1; }
	public static boolean Pticha() { return mode==2; }
	public static boolean Mesirra() { return mode==3; }
	public static void reset() { mode = -1; }


	

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inst = this;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;
	}
	
	
	@Click
	void btn_pticha() {
		mode = 2;
		ActionValue.set(ActionValue.Values.Pticha);
		BreadCrumbs.append("פתיחה");
//		pnimmiMode = PnimiMode.PTICHA;
		CarNumberPnimiPtichaActivity_.intent(this).start();
	}

	@Click
	void btn_sgirra() {
		ActionValue.set(ActionValue.Values.Sgirra);
		mode = 0;
		BreadCrumbs.append("סגירה");
//		pnimmiMode = PnimiMode.SGIRRA;
		CarNumberActivity_.intent(this).start();
	}
	
	@Click
	void btn_mesirra() {
		mode = 3;
		ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
		BreadCrumbs.append("מסירה");
//		pnimmiMode = PnimiMode.MESIRRA;
		CarNumberActivity_.intent(this).start();
	}
		
	@Click
	void btn_hachzarra() {
		ActionValue.set(ActionValue.Values.Hachzarra);
		mode = 1;
		BreadCrumbs.append("החזרה");
//		pnimmiMode = PnimiMode.HACHZARRA;
		CarNumberActivity_.intent(this).start();
	}
	
	
	//=========


	@Override
	protected void onResume() {
		super.onResume();
		CarNumberActivity.lastForm = null; 
		LoginActivity.call_finish();
		UniqueId.clear();
		BreadCrumbs.set("פנימי");
//		CarSortUtils.set("");  
//		old_caption = "";
		caption_elem = "";
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_menu, menu);
		return true;
	}

	public static volatile boolean is_isuf = false; // isuf/medira
	public static volatile boolean rechev_kavua = false; // kavua/chalufi
	public static volatile boolean tofrom_lakoach = false; // sapak/lakoach 

	@Override
	protected void onStart() {
		super.onStart();
		reset();
		caption.setText("חוזה פנימי");
		MainMenuActivity.set_username_branch(txt_hello_username, txt_branch);
		is_isuf = false;
		rechev_kavua = false;
		tofrom_lakoach = false;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		int i = item.getItemId();
		switch (i)
		{
		case R.id.menu_testapp:
//			ZTestActivity_.intent(this).start(); 
			return true;
			
		case R.id.menu_ver:
			show_ver();
			return true;				
		default:
				return super.onOptionsItemSelected(item);
		}
	}
	

	private void show_ver() {
		String versionName;
		try {
			versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("סיקסט השכרה" + " גרסא " + versionName)
		       .setCancelable(false)
		       .setPositiveButton("אישור", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                //do things
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();				
	}

	@Click
	void btn_mischarri() {
//		is_isuf = true;
//		caption_elem = caption = "איסוף";		
//		CurrentAction.set(CurrentAction.Values.Isuf_Rechev);
//		CarNumberActivity.number = null;
//		KavuaChalufiActivity_.intent(this).start();
		MenuMischarriActivity_.intent(this).start();
	}
	


//	@Click
	void btn_report_event() {
		is_isuf = false;
//		caption = "אירוע";
//		CurrentAction.set(null); //what is 'action' val for ReportEvent ?
//		CarNumberActivity_.intent(this).start(); 
	}

//	@Click
	void btn_mesirat_rechev() {
		is_isuf = false;
		caption_elem = /*old_caption =*/ "מסירה";
		ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
		KavuaChalufiActivity_.intent(this).start();
	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}

 

}
