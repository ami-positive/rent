package com.sixt.android.activities.customerYard.activities;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnLoadCompleteListener;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.sixt.android.MyApp;
import com.sixt.android.activities.CarNumberMischarriMesirraActivity;
import com.sixt.android.activities.CarNumberMischarriMesirraActivity_;
import com.sixt.android.activities.FuelmesirraHachzarraActivity;
import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.RejectListActivity;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.models.ContractDoc;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.GetContactDocRequest;
import com.sixt.android.activities.customerYard.network.requests.SendContractDocRequest;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDocResponse;
import com.sixt.android.activities.customerYard.pdf.CopyAssetTask;
import com.sixt.android.activities.customerYard.pdf.FileDownloaderTask;
import com.sixt.android.activities.customerYard.pdf.PDFUtil;
import com.sixt.android.activities.customerYard.utils.BitmapUtil;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.activities.customerYard.utils.SignatureDialogListener;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.SignatureDrawDialog;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by natiapplications on 24/12/15.
 */
@EActivity(R.layout.activity_contract_doc)
@NoTitle
public class ContractDocActivity extends BaseActivity {


    @ViewById
    Button btnSend;

    @ViewById
    Button btnCancel;

    @ViewById
    Button btnSign;

    @ViewById
    FrameLayout pdfContainer;

    @ViewById
    TextView txtPageIndicator;

    @ViewById
    PDFView pdfview;

    @Extra
    int km;
    @Extra
    int fuel;

    private ContractDoc contractDoc;


    private String currentPdfFilePath;
    private String inPath;
    private String outPath;

    private Handler pageIndicatorHandler = new Handler();


    private Runnable hidePageIndicator = new Runnable() {
        @Override
        public void run() {
            txtPageIndicator.setVisibility(View.GONE);
        }
    };


    private void startShowPageIndicator() {
        try {
            pageIndicatorHandler.removeCallbacks(hidePageIndicator);
        } catch (Exception e) {
        }
        txtPageIndicator.setVisibility(View.VISIBLE);
        pageIndicatorHandler.postDelayed(hidePageIndicator, 1000);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @AfterViews
    public void onPostCreate() {

        showProgressDialog();

        //from url
        //loadPdfFromURL("https://www.cse.msu.edu/~chooseun/Test2.pdf");

        //from sdcard
        //loadPdfFromSdcard("/Download/before_sign.pdf");

        //from assets
        //loadPdfFromAsset("before_sign.pdf");

        //from server
        loadPdfFromServer();

    }

    private void loadPdfFromServer() {

        MyApp.networkManager.makeRequest(new GetContactDocRequest(this.fuel, this.km), new NetworkCallback<ContractDocResponse>() {
            @Override
            public void onResponse(boolean success, String errorDesc, ContractDocResponse response) {
                super.onResponse(success, errorDesc, response);

                if (success) {
                    System.out.println("OPEL " + response.getPDASignatureID());
                    MyApp.generalSettings.setPDASignatureID(response.getPDASignatureID());
                    response.decodeReceivedPDF(new ContractDocResponse.DecodePDFCallback() {
                        @Override
                        public void onDone(boolean success, String errDesc, String fileName) {
                            if (success) {
                                inPath = fileName;
                                outPath = BitmapUtil.createStringPathForStoringMedia("destNewPdf.pdf");
                                parsePDF(inPath);
                            } else {
                                dismissProgressDialog();
                                toast(errDesc);
                            }
                        }
                    });

                } else {
                    dismissProgressDialog();
                    toast(errorDesc);
                }
            }
        });

    }


    public void loadPdfFromSdcard(String fileName) {
        inPath = Environment.getExternalStorageDirectory().getAbsolutePath() + fileName;
        outPath = BitmapUtil.createStringPathForStoringMedia("destNewPdf.pdf");
        parsePDF(inPath);
    }


    public void loadPdfFromAsset(String assetFileName) {

        new CopyAssetTask(new CopyAssetTask.AssetCopyCallback() {
            @Override
            public void onCopy(boolean success, String filePath, String errDesc) {
                if (success) {
                    inPath = filePath;
                    outPath = BitmapUtil.createStringPathForStoringMedia("DestinationPDF.pdf");
                    parsePDF(inPath);
                } else {
                    dismissProgressDialog();
                    toast(errDesc);
                }
            }
        }).execute(assetFileName);
    }


    public void loadPdfFromUrl(String url) {

        new FileDownloaderTask(new FileDownloaderTask.FileDownloaderCallback() {
            @Override
            public void onDownload(boolean success, String filePath, String errDesc) {
                if (success) {
                    inPath = filePath;
                    outPath = BitmapUtil.createStringPathForStoringMedia("DestinationPDF.pdf");
                    parsePDF(inPath);
                } else {
                    dismissProgressDialog();
                    toast(errDesc);
                }
            }
        }).execute(url);
    }


    private void parsePDF(final String inPath) {

        ContractDoc.createByPDF(inPath, new ContractDoc.OnPageCreated() {
            @Override
            public void onPageCreated(boolean success, ContractDoc doc) {
                if (success) {
                    contractDoc = doc;
                    showPdf(inPath, 1, false);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog();
                            ToastUtil.toaster("Fail to parse pdf file", true);
                        }
                    });

                }
            }
        });

    }

    private void reinitPdfView() {
        ViewGroup group = (ViewGroup) pdfview.getParent();
        group.removeView(pdfview);
        pdfview = null;

        pdfview = new PDFView(this, null);
        pdfview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        group.addView(pdfview);
    }


    @UiThread
    public void showPdf(String filePath, int page, boolean sign) {
        reinitPdfView();
        try {
            currentPdfFilePath = filePath;

            /*CopyAsset copyAsset = new CopyAssetThreadImpl(this, new Handler());
            copyAsset.copy("asset", new File(getCacheDir(), "before_sign.pdf.pdf").getAbsolutePath());

            PDFViewPager pdfViewPager = (PDFViewPager) findViewById(R.id.pdfViewPager);
            PDFPagerAdapter adapter = new PDFPagerAdapter(this, "before_sign.pdf");
            pdfViewPager.setAdapter(adapter);*/


            pdfview.fromFile(new File(filePath))
                    .defaultPage(page)
                    .showMinimap(false)
                    .enableSwipe(true)
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(int nbPages) {
                            dismissProgressDialog();
                            if (contractDoc == null) {
                                updateSignBtn(1);
                            }
                        }
                    })
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int currentPage, int pageCount) {
                            if (contractDoc != null) {
                                updateSignBtn(currentPage);
                            }
                            txtPageIndicator.setText(currentPage + "/" + pageCount);
                            startShowPageIndicator();
                        }
                    })
                    .load();
        } catch (Exception e) {
        }

        if (sign) {
            contractDoc.addSignatureToPage(page);
            updateSignBtn(page);
        }

    }


    private void updateSignBtn(int pageNumber) {
        btnSign.setVisibility(contractDoc.isPageNeedSignature(pageNumber) ? View.VISIBLE : View.GONE);
        btnSign.setText("חתום" + " (" + contractDoc.getPageLeftSignaturesCount(pageNumber) + ")");
    }


    @Click
    public void btnCancel() {
        Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {
            @Override
            public void run() {
                doCancel();
            }
        }).show();
    }

    @Click
    public void btnSign() {


        final int page = pdfview.getCurrentPage() + 1;

        AlertDialog signatureDialog = SignatureDrawDialog.create(ContractDocActivity.this, new SignatureDialogListener() {
            @Override
            public void onSign(Bitmap signature) {

                //String signatureLocalFilename = BitmapUtil.saveImageToSDCard("pdfSing.png", signature);

                showProgressDialog();
                injectSignatureToFile(scaleSignature(signature), page);
            }
        });
        signatureDialog.show();
    }


    @Click
    public void btnSend() {


        if (contractDoc == null) {
            finish();
            startActivity(getIntent());  // refresh activity
            return;
        }

        ArrayList<Integer> missingPage = contractDoc.isHasAllSignatures();

        if (missingPage == null) {
            showProgressDialog();
            convertDocToBase64();
        } else {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < missingPage.size(); i++) {
                builder.append(missingPage.get(i) + ",");
            }
            ToastUtil.toaster("נא לחתום על מסמך מס׳ " + builder.toString(), true);
        }

    }


    private Bitmap scaleSignature(Bitmap src) {


        float scaleFactor = 1f;

        float newWidth = src.getWidth();
        float newHeight = src.getHeight();


        while (newHeight > 120) {
            newWidth *= scaleFactor;
            newHeight *= scaleFactor;
            scaleFactor -= 0.05f;
        }

        Log.e("Bitmaplog", "scale factor: " + scaleFactor + " h: " + newHeight + " w: " + newWidth);

        if (newWidth > 0 && newHeight > 0) {
            return Bitmap.createScaledBitmap(src, (int) newWidth, (int) newHeight, true);
        } else {
            return Bitmap.createScaledBitmap(src, 300, 100, true);
        }

    }

    @Background
    public void injectSignatureToFile(Bitmap signature, int page) {

        try {
            String dest = createSignedPdfFile(currentPdfFilePath, signature, page);
            showPdf(dest, page, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            showPdf(currentPdfFilePath, 1, false);
        }

    }

    private String getCurrentOutPath(String currentIn) {
        if (currentIn.equals(outPath)) {
            return inPath;
        } else {
            return outPath;
        }
    }


    private String createSignedPdfFile(String sourceFilePath, Bitmap signature, int pageNo) throws IOException, DocumentException {

        String dest = getCurrentOutPath(sourceFilePath);

        FileOutputStream fos = new FileOutputStream(new File(dest));

        PdfReader reader = new PdfReader(sourceFilePath);

        PdfStamper stamper = new PdfStamper(reader, fos);

        for (int i = 0; i < reader.getNumberOfPages(); i++) {
            //put content over
            if (pageNo == i + 1) {
                PdfContentByte content = stamper.getOverContent(pageNo);
                content.addImage(createSignaturePDFImage(signature, 0, (int) contractDoc.getPageSignaturePositionY(pageNo)));
            }
        }

        stamper.close();

        return dest;
    }


    private Image createSignaturePDFImage(Bitmap signature, int posX, int posY) throws BadElementException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        signature.compress(Bitmap.CompressFormat.JPEG, 40, stream);
        Image image = Image.getInstance(stream.toByteArray());
        image.setAbsolutePosition(posX, posY - (signature.getHeight() / 2));

        return image;
    }


    @UiThread
    void doCancel() {
        this.finish();
        ActivityUtils.close_all();
    }

    @Background
    public void convertDocToBase64() {
        try {
            String docAsBase64 = PDFUtil.convertPDFFileToBase64String(currentPdfFilePath);
            sendContractDocToServer(true, docAsBase64);
        } catch (IOException e) {
            sendContractDocToServer(false, null);
        }
    }

    @UiThread
    public void sendContractDocToServer(boolean successConverting, String base64) {

        if (successConverting) {
            System.out.println("OPEL2 " + MyApp.generalSettings.getPDASignatureID());
            MyApp.networkManager.makeRequest(new SendContractDocRequest(this.km, this.fuel, base64, MyApp.generalSettings.getPDASignatureID()), new NetworkCallback<BaseResponseObject>() {
                @Override
                public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
                    super.onResponse(success, errorDesc, response);
                    dismissProgressDialog();
                    if (success) {
                        nextActivity();
                    } else {
                        toast(errorDesc);
                    }
                }
            });

        } else {
            dismissProgressDialog();
            toast("Can no convert doc to base 64");
        }

    }

    private void nextActivity() {
        MenuMischarriActivity.mode = 0; // enabling the mandatory rejects messages by changing to Mesirra()
        RejectListActivity.showHearot = true; // no need for showing Hearot screen
        CarNumberMischarriMesirraActivity.fromDocActivity = true;
        FuelmesirraHachzarraActivity.isTheLastScreen = true;
        CarNumberMischarriMesirraActivity.carNumber = ContractChetchData.currentLicensingNumber;
        CarNumberMischarriMesirraActivity_.intent(this).start();
        finish();
    }

}

