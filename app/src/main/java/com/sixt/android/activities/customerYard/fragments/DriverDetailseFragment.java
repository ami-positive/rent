package com.sixt.android.activities.customerYard.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.activities.ContractDocActivity_;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.models.LicenseDrive;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.SendDriverDetailsRequest;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.activities.customerYard.utils.DateUtil;
import com.sixt.android.activities.customerYard.utils.FragmentsUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by natiapplications on 18/12/15.
 */
@EFragment(R.layout.fragment_driver_details)
public class DriverDetailseFragment extends BaseFragment implements DriverDetailseActivity.ActionButtonsListener{




    @ViewById
    TextView txtFragTitle;
    @ViewById
    EditText etFirstName;
    @ViewById
    EditText etLastName;
    @ViewById
    TextView etBirthDate;
    @ViewById
    EditText etID;
    @ViewById
    EditText etAddress;
    @ViewById
    EditText etCity;
    @ViewById
    EditText etPhone;
    @ViewById
    EditText etLicenseNumber;
    @ViewById
    EditText etLicenseStartDate;
    @ViewById
    TextView etLicenseEndDate;
    @ViewById
    EditText etLicenseType;
    @ViewById
    EditText etEmailAddress;
    @ViewById
    CheckBox cbSendEmail;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @AfterViews
    public void onPostCreate () {

        AppUtil.hideKeyBoard(this);

        ((DriverDetailseActivity)getActivity()).setActionButtonsListener(this);
        ((DriverDetailseActivity)getActivity()).showBaseDetails();
        txtFragTitle.setText(getFragmentName());
        fillScreenData();
    }

    private void fillScreenData() {

        ContractDetailsResponse  contractDetails = getContractDetails();
        if (contractDetails != null){
            etFirstName.setText(contractDetails.getDriverFName());
            etLastName.setText(contractDetails.getDriverLName());
            etLicenseNumber.setText(contractDetails.getDriverLicense());
            etBirthDate.setText(contractDetails.getDriverBDate());
            etAddress.setText(contractDetails.getDriverAddress());
            etCity.setText(contractDetails.getDriverCity());
            etID.setText(contractDetails.getPrivateID());
            etPhone.setText(contractDetails.DriverPhone);
            etLicenseEndDate.setText(contractDetails.getDriverExpire());
            etLicenseType.setText(contractDetails.getLicenseType());
            etLicenseStartDate.setText(contractDetails.getDriverYear());
            etEmailAddress.setText(contractDetails.DriverMail);
            cbSendEmail.setChecked(contractDetails.DriverAgree > 0);
        }

    }




//    @EditorAction(R.id.etLastName)
//    void onEditorActionsOnetLastName(TextView tv, int actionId, KeyEvent keyEvent) {
//
//        if (actionId == EditorInfo.IME_ACTION_NEXT){
//            showDatePickerDialog(etBirthDate, etID);
//        }
//
//    }
//
//    @EditorAction(R.id.etLicenseNumber)
//    void onEditorActionsOnetCarNumber(TextView tv, int actionId, KeyEvent keyEvent) {
//
//        if (actionId == EditorInfo.IME_ACTION_NEXT){
//            showDatePickerDialog(etLicenseStartDate, null);
//        }
//
//    }


    @Click
    public void etBirthDate (){
        showDatePickerDialog(etBirthDate, etID);
    }
/*
    @Click
    public void etLicenseStartDate () {
        showDatePickerDialog(etLicenseStartDate, null);
    }*/

    @Click
    public void etLicenseEndDate () {
        showDatePickerDialog(etLicenseEndDate, etLicenseType);
    }



    @Override
    public int getFragmentIndex() {
        return 3;
    }

    @Override
    public String getFragmentName() {
        return "פרטי הנהג";
    }

    @Override
    public void onPrevious() {

    }

    @Override
    public void onClear() {
        Dialog2Btns.create(getActivity(), "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
            @Override
            public void run() {
                doClear();
            }
        }).show();


    }

    @Override
    public boolean onNext() {

        if (validation()){
            updateContractDetails();
            sendDriverDetailsToServer();
            return true;
        }

        return false;
    }


    private void sendDriverDetailsToServer () {
        showProgressDialog();

        MyApp.networkManager.makeRequest(new SendDriverDetailsRequest(getContractDetails()),new NetworkCallback<BaseResponseObject>(){

            @Override
            public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
                super.onResponse(success, errorDesc, response);
                dismisProgressDialog();
                if (success){
                    Fragment nextFragment = DriverDetailseActivity.getNextFragment(DriverDetailseFragment_.class.getSimpleName(),getContractDetails());
                    if (nextFragment != null){
                        FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                                nextFragment,R.id.driverDetailsContainer);
                    }else {
                        ContractDocActivity_.intent(getActivity())
                                .extra("km",getContractDetails().Km)
                                .extra("fuel", getContractDetails().Fuel)
                                .start();
                    }
                }else {
                    toast(errorDesc);
                }
            }
        });
    }


    private void showDatePickerDialog (final  TextView toSet, final  TextView next) {

        int[] bastDate = DateUtil.getBestDateForDatePicker(toSet.getText().toString());


        DatePickerDialog datePickerDialog = new DatePickerDialog
                (getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {

                        String dateToSet = DateUtil.getDateAsStringByPickerFields(DateUtil.FORMAT_JUST_DATE,dayOfMonth,monthOfYear,year,0,0,0);
                        toSet.setText(dateToSet);

                        // no need to skip fields
                       /* if (next != null && next.getText().toString().isEmpty()){
                            next.requestFocus();
                            AppUtil.showKeyboard(next);

                        }else {
                            showDatePickerDialog(etLicenseEndDate,etLicenseType);
                        }*/
                    }
                }, bastDate[2], bastDate[1], bastDate[0]);


        datePickerDialog.show();
    }


    private boolean validation() {
        StringBuilder errorMessage = new StringBuilder();
        boolean hasError = false;

        errorMessage.append("אנא השלם את השדות החסרים:" + "\n\n");
        if (etFirstName.getText().toString().isEmpty()){hasError = true;errorMessage.append("שם פרטי"+"\n");}
        if (etLastName.getText().toString().isEmpty()){hasError = true; errorMessage.append("שם משפחה"+"\n");}
        if (etBirthDate.getText().toString().isEmpty()){hasError = true;errorMessage.append("תאריך לידה"+"\n");}
        if (etID.getText().toString().isEmpty()){hasError = true;errorMessage.append("תעודת זהות"+"\n");}else {
            if (!IDValidator(etID.getText().toString())){
                hasError = true;
                errorMessage.append("תעודת זהות - ערך לא חוקי"+ "\n");
            }
        }
        if (etAddress.getText().toString().isEmpty()){ hasError = true; errorMessage.append("כתובת"+"\n"); }
        if (etCity.getText().toString().isEmpty()){hasError = true;errorMessage.append("עיר"+"\n");}
        if (etPhone.getText().toString().isEmpty()){hasError = true; errorMessage.append("טלפון"+"\n");}
        if (etLicenseNumber.getText().toString().isEmpty()){hasError = true; errorMessage.append("מספר רישיון"+"\n");}else {
            if (etLicenseNumber.getText().toString().length() < 7){
                hasError = true;
                errorMessage.append("מספר רישיון - ערך לא חוקי"+"\n");
            }
        }
        if (etLicenseStartDate.getText().toString().isEmpty()){ hasError = true;errorMessage.append("ת. הוצאת רשיון"+"\n"); }
        if (etLicenseEndDate.getText().toString().isEmpty()){ hasError = true;errorMessage.append("תאריך תוקף רישיון"+"\n");}
        if (etLicenseType.getText().toString().isEmpty()){ hasError = true;errorMessage.append("סוג רישיון"+"\n");}

        if (cbSendEmail.isChecked()){
            if (etEmailAddress.getText().toString().isEmpty()){
                hasError = true;
                errorMessage.append("כתובת דוא״ל" + "\n");
            }else {
                if (!isValidEmail(etEmailAddress.getText().toString())){
                    hasError = true;
                    errorMessage.append("כתובת דוא״ל - ערך לא חוקי" + "\n");
                }
            }
        }
        if (hasError){
            toast(errorMessage.toString());
        }

        return !hasError;
    }

    // check if id number is valid
    private boolean IDValidator(String id) {
        if(id == null){
            return false;
        }

        int idLenght = id.length();

        /*if (id.length() != 9) {
            return false;
        }*/
        int counter = 0, incNum;

        for (int i = 0; i < id.length(); i++) {
            incNum = Integer.parseInt(String.valueOf(id.charAt(i))) * ((i % 2) + 1);//multiply digit by 1 or 2
            counter += (incNum > idLenght) ? incNum - idLenght : incNum;//sum the digits up and add to counter
        }
        return (counter % 10 == 0);
    }

    public final static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void updateContractDetails () {

        getContractDetails().DriverFName = etFirstName.getText().toString();
        getContractDetails().DriverLName = etLastName.getText().toString();
        getContractDetails().DriverCity = etCity.getText().toString();
        getContractDetails().DriverAddress = etAddress.getText().toString();
        getContractDetails().privateID = etID.getText().toString();
        getContractDetails().DriverLicense = etLicenseNumber.getText().toString();
        getContractDetails().DriverPhone = etPhone.getText().toString();
        getContractDetails().DriverExpire = etLicenseEndDate.getText().toString().replaceAll("/","");
        getContractDetails().DriverBDate = etBirthDate.getText().toString().replaceAll("/","");
        getContractDetails().DriverAgree = cbSendEmail.isChecked() ? 1:0;
        getContractDetails().DriverMail = etEmailAddress.getText().toString();

        LicenseDrive update = null;
        if (getContractDetails().getLicenseDrive() != null){
            update = getContractDetails().getLicenseDrive();
        }else{
            update = new LicenseDrive();
        }

        update.setLicenseEndDate(etLicenseEndDate.getText().toString());
        update.setLicenseStartDate(etLicenseStartDate.getText().toString());
        update.setLicenseType(etLicenseType.getText().toString());

        getContractDetails().setLicenseDrive(update);
        getContractDetails().saveToFile();

    }



    private void doClear (){
        String EMPTY = "";

        etFirstName.setText(EMPTY);
        etID.setText(EMPTY);
        etLastName.setText(EMPTY);
        etBirthDate.setText(EMPTY);
        etAddress.setText(EMPTY);
        etCity.setText(EMPTY);
        etPhone.setText(EMPTY);
        etLicenseNumber.setText(EMPTY);
        etLicenseStartDate.setText(EMPTY);
        etLicenseEndDate.setText(EMPTY);
        etLicenseType.setText(EMPTY);
        etEmailAddress.setText(EMPTY);
        cbSendEmail.setChecked(false);
    }
}
