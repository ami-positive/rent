package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.ContractDocResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class GetContactDocRequest extends RequestObject<ContractDocResponse> {

    public int Fuel;
    public int Km;


    @SerializedName("GetContractDoc_Req")
    private ContractDocRequestInner  contractDocRequestInner;

    public GetContactDocRequest(int fuel, int km) {
        extandTimeOut = true;
        Fuel = fuel;
        Km = km;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ContractDocResponse> callback) {
        contractDocRequestInner = new ContractDocRequestInner(Fuel, Km, "GetContractDoc_Request");
        apiInterface.getContractDoc(contractDocRequestInner, callback);

    }





    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }





    public class ContractDocRequestInner {


        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public String Fuel;
        public String Km;
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "GetContractDoc_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();


        public ContractDocRequestInner(int fuel, int km, String msg_type) {
            this.Fuel = String.valueOf(fuel);
            this.Km = String.valueOf(km);
            this.msg_type = msg_type;
        }

    }

}
