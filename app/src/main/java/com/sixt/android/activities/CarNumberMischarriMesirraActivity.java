package com.sixt.android.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.CarNumberUtils;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.NetworkState;
import com.sixt.android.app.util.TEST;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

@EActivity(R.layout.mischarri_mesirra_number)
@NoTitle
public class CarNumberMischarriMesirraActivity extends BaseActivity {

    public static volatile SendForm_Request lastForm; // to be used at Mesirrat_Rechev

    private static final boolean _DEBUG = TEST.debug_mode(false);

    private static final String DEBUG_NO = "8848368";//TEST.NUM_MISCHARRI;

    /*customer yard*/
    public static boolean fromDocActivity; //  after finishing signing customer yard doc
    public static String carNumber; // carNumber from previuos screen

    private static CarNumberMischarriMesirraActivity inst;

//	public static String number;


    public static boolean car_is_private = true;


    @ViewById
    TextView caption;

    @ViewById
    EditText car_number; //==login

    @ViewById
    Button btn_ok; // send  void btn_ok() 


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inst = this;
        lastForm = null;
        car_is_private = true;
        GetCarDetails_Response.carDetails_response = null;
//    	number = ""; // clear old value
    }

    @AfterViews
    void post_onCreate() {
        CarNumberValue.clear();
        if (DeviceAndroidId.is_lab_device(this)) {
            car_number.setText(DEBUG_NO);
//			number = CarNumberUtils.format_car_number(DEBUG_NO);
        }

        System.out.println("HELP  " + carNumber);
        if (fromDocActivity) {
            BreadCrumbs.set("מסחרי");
            BreadCrumbs.append("מסירה");
            System.out.println("MY_CAR_NUMBER  " + carNumber);
            car_number.setText(CarNumberMischarriMesirraActivity.carNumber);
            logIn();
            /* initial data */
            CarNumberMischarriMesirraActivity.fromDocActivity = false;
            CarNumberMischarriMesirraActivity.carNumber = "";
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        inst = null;
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
//    	caption.setText(MainMenuActivity.caption);
        caption.setText(BreadCrumbs.get());
    }


    @Click
    void btn_ok() {
        logIn();
    }

    private void logIn(){
        GetCarDetails_Response.carDetails_response = null;
//    	number = "";
        String carNum = car_number.getText().toString();
        CarNumberValue.set(carNum);

        if (_DEBUG) {
            open_progress_dialog("מעביר נתונים לשרת...");
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    open_next_activity();
                }
            }, 800);
        } else {
            if (carNum == null || carNum.length() != 7) {
                toast("מספר הרכב אינו חוקי!");
                return;
            }
            carNum = CarNumberUtils.format_car_number(carNum);
            if (!NetworkState.is_connected(this)) {
                toast("אין חיבור רשת פעיל!");
                return;
            }
            open_progress_dialog("מתחבר לשרת. אנא המתן...");
            bg_send_car_number(carNum);
        }
    }


    @Background
    void bg_send_car_number(final String car_no) {
        final String action = ActionValue.get();

        // if came from customer yard set "Action":"D" and "MenuAction":"B"
        if(fromDocActivity){
            ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
            MainActionValue.set("B");
        }

//		boolean goto_next_activity = true;				
        try {
//			boolean skip_server = false;
//			final String MESIRRA = Values.Mesirrat_Rechev.value;
//
//			if (MESIRRA.equals(action)) { //!!
//				if (lastForm != null) {
//					car_is_private = lastForm.is_p_or_pm();
//					skip_server = true;
//				}
//			}
//
//			if (!skip_server) {
//				goto_next_activity = get_car_details_from_server(car_no, action);		
//			}

            boolean success = get_car_details_from_server(car_no, action);

            if (success) {
                open_next_activity(); // move to next activity even if error
            }
        } finally {
            close_progress_dialog();
        }

    }


    private boolean get_car_details_from_server(String car_no, String action) {
//		boolean goto_next_activity = true;
        GetCarDetails_Response.clear();
        GetCarDetails_Request req = new GetCarDetails_Request(car_no);
        String req_str = GGson.toJson(req);
        try {
            Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
            // if success - keep "privateness" of car
            if (res instanceof GetCarDetails_Response) {
                car_is_private = GetCarDetails_Response.is_p_or_pm();
                return true; //success
            } else {
                do_toast("לא ניתן לקבל פרטי הרכב: " + "התקבלו נתונים שגויים מהשרת");
            }
        } catch (ConnectionError e) {
            Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
            e.printStackTrace();
//			do_toast("ארעה תקלה בחיבור  " );
            do_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
        } catch (ActionFailedError e) {
            Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
            do_toast(e.getReasonDesc());
            boolean stay_in_screen = !e.is_recoverable_error(); // == 99
//			goto_next_activity = !stay_in_screen; 
        } catch (Exception e) {
            // do not propagate
            do_toast("לא ניתן לקבל פרטי הרכב: " + e);
        }

        return false;
    }


    @UiThread
    void do_toast(String msg) {
        toast(msg);
    }

    @UiThread
    void open_next_activity() {
        FuelmesirraHachzarraActivity_.intent(this).start();
    }

    public static void call_finish() {
        if (inst != null) {
            inst.finish();
        }

    }


}

