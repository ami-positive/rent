package com.sixt.android.activities.customerYard.pdf;

import android.os.AsyncTask;

import com.sixt.android.activities.customerYard.utils.BitmapUtil;

import java.io.File;

/**
 * Created by natiapplications on 24/12/15.
 */
public class FileDownloaderTask extends AsyncTask<String, Boolean, Boolean> {



    private String filePath;
    private String errDesc;
    private FileDownloaderCallback callback;

    public FileDownloaderTask (FileDownloaderCallback callback){
        this.callback = callback;
    }


    @Override
    protected Boolean doInBackground(String... strings) {

        boolean state = false;

        String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf

        try{
            this.filePath = BitmapUtil.createStringPathForStoringMedia( "OriginalPDFFromAsset.pdf");
            PDFUtil.downloadFile(fileUrl, new File(filePath));
            state = true;
        }catch (Exception e){
            e.printStackTrace();
            this.errDesc = e.getMessage();
        }


        return state;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (callback != null){
            callback.onDownload(aBoolean.booleanValue(),filePath,errDesc);
        }

    }


    public interface FileDownloaderCallback {
        public void onDownload(boolean success,String filePath,String errDesc);
    }

}
