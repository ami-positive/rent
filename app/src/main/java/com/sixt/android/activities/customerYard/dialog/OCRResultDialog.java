/**
 * 
 */
package com.sixt.android.activities.customerYard.dialog;


import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.LicenseDrive;
import com.sixt.rent.R;


/**
 * @author natiapplications
 *
 */
public class OCRResultDialog extends BaseDialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "OCRResultDialog";


	public EditText etFirstNameEng;
	public EditText etLastNameEng;
	public EditText etFirstName;
	public EditText etLastName;
	public EditText etLicenseNumber;
	public EditText etID;
	public EditText etAddress;

	public EditText etCity;
	public EditText etStartDate;
	public EditText etValidity;
	public EditText etBirthDate;
	public EditText etType;


	public Button btnOk;
	public Button btnCancel;

	private LicenseDrive licenseDriveToShow;

	
	private DialogCallback callback;
	

	public OCRResultDialog(){}

	public void setLicenseDriveToShow (LicenseDrive licenseDrive){
		this.licenseDriveToShow = licenseDrive;
	}

	public void setCallback (DialogCallback callback){
		this.callback = callback;
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_ocr_result, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		etFirstNameEng = (EditText)view.findViewById(R.id.etFirstNameEng);
		etLastNameEng = (EditText)view.findViewById(R.id.etLastNameEng);
		etFirstName = (EditText)view.findViewById(R.id.etFirstName);
		etLastName = (EditText)view.findViewById(R.id.etLastName);
		etLicenseNumber = (EditText)view.findViewById(R.id.etLicensNumber);
		etID = (EditText)view.findViewById(R.id.etID);
		etAddress = (EditText)view.findViewById(R.id.etAddress);
		etCity = (EditText)view.findViewById(R.id.etCity);
		etStartDate = (EditText)view.findViewById(R.id.etStartDate);
		etValidity = (EditText)view.findViewById(R.id.etValidity);
		etType = (EditText)view.findViewById(R.id.etLicenseType);
		etBirthDate = (EditText)view.findViewById(R.id.etBirthDate);

		btnCancel = (Button)view.findViewById(R.id.btnCancel);
		btnOk = (Button)view.findViewById(R.id.btnOk);
        
        btnCancel.setOnClickListener(this);
		btnOk.setOnClickListener(this);

	/*	if (licenseDriveToShow != null){
			etFirstNameEng.setText(licenseDriveToShow.getFirstNameEngOCR());
			etLastNameEng.setText(licenseDriveToShow.getLastNameEngOCR());
			etFirstName.setText(licenseDriveToShow.getFirsNameOCR());
			etLastName.setText(licenseDriveToShow.getLastNameOCR());
			etLicenseNumber.setText(licenseDriveToShow.getLicenseNumberOCR());
			etID.setText(licenseDriveToShow.getUserIdOCR());
			etAddress.setText(licenseDriveToShow.getAddressOCR());
			etCity.setText(licenseDriveToShow.getCityOCR());
			etStartDate.setText(licenseDriveToShow.getLicenseStartDate());
			etValidity.setText(licenseDriveToShow.getLicenseEndDate());
			etBirthDate.setText(licenseDriveToShow.getDriverBirthDate());
			etType.setText(licenseDriveToShow.getLicenseType());
		}*/

		etFirstNameEng.setText(MyApp.lsdManager.getStrFirstNameEng());
		etLastNameEng.setText(MyApp.lsdManager.getStrLastNameEng());
		etFirstName.setText(MyApp.lsdManager.getStrFirstNameHeb());
		etLastName.setText(MyApp.lsdManager.getStrLastNameHeb());
		etLicenseNumber.setText(MyApp.lsdManager.getStrDrivinLicenseNumber());
		etID.setText(MyApp.lsdManager.getStrIdNumber());
		etAddress.setText(MyApp.lsdManager.getStrAddress());
		etCity.setText("");
		etStartDate.setText(MyApp.lsdManager.getStrDateOfIssue());
		etValidity.setText(MyApp.lsdManager.getStrDateOfExpiry());
		etBirthDate.setText(MyApp.lsdManager.getStrDateOfBirth());
		etType.setText("");
 
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if (callback != null){
			callback.onDismiss(this,null);
		}
	}

	@Override
	public void onClick(View v) {
				
    	dismissDialog();

		if (v.getId() == R.id.btnOk){

			licenseDriveToShow.setFirsName(etFirstName.getText().toString());
			licenseDriveToShow.setLastName(etLastName.getText().toString());
			licenseDriveToShow.setLicenseNumber(etLicenseNumber.getText().toString());
			licenseDriveToShow.setUserID(etID.getText().toString());
			licenseDriveToShow.setAddress(etAddress.getText().toString());
			licenseDriveToShow.setFirstNameEng(etFirstNameEng.getText().toString());
			licenseDriveToShow.setLastNameEng(etLastNameEng.getText().toString());
			licenseDriveToShow.setCity(etCity.getText().toString());
			licenseDriveToShow.setLicenseStartDate(etStartDate.getText().toString());
			licenseDriveToShow.setLicenseEndDate(etValidity.getText().toString());
			licenseDriveToShow.setDriverBirthDate(etBirthDate.getText().toString());
			licenseDriveToShow.setLicenseType(etType.getText().toString());

			if(callback != null){
				callback.onDialogButtonPressed(v.getId(), this, licenseDriveToShow);
			}
		}
	}




	

	
}

