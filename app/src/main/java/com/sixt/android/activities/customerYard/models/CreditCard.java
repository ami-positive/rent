package com.sixt.android.activities.customerYard.models;

import java.io.Serializable;

/**
 * Created by natiapplications on 28/12/15.
 */
public class CreditCard implements Serializable {

    private String amount;
    private String currencyCode;
    private String cardNumber;
    private int monthValidity;
    private int yearValidity;
    private String cvv;
    private String userID;
    private String userName;
    private String firstName;
    private String lastName;


    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getMonthValidity() {
        return monthValidity;
    }

    public void setMonthValidity(int monthValidity) {
        this.monthValidity = monthValidity;
    }

    public int getYearValidity() {
        return yearValidity;
    }

    public void setYearValidity(int yearValidity) {
        this.yearValidity = yearValidity;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "amount='" + amount + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", monthValidity=" + monthValidity +
                ", yearValidity=" + yearValidity +
                ", cvv='" + cvv + '\'' +
                ", userID='" + userID + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
