package com.sixt.android.activities.customerYard.utils;

import android.graphics.Bitmap;

/**
 * Created by natiapplications on 24/12/15.
 */
public interface SignatureDialogListener {

    public void onSign (Bitmap signature);

}
