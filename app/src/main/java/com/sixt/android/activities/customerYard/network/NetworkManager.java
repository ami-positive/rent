package com.sixt.android.activities.customerYard.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.requests.RequestObject;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


/**
 * Created by natiapplications on 16/08/15.
 */
public class NetworkManager {

    public static final String BASE_URL = "http://*";
    public static final String CREDIT_GUARD = "212.150.71.198:2013";
    public static final String DEV = "212.150.71.199:2015";
    public static final String PROD = "212.150.71.199:2013";



    public static final int REMOTE_CONT_TYPE_Gateway = 0;
    public static final int REMOTE_CONT_TYPE_CLOUD = 1;


    private Context context;
    private RestAdapter restAdapter;
    private ApiInterface apiInterface;
    // connectivity manager to check network state before send the requests
    private ConnectivityManager connectivityManager;

    private NetworkManager(Context context) {
        this.context = context;
    }

    private static NetworkManager instance;

    public static NetworkManager getInstance (Context context) {
        if (instance == null){
            instance = new NetworkManager(context);
        }
        instance.initRestAdapter(MyApp.serverEndPoint, false);
        return instance;
    }

    public ApiInterface initRestAdapter (String serverEndPoint, boolean extandTimeOut){

       /* OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(15, TimeUnit.SECONDS);
        OkClient okClient = new OkClient(client);*/



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL.replace("*",serverEndPoint))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(extandTimeOut ? getOkClient2() :getOkClient())
                .build();

        apiInterface = restAdapter.create(ApiInterface.class);

        return  apiInterface;

    }

    public void makeRequest (RequestObject request,NetworkCallback<?> callback){
        if (request != null){
            request.request(this.apiInterface,callback);
        }
    }


    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) MyApp.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isWifiAvailable () {
        ConnectivityManager connManager = (ConnectivityManager)MyApp.appContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        return false;
    }


    public static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    if (hostname.equals("https://somevendor.com"))
                        return true;
                    else
                        return false;
                }
            });

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static OkClient getOkClient (){
        OkHttpClient client1 = new OkHttpClient();
        client1 = getUnsafeOkHttpClient();
        client1.setConnectTimeout(30, TimeUnit.SECONDS); // connect timeout
        client1.setReadTimeout(30, TimeUnit.SECONDS);
        OkClient _client = new OkClient(client1);
        return _client;
    }

    public static OkClient getOkClient2 (){
        OkHttpClient client1 = new OkHttpClient();
        client1 = getUnsafeOkHttpClient();
        client1.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout
        client1.setReadTimeout(60, TimeUnit.SECONDS);
        OkClient _client = new OkClient(client1);
        return _client;
    }






}
