package com.sixt.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.sixt.android.app.json.request.CheckCarExists_Request;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.GetRejects_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.AppContext;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.JsonDebug;
import com.sixt.android.app.util.NetworkConnection;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;


@EActivity
public class ZTestActivity extends Activity {

	//	private static final String REMOTE_FILENAME = "testtest33.jpg";
	private static final String DEBUG_LOGIN = "300157492"; //"29435555"; 
	private static final String DEBUG_ACTION_TYPE = ActionValue.Values.Knissa_Lemigrash.value;
	private static final String DEBUG_CAR_NO = "30-981-75";

	private static final int DLG_ERROR = 213;
	private static final int DLG_SUCCESS = 4353;

	private static final boolean USE_DEBUG_URL = false; // for heb tests 


	@ViewById
	Button btn_auth;
	@ViewById
	Button btn_getRejects;
	@ViewById
	Button btn_chekCarExists;
	@ViewById
	Button btn_getCarDetails;
	@ViewById
	Button btn_sendForm;
	@ViewById
	Button btn_sendMultiForm;



	public static Activity inst;


	@Click
	void btn_auth() {
		run_auth();
	}
	@Click
	void btn_getRejects() {
		run_getRejects();
	}
	@Click
	void btn_chekCarExists() {
		run_chekCarExists();
	}

	@Click
	void btn_getCarDetails() {
		run_getCarDetails();
	}
	@Click
	void btn_sendForm() {
		run_sendForm();
	}
	@Click
	void btn_sendMultiForm() {
		run_sendMultiForm();
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.debug_testlayout);
		AppContext.appContext = getApplicationContext();


		//		boolean ok1 = IdValidator.validate("5911627");
		//		boolean ok2 = IdValidator.validate("59116277");
		//		boolean ok3 = IdValidator.validate("05911627");
		//		boolean ok4 = IdValidator.validate("005911627");
		//		boolean ok5 = IdValidator.validate("059116277");
		//		int jj=234;
		//		jj++;

	}

	protected void old__onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inst = this;		
		new Thread() {
			public void run() {
				debug_run_tests();
			};
		}.start();
		//		setContentView(R.layout.activity_main); 
	}



	//---------------------------------------------------------------
	void debug_run_tests() {
		//			final String FILE_EXT = ".jpg";
		//			String remote_Filename = FileUtils.get_randon_filename(FILE_EXT);
		//			File local_file = new File("/mnt/sdcard/yytt2" + FILE_EXT);      
		//			long ll = local_file.length();
		//			FtpUploader.upload(local_file, remote_Filename);
		//			WebServices.activate("hello world!");

//		LoginActivity.the_login = DEBUG_LOGIN; 
//		Authentication_Request r = new Authentication_Request(DEBUG_LOGIN);
//		String str = GGson.toJson(r);
//		try {
//			JsonTransmitter.send_blocking(str, USE_DEBUG_URL); 
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return;
//		}


		//		GetRejects_Request r2 = new GetRejects_Request();
		//		String str2 = GGson.toJson(r2); 
		//		try {
		//			HttpSender.send_blocking(str2);
		//		} catch (ConnectionError e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		} 


//		CarNumberActivity.number = DEBUG_CAR_NO;


		//		CheckCarExists_Request req7 = new CheckCarExists_Request(DEBUG_CAR_NO, DEBUG_ACTION_TYPE); 
		//		String str7 = GGson.toJson(req7);
		//		try {
		//			HttpSender.send_blocking(str7);
		//		} catch (ConnectionError e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//			return;
		//		}
		//		int jj=234;
		//		jj++;


		GetCarDetails_Request req = new GetCarDetails_Request(DEBUG_CAR_NO); 
		String str3 = GGson.toJson(req);
		try {
			JsonTransmitter.send_blocking(str3, USE_DEBUG_URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		int jj=234;  
		jj++;


		SendForm_Request req3 = new SendForm_Request(ZTestActivity.this, true, null, null); // AFTER GetCarDetails !! 
		String str4 = GGson.toJson(req3);
		try {
			JsonTransmitter.send_blocking(str4, USE_DEBUG_URL);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		jj=234;
		jj++;

		//		SendMultiForm_Request smf = new SendMultiForm_Request(this); 
		//		String str4 = GGson.toJson(smf);
		//		try {
		//			HttpSender.send_blocking(str4);
		//		} catch (ConnectionError e) {
		//			e.printStackTrace();
		//			return;
		//		}
		//		int jj=234; 
		//		jj++;

		int jjj=324;
		jjj++;
	}


	@UiThread
	protected void debug_handle_error(Exception e) {
		//		showDialog(DLG_ERROR);
		create_dlg("ERROR", JsonDebug.get_request(), JsonDebug.get_response());
	}

	@UiThread
	void debug_handle_success() {
		//		showDialog(DLG_SUCCESS);
		create_dlg("SUCCESS", JsonDebug.get_request(), JsonDebug.get_response());
	}

	//
	//	@SuppressWarnings("deprecation")
	//	@Override
	//	protected Dialog onCreateDialog(int id) {
	//		String txt = null;
	//		if (id == DLG_ERROR) {
	//			txt = "ERROR";
	//		}
	//		else if (id==DLG_SUCCESS) {
	//			txt = "SUCCESS";
	//		}
	//		if (txt != null) {
	//			return create_dlg(txt, JsonDebug.get_request(), JsonDebug.get_response());
	//		}
	//		return super.onCreateDialog(id);
	//	}
	//	

	void check_network_connected() {
		if (!NetworkConnection.is_connected(this)) {
			JsonDebug.set_request("Device is Not Connected to Network!"); 
			throw new RuntimeException();
		}
	}



	public void run_auth() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try { 
					check_network_connected();
					block_auth();
					debug_handle_success();					
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}


	public void run_getRejects() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try { 
					check_network_connected();
					block_auth();
					block_getRejects();
					debug_handle_success();
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}


	public void run_chekCarExists() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try { 
					check_network_connected();
					block_auth();
					block_chekCarExists();
					debug_handle_success();
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}



	public void run_getCarDetails() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try { 					
					check_network_connected();
					block_auth();
					block_getCarDetails();
					debug_handle_success();
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}


	public void run_sendForm() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try {
					check_network_connected();
					block_auth();
					block_getCarDetails(); // required for sendForm!
					block_sendForm();
					debug_handle_success();
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}


	void run_sendMultiForm() {
		new Thread() {
			public void run() {
				JsonDebug.clear();
				try { 
					check_network_connected();
					block_auth();
					//				block_getCarDetails();
					block_sendMultiForm();
					debug_handle_success();
				}
				catch(Exception e) {
					debug_handle_error(e);
				}
			};
		}.start();

	}


	protected void block_sendMultiForm() {
//		SendMultiForm_Request smf = new SendMultiForm_Request(this, ""); 
//		String str4 = GGson.toJson(smf);
//		try {
//			JsonTransmitter.send_blocking(str4, USE_DEBUG_URL);
//		} catch (Exception e) {
//			e.printStackTrace();
//			//			return;
//			throw new RuntimeException();
//		}
//		int jj=234; 
//		jj++;
	}

	protected void block_sendForm() {
		SendForm_Request req3 = new SendForm_Request(ZTestActivity.this, true, null, null); // AFTER GetCarDetails !! 
		String str4 = GGson.toJson(req3);
		try {
			JsonTransmitter.send_blocking(str4, USE_DEBUG_URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//			return;
			throw new RuntimeException();
		}
		int jj=234;
		jj++;
	}

	protected void block_getCarDetails() {
//		CarNumberActivity.number = DEBUG_CAR_NO;
		GetCarDetails_Request req = new GetCarDetails_Request(DEBUG_CAR_NO); 
		String str3 = GGson.toJson(req);
		try {
			JsonTransmitter.send_blocking(str3, USE_DEBUG_URL);
		} catch (Exception e) {
			Log.e("SIXTJSON", "\n\n\n block_getCarDetails() error " + e + " \n\n\n");
			// TODO Auto-generated catch block
			e.printStackTrace();
			//			return;
			throw new RuntimeException();
		}
		int jj=234;  
		jj++;
	}

	protected void block_chekCarExists() {
//		CarNumberActivity.number = DEBUG_CAR_NO;
		CheckCarExists_Request req7 = new CheckCarExists_Request(DEBUG_CAR_NO, DEBUG_ACTION_TYPE); 
		String str7 = GGson.toJson(req7);
		try {
			JsonTransmitter.send_blocking(str7, USE_DEBUG_URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//			return;
			throw new RuntimeException();
		}
		int jj=234;
		jj++;
	}

	protected void block_getRejects() {
		GetRejects_Request r2 = new GetRejects_Request();
		String str2 = GGson.toJson(r2); 
		try {
			JsonTransmitter.send_blocking(str2, USE_DEBUG_URL);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException();
		} 
	}

	protected void block_auth() {
//		LoginActivity.login = DEBUG_LOGIN; 
//		Authentication_Request r = new Authentication_Request(DEBUG_LOGIN);
//		String str = GGson.toJson(r);
//		try {
//			JsonTransmitter.send_blocking(str, USE_DEBUG_URL);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			//			return;
//			throw new RuntimeException();
//		}
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}


	public Dialog create_dlg(String txt, String req, String res) {
		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setTitle(txt);
		builder1.setMessage("Request: \n\n" + req + "\n\nResponse:\n" + res);  
		builder1.setCancelable(true);
		builder1.setNeutralButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert11 = builder1.create();
		alert11.show();
		return alert11;
	}

	//////////////////// igal test /////////////////////////


//	private static final String SERVER_URL ="http://localhost:8080/testing/test.jsp";
//	private static final String SERVER_URL ="http://79.183.5.83:8080/testing/test.jsp";
	private static final String SERVER_URL ="http://79.177.17.232:8080/testing/test.jsp";
	private static final int CONNECTION_TIMEOUT = 5000; 
	private static final int SOCKET_TIMEOUT = 7000;

	public static void igal_connect() {
		new Thread() {
			public void run() {
				String jsonString = "{\"Authentication_req\":{\"App\":\"L\",\"Debug\":\"אבגדהוזחטי\",\"Login\":\"29435555\",\"Pass\":\"29435555\",\"msg_type\":\"Authentication_Request\"}}";

				try {
					HttpPost request = new_get_post_obj(jsonString); //giladnew 

					HttpParams connParams = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(connParams, CONNECTION_TIMEOUT);
					HttpConnectionParams.setSoTimeout(connParams, SOCKET_TIMEOUT);

					DefaultHttpClient httpClient = new DefaultHttpClient(connParams);
								

					HttpResponse _response = null;
					try {
						_response = httpClient.execute(request);
					} catch (Exception e) {
						//                throw new RTException();
						throw new RuntimeException();
					}

					HttpEntity response_entity = _response.getEntity();
					InputStream is = response_entity.getContent();
					try {
						String response = "";
						int len = 0;
						byte[] data1 = new byte[1024];
						while (-1 != (len = is.read(data1))) {
							response += (new String(data1, 0, len));
						}
						// print response
						System.out.println(response);
						String orig_request = jsonString;

					} finally {
						is.close();
					}

				} catch (Exception e) {
					String errmsg = "Error attempting to send Http request to server: " + e;
					throw new RuntimeException(errmsg);
				}             
				
			};
		}.start();

	}

	private static HttpPost new_get_post_obj(String json) {
		HttpPost request = new HttpPost(SERVER_URL);
		request.setHeader("Content-type", "application/json; text/html; charset=utf-8");
		StringEntity entity;
		try {
			entity = new StringEntity(json, "UTF8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} //giladnew
		request.setEntity(entity);
		return request;
	}

}	



