package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class SendCreditCardRequest extends RequestObject<BaseResponseObject> {


    private CreditCard creditCard;


    public SendCreditCardRequest(CreditCard creditCard){
        this.creditCard = creditCard;
    }

    @SerializedName("SendJ5_Req")
    private CreditCardRequestInner creditCardReqeustInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<BaseResponseObject> callback) {

        initRequestObj ();
        apiInterface.sendCreditCard(creditCardReqeustInner, callback);

    }

    private void initRequestObj() {
        creditCardReqeustInner = new CreditCardRequestInner(this.creditCard);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }



    public class CreditCardRequestInner {


        public CreditCardRequestInner (CreditCard creditCard){
             Sum = Integer.parseInt(creditCard.getAmount());
            CurrencyCode = creditCard.getCurrencyCode();
            CardNo = creditCard.getCardNumber();
            ValidYear = creditCard.getYearValidity();
            ValidMonth = String.valueOf(creditCard.getMonthValidity());
            CVV = creditCard.getCvv();
            Tz = creditCard.getUserID();
            Name = creditCard.getUserName();

        }

        public int Sum;
        public String CurrencyCode;
        public String CardNo;
        public int ValidYear;
        public String ValidMonth;
        public String CVV;
        public String Name;
        public String Tz;

        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "SendJ5_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
