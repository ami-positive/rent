package com.sixt.android.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.db.JsonsTable;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.VerName;
import com.sixt.android.app.json.objects.AvailableBranchesRecord;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.RejectCodeDesc;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.objects.SubRejectValue;
import com.sixt.android.app.json.request.Authentication_Request;
import com.sixt.android.app.json.request.GetAllData_Request;
import com.sixt.android.app.json.request.GetRejects_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.json.response.GetAllData_Response;
import com.sixt.android.app.json.response.GetRejects_Response;
import com.sixt.android.app.json.response.GetRejects_Response_Inner;
import com.sixt.android.app.json.response.SendForm_Response;
import com.sixt.android.app.location.MyLocationManager;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.AppContext;
import com.sixt.android.app.util.BranchNoValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.LoginSixt;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.NetworkState;
import com.sixt.android.app.util.TEST;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;


@EActivity(R.layout.login_layout)
@NoTitle
public class LoginActivity extends BaseActivity {

	public static String the_pwd;   

	private static final boolean _DEBUG = TEST.debug_mode(false);

	protected static final int _MINUTE = 1000*60;

//	public static String the_VersionName = null;

	public static LoginActivity inst;
	 
//	 gggggggggggggg -- paid fixes below
	// after 20 minutes > throw user after user moving to next screen / pressig send
	//        timer starts upon entering screen
	// custom error dialog for all and only errors
	// Reject List: special color for selected ok and NOT ok items (BG Color) (GREEN/RED)
	



	//	public static volatile String the_login;

	//	private final String TRANSPORT_USER = "32700155"; 
	//	private final String RENT_USER = "DavidBe"; 	 
	//	private final String RENT_USER = "SHIRANA";
	//	private final String RENT_PWD = "2561";
	//	private final String RENT_USER = "davidbe"; 
	//	private final String RENT_PWD = "87000";
//	private final String RENT_USER = "avinoamz";
//	private final String RENT_PWD = "1912";
	private final String RENT_USER = /*"sharonp"*/"";
	private final String RENT_PWD = /*"1234567"*/"";
	//	private final String RENT_PWD = "87000";
	//	private final String TRANSPORT_USER = "38314050"; //"32700155";    


	@ViewById
	EditText username; //==login
	@ViewById
	EditText password; //==login	
	@ViewById
	TextView txt_apk_ver;


	@ViewById
	Button loginButton; //send

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);        
		inst = this;
		//  MyDateFormatter.format_now();
		AppContext.appContext = getApplicationContext();

		VerName.set_ver_code(this);
		
		int num_silent_trasmit = JsonsTable.get().getRecordCount();
		int jj=234;
		jj++;
		BreadCrumbs.set("");

		//	LprTester.http_send_file(this); // remove!! 

		MyLocationManager.get(); // initialize!

		//	turn_on_gps(); //

		//		debug_delete_old_photos();

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (/*DeviceAndroidId.is_sharon_device(this) || DeviceAndroidId.is_lab_device(this)*/true) {
			getMenuInflater().inflate(R.menu.select_server_mode, menu);
			return true;
		}
		else {
			return super.onCreateOptionsMenu(menu);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		int i = item.getItemId();
		switch (i)
		{
		//		case R.id.menu_server_prod_csp:
		//			JsonTransmitter.setUrl(1);
		//			return true;

		case R.id.menu_server_prod_http:
			JsonTransmitter.setUrl(2); 
			return true;

		case R.id.menu_server_dev:
			JsonTransmitter.setUrl(0);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}




	// >> block from no mandatory transmit; add 'is israeli id' selector; LPR support



	//	private boolean turn_on_gps() {
	//		Context context = this.getApplicationContext();
	//		String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	//		if (provider.contains("gps")) {
	//			// gps already on
	//			return false;
	//		}
	//		final Intent poke = new Intent();
	//		poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
	//		poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	//		poke.setData(Uri.parse("3")); 
	//		context.sendBroadcast(poke);
	//		return true;
	//	}  

	//	static int debg__ctr = 0;
	//	private void debug_delete_old_photos() {
	//		debg__ctr = 0;
	//		if (DeviceAndroidId.is_lab_device(this)) {
	//			File sdcard = new File("/mnt/sdcard/");
	//			dodo_dolder(sdcard);
	//		}
	//		int jj=234;
	//		jj = debg__ctr;
	//		Toast.makeText(this, "num == " + debg__ctr, Toast.LENGTH_LONG);
	//	}
	//
	//	private void dodo_dolder(File folder) { 
	//		File[] content = folder.listFiles();
	//		final long WEEK = 1000*60*60*24*7;
	//		final long TIMEPERIOD = 6 * WEEK; 
	//		for (File f: content) {
	//			if (f.isDirectory()) {
	////				dodo_dolder(f);
	//			}
	//			else if (f.getName().startsWith("carimg_") &&
	//					(System.currentTimeMillis() - f.lastModified() < TIMEPERIOD)) {
	//				f.delete();
	//				debg__ctr++;
	//				int jj=324;
	//				jj++;
	//			}
	//		}
	//	}
	//


	@AfterViews
	void post_onCreate() {

		username.setText("");
		password.setText("");
		show_ver_name();
		if (DeviceAndroidId.is_lab_device(this)) {
			username.setText(RENT_USER);
			password.setText(RENT_PWD); 
		}		
	}

	private void show_ver_name() { 
		txt_apk_ver.setText("השכרה " + VerName.the_VersionName); 
		//		try {
		//			String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		//			txt_apk_ver.setText("לוגיסטיקה " + versionName);
		//		} 
		//		catch (NameNotFoundException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace(); 
		//			txt_apk_ver.setText("לוגיסטיקה " );
		//		}
	}

	@Override
	protected void onResume() { 
		super.onResume();

		FuelmesirraHachzarraActivity.isTheLastScreen = false; // init

		//		debug_show_device_id();
	}


	//	private void debug_show_device_id() {
	//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//		builder.setMessage(DeviceAndroidId.getId(this))
	//		       .setCancelable(false)
	//		       .setPositiveButton("", new DialogInterface.OnClickListener() {
	//		           public void onClick(DialogInterface dialog, int id) {
	//		                //do things
	//		           }
	//		       });
	//		AlertDialog alert = builder.create();
	//		alert.show();				
	//	}



	@Override 
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}
	
//	@Click (R.id.loginButton)
//	public void startMainMenu(){
//
//		OkCancelDialog.show(this, "ראשית דגכדג דגכ דגכ דג כדג כדג כדג כ דגכ דגכ דג כדג כדגכדג כדגכד סוף", 
//				new Runnable() {
//					
//					@Override
//					public void run() {
//						int okok=2342;
//						okok++;
//						okok++;
//						okok--;
//						
//					}
//				}, 
//				new Runnable() {					
//					@Override
//					public void run() {
//						int cancelcancel = 23423;
//						cancelcancel--;
//						cancelcancel--;
//						cancelcancel--;
//						
//					}
//				} );
//	}
	

	@Click(R.id.loginButton)
	public void startMainMenu(){
		the_pwd = password.getText().toString();

		//		SignatureActivity_.intent(this).start();

		if (_DEBUG) { 			
			open_progress_dialog("מתחבר לשרת. אנא המתן...");
			new Timer().schedule(new TimerTask() {				
				@Override
				public void run() {
					open_menu_activity();
				}
			}, 600);    
		} 
		else {
			do_connect();
		} 
	}



	protected void debug_run_json_test() { 

		String req_str;

		LoginSixt.set_password(RENT_PWD);


		//////		//auth  
		//		Authentication_Request req = new Authentication_Request(RENT_USER, RENT_PWD); 
		//		req_str = GGson.toJson(req);
		//		try {
		//			Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
		//			if (response==null || !(response instanceof Authentication_Response)) {
		//				throw new ConnectionError("תקלה בחיבור לשרת");
		//			}
		//			Authentication_Response res = (Authentication_Response) response;
		//			int jj=234;
		//			jj++;
		//		}
		//		catch (Exception e) {
		//			int jj=234;
		//			jj++;
		//		}
		//


		//		// car details
		//		LoginSixt.set_password("2561");
		//		BranchNoValue.set(64, "");
		//		MainActionValue.set("I");
		//		ActionValue.set(ActionValue.Values.Pticha);
		////		GetCarDetails_Request req2 = new GetCarDetails_Request("4204776"); 
		//		GetCarDetails_Request req2 = new GetCarDetails_Request("8361873");
		//		req_str = GGson.toJson(req2);
		//    	try {  
		//			Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
		//			// if success - keep "privateness" of car
		//			if (res instanceof GetCarDetails_Response) {
		//				int jj=234;
		//				jj++;
		//			}			
		//		} 
		//    	catch (ConnectionError e) {
		//			Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
		//			e.printStackTrace();
		////			do_toast("ארעה תקלה בחיבור  " );
		//		} 
		//    	catch (ActionFailedError e) {
		//			Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
		////			do_toast(e.getReasonDesc());
		//			boolean stay_in_screen = !e.is_recoverable_error(); // == 99
		////			goto_next_activity = !stay_in_screen; 
		//		} 
		//    	catch (Exception e) {
		//    		// do not propagate
		//    		int jj=234;
		//    		jj++;
		//    	}
		//    	 

		////		// all data
		//		MainActionValue.set("I");
		//		ActionValue.set(ActionValue.Values.Pticha);
		//		LoginSixt.set_password(RENT_PWD);
		//		GetAllData_Request req4 = new GetAllData_Request(); 
		//		req_str = GGson.toJson(req4);
		//    	try {
		//			Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
		//			// if success - keep "privateness" of car
		//			if (res instanceof GetAllData_Response) {
		//				int jj=234;
		//				jj++;
		//			}			
		//		} 
		//    	catch (Exception e) {
		//			Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
		//			e.printStackTrace();
		//    	}
		//    	
		//    	
		// 
		////    	// rejects
		//    	LoginSixt.set_password(RENT_PWD);
		//		GetRejects_Request req11 = new GetRejects_Request(); 
		//		req_str = GGson.toJson(req11);
		//		try {
		//			Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
		//			if (response != null && response instanceof GetRejects_Response) {
		//				int jj=234;
		//				jj++;
		//			}
		////			return; //done 
		//		}  
		//		catch (Exception e) {
		//			int jj=435; 
		//			jj++;
		//			try { Thread.sleep(5000);} catch (InterruptedException e1) {}
		//			// and go on
		//		} 


		// send form 
		MainActionValue.set("I");
		ActionValue.set(ActionValue.Values.Pticha);
		LoginSixt.set_password(RENT_PWD);
		SendForm_Request req22 = new SendForm_Request(this, true, "xx.jpeg", null); 
		req_str = GGson.toJson(req22);
		try {
			Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
			// if success - keep "privateness" of car
			if (res instanceof SendForm_Response) {
				int jj=234; 
				jj++;
			}	
			else {
				int jj=24;
				jj++;
			}
		} 
		catch (ConnectionError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
			e.printStackTrace();
			//			do_toast("ארעה תקלה בחיבור  " );
		} 
		catch (ActionFailedError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
			//			do_toast(e.getReasonDesc());
			boolean stay_in_screen = !e.is_recoverable_error(); // == 99
			//			goto_next_activity = !stay_in_screen; 
		} 
		catch (Exception e) {
			// do not propagate
			int jj=234;
			jj++;
		}


	}


	private void do_connect() {    	
		if (!NetworkState.is_connected(this)) {
			toast("אין חיבור רשת!");
			clear_password();
			return;
		}
		String username_txt = username.getText().toString(); 
		String password_txt = password.getText().toString(); 
		if (username_txt==null || username_txt.length()==0) {
			toast("יש להזין שם משתמש");
			clear_password();
			return;    			
		}
		if (password_txt==null || password_txt.length()==0) {
			toast("יש להזין סיסמא");  
			clear_password();
			return;    			
		}
		//		if (!username_txt.equals(password_txt)) { // must be identical
		//			toast("פרטי המשתמש לא חוקיים!");
		//			clear_password();
		//			return;    			
		//		} 
		//		the_login = username_txt; 

		open_progress_dialog("מתחבר לשרת. אנא המתן...");
		bg_attempt_connection(username_txt, password_txt);		    		
	}


	@Background
	void bg_attempt_connection(final String username_txt, final String password_txt) {
		Authentication_Request req = new Authentication_Request(username_txt, password_txt); 
		String req_str = GGson.toJson(req);
		try {
			Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
			if (response==null || !(response instanceof Authentication_Response)) {
				throw new ConnectionError("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
				//				call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
			}
			Authentication_Response res = (Authentication_Response) response;

			start_loadRejects_thread();

			start_GetAllData_thread();

			start_json_sender_thread();


			open_menu_activity();
		}
		catch (ActionFailedError err) {
			call_toast(err.getReasonDesc());
		} 
		catch (ConnectionError e) {
			e.printStackTrace();
			//			call_toast("תקלה בחיבור לשרת");
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		finally {
			clear_password();
			close_progress_dialog();
		}
	} 




	private void start_GetAllData_thread() {

		final long _DAY = 1000*60*60*20; //24;
		long interval = AllDataRepository.time_since_last_allData(); 
		if (interval != -1 && interval < _DAY) {
			return; 
		}

		new Thread() {
			public void run() {
				final int MAX_ATTEMPTS = 5;
				for (int ii = 0; ii < MAX_ATTEMPTS; ii++) {
					if (ii > 0 && has_stored_rejects()) {
						break;
					}
					GetAllData_Request allData= new GetAllData_Request();  
					String req_str = GGson.toJson(allData);
					try {
						Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
						if (response != null && response instanceof GetAllData_Response) {
							int jj=234;
							jj++; // ok
							do_toast("מידע ראשוני מהשרת התקבל בהצלחה!" , true); 
						}
						return; //done 
					}  
					catch (Exception e) {
						int jj=435; 
						jj++;
						try { Thread.sleep(5000);} catch (InterruptedException e1) {}
						// and go on
					} 
				}				
			}
		}.start();
	}


	@UiThread
	void do_toast(String text) {
		toast(text);
	}

	@UiThread
	void do_toast(String text, boolean success) {
		toast(text, success);
	}


	@UiThread
	void clear_password() {
		if (!DeviceAndroidId.is_lab_device(this)) {
			password.setText("");
		}
	}


	private static boolean start_json_sender_thread_started = false;



	private void start_json_sender_thread() {
		if (start_json_sender_thread_started) {
			return; 
		}
		
		new Thread() {
			public void run() {
				start_json_sender_thread_started = true;
				setPriority(Thread.NORM_PRIORITY-2); 
				
				try { JsonsTable.get().delete_old_image_folders(); } catch(Exception e) {} //ggggfix
				
				for (;;) {      
					try { Thread.sleep(3*_MINUTE); } catch(Exception e) {} //return 
					//					try { Thread.sleep(4*1000); } catch(Exception e) {} //

					try { JsonsTable.get().send_all_pending_jsons(); } catch(Exception e) {}
				}
			}
		}.start();

	}

	@UiThread
	void call_toast(String msg) {
		toast(msg);		
	}


	private void start_loadRejects_thread() {
		new Thread() {
			public void run() {
				final int MAX_ATTEMPTS = 5;
				for (int ii = 0; ii < MAX_ATTEMPTS; ii++) {
					if (ii > 0 && has_stored_rejects()) {
						int jj=345;
						jj++;
						break;
					}
					GetRejects_Request req = new GetRejects_Request(); 
					String req_str = GGson.toJson(req);
					try {
						Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
						if (response != null && response instanceof GetRejects_Response) {
							int jj=345;
							jj++;
							//							correct_rejects_strings((GetRejects_Response)response); //
						}
						return; //done 
					}  
					catch (Exception e) {
						int jj=435; 
						jj++;
						try { Thread.sleep(5000);} catch (InterruptedException e1) {}
						// and go on
					} 
				}				
			}
		}.start();
	}  



	protected static boolean has_stored_rejects() {
		GetRejects_Reject[] rej_arr = GetRejects_Response.get_all_available_rejects(true);
		if (rej_arr != null && rej_arr.length > 0) {
			return true;
		}
		rej_arr = GetRejects_Response.get_all_available_rejects(false);
		if (rej_arr != null && rej_arr.length > 0) {
			return true;
		}
		return false;
	}

	protected void correct_rejects_strings(GetRejects_Response response) {
		GetRejects_Response_Inner inner = response.GetRentRejects_res;
		GetRejects_Reject[] reject_arr = inner.Rejects;
		if (reject_arr==null) { 
			int jj=243;
			jj++;
			return;
		}
		for (GetRejects_Reject r: reject_arr) {
			String orig = r.RejectDesc;
			if (!is_legal_hebrew(orig)) {
				int jj=234;
				jj++;
			}
			RejectCodeDesc[] vals = r.RejectValues;
			if (vals != null) {
				for (RejectCodeDesc v: vals) {
					String desc = v.RejectValueDesc;
					if (!is_legal_hebrew(desc)) {
						int jj=234;
						jj++;
					}
				}

				SubReject[] sr_arr = r.SubReject;
				if (sr_arr != null) {
					for (SubReject sr: sr_arr) {
						String desc = sr.SubRejectDesc;
						if (!is_legal_hebrew(desc)) {
							int jj=234;
							jj++;
						}
						SubRejectValue[] val_arr = sr.SubRejectValues;
						if (val_arr != null) {
							for (SubRejectValue srv: val_arr) {
								String d = srv.SubRejectValueDesc;						
								if (!is_legal_hebrew(d)) {
									int jj=234;
									jj++;
								}
							}
						}
					}
				}
			}
		}  
		int jjj=243; 
		jjj++;

	}


	public static String fixEncoding(String Hebrew1) {
		try {
			byte[] bytes = Hebrew1.getBytes("ISO-8859-1");
			if (!validUTF8(bytes)) {
				return Hebrew1;  
			}
			return new String(bytes, "UTF-8"); 
		} catch (UnsupportedEncodingException e) {
			// Impossible, throw unchecked 
			throw new IllegalStateException("No Hebrew1 or UTF-8: " + e.getMessage());
		}

	}

	public static boolean validUTF8(byte[] input) {
		int i = 0;
		// Check for BOM
		if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
				&& (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
			i = 3;
		}

		int end;
		for (int j = input.length; i < j; ++i) {
			int octet = input[i];
			if ((octet & 0x80) == 0) {
				continue; // ASCII
			}

			// Check for UTF-8 leading byte
			if ((octet & 0xE0) == 0xC0) {
				end = i + 1;
			} else if ((octet & 0xF0) == 0xE0) {
				end = i + 2;
			} else if ((octet & 0xF8) == 0xF0) {
				end = i + 3;
			} else {
				// Java only supports BMP so 3 is max
				return false;
			}

			while (i < end) {
				i++;
				octet = input[i];
				if ((octet & 0xC0) != 0x80) {
					// Not a valid trailing byte
					return false;
				}
			}
		}
		return true;
	}





	//		String all = "";
	//		for (GetRejects_Reject r: reject_arr) {
	//			String desc = r.RejectDesc;
	//			all += " "  + desc;
	////			if (!legal_hebrew(desc)) {
	////				int place_breakpoint_here = 2;
	////				place_breakpoint_here++;
	////			}
	//		} 
	//		all = all;
	//		int place_breakpoint_here=0;
	//		place_breakpoint_here++;		
	//	}

	private static boolean is_legal_hebrew(String desc) {
		if (desc==null) {
			return true;
		}
		for (int i = 0; i < desc.length(); i++) {
			char c = desc.charAt(i);
			if (c != ' ' && c != '\'' && (c > 'ת' || c < 'א')) {

				//U+FFFD � replacement character used to replace an unknown or unrepresentable character
				if (c == '\uFFFD') {
					int jjj=2342;
					jjj++;
				}

				//				if (desc.contains("\uFFFD")) {
				//					int jj = 423;
				//					jj++;
				//				}

				return false;
			}
		}
		return true;
	}


	@UiThread 
	void open_menu_activity() {
		//		MainMenuActivity_.intent(this).start();
		if (_DEBUG) {
			BranchSelectActivity_.intent(this).start();
			return; 
		}
		
		if (has_many_branches()) {
			BranchSelectActivity_.intent(this).start();
		}
		else {
			set_default_branch_number();
			MainMenuActivity_.intent(this).start();
		}
	}


	private void set_default_branch_number() {
		AvailableBranchesRecord[] branches = Authentication_Response.last_AvailableBranches;
		int BranchNo = -1;
		String BranchName = "";
		if (branches != null && branches.length > 0) {
			BranchNo = branches[0].BranchNo;
			BranchName = AllDataRepository.getBranchName(BranchNo);
		}
		BranchNoValue.set(BranchNo, BranchName);
	}

	private static boolean has_many_branches() {
		AvailableBranchesRecord[] branches = Authentication_Response.last_AvailableBranches;
		return branches != null && branches.length > 1; 
	}


	public static void call_finish() {
		if (inst != null) {
			inst.finish(); 
		}		
	}

}
