package com.sixt.android.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LevelEndEvent;
import com.crashlytics.android.answers.LevelStartEvent;
import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity_;
import com.sixt.android.activities.customerYard.gmailsender.GmailSender;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.db.DbUtils;
import com.sixt.android.app.db.JsonsTable;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.dialog.DialogError;
import com.sixt.android.app.json.objects.RefusalReasonRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.request.SendForm_Request_Inner;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.SendForm_Response;
import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CameraUtils;
import com.sixt.android.app.util.CaptionSetter;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.EditTextTo;
import com.sixt.android.app.util.FileUtils;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.LoginSixt;
import com.sixt.android.app.util.NetworkConnection;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.app.util.SdcardImageSaver;
import com.sixt.android.app.util.SignatureDrawDialog;
import com.sixt.android.app.util.SignatureDrawDialogB;
import com.sixt.android.app.util.TEST;
import com.sixt.android.app.util.ViewScreenshot;
import com.sixt.android.ftp.FtpUploader;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.IWebServiceClientFactory;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.httpClient.WebServiceClientFactory;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;

@EActivity(R.layout.mischarri_mesirra_signature_activity)
@NoTitle
public class SignatureActivity extends BaseActivity implements
        ISignatureActivity, Handler.Callback {

    private static final boolean CALLED_BY_SIGNATURE = true;
    private static final boolean _DEBUG = TEST.debug_mode(false);
    private static final int DLG_SEND = 22;
    private static final int DLG_CANCEL = 33;
    private static final int DLG_CLEAR = 44;
    public static String sAddress = "";
    public static String sSendingMean = "";
    public static boolean sRefusedSignature = false;
    public static int sRefusalReasonInd;
    // private static final boolean DEBUG_PDF_FILE = TEST.debug_mode(false);
    public static SignatureActivity inst;
    public static ArrayList<RejectRecord> image_reject_arr;
    public static ArrayList<RejectRecord> list_reject_arr;
    private final int TAG_MESIRRAT_RECHEV = 100;
    private final int TAG_HACHZARRA = 101;
    private final int TAG_SGIRRA = 102;
    public int imageCounterA = 0;
    public static int imageCounterB = 0;
    String signatureRemoteFile;
    @ViewById
    LinearLayout show_decline_container;
    @ViewById
    LinearLayout confirm_layout;
    @ViewById
    CheckBox reject_sign_check; // refuse decline seruv seiruv
    @ViewById
    CheckBox confirm_check; //
    @ViewById
    Spinner decline_reason_spinner; // seruv seiruv HAS first empty line; NO default
    @ViewById
    TextView txt_car_id;
    @ViewById
    Button btn_camera;
    ;
    @ViewById
    FrameLayout signature_container;
    @ViewById
    TextView txt_caption;
    @ViewById
    Button btn_send;
    @ViewById
    Button btn_clear;
    @ViewById
    Button btn_cancel;
    @ViewById
    TextView caption;
    @ViewById
    TextView lbl_email;
    @ViewById
    LinearLayout emailLayout; //
    @ViewById
    EditText email_addr;
    @ViewById
    ToggleButton opt_fax;
    @ViewById
    ToggleButton opt_email;
    @ViewById
    ToggleButton opt_none; // ggg
    @ViewById
    ImageView signatureImage;
    @ViewById


    TextView signatureText;
    private ArrayList<File> all_taken_photos;
    private ArrayList<File> all_REMOTE_photos;
    private File current_photo_file;
    private boolean show_driver_fields;
    private String signature_localFilename;
    private boolean mode_email = true;
    private int currentFormTag;
    private boolean isMultiForm;
    private String transId;
    private Handler handler;
    private AlertDialog signature_draw_dlg;
    private StringBuilder sb;
    private ArrayList<String> file_arr;
    private boolean signature_was_set;
    private int num;

    public static boolean send_pending_forms() {
        boolean all_sent;

        try {
            if (!NetworkConnection.is_connected(inst)) {
                return false;
            }
            all_sent = JsonsTable.get().sendPendingJsonRequest();
        } catch (Exception e) {
            all_sent = false;
        }
        return all_sent;
    }

    private static void populate_photos_arr(ArrayList<File> _photos) {
        SendForm_Request_Inner.reset_photo_arr();
        if (_photos == null) {
            return;
        }
        for (File photo : _photos) {
            String photo_Filename = photo.getName();
            String wrapped = DbUtils.wrap_filename(photo_Filename);
            SendForm_Request_Inner.add_to_photo_arr(wrapped);
        }
    }

    public static String getSuccessMsg(SendForm_Response res2) { // ggg print senddForm success msg from server
        final String DEF_MSG = "הטופס שודר בהצלחה";
        String cur_msg = res2.getSuccessMsg();
        if (cur_msg != null) {
            return DEF_MSG + ": " + cur_msg;
        }
        return DEF_MSG;
    }

    private static ArrayList<RejectRecord> get_all_rejects() {
        ArrayList<RejectRecord> rejects = new ArrayList<RejectRecord>();
        if (image_reject_arr != null) {
            rejects.addAll(image_reject_arr);
        }
        if (list_reject_arr != null) {
            rejects.addAll(list_reject_arr);
        }
        return rejects;
    }

    private static void clear_statics() {
        sAddress = "";
        // sSendingMean = "";
        // sEmailMode = true; // if not: fax
        sRefusedSignature = false;
        sRefusalReasonInd = -1;
        // photo_arr = new ArrayList<File>();
        // signature_image_file = null;
    }

    private static void populate_with_real_data(ArrayAdapter<String> adapter) {
        adapter.add("");
        RefusalReasonRecord[] all = AllDataRepository.getRefusalReasons();
        if (all == null) {
            return;
        }
        for (RefusalReasonRecord r : all) {
            adapter.add(r.RefusalName);
        }
    }

    public static void report(final String title, final String message) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    GmailSender sender = new GmailSender("shlomo.sixt.app@gmail.com", "gabitheking");
//
                    sender.sendMail(title, message, "shlomo.sixt.app@gmail.com", "itzik@positive-apps.com, SharonP@shlomo.co.il, AvinoamZ@shlomo.co.il");

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    MyApp.appSetting.restImageEvent();
                }
            }
        }).start();
    }

    public static void reportX(final String title, final String message) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    GmailSender sender = new GmailSender("shlomo.sixt.app@gmail.com", "gabitheking");
//
                    sender.sendMail(title, message, "shlomo.sixt.app@gmail.com", "itzik@positive-apps.com, AvinoamZ@shlomo.co.il");

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    MyApp.appSetting.restImageEvent();
                }
            }
        }).start();
    }

    public static void call_finish() {
        if (inst != null) {
            inst.finish();
        }
    }

    @Click({R.id.signatureImage, R.id.signatureText})
    void showDialog() {
        signature_draw_dlg.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clear_statics();
        init_photo_arrays();
        handler = new Handler(this);
        UniqueId.set_only_if_notset();
        inst = this;
        currentFormTag = -1;
        initFormSendingMode();
    }

    private void init_photo_arrays() {
        all_taken_photos = new ArrayList<File>();
        all_REMOTE_photos = new ArrayList<File>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        transId = UniqueId.current_transaction_id;
        int DriverExist = GetCarDetails_Response.get_DriverExist();
        boolean Mesirra_Mode = ActionValue.Messira();
        if (Mesirra_Mode) {
            show_driver_fields = false;
        } else {
            show_driver_fields = (DriverExist == 0); // show if driver does NOT	// exist
        }

        set_driver_fields_visibility();
    }

    private void set_driver_fields_visibility() {
        int flg;
        if (show_driver_fields) {
            flg = View.VISIBLE;
        } else {
            flg = View.GONE;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        inst = null;

    }

    @Click
    void btn_send() {
        if(has7picture()) {
            showDialog(DLG_SEND); // display dialogue sendForms if yes click
        }else{
            MyToast.makeText(this, "אנא צלם 7 תמונות או יותר.", Toast.LENGTH_LONG).show();
        }
    }

    private boolean has7picture() {
        if(num >= 7){
            return true;
        }
        return false;
    }

    ;

    @Click
    void btn_cancel() {
        showDialog(DLG_CANCEL); // do_cancel
    }

    @Click
    void btn_clear() {
        showDialog(DLG_CLEAR); // do_clear
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DLG_SEND) {
            return Dialog2Btns.create(this,
                    "האם אתה בטוח כי ברצונך לשלוח את הטופס?", new Runnable() {
                        @Override
                        public void run() {
                            sendForms();
                        }
                    });
        } else if (id == DLG_CANCEL) {
            return Dialog2Btns.create(this,
                    "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {
                        @Override
                        public void run() {
                            do_cancel();
                        }
                    });
        } else if (id == DLG_CLEAR) {
            return Dialog2Btns.create(this,
                    "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
                        @Override
                        public void run() {
                            do_clear();
                        }
                    });
        } else {
            return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(MyApp.appSetting.showImageLoadingProgressBar){
            open_progress_dialog("מעביר נתו" + "נים לשרת..."); // גדכ גדכע
        }
    }

    void do_clear() {
        clear_signature();
        init_photo_arrays();
        clear_statics();
        signature_was_set = false;
        reject_sign_check.setChecked(false);
        confirm_check.setChecked(false);
        decline_reason_spinner.setSelection(0);
        String def_mail = GetCarDetails_Response.getCustMail();
        email_addr.setText(def_mail);
    }

    private void clear_signature() {
        if (signature_draw_dlg != null) {
            SignatureDrawDialog.clear_signature();
        }
        signature_was_set = false;
    }

    void do_cancel() {
        do_finish();
        ActivityUtils.close_all();
    }

    private void initFormSendingMode() {
        final boolean pnimmi_mesirra = PnimmiValue.Pnimmi() && MenuPnimmiActivity.Mesirra();
        final boolean mischari_messira = PnimmiValue.Mischari() && MenuMischarriActivity.Mesirra();
        final boolean hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();
        final boolean pticha = PnimmiValue.Pnimmi() && MenuPnimmiActivity.Pticha();
        final boolean sgirra = PnimmiValue.Pnimmi() && MenuPnimmiActivity.Sgirra();
        final boolean pnimmi = PnimmiValue.Pnimmi();
        final boolean mischarri = PnimmiValue.Mischari();
        isMultiForm = false;
        if ((mischari_messira) || pnimmi_mesirra || (pnimmi && pticha)) {
            currentFormTag = TAG_MESIRRAT_RECHEV;
        } else if ((mischarri && hachzarra) || (pnimmi && hachzarra)) {
            currentFormTag = TAG_HACHZARRA;

        } else if (pnimmi && sgirra) {
            isMultiForm = true;
            currentFormTag = TAG_HACHZARRA;
        }

//        logBuilder("initFormSendingMode " + currentFormTag + "  ---------");
/*        logBuilder("PnimmiValue.Pnimmi()" + PnimmiValue.Pnimmi());
        logBuilder("PnimmiValue.Mischari()" + PnimmiValue.Mischari());
        logBuilder("MenuPnimmiActivity.Mesirra" + MenuPnimmiActivity.Mesirra());
        logBuilder("MenuMischarriActivity.Mesirra()" + MenuMischarriActivity.Mesirra());
        logBuilder("MenuPnimmiActivity.Pticha()" + MenuPnimmiActivity.Pticha());
        logBuilder("MenuPnimmiActivity.Sgirra()" + MenuPnimmiActivity.Sgirra());
        logBuilder("MenuPnimmiActivity.Hachzarra()" + MenuPnimmiActivity.Hachzarra());
        logBuilder("MenuMischarriActivity.Hachzarra()" + MenuMischarriActivity.Hachzarra());*/
    }

    void sendForms() {
        boolean was_rejected = reject_sign_check.isChecked();
        boolean was_confirm = confirm_check.isChecked();
        int reject_reason = -1;
        reject_reason = decline_reason_spinner.getSelectedItemPosition();
        boolean show_decline_option = PnimmiValue.Mischari() && MenuMischarriActivity.Hachzarra();
        if (show_decline_option) {
            if (!was_confirm) {
                MyToast.makeText(this, "לא אושר ביטול אחריות על ציוד שהושאר", Toast.LENGTH_LONG).show();
                return;
            }
        }

        log("=========== " + was_rejected);
        if (was_rejected) {
            if (reject_reason <= 0) {
                MyToast.makeText(this, "אנא הזן סיבת סירוב", Toast.LENGTH_LONG).show();
                return;
            }
        }


        sAddress = "";
        sSendingMean = "";
        if (opt_fax.isChecked() || opt_email.isChecked()) {
            sAddress = EditTextTo.str(email_addr); // fax/email
            sSendingMean = opt_email.isChecked() ? "M" : "F";

        }

        sRefusalReasonInd = -1;
        sRefusedSignature = was_rejected;
        sRefusalReasonInd = reject_reason;

        if (!was_rejected && !signature_was_set) {
            MyToast.makeText(this, "אנא הזן חתימה", Toast.LENGTH_LONG).show();
            return;
        }

        if (opt_fax.isChecked() || opt_email.isChecked()) {
            int len = email_addr.getText().toString().trim().length();
            if (len == 0) {
                MyToast.makeText(this, "יש לציין כתובת שליחה", Toast.LENGTH_LONG).show();
                return;
            }
        }

        signature_localFilename = null;
        if (!was_rejected && signature_was_set) {
            signature_localFilename = FileUtils.get_randon_filename("sign",".jpeg");
            Bitmap signature = ViewScreenshot.get(signatureImage);
            SdcardImageSaver.save(signature, signature_localFilename); //
        }

        // Check for test mode
        open_progress_dialog("מעביר נתו" + "נים לשרת..."); // גדכ גדכע
        if (_DEBUG) {
            do_finish();
            ActivityUtils.close_all();
        } else {
            sendRequest(was_rejected);
        }

    }

    @Override
    public void onSignatureSet() {
        signature_was_set = true;
    }

    @Override
    public void onSignatureClear() {
        signature_was_set = false;
    }

    @UiThread
    void ui_close_progress_dialog() {
        close_progress_dialog();
    }

    private void sendRequest(boolean wasRejected) {
        MyApp.appSetting.showImageLoadingProgressBar = true;
        Answers.getInstance().logLevelStart(new LevelStartEvent()
                .putLevelName("Image Report"));

        logBuilder("CAR NUMBER: " + MyApp.appSetting.getCarNumber());
        logBuilder("\n\n");
        imageCounterA = 0;
        imageCounterB = 0;
        logBuilder("---- list of images before sending to server ------");
        file_arr = new ArrayList<String>();

        if (!wasRejected && signature_was_set) {
            file_arr.add(signature_localFilename);
        }

        SendForm_Request_Inner.reset_photo_arr();
        for (File photo : all_taken_photos) {
            String photo_Filename = photo.getName();
            file_arr.add(photo_Filename);
            imageCounterA++;
            logBuilder(photo_Filename);
        }

        SendForm_Request_Inner.reset_photo_arr();
        ftpAsyncSend(wasRejected);

    }

    @Background
    void ftpAsyncSend(final boolean wasRejected) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    signatureRemoteFile = null;

                    if (!wasRejected && signature_was_set) {
                        logBuilder("\n\n");
                        logBuilder("** Upload Signature Image **");
                        signatureRemoteFile = FtpUploader.upload_blocking(signature_localFilename, FtpUploader.SIGNATURE_MAX_SIZE, UniqueId.current_transaction_id);
                    }

                    // ActionValue.should_pass_image();

                    all_REMOTE_photos.clear();
                    logBuilder("\n\n---- start uploading images to ftp server ------");
                    for (File photo : all_taken_photos) {
                        String photo_Filename = photo.getName();
                        imageCounterB++;
                        logBuilder(photo_Filename + "... " + imageCounterB + "/" + imageCounterA + " upload to ftp server");
                        String remote_photo_fname = FtpUploader.upload_blocking(photo_Filename);
                        all_REMOTE_photos.add(new File(remote_photo_fname));
                    }
                    wsSendRequest(currentFormTag);
                } catch (Exception e) {
                    MyApp.appSetting.showImageLoadingProgressBar = false;
                    wsResponseHandle(false, e.getMessage());
                }finally {
                    MyApp.appSetting.showImageLoadingProgressBar = false;
                }
            }
        }).start();
        ;
    }

    private void wsSendRequest(int option) {
        logBuilder("\n\n");
        try {
            if (option == TAG_HACHZARRA) {
                logBuilder("type: HACHZARRA");
                ActionValue.set(ActionValue.Values.Hachzarra);
            } else if (option == TAG_MESIRRAT_RECHEV) {
                logBuilder("type: MESIRRAT_RECHEV");
                ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
            } else if (option == TAG_SGIRRA) {
                logBuilder("type: SGIRRA");
                ActionValue.set(ActionValue.Values.Sgirra);
            }
            logBuilder("\n\n");
            String strJsonObj = getJsonRequestForm(signatureRemoteFile, all_REMOTE_photos);
            logBuilder("Json object sent to " + JsonTransmitter.MAIN_SERVER_URL + " ----\n\n " + strJsonObj);

            Answers.getInstance().logLevelEnd(new LevelEndEvent()
                    .putCustomAttribute("Image Report " + MyApp.appSetting.getCarNumber(), imageCounterB + "/" + imageCounterA)
                    .putSuccess(imageCounterB == imageCounterA));

            if(imageCounterB != imageCounterA) {
                report("Image Report CarNO – "+
                        MyApp.appSetting.getCarNumber().replaceAll("-", "") +", Branch – "+
                        MyApp.appSetting.getReturnBranch()+", Login – " +
                        LoginSixt.get_Worker_No() , MyApp.appSetting.getImageEvent().toString());
            }
           /* reportX("Image Report CarNO – "+
                    MyApp.appSetting.getCarNumber().replaceAll("-", "") +", Branch – "+
                    MyApp.appSetting.getReturnBranch()+", Login – " +
                    LoginSixt.get_Worker_No(), MyApp.appSetting.getImageEvent().toString());*/
            WebServiceClientFactory.getInstance().getHttpRequestsClient(true).request(strJsonObj, currentFormTag, handler);

        } catch (ActionFailedError e) {
            wsResponseHandle(false, e.getMessage());
        } catch (ConnectionError e) {
            wsResponseHandle(false, e.getMessage());
        }
    }

    // Handle response from webwervice and display/redirect depending on response
    private void wsResponseHandle(boolean isSuccess, String s) {
        if (isSuccess) {
            UniqueId.delete_unique_folderWithId(transId);
            MainMenuActivity.success_mst_to_show = "הטופס נשלח בהצלחה";
            return_to_main_menu_on_success();
        } else {
            ui_close_progress_dialog();
            displayAlert(s);
//			displayAlert("אין חיבור נסה שנית מאוחר יותר");
        }
    }

    private void return_to_main_menu_on_success() {

        if (MyApp.generalSettings.isInCustomerYardMode()) {
            DriverDetailseActivity_.intent(this).start();
            this.finish();
            return;
        }

        this.finish();
        ActivityUtils.close_all();
    }

    // new implementation to return json request to save in db
    private String getJsonRequestForm(String _remote_signature_filename, ArrayList<File> photos)
            throws ConnectionError, ActionFailedError {
        ArrayList<RejectRecord> rejects_arr = get_all_rejects();
        populate_photos_arr(photos);

        SendForm_Request req = new SendForm_Request(this, CALLED_BY_SIGNATURE,
                _remote_signature_filename, rejects_arr);
        if (!req.is_valid_form()) {
            return null;
        }

        return GGson.toJson(req);
    }

    @UiThread
    void do_finish() {
        this.finish();
    }

    @UiThread
    void displayAlert(String strMsg) {
        DialogError.show(this, strMsg);
    }

    @AfterViews
    void post_onCreate() {
        // btn_camera_container.setVisibility(View.VISIBLE);

        clear_statics();
        signature_was_set = false;

        set_spinner_enabled();

        CaptionSetter.set(txt_caption);

        txt_car_id.setText("" + CarNumberValue.get());

        String def_mail = GetCarDetails_Response.getCustMail();
        email_addr.setText(def_mail);

        reject_sign_check
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        set_spinner_enabled();
                    }
                });

        // show only in mischari hachzarra
        boolean show_decline_option = PnimmiValue.Mischari()
                && MenuMischarriActivity.Hachzarra();

        if (show_decline_option) {
            confirm_layout.setVisibility(View.VISIBLE);
            show_decline_container.setVisibility(View.VISIBLE);
            populate_reason_spinner();
        } else {
            confirm_layout.setVisibility(View.GONE);
            show_decline_container.setVisibility(View.GONE);
        }

        signature_draw_dlg = SignatureDrawDialogB.create(this, signatureText, signatureImage);
        manageContactButtons();

        // BreadCrumbs.append("חתימה");
        final String cur_bcrums = BreadCrumbs.get() + BreadCrumbs.SEP + "חתימה";
        caption.setText(cur_bcrums);

        mark_selected_mode(opt_none); // default is email ggg

        // hide the keyboard
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    protected void set_spinner_enabled() { //
        boolean user_decline = reject_sign_check.isChecked();
        decline_reason_spinner.setEnabled(user_decline);
        if (user_decline) {
            clear_signature();
            // signature_container.setEnabled(false);
            // signatureText.setEnabled(false);
        } else {
            // signature_container.setEnabled(true);
        }
        signature_container.setEnabled(!user_decline);
        signatureText.setEnabled(!user_decline);
    }

    private void populate_reason_spinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (_DEBUG) {
            adapter.add("");
            adapter.add("לקוח מתכחש לנזקים");
            adapter.add("לקוח לא סיים תהליך");
            adapter.add("לקוח לא מבין שפה");
        } else {
            populate_with_real_data(adapter);
        }
        decline_reason_spinner.setAdapter(adapter);
    }

    private void manageContactButtons() {
        // final ToggleButton contactButtons [] = {opt_fax,opt_email,opt_empty};
        final ToggleButton contactButtons[] = {opt_fax, opt_email, opt_none};

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (ToggleButton button : contactButtons) {
                    button.setChecked(false);
                }
                mark_selected_mode(v);
            }
        };

        for (ToggleButton button : contactButtons) {
            button.setOnClickListener(onClickListener);
        }

    }

    protected void mark_selected_mode(View v) { // ggg
        ((ToggleButton) v).setChecked(true);
        mode_email = (v == opt_email);
        boolean is_email = mode_email;
        boolean is_none = (v == opt_none);
        // set keyboard type

        email_addr.setEnabled(true);
        if (is_email) {
            lbl_email.setText("מייל");
            // email_addr.setRawInputType(InputType.TYPE_);
            email_addr.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            emailLayout.setVisibility(View.VISIBLE);
            email_addr.setVisibility(View.VISIBLE);
        } else if (is_none) {
            lbl_email.setText("");
            email_addr.setText("");
            email_addr.setEnabled(true);
            emailLayout.setVisibility(View.GONE);
            email_addr.setVisibility(View.INVISIBLE);
        } else { // fax
            lbl_email.setText("פקס");
            email_addr.setText("");
            // email_addr.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            email_addr.setInputType(InputType.TYPE_CLASS_NUMBER);
            emailLayout.setVisibility(View.VISIBLE);
            email_addr.setVisibility(View.VISIBLE);
        }
    }

    @Click
    void btn_camera() {
        current_photo_file = CameraUtils.start_camera_app(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        current_photo_file = CameraUtils.handle_picture_taken(requestCode,
                resultCode, data, current_photo_file, this);
        if (current_photo_file != null) {
            if (_DEBUG) {
                log("_DEBUG");
                CameraUtils.debug_write_image_to_sdcard(current_photo_file, this);
            }
            all_taken_photos.add(current_photo_file);
            num = all_taken_photos.size();
            btn_camera.setText("צלם" + " (" + num + ")");
        } else {
            // some error in taking/reading photo..
            Answers.getInstance().logCustom(new CustomEvent("Rent Photos")
                    .putCustomAttribute(MyApp.appSetting.getCarNumber(), MyApp.appSetting.getCameraError()));


            toast(MyApp.appSetting.getCameraError());
        }
    }

//
//	protected void do_show_pdf(File pdfFile) {
//		close_progress_dialog();
//		if (pdfFile == null) {
//			MyToast.makeText(SignatureActivity.this,
//					"לא ניתן להציג את הטופס ברגע זה", Toast.LENGTH_LONG).show();
//			return;
//		}
//		PdfViewer.open(pdfFile, this);
//	}

    @Override
    public boolean handleMessage(Message msg) {
        ui_close_progress_dialog();
        if (msg.what == IWebServiceClientFactory.Response.RESPONSE_OK.getCode()) {
            if ((msg.arg1 == TAG_HACHZARRA) && isMultiForm) {
                currentFormTag = TAG_SGIRRA;
                wsSendRequest(TAG_SGIRRA);
            } else {
                wsResponseHandle(true, "");
            }

        } else if (msg.what == IWebServiceClientFactory.Response.UNKNOWN.getCode() ||
                msg.what == IWebServiceClientFactory.Response.RUNTIME_ACTION_FAILURE.getCode() ||
                msg.what == IWebServiceClientFactory.Response.RUNTIME_EXCEPTION.getCode()) {
            wsResponseHandle(false, "problem with msg.what: " + msg.what);
        }
        return false;
    }

    public void log(String log) {
        Log.i("Image_log", "" + log);
    }

    public void logBuilder(String log) {
        if (sb == null) {
            sb = new StringBuilder();
        }

        Log.i("Image_log", "" + log);
        sb.append("" + log + "\n");
        MyApp.appSetting.appendImageEvent(log);
    }

    enum SaveAction {
        SAVE, DELETE
    }
}


