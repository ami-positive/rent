package com.sixt.android.activities.customerYard.network.response;

import com.google.gson.annotations.SerializedName;
import com.sixt.android.activities.customerYard.pdf.FileConverterTask;

/**
 * Created by natiapplications on 31/12/15.
 */
public class ContractDocResponse extends  BaseResponseObject {

    @SerializedName("ContractDoc")
    private String contractDocAsBase64;
    private int PDASignatureID;


    public int getPDASignatureID() {
        return PDASignatureID;
    }

    public void setPDASignatureID(int PDASignatureID) {
        this.PDASignatureID = PDASignatureID;
    }

    public void decodeReceivedPDF (final DecodePDFCallback callback){

       /* byte[] data =  PDFUtil.readAssetFile(MyApp.appContext,"test_pdf_text.txt");
        contractDocAsBase64 = new String(data);*/

        if (contractDocAsBase64 != null && !contractDocAsBase64.isEmpty()){
            new FileConverterTask(new FileConverterTask.FileConverterCallback() {
                @Override
                public void onConverted(boolean success, String filePath, String errDesc) {
                    if (callback != null){
                        callback.onDone(success,errDesc,filePath);
                    }
                }
            }).execute(contractDocAsBase64);
        }else {
            callback.onDone(false,"The downloaded file is empty",null);
        }


    }

    public String getContractDocAsBase64() {
        return contractDocAsBase64;
    }

    public void setContractDocAsBase64(String contractDocAsBase64) {
        this.contractDocAsBase64 = contractDocAsBase64;
    }

    public interface DecodePDFCallback {
        public void onDone (boolean success,String errDesc,String fileName);
    }


}
