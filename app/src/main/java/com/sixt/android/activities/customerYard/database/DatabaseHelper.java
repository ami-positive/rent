/**
 * 
 */
package com.sixt.android.activities.customerYard.database;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sixt.android.activities.customerYard.database.tables.TableWordsReport;


/**
 * @author natiapplications
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	//database name
	private static final String DATABASE_NAME = "rentapp.db";


	//database version
	private static final int DATABASE_VERSION = 2;

	

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		TableWordsReport.onCreate(database);

	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
						  int newVersion) {

		TableWordsReport.onUpgrade(database,oldVersion,newVersion);

	}



}
