package com.sixt.android.activities.customerYard.network.requests;

import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.NetworkManager;
import com.sixt.android.activities.customerYard.network.response.CreditGuardQueryResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by Izakos on 14/02/2016.
 */
public class CreditGuardQueryRequest extends RequestObject<CreditGuardQueryResponse>{


    private long Sum;
    private String CurrencyCode = "";
    private String FirstName = "";
    private String LastName= "";
    private String Tz= "";
    private String CVV= "";
    private long requestId;
    private String Token= "";

    @SerializedName("CreditGuardQuery_Req")
    private CreditGuardQueryRequestInner CreditGuardQueryRequestInner;

    public CreditGuardQueryRequest(CreditCard creditCard, long requestId, String token) {
        Sum = Long.parseLong(creditCard.getAmount());
        CurrencyCode = creditCard.getCurrencyCode();
        FirstName = creditCard.getFirstName();
        LastName = creditCard.getLastName();
        Tz = creditCard.getUserID();;
        this.CVV = creditCard.getCvv();
        this.requestId = requestId;
        Token = token;
    }

    @Override
    protected void execute(ApiInterface apiInterface, Callback<CreditGuardQueryResponse> callback) {
        initGetCompaniesRequest();
        apiInterface.getCreditGuardQuery(CreditGuardQueryRequestInner, callback);
    }

    private void initGetCompaniesRequest() {
        CreditGuardQueryRequestInner = new CreditGuardQueryRequestInner();
//        CreditGuardQueryRequestInner.Action = "O";
//        CreditGuardQueryRequestInner.App = "R";
//        CreditGuardQueryRequestInner.CarNo = ContractChetchData.currentLicensingNumber;;
//        CreditGuardQueryRequestInner.MenuAction = "B";
//        CreditGuardQueryRequestInner.Login = LoginSixt.get_Worker_No();
//        CreditGuardQueryRequestInner.OffSite = 1;
//        CreditGuardQueryRequestInner.Sum = CreditGuardQueryRequest.this.Sum;
//        CreditGuardQueryRequestInner.CurrencyCode = CreditGuardQueryRequest.this.CurrencyCode;
//        CreditGuardQueryRequestInner.FirstName = CreditGuardQueryRequest.this.FirstName;
//        CreditGuardQueryRequestInner.LastName = CreditGuardQueryRequest.this.LastName;
//        CreditGuardQueryRequestInner.Tz = CreditGuardQueryRequest.this.Tz;
//        CreditGuardQueryRequestInner.CVV = CreditGuardQueryRequest.this.CVV;
//        CreditGuardQueryRequestInner.requestId = CreditGuardQueryRequest.this.requestId;
//        CreditGuardQueryRequestInner.Token = CreditGuardQueryRequest.this.Token;
//        CreditGuardQueryRequestInner.debugVersion = MyApp.appVersion;
//        CreditGuardQueryRequestInner.imei = DeviceAndroidId.get_imei();
//        CreditGuardQueryRequestInner.msg_type = "CreditGuardQuery_Request";
//        CreditGuardQueryRequestInner.simcard_serial = DeviceAndroidId.get_Sim_serial();
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }

    public class CreditGuardQueryRequestInner{

        public String Action;
        public String App;
        public String CarNo;
        public String MenuAction;
        public long Login;
        public int OffSite;
        public long Sum;
        public String CurrencyCode;
        public String FirstName;
        public String LastName;
        public String Tz;
        public int CVV;
        public long requestId;
        public String Token;
        public String debugVersion;
        public String imei;
        public String msg_type;
        public String simcard_serial;

        public CreditGuardQueryRequestInner() {

            Action = "O";
            App = "R";
            CarNo = ContractChetchData.currentLicensingNumber;;
            MenuAction = "B";
            Login = Long.parseLong(LoginSixt.get_Worker_No());
            OffSite = 1;
            Sum = CreditGuardQueryRequest.this.Sum;
            CurrencyCode = CreditGuardQueryRequest.this.CurrencyCode;
            FirstName = CreditGuardQueryRequest.this.FirstName;
            LastName = CreditGuardQueryRequest.this.LastName;
            Tz = CreditGuardQueryRequest.this.Tz;
            CVV = Integer.parseInt(CreditGuardQueryRequest.this.CVV);
            requestId = CreditGuardQueryRequest.this.requestId;
            Token = CreditGuardQueryRequest.this.Token;
            debugVersion = MyApp.appVersion;
            imei = DeviceAndroidId.get_imei();
            msg_type = "CreditGuardQuery_Request";
            simcard_serial = DeviceAndroidId.get_Sim_serial();


        }
    }

    @Override
    public String getServerEndPoint() {
        return NetworkManager.CREDIT_GUARD;
    }
}
