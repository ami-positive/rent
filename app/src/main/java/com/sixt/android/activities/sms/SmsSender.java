package com.sixt.android.activities.sms;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

import com.sixt.android.app.util.AppContext;

public class SmsSender {


	private static final long RESEND_SMS_INTERVAL = 10*1000; // 10 secs
	private static final int MAX_SMS_SEND_PER_BID = 4;
	private static final int MAX_DELETE_ATTEMPTS = 7;

	private static final String SMS_SEP = "^"; 

	public static volatile int num_sent = 0;
	public static volatile long last_sent_time = Long.MAX_VALUE;

	private static final String MIZAMIN_SERVER_NUMBER = "0529994050"; // phone before: "0503355247"; 
	private static final String SENT = "SMS_SENT"; // do not change!



	private SmsSender() {}


	public static void clear() {
		num_sent = 0;
		last_sent_time = Long.MAX_VALUE;
	}


	public static void send_sms(final String msg ) {
		new Thread() {
			@Override
			public void run() {
				inner_send_sms(msg);
			}
		}.start();
	}


	private static void inner_send_sms(String msg) {		
		String phoneNumber = "0544591162";

		String message = msg;
		
		Context c = AppContext.appContext;

		SmsManager smsManager = SmsManager.getDefault();

		PendingIntent sentPI = PendingIntent.getBroadcast(c, 0,new Intent(SENT), 0);
		// another option: sentPI = null
		num_sent++;
		last_sent_time = System.currentTimeMillis();

		smsManager.sendTextMessage(phoneNumber, null, message, sentPI, null);

	} 


}
