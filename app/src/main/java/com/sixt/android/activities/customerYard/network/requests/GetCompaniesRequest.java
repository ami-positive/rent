package com.sixt.android.activities.customerYard.network.requests;



import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.GetCompaniesResponse;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class GetCompaniesRequest extends RequestObject<GetCompaniesResponse> {


    private String login;


    public GetCompaniesRequest(String login){
        this.login = login;
    }

    private GetCompanies_Req getCompanies_req;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<GetCompaniesResponse> callback) {

        initGetCompaniesRequest();
        apiInterface.getCompanies(getCompanies_req, callback);

    }

    private void initGetCompaniesRequest() {
        getCompanies_req = new GetCompanies_Req();
        getCompanies_req.App = "I";
        getCompanies_req.Login = this.login;
        getCompanies_req.debugVersion = MyApp.appVersion;
        getCompanies_req.msg_type = "CheckCompanies_Request";
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }


    public class GetCompanies_Req {
        public String App;
        public String Login;
        public String debugVersion;
        public String msg_type;


       public GetCompanies_Req (){

       }
    }
}
