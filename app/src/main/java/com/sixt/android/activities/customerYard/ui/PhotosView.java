package com.sixt.android.activities.customerYard.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.utils.BitmapUtil;
import com.sixt.android.activities.customerYard.utils.CommonIntentsUtil;
import com.sixt.android.activities.customerYard.utils.ImageLoader;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.android.scanovate.IsraelDriversLicenseScanActivity;
import com.sixt.rent.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by natiapplications on 23/12/15.
 */
public class PhotosView  extends GridView implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener{


    private Activity activity;
    private Fragment fragment;
    private PhotoChangeListener listener;

    private ArrayList<Photo> photos = new ArrayList<Photo>();

    private GridAdapter adapter;


    public PhotosView(Context context) {
        super(context);
        init();
    }

    public PhotosView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PhotosView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public void init (){
        setNumColumns(2);
    }

    public void config (Activity activity,PhotoChangeListener listener,String... names){
        this.fragment = null;
        this.activity = activity;
        this.listener = listener;
        createPhotosByNames(names);
        adapter = new GridAdapter(getContext(),0,this.photos);
        setAdapter(adapter);
        setOnItemClickListener(this);
        setOnItemLongClickListener(this);
    }

    public void config (Fragment fragment,PhotoChangeListener listener,ArrayList<Photo> photos,String... names){
        this.fragment = fragment;
        this.activity = null;
        this.listener = listener;

        if (photos == null){
            createPhotosByNames(names);
        }else {
            this.photos = photos;
        }

        adapter = new GridAdapter(getContext(),0,this.photos);
        setAdapter(adapter);
        setOnItemClickListener(this);
        setOnItemLongClickListener(this);
    }



    public void clear () {
        try{
            for (int i = 0; i < photos.size(); i++) {
                photos.get(i).hasImage = false;
//                photos.get(i).photoPath = "";
            }
            adapter.notifyDataSetChanged();
        }catch (Exception e){}
    }

    public void clearPhoto(Photo mTempPhoto) {
        photoLog("photos array size: " + photos.size());
        photoLog("start looping on photos...");
        try{
            for (int i = 0; i < photos.size(); i++) {
                photoLog("match objects org|found - " + mTempPhoto +"|"+photos.get(i));
                if(photos.get(i).equals(mTempPhoto)){
                    photoLog("FOUND MATCH!!...setting to false");
                    photos.get(i).hasImage = false;
                }
            }
            photoLog("notifyDataSetChanged");
            adapter.notifyDataSetChanged();
        }catch (Exception e){}
    }

    public void photoLog(String log){
        Log.e("PhotoLog", "form PhotoView class: " + log);
    }

    private void createPhotosByNames (String... names){
        if (photos.size() == 0){
            for (int i = 0; i < names.length; i++) {
                Photo temp = new Photo(names[i]);
                temp.photoPath = BitmapUtil.createStringPathForStoringMedia(names[i]  + ".png");
                temp.hasImage = false;
                photos.add(temp);
            }
        }
    }


    public int getPhotoPositionByName (String name){
        for (int i = 0; i < photos.size(); i++) {
            if (name.equals(photos.get(i).name)){
                return i;
            }
        }

        return -1;
    }


    public Photo getPhotoByName (String name){
        int pos = getPhotoPositionByName(name);
        if (pos >= 0){
            return photos.get(pos);
        }
        return null;
    }

    public boolean checkIfPhotoHasImageByName (String name){
        int pos = getPhotoPositionByName(name);
        if (pos >= 0){
            return photos.get(pos).hasImage;
        }
        return false;
    }

    public String getPhotoHasBase64ByName (String name){
        int pos = getPhotoPositionByName(name);
        String result = "";

        if (pos >= 0){
            if (photos.get(pos).hasImage){
                Bitmap originalA = BitmapFactory.decodeFile(photos.get(pos).photoPath);
                int nh = (int) ( originalA.getHeight() * (512.0 / originalA.getWidth()) );
                Bitmap original = Bitmap.createScaledBitmap(originalA, 512, nh, true);
                if (original != null){
                    Log.e ("BitmapLog","ORIGINAL H = " + original.getHeight() + " W = " + original.getWidth());

                    Bitmap copy = scaleOrigianlImage (original);
                    if (copy != null){
                        Log.e ("BitmapLog","COPY H = " + copy.getHeight() + " W = " + copy.getWidth());
                        result = BitmapUtil.getImageAsStringEncodedBase64(original);
                        System.out.println(result);
                    }
                }
            }
        }

        return result;
    }

    private Bitmap scaleOrigianlImage(Bitmap original) {

        try{
            int bigSide = original.getWidth() >= original.getHeight() ? original.getWidth():original.getHeight();
            int newWidth = original.getWidth();
            int newHeight = original.getHeight();

            while (bigSide > 400){
                newWidth = newWidth/2;
                newHeight = newHeight/2;
                bigSide = newWidth >= newHeight ? newWidth:newHeight;
            }

            return Bitmap.createScaledBitmap(original,newWidth,newHeight,false);

        }catch (Exception e){
            return original;
        }

    }

    public boolean isHasAllPhotos (){
        for (int i = 0; i < photos.size(); i++) {
            if (!photos.get(i).hasImage){
                return false;
            }
        }

        return true;
    }

    public String getMissingPhotosNames () {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < photos.size(); i++) {
            if (!photos.get(i).hasImage){
                stringBuilder.append(photos.get(i).name+ "\n");
            }
        }

        return stringBuilder.toString();
    }

    public ArrayList<Photo> getPhotos () {
        return this.photos;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        int requestCode = photos.get(position).id;
        String path = photos.get(position).photoPath;

        if (!photos.get(position).hasImage){

            if (photos.get(position).name.equals("רשיון נהיגה") ){
                if (activity != null) {

                    MyApp.lsdManager.path = path;
                    Intent startCustomCameraIntent = new Intent(activity, IsraelDriversLicenseScanActivity.class);
                    activity.startActivityForResult(startCustomCameraIntent, requestCode);
                } else if (fragment != null) {

                    MyApp.lsdManager.path = path;
                    Intent startCustomCameraIntent = new Intent(fragment.getActivity(), IsraelDriversLicenseScanActivity.class);
                    fragment.startActivityForResult(startCustomCameraIntent, requestCode);
                }

            }else {
                if (activity != null) {
                    CommonIntentsUtil.openCameraForResultFile(activity, path, requestCode);
                } else if (fragment != null) {
                    CommonIntentsUtil.openCameraForResultFile(fragment, path, requestCode);
                }
            }

        }else {

            if (activity != null){
                CommonIntentsUtil.openImageInGallery(activity, path);
            }else if (fragment != null) {
                CommonIntentsUtil.openImageInGallery(fragment,path);
            }

            ToastUtil.toaster("לחץ לחיצה ארוכה לשינוי התמונה",true);
        }

    }

    public void onActivityResult (int requestCode,int resultCode,Intent data){
        for (int i = 0; i < photos.size() ; i++) {
            if (requestCode == photos.get(i).id){
                if (resultCode == Activity.RESULT_OK){

                    System.out.println("photoPath " + photos.get(i).photoPath);
                    photos.get(i).hasImage = true;


                    if (listener != null){
                        listener.onPhotoChanged(photos.get(i),
                                BitmapFactory.decodeFile(photos.get(i).photoPath));
                    }
                    adapter.notifyDataSetChanged();
                }
                break;
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {



        if (photos.get(position).hasImage){

            int requestCode = photos.get(position).id;
            String path = photos.get(position).photoPath;

            try{
                Picasso.with(MyApp.appContext).invalidate(new File(path));
            }catch (Exception e){}

            if (photos.get(position).name.equals("רשיון נהיגה") ){
                if (activity != null) {
                    MyApp.lsdManager.path = path;
                    Intent startCustomCameraIntent = new Intent(activity, IsraelDriversLicenseScanActivity.class);
                    activity.startActivityForResult(startCustomCameraIntent, requestCode);
                } else if (fragment != null) {
                    MyApp.lsdManager.path = path;
                    Intent startCustomCameraIntent = new Intent(fragment.getActivity(), IsraelDriversLicenseScanActivity.class);
                    fragment.startActivityForResult(startCustomCameraIntent, requestCode);
                }

            }else {
                if (activity != null) {
                    CommonIntentsUtil.openCameraForResultFile(activity, path, requestCode);
                } else if (fragment != null) {
                    CommonIntentsUtil.openCameraForResultFile(fragment, path, requestCode);
                }
            }
            return true;
        }

        return false;
    }


    class GridAdapter extends ArrayAdapter<Photo> {

        public GridAdapter(Context context, int textViewResourceId, List<Photo> objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            GridAdapter.ViewHolder holder = null;

            if (convertView ==  null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.photo_gird_item,null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            Photo photo = getItem(position);

            holder.txtPhotoDesc.setText(photo.name);
            if (photo.hasPhoto()){
                holder.imgContent.setImageResource(R.drawable.empty);
                System.out.println("photoPath " + photo.photoPath);
                ImageLoader.byStringPathUsingPicso
                        (photo.photoPath, holder.imgContent, R.drawable.empty, true, true, 0, 0, null);
            }else{
                holder.imgContent.setImageResource(R.drawable.empty);
            }

            return convertView;
        }

        class ViewHolder {

            public ImageView imgContent;
            public TextView txtPhotoDesc;

            public ViewHolder (View convertView){
                imgContent = (ImageView)convertView.findViewById(R.id.imgContent);
                txtPhotoDesc = (TextView)convertView.findViewById(R.id.txtPhotoDesc);
            }
        }

    }





    public class Photo {
        public int id;
        public String photoPath;
        public String name;
        public boolean hasImage;

        public Photo (String naem){
            this.name = naem;
            this.id = getRandomIntBetween(1000,2000);
        }
        public boolean hasPhoto () {
            return hasImage && photoPath != null && !photoPath.isEmpty();
        }

        public  int getRandomIntBetween (int from,int to){
            Random r = new Random();
            return  r.nextInt(to-from) + from;
        }
    }

    public interface PhotoChangeListener  {
        public void onPhotoChanged (Photo photo,Bitmap bitmap);
    }

}
