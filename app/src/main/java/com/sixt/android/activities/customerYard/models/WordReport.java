package com.sixt.android.activities.customerYard.models;

import java.io.Serializable;

/**
 * Created by natiapplications on 22/11/15.
 */
public class WordReport implements Serializable {

    private long DBID;
    private String originalWord;
    private String correctWord;
    private long reportedAd;
    private String type;


    public long getDBID() {
        return DBID;
    }

    public void setDBID(long DBID) {
        this.DBID = DBID;
    }

    public String getOriginalWord() {
        if (originalWord == null){
            return "";
        }
        return originalWord;
    }

    public void setOriginalWord(String originalWord) {
        this.originalWord = originalWord;
    }

    public String getCorrectWord() {
        if (correctWord == null){
            return "";
        }

        return correctWord;
    }

    public void setCorrectWord(String correctWord) {
        this.correctWord = correctWord;
    }

    public long getReportedAd() {
        return reportedAd;
    }

    public void setReportedAd(long reportedAd) {
        this.reportedAd = reportedAd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordReport that = (WordReport) o;

        return correctWord.equals(that.correctWord);

    }

    @Override
    public int hashCode() {
        return correctWord.hashCode();
    }
}
