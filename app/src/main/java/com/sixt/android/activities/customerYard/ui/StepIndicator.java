package com.sixt.android.activities.customerYard.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sixt.rent.R;

import java.util.ArrayList;

/**
 * Created by natiapplications on 18/12/15.
 */
public class StepIndicator extends LinearLayout {



    private int currentPosition;

    private ArrayList<StepView> steps = new ArrayList<StepView>();

    public StepIndicator(Context context) {
        super(context);
        init();
    }

    public StepIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StepIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    private void init (){
        setOrientation(HORIZONTAL);
    }

    public void setSteps (int index,String... names){

        currentPosition = index;

        if (names.length > 0){
            for (int i = names.length -1 ; i >= 0; i--) {
                StepView temp = new StepView(i + 1, names[i]);

                temp.setState(index==i);

                steps.add(0,temp);

                addView(temp.getView());
            }
        }


    }

    public void setSteps (String... names){

        setSteps(0,names);

    }


    public void setCurrentPosition (int position) {
        this.currentPosition = position;
        if (currentPosition < steps.size()){
            for (int i = 0; i < steps.size(); i++) {
                steps.get(i).setState(i == position);
            }
        }
    }

    public int getCurrentPosition () {
        return this.currentPosition;
    }

    public void next () {

        if (this.currentPosition+1 < steps.size()){
            setCurrentPosition(this.currentPosition+1);
        }
    }

    public void previous (){

        if (this.currentPosition > 0 && steps.size() > 0 ){
            setCurrentPosition(this.currentPosition-1);
        }
    }

    public int getCount () {
        return this.steps.size();
    }

    public class StepView {


        private boolean state;
        private int stepIndex;
        private String stepName;

        private View stepView;
        private TextView txtIndex;
        private TextView txtStepName;


        public StepView (int stepIndex,String stepName){
            this.stepIndex = stepIndex;
            this.stepName = stepName;
            this.stepView = LayoutInflater.from(getContext()).inflate(R.layout.layout_step_indicator_item,null);
            this.txtStepName = (TextView)stepView.findViewById(R.id.txtStepName);
            this.txtIndex = (TextView)stepView.findViewById(R.id.txtIndex);
            this.txtStepName.setText(this.stepName);
            this.txtIndex.setText(String.valueOf(this.stepIndex));
        }

        public void setState (boolean state){
            this.state = state;
            txtIndex.setSelected(state);
            txtStepName.setSelected(state);
        }

        public boolean getState () {
            return this.state;
        }

        public View getView (){
            return stepView;
        }

    }

}
