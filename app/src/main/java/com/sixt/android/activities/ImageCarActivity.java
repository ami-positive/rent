package com.sixt.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.gmailsender.GmailSender;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.json.objects.MandatoryImgRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetRejects_Response;
import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.CameraUtils;
import com.sixt.android.app.util.FileUtils;
import com.sixt.android.app.util.PrivatenessOfCar;
import com.sixt.android.app.util.SdcardImageSaver;
import com.sixt.android.app.util.TEST;
import com.sixt.android.app.util.ViewScreenshot;
import com.sixt.android.data.ImageRejectContainer;
import com.sixt.android.data.RejectPoint;
import com.sixt.android.ui.DisplayLogic;
import com.sixt.android.ui.ImageMarkerPoint;
import com.sixt.android.ui.RejectView;
import com.sixt.rent.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

@EActivity
@NoTitle
public class ImageCarActivity extends BaseActivity {

    // selection of marker point: onClick_MarkerPoint

    // see show_all_rejects_on_screen()

    // set rejects are stored in active_reject_arr

    // ImageMarkerPoint represents a single toggleButton


    public static final int MARKER_NOTSET_CODE = 0;
    private static final int DLG_CANCEL = 3233;
    private static final int DLG_CLEAR = 4443;
    private static final boolean _DEBUG = TEST.debug_mode(false);
    private static StringBuilder errorString;
    private static ImageCarActivity inst;
    private static int debug_counter = 0;
    private static String mandatory_images_error = "";
    private static String r_desc;
    private File current_photo_file;
    private AlertDialog leftButtonDialog;
    private AlertDialog rightButtonDialog;
    private RejectView rejectView;
    private RejectPoint[] all_available_rejectPoints;
    private ArrayList<RejectPoint> active_reject_arr;
    private RelativeLayout img_container;
    private boolean[] selectedCheckBoxes;

    private static boolean debug_close_to_jlb(int xx, int yy) {
        if (Math.abs(xx - 623) > 22) {
            return false;
        }
        if (Math.abs(yy - 739) > 22) {
            return false;
        }
        return true;
    }

    public static void call_finish() {
        if (inst != null) {
            inst.finish();
        }
    }

    public static void setErrorString(String log) {
        if (errorString == null) {
            errorString = new StringBuilder();
        }
        errorString.append("" + log + "\n");
        Log.i("errorString", "" + log);
    }

    public static void report(final String title, final String message) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    GmailSender sender = new GmailSender("itzik@positive-apps.com", "mesh2015");
                    sender.sendMail(title, message, "itzik@positive-apps.com", "itzik@positive-apps.com, SharonP@shlomo.co.il");

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        inst = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inst = this;

        SignatureActivity.image_reject_arr = null;

        UniqueId.set_new();

        //		SignatureActivity.photo_arr.clear(); //!

        hide_statusbar();
        setContentView(R.layout.car_p_schema_activity);
        rejectView = (RejectView) findViewById(R.id.reject_image);

        img_container = (RelativeLayout) findViewById(R.id.mainContainer);

        reset_data();

        createDialogs();
        //Setup left button click listener
        findViewById(R.id.left_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leftButtonDialog.show();
            }
        });

        //Setup right_button listener
        findViewById(R.id.right_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightButtonDialog.show();
            }
        });

    }

    private void reset_data() {
        ImageRejectContainer.reset_all_rejects();

        active_reject_arr = new ArrayList<RejectPoint>();

        boolean is_private;
        if (_DEBUG) {
            is_private = (++debug_counter % 2) == 0;
            ImageRejectContainer.AM.reject_code_position = 1;
            ImageRejectContainer.BL.reject_code_position = 2;
            ImageRejectContainer.CRF.reject_code_position = 1;
            active_reject_arr.add(ImageRejectContainer.AM);
            active_reject_arr.add(ImageRejectContainer.BL);
            active_reject_arr.add(ImageRejectContainer.CRF);
        } else {
            is_private = PrivatenessOfCar.is_p_or_pm();
        }

        if (is_private) {
            all_available_rejectPoints = ImageRejectContainer.getRejectPoints("p");
            rejectView.setSourceImage(R.drawable.image_p);
        } else {
            all_available_rejectPoints = ImageRejectContainer.getRejectPoints("m");
            rejectView.setSourceImage(R.drawable.image_m);
        }

        if (!_DEBUG) {
            GetCarDetails_Response.add_actual_rejects_type_image(active_reject_arr, all_available_rejectPoints);
        }
        show_all_rejects_on_screen();
    }

    private void show_all_rejects_on_screen() {
        selectedCheckBoxes = new boolean[]{false, false};

        if (all_available_rejectPoints == null || active_reject_arr == null) {
            return;
        }

        int jjj = 234;
        jjj++;

		/*
            get screen size hight and width
			 */
        float Height = MyApp.generalSettings.getScreenHight();
        float Width = MyApp.generalSettings.getScreenWidth();
        Log.i("Car Display", "screen width|hight: " + Width + "|" + Height);

        for (RejectPoint rejectPoint : all_available_rejectPoints) {


//			float offSetX = AppUtil.convertDpToPixel(1, this);
//			float offSetY = AppUtil.convertDpToPixel(2, this);
            float offSetX = AppUtil.convertDpToPixel(0, this);
            float offSetY = AppUtil.convertDpToPixel(0, this);

            /** edit by izakos **/
            /*
			the orginal image size is 720 * 1280
			the next formula multiple the screenPoints in related to the size of the screen
			so this program will be matches to any screen size
			 */
            if (Height != 0 && Width != 0) {
                rejectPoint.setScreenPosition((rejectPoint.screenPosition.x * (Width / 720)), (rejectPoint.screenPosition.y * (Height / 1280)));
            }

			/*
			this log present the screen points before and after the calculation and its multiple
			 */
            Log.i("Car Display", "old x|y: " + rejectPoint.screenPosition.x + "|" + rejectPoint.screenPosition.y +
                    " - new x|y: " + rejectPoint.screenPosition.x * (Width / 720) + "|" + rejectPoint.screenPosition.y * (Height / 1280)
                    + " multiple: x|y: " + (Width / 720) + "|" + (Height / 1280));

            ImageMarkerPoint rejectMarker = new ImageMarkerPoint(this, rejectPoint, img_container);
//			if (TEST.MODE) {
//				int xx = rejectPoint.screenPosition.x;
//				int yy = rejectPoint.screenPosition.y;
//				String _code = rejectPoint.Reject_Code;
//				String _desc = rejectPoint.Reject_Desc;
//				if ("JLB".equals(_code)) {
//					int jj=234;
//					jj++;
//				}
//				else if (debug_close_to_jlb(xx, yy)) {
//					int jj=234;
//					jj++;
//
//				}
//			}

            img_container.addView(rejectMarker);
            if (active_reject_arr.contains(rejectPoint) && rejectPoint.reject_code_position > MARKER_NOTSET_CODE) {
                if (rejectPoint.is_lower_right()) {
                    if (rejectPoint.Reject_Code.contains("001")) {
                        selectedCheckBoxes[0] = true;
                    } else {
                        selectedCheckBoxes[1] = true;
                    }
                } else {
                    rejectMarker.setChecked(true);
                }
            }
        }
    }

    private void hide_statusbar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void createDialogs() {
        ListAdapter right_btn_itemlist;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        right_btn_itemlist = new ArrayAdapter(this,
                R.layout.dialog_right_aligned_checkbox,
                getResources().getTextArray(R.array.ImagePRightBtnDialog));

        builder.setAdapter(right_btn_itemlist, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int jj = 234;
                jj++;
            }
        }).setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });


        rightButtonDialog = builder.create();

        final ListView listView = rightButtonDialog.getListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    CheckedTextView checkedTextView = (CheckedTextView) view;

                    RejectPoint rejectPoint;
                    if (position == 0) {
                        rejectPoint = ImageRejectContainer._001;
                    } else if (position == 1) {
                        rejectPoint = ImageRejectContainer._002;
                    } else {
                        throw new RuntimeException();
                    }

                    //					rejectPoint.reject_code_position = 1;

                    active_reject_arr.remove(rejectPoint);
                    if (checkedTextView.isChecked()) {
                        // turn off
                        checkedTextView.setChecked(false);
                        selectedCheckBoxes[position] = false;
                    } else {
                        // turn on
                        checkedTextView.setChecked(true);
                        active_reject_arr.add(rejectPoint);
                        selectedCheckBoxes[position] = true;
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
        });

        rightButtonDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ListView dialogElements = ((AlertDialog) dialog).getListView();
                int childCount = dialogElements.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    CheckedTextView textView = (CheckedTextView) dialogElements.getChildAt(i);
                    textView.setChecked(selectedCheckBoxes[i]);
                }
            }
        });

        ListAdapter left_btn_itemlist;
        left_btn_itemlist = new ArrayAdapter(this,
                R.layout.dialog_right_aligned_item,
                getResources().getTextArray(R.array.ImagePLeftBtnDialog)); // left contextMenu
        builder.setAdapter(left_btn_itemlist, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handle_left_btn_menu_clicked(which);

            }
        }).setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        leftButtonDialog = builder.create();
    }

    protected void handle_left_btn_menu_clicked(int which) {
        //		habba
        //		zalemm
        //		nakke
        //		battel
        int jjj = 234;
        jjj++;
        if (which == 0) { //send = Shlach
            send_btn();
        }
//		else if (which==1) { // zalem picture
//			open_camera();
//		}
        else if (which == 1) { // clear = Nakke
            showDialog(DLG_CLEAR); //do_clear
        } else { // cancel = Battel
            showDialog(DLG_CANCEL); //do_clear
        }
    }

    private void send_btn() {
        mandatory_images_error = "";
        if (!validate_mandatory_images()) {
            //toast("לא ניתן להמשיך לפני סימון כל תקלות החובה כמתוקנות!");
            toast(mandatory_images_error);
            return;
        }
        write_image_to_sdcard();//
        Activity a = ImageCarActivity.this;
        SignatureActivity.image_reject_arr = create_image_reject_arr();
        SignatureActivity_.intent(a).start();
    }

    private boolean is_mandatory_mok(RejectPoint rejectPoint) {
        boolean reject_mode_mesirra = DisplayLogic.reject_mode_mesirra();
        if (!reject_mode_mesirra) {
            return false; // no problem
        }
        final String cur_code = rejectPoint.Reject_Code;
        MandatoryImgRecord[] arr = GetRejects_Response.get_mandatory_img();
        if (cur_code == null || arr == null) {
            return false;
        }
        for (MandatoryImgRecord r : arr) {
            String rejectCode = r.Code;
            if (cur_code.equals(rejectCode)) {
                return true;
            }
        }
        return false;
    }

    private boolean validate_mandatory_images() {
        mandatory_images_error = "יש לסמן פריטי חובה כתקינים: ";
        boolean reject_mode_mesirra = DisplayLogic.reject_mode_mesirra();
        if (!reject_mode_mesirra) {
            return true;
        }

        // all rejects in this array must be ok!
        MandatoryImgRecord[] arr = GetRejects_Response.get_mandatory_img();
        if (arr == null) {
            return true;
        }
        boolean result = true;
        for (MandatoryImgRecord r : arr) {
            String rejectCode = r.Code;
            r_desc = "";
            if (reject_was_set(rejectCode)) {
                mandatory_images_error += (", " + r_desc);
                result = false;
//				return false;
            }
        }
        return result;
    }

    private boolean reject_was_set(String rejectCode) {
        r_desc = "";
        for (RejectPoint pt : active_reject_arr) {
            String cur_code = pt.Reject_Code;
            String desc = pt.Reject_Desc;
            if (cur_code.equals(rejectCode)) {
                r_desc = desc;
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DLG_CANCEL) {
            return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {
                @Override
                public void run() {
                    do_cancel();
                }
            });
        } else if (id == DLG_CLEAR) {
            return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
                @Override
                public void run() {
                    do_clear();
                }
            });
        } else {
            return super.onCreateDialog(id);
        }
    }

    @UiThread
    void do_clear() {
//		selectedCheckBoxes[0] = selectedCheckBoxes[1] = false;
        clearActiveMarkers();
        reset_data();
    }

    @UiThread
    void do_cancel() {
        this.finish();
        ActivityUtils.close_all();
    }

    private ArrayList<RejectRecord> create_image_reject_arr() {
        final boolean IS_IMAGE = true;
        ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
        for (RejectPoint pt : active_reject_arr) {
            String code = pt.Reject_Code;
            int position = pt.reject_code_position;
            if (position < 0) {
                throw new RuntimeException();
            }
            if (!pt.is_lower_right() && position == 0) {
                continue; // 0 -- all is ok gilad
            }

            if (code.contains("00")) {
                int jj = 345;
                jj++;
            }

            final int mark = 0; // true for all parent rejects
            int value = pt.get_value();
            if (pt.is_lower_right()) {
                value = RejectPoint.lower_right_selected_value();
            }
            res.add(new RejectRecord(code, value, mark, IS_IMAGE));
        }

        return res;
    }

    private void write_image_to_sdcard() {
        if (ActionValue.should_pass_image()) {
            String imageFilename = FileUtils.get_randon_filename("carimg", ".jpeg");
            Bitmap img = ViewScreenshot.get(img_container);
            if (!UniqueId.id_exists()) {
                throw new RuntimeException();
            }
            SdcardImageSaver.save(img, imageFilename);//
//			SendForm_Request_Inner.Image_local_filename = imageFilename;
        } else {
//			SendForm_Request_Inner.Image_local_filename = null;
        }
    }


//	private void open_camera() { 
//		current_photo_file = CameraUtils.start_camera_app(this);		
//	}

    public void onClick_MarkerPoint(RejectPoint rejectPoint, boolean is_checked) {
        boolean deleted = active_reject_arr.remove(rejectPoint);
        if (is_checked) {
            active_reject_arr.add(rejectPoint);
            if (is_mandatory_mok(rejectPoint)) {
                MyToast.makeText(inst, RejectListActivity.MANDATORY_SET_ERROR, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void clearActiveMarkers() {
        //		ArrayList<RejectMarker> markers = new ArrayList<RejectMarker>();
        int count = img_container.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = img_container.getChildAt(i);
            if (child instanceof ImageMarkerPoint) {
                //	markers.add((RejectMarker)child);
                ((ImageMarkerPoint) child).clear();
            }
        }
        //		for (RejectMarker marker:markers){
        //			marker.clear();
        //		}

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        current_photo_file = CameraUtils.handle_picture_taken(requestCode, resultCode, data, current_photo_file, this);

        try {
            setErrorString("requestCode " + requestCode);
            setErrorString("resultCode " + resultCode);
            setErrorString("data " + data);
            setErrorString("current_photo_file " + current_photo_file);
            setErrorString("Activity caller " + this.getClass().getName());
        } catch (Exception e) {
        }

        if (current_photo_file != null) {
            if (_DEBUG) {
                CameraUtils.debug_write_image_to_sdcard(current_photo_file, this);
            }
            //			SignatureActivity.photo_arr.add(current_photo_file);
            //			int num = SignatureActivity.photo_arr.size();
        } else {
            // some error in taking/reading photo..
            report("שגיאת מצלמה RENT" + "   " + new Date(), errorString.toString());
            toast("לא ניתן לצלם ברגע זה");
        }
    }

    public boolean is_legal_reject_level(String reject_code, int new_value) {
        RejectRecord rec = GetCarDetails_Response.get_reject(reject_code);
        if (rec == null) {
            return true;
        }
        int orig_val = rec.Value;
        if (new_value >= orig_val) {
            return true;
        }
        return false;
    }

    public void onClick_IllegalRejectLevel() {
        if (inst != null) {
            inst.toast("לא ניתן להפחית מחומרת התקלה!");
        }

    }


}
