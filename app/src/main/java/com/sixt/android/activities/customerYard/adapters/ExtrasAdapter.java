package com.sixt.android.activities.customerYard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.rent.R;

import java.util.List;

/**
 * Created by natiapplications on 27/12/15.
 */
public class ExtrasAdapter extends ArrayAdapter<ContractDetailsResponse.Extra> {


    public ExtrasAdapter(Context context, int textViewResourceId, List<ContractDetailsResponse.Extra> objects) {
        super(context, textViewResourceId, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_extras,null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        ContractDetailsResponse.Extra extra = getItem(position);

        holder.extraName.setText(extra.ExtraDesc);
        holder.extraPrice.setText(extra.Price);
        holder.extraCheck.setChecked(extra.isChecked());

        return convertView;
    }

    public class ViewHolder {
        public TextView extraName;
        public TextView extraPrice;
        public CheckBox extraCheck;


        public ViewHolder (View convertView){
            extraName = (TextView)convertView.findViewById(R.id.txtExtraDesc);
            extraPrice = (TextView)convertView.findViewById(R.id.txtExtraAmount);
            extraCheck = (CheckBox)convertView.findViewById(R.id.cbIsChecked);
        }
    }
}
