package com.sixt.android.activities.customerYard.network.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.models.LicenseDrive;
import com.sixt.android.activities.customerYard.storage.StorageListener;
import com.sixt.android.activities.customerYard.storage.StorageManager;
import com.sixt.android.activities.customerYard.utils.DateUtil;
import com.sixt.android.app.json.objects.CurrencyRecord;
import com.sixt.android.app.util.AllDataRepository;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by natiapplications on 04/10/15.
 */
public class ContractDetailsResponse extends BaseResponseObject implements Serializable {


    public int Usercg;
    public int requestId;
    public String BContractNo;
    public String DriverFName;
    public String DriverLName;
    public String DriverBDate;
    @SerializedName("DriverTz")
    public String privateID;
    public String DriverAddress;
    public String DriverCity;
    public String DriverPhone;
    public String DriverLicense;
    public String DriverExpire;
    public String DriverMail;
    public int DriverAgree;
    public int Km;
    public int Fuel;
    public String DateOut;
    public String DateIn;
    @SerializedName("LicenseIssue")
    public String driverYear = "";
    public String BranchOut;
    public String BranchIn;
    public int J5;
    public int DriverDetails;
    public int ConnectionDoc;
    public ArrayList<String> Currencies;
    @SerializedName("Photos")
    public ArrayList<Photo> photosArray;
    public ArrayList<Extra> Extras;

    private LicenseDrive licenseDrive;
    private CreditCard creditCard;

    public String getFullName (){
        return DriverFName + " " + DriverLName;
    }

    public String getFullAddress(){
        return DriverAddress + " " + DriverCity;
    }

    public boolean isNeedTakePhotos () {
        return photosArray != null && photosArray.size() > 0 /*&& isNeedTakeDriverDetails()*/;
    }

    public ArrayList<Photo> getPhotosArray() {
        return photosArray;
    }

    public void setPhotosArray(ArrayList<Photo> photosArray) {
        this.photosArray = photosArray;
    }

    public boolean isNeedTakeCreditCard () {
        return J5 > 0;
    }

    public boolean isHasExtras () {
        return Extras != null && Extras.size() > 0;
    }

    public boolean isConnectionDoc (){
        return ConnectionDoc > 0;
    }

    public boolean isNeedTakeDriverDetails () {
        return DriverDetails > 0;
    }


    public String getDriverYear() {
        return driverYear;
    }

    public void setDriverYear(String driverYear) {
        this.driverYear = driverYear;
    }

    public String[] getSteps (){
        ArrayList<String> steps = new ArrayList<String>();

        steps.add("פרטים");

        if (isNeedTakeCreditCard()){
            steps.add("אשראי");
        }
        if (isNeedTakePhotos()){
            steps.add("תמונות");
        }
        if (isNeedTakeDriverDetails()){
            steps.add("פרטי נהג");
        }
        if (isHasExtras()){
            steps.add("אקסטרות");
        }

        String[] result = new String[steps.size()];
        for (int i = 0; i < steps.size(); i++) {
            result[i] = steps.get(i);
        }

        return result;
    }


    public ArrayList<String> getCheckedExtras () {
        ArrayList<String> result = new ArrayList<String>();

        if (Extras != null){
            for (int i = 0; i < Extras.size(); i++) {
                if (Extras.get(i).isChecked()){
                    result.add(Extras.get(i).ExtraCode);
                }
            }
        }

        return result;
    }

    public boolean isHasCheckedExtras () {
        if (Extras != null) {
            for (int i = 0; i < Extras.size() ; i++) {
                if (Extras.get(i).isChecked()){
                    return true;
                }
            }
        }

        return false;
    }

    public String[] getCurrencies () {

        String[] result = null;

        Log.e("CurrencyLog", "Currencies is null ? " + (Currencies == null));
        if (Currencies != null){
            result = new String[Currencies.size()];
            for (int i = 0; i < Currencies.size(); i++) {
                try{
                    Log.e("CurrencyLog","code:" + Currencies.get(i));
                    String temp = AllDataRepository.getCurrencyByCode(Currencies.get(i).trim()).CurrencyDesc;
                    Log.e("CurrencyLog","temp: " + temp + " code:" + Currencies.get(i));
                    result[i] = temp;
                }catch (Exception e){
                    Log.e("CurrencyLog","ex = " + e.getMessage());
                    e.printStackTrace();
                    continue;
                }

            }
        }else {
            final CurrencyRecord[] all = AllDataRepository.getCurrencies();
            if (all != null){
               result = new String[all.length];
                for (int i = 0; i < all.length; i++) {
                    result[i] = all[i].CurrencyDesc;
                }

            }
        }

        if (result == null){
            result = new String[3];
            result[0] = "ש״ח";
            result[1] = "דולר";
            result[2] = "יורו";
        }

        return result;

    }


    public String[] getPhotos () {

        if (photosArray != null && photosArray.size() > 0){
            String[] result = null;
            int start = 0;
            if (!photosContainsLicenseDriver()){
                result = new String[photosArray.size()+1];
                result[0] = "רשיון נהיגה";
                start = 1;
            }else {
                result = new String[photosArray.size()];
                start = 0;
            }

            for (int i = 0; i < photosArray.size() ; i++) {
                try{
                    result[i+start] = AllDataRepository.getPhotoByCode(String.valueOf(photosArray.get(i).getPhotoCode())).PhotoDesc;
                }catch (Exception e){
                    continue;
                }
            }
            return result;
        }else{
            String[] result = new String[1];
            result[0] = "רשיון נהיגה";
            return result;
        }
    }

    private boolean photosContainsLicenseDriver () {
        for (int i = 0; i < photosArray.size(); i++) {
            String name = AllDataRepository.getPhotoByCode(String.valueOf(photosArray.get(i).getPhotoCode())).PhotoDesc;
            if (name.trim().equalsIgnoreCase("רשיון נהיגה")){
                return true;
            }
        }
        return false;
    }

    public boolean isNeedToSendLicenseDrivePhotoToServer () {
        if (this.photosArray != null){
            return photosContainsLicenseDriver();
        }

        return false;
    }


    public String getContractNomber(){
        if (BContractNo != null){return BContractNo.replaceAll("/","");}
        return "";
    }

    public void saveToFile () {
        MyApp.storageManager.writeToStorage(getContractNomber() + StorageManager.FILE_CURRENT_CONTRACT, this);
    }

    public static void readFromFile (String contractNumber,StorageListener callback){
        MyApp.storageManager.readFromStorage(contractNumber + StorageManager.FILE_CURRENT_CONTRACT,callback);
    }

    public class Photo implements Serializable{
        private int PhotoCode;
        private int Mandatory;

        public int getPhotoCode() {
            return PhotoCode;
        }

        public void setPhotoCode(int photoCode) {
            PhotoCode = photoCode;
        }

        public int getMandatory() {
            return Mandatory;
        }

        public void setMandatory(int mandatory) {
            Mandatory = mandatory;
        }
    }

    public class Extra implements Serializable{
        public String ExtraCode;
        public String ExtraDesc;
        public String Price;
        public int checked;


        public boolean isChecked () {
            return checked > 0;
        }
    }


    public LicenseDrive getLicenseDrive() {
        return licenseDrive;
    }

    public void setLicenseDrive(LicenseDrive licenseDrive) {
        this.licenseDrive = licenseDrive;
    }

    public String getDriverFName() {
        System.out.println("PUSHT F " + DriverFName);
        if (DriverFName != null && !DriverFName.isEmpty()){
            return DriverFName;
        }else {
            if (getLicenseDrive() != null && getLicenseDrive().getFirsName() != null){
                return getLicenseDrive().getFirsName();
            }
        }

        return "";
    }

    public String getDriverLName() {
        System.out.println("PUSHT L " + DriverLName);
        if (DriverLName != null && !DriverLName.isEmpty()){
            return DriverLName;
        }else {
            if (getLicenseDrive() != null && getLicenseDrive().getLastName() != null){
                return getLicenseDrive().getLastName();
            }
        }

        return "";
    }

    public String getDriverBDate() {
        if (DriverBDate != null && !DriverBDate.isEmpty()){
            return DateUtil.parseDateStringByFormat("ddMMyyyy",DriverBDate,DateUtil.FORMAT_JUST_DATE);
        }else{
           if (getLicenseDrive() != null && getLicenseDrive().getDriverBirthDate() != null){
               return getLicenseDrive().getDriverBirthDate();
           }
        }

        return DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE);
    }

    public String getPrivateID() {
        if (privateID != null && !privateID.isEmpty()){
            return privateID;
        }else{
            if (getLicenseDrive() != null && getLicenseDrive().getUserID() != null){
                return getLicenseDrive().getUserID();
            }
        }
        return "";
    }

    public String getDriverAddress() {
        if (DriverAddress != null && !DriverAddress.isEmpty()){
            return DriverAddress;
        }else {
            if (getLicenseDrive() != null && getLicenseDrive().getAddress() != null){
                return getLicenseDrive().getAddress();
            }
        }
        return "";
    }

    public String getDriverCity() {
        if (DriverCity != null && !DriverCity.isEmpty()){
            return DriverCity;
        }else{
            if (getLicenseDrive() != null && getLicenseDrive().getCity() != null){
                return getLicenseDrive().getCity();
            }
        }

        return "";
    }

    public String getDriverLicense() {
        if (DriverLicense != null && !DriverLicense.isEmpty()){
            return DriverLicense;
        }else {
            if (getLicenseDrive() != null && getLicenseDrive().getLicenseNumber() != null){
                return getLicenseDrive().getLicenseNumber();
            }
        }
        return "";
    }

    public String getDriverExpire() {
        if (DriverExpire != null && !DriverExpire.isEmpty()){
            return DateUtil.parseDateStringByFormat("ddMMyyyy",DriverExpire,DateUtil.FORMAT_JUST_DATE);
        }else{
            if (getLicenseDrive() != null && getLicenseDrive().getLicenseEndDate() != null){
                return getLicenseDrive().getLicenseEndDate();
            }
        }
        return "";
    }

    public String getLicenseType (){
        if (getLicenseDrive() != null && getLicenseDrive().getLicenseType() != null){
            return getLicenseDrive().getLicenseType();
        }

        return "";
    }

    public String getLicenseStartDate (){
        if (getLicenseDrive() != null && getLicenseDrive().getLicenseStartDate() != null){
            return getLicenseDrive().getLicenseStartDate();
        }

        return "";
    }


    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public String getAmount () {
        if (getCreditCard() != null && getCreditCard().getAmount() != null){
            return getCreditCard().getAmount();
        }

        return "";
    }

    public String getCurrencyCode () {
        if (getCreditCard() != null && getCreditCard().getCurrencyCode() != null){
            return getCreditCard().getCurrencyCode();
        }

        return "";
    }

    public String getCreditCardNumber () {
        if (getCreditCard() != null && getCreditCard().getCardNumber() != null){
            return getCreditCard().getCardNumber();
        }

        return "";
    }

    public int getValidityMonth () {
        if (getCreditCard() != null && getCreditCard().getMonthValidity() > 0){
            return getCreditCard().getMonthValidity();
        }

        return -1;
    }

    public int getValidityYear () {
        if (getCreditCard() != null && getCreditCard().getYearValidity() > 0) {
            return getCreditCard().getYearValidity();
        }

        return -1;
    }

    public String getCVV () {
        if (getCreditCard() != null && getCreditCard().getCvv() != null){
            return getCreditCard().getCvv();
        }

        return "";
    }

    public String getCreditCardUserID () {
        if  (getCreditCard() != null && getCreditCard().getUserID() != null){
            return getCreditCard().getUserID();
        }

        return "";
    }


    public String getCreditCardUserName () {
        if  (getCreditCard() != null && getCreditCard().getUserName() != null){
            return getCreditCard().getUserName();
        }

        return "";
    }


    @Override
    public String toString() {
        return "ContractDetailsResponse{" +
                "Usercg=" + Usercg +
                ", requestId=" + requestId +
                ", BContractNo='" + BContractNo + '\'' +
                ", DriverFName='" + DriverFName + '\'' +
                ", DriverLName='" + DriverLName + '\'' +
                ", DriverBDate='" + DriverBDate + '\'' +
                ", privateID='" + privateID + '\'' +
                ", DriverAddress='" + DriverAddress + '\'' +
                ", DriverCity='" + DriverCity + '\'' +
                ", DriverPhone='" + DriverPhone + '\'' +
                ", DriverLicense='" + DriverLicense + '\'' +
                ", DriverExpire='" + DriverExpire + '\'' +
                ", DriverMail='" + DriverMail + '\'' +
                ", DriverAgree=" + DriverAgree +
                ", Km=" + Km +
                ", Fuel=" + Fuel +
                ", DateOut='" + DateOut + '\'' +
                ", DateIn='" + DateIn + '\'' +
                ", BranchOut='" + BranchOut + '\'' +
                ", BranchIn='" + BranchIn + '\'' +
                ", J5=" + J5 +
                ", DriverDetails=" + DriverDetails +
                ", ConnectionDoc=" + ConnectionDoc +
                ", Currencies=" + Currencies +
                ", Photos=" + photosArray +
                ", Extras=" + Extras +
                ", licenseDrive=" + licenseDrive +
                ", creditCard=" + creditCard +
                '}';
    }
}
