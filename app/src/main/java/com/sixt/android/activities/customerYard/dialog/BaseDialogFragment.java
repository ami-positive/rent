/**
 * 
 */
package com.sixt.android.activities.customerYard.dialog;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;


/**
 * @author natiapplications
 *
 */
public class BaseDialogFragment extends DialogFragment {
	
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);



		setCancelable(false);

	}

}
