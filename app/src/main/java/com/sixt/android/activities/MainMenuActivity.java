package com.sixt.android.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;

import com.sixt.android.activities.customerYard.activities.CustomerYardSubMenuActivity_;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.app.dialog.DialogError;
import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.BranchNoValue;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.android.ui.ClearAllTransactionData;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.main_menu_activity)
@NoTitle
public class MainMenuActivity extends BaseActivity {


	public static String caption = "";
	public static String caption_elem = "";

	public static volatile String success_mst_to_show; // SHOW_SUCCESS_MSG_DIALOG

	@ViewById
	Button btn_mischarri;
	@ViewById
	Button btn_pnimmi;
	@ViewById
	TextView txt_hello_username;
	@ViewById
	TextView txt_branch;
	@ViewById
	Button btnCustomerYard;

	//	@ViewById
	//	Button btn_chatzar_lakoach;

	//=========

	private static int mode = 0;

	//	public static boolean Mischari() { return mode==0; } 
	//	public static boolean Pnimmi() { return mode==1; }
	public static boolean ChatzarLakoach() { return mode==2; }


	@Override
	protected void onResume() {
		super.onResume();
		AppUtil.saveScreenDimention(this);
		LoginActivity.call_finish();
		ClearAllTransactionData.clear();

		//	 CarSortUtils.set("");

		caption = "";
		caption_elem = "";
		BreadCrumbs.set("");
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_menu, menu);
		return true;
	}

	public static volatile boolean is_isuf = false; // isuf/medira
	public static volatile boolean rechev_kavua = false; // kavua/chalufi
	public static volatile boolean tofrom_lakoach = false; // sapak/lakoach 


	@Override
	protected void onStart() {
		super.onStart();
		MainActionValue.set("B");
		set_username_branch(txt_hello_username, txt_branch);
		is_isuf = false;
		rechev_kavua = false;
		tofrom_lakoach = false;
		FuelSgirraActivity_.toLicenseNumberActivity = false; // initial reference to finish activities and not jump into LicensingNumberActivity

		if (success_mst_to_show != null) {
			String msg = success_mst_to_show;
			success_mst_to_show = null;
			DialogError.show(this, msg);
		}
	}


	public static void set_username_branch(TextView username, TextView branch) {
		String name = Authentication_Response.get_WorkerName();
		name = (name==null ? "" : name);
		username.setText("משתמש: " + name); 
		branch.setText("סניף: " + BranchNoValue.getName()); 
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		int i = item.getItemId();
		switch (i)
		{
		case R.id.menu_testapp:
			//			ZTestActivity_.intent(this).start(); 
			return true;

		case R.id.menu_ver:
			show_ver();
			return true;				
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	private void show_ver() {
		String versionName;
		try {
			versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("סיקסט השכרה" + " גרסא " + versionName)
		.setCancelable(false)
		.setPositiveButton("אישור", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//do things
			}
		});
		AlertDialog alert = builder.create();
		alert.show();				
	}

	@Click
	void btn_mischarri() {
		mode = 0;
		PnimmiValue.set_is_mischarri();
		MainActionValue.set("B");
		BreadCrumbs.set("מסחרי");
		//		is_isuf = true;
		//		caption_elem = caption = "איסוף";		
		//		CurrentAction.set(CurrentAction.Values.Isuf_Rechev);
		//		CarNumberActivity.number = null;
		//		KavuaChalufiActivity_.intent(this).start();
		MyApp.generalSettings.setInCustomerYardMode(false);
		MenuMischarriActivity_.intent(this).start();
	}

	@Click
	void btn_pnimmi() {
		PnimmiValue.set_is_pnimmi();
		BreadCrumbs.set("פנימי");
		MainActionValue.set("I");
		mode = 1;
		MyApp.generalSettings.setInCustomerYardMode(false);
		MenuPnimmiActivity_.intent(this).start();
	}


	@Click
	void btnCustomerYard (){

		MyApp.generalSettings.setInCustomerYardMode(true);
		CustomerYardSubMenuActivity_.intent(this).start();
	}

	//	@Click
	//	void btn_chatzar_lakoach() {
	//		BreadCrumbs.set("חצר לקוח");
	//		MainActionValue.set("CHATZAR_LAKOACH");
	//		mode = 2;
	//	}


	//	@Click
	void btn_report_event() {
		is_isuf = false;
		//		caption = "אירוע";
		//		CurrentAction.set(null); //what is 'action' val for ReportEvent ?
		//		CarNumberActivity_.intent(this).start(); 
	}

	//	@Click
	void btn_mesirat_rechev() {
		is_isuf = false;
		caption_elem = caption = "מסירה";
		ActionValue.set(ActionValue.Values.Mesirrat_Rechev);
		KavuaChalufiActivity_.intent(this).start();
	}



}
