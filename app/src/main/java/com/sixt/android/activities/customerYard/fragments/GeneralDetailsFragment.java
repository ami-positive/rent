package com.sixt.android.activities.customerYard.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.activities.customerYard.activities.ContractDocActivity_;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.activities.customerYard.utils.FragmentsUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response_Inner;
import com.sixt.android.ui.FuelSlide;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by natiapplications on 18/12/15.
 */
@EFragment(R.layout.fragment_general_details)
public class GeneralDetailsFragment extends BaseFragment implements DriverDetailseActivity.ActionButtonsListener{


    private boolean fuel_was_alerted;
    private boolean fuel_alert_stop_mode;


    @ViewById
    TextView txtFragTitle;





    @ViewById
    TextView txtContractNumber;
    @ViewById
    TextView txtDriverName;
    @ViewById
    TextView model;
    @ViewById
    TextView txtRateDateBack;
    @ViewById
    TextView txtRateDateOut;
    @ViewById
    TextView txtBranchesBack;
    @ViewById
    TextView txtBranchesOut;
    @ViewById
    TextView txtLicensingNumber;
    @ViewById
    TextView txtBrandAndModel;
    @ViewById
    EditText etOUZMeter;
    @ViewById
    FuelSlide fuelSlide;
    private String MadUtz = "0";
    private boolean informUtzUp;
    private boolean informUtzDown;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @AfterViews
    public void onPostCreate () {

        AppUtil.hideKeyBoard(this);

        ((DriverDetailseActivity)getActivity()).setActionButtonsListener(this);
        ((DriverDetailseActivity)getActivity()).hideBaseDetails();
        txtFragTitle.setText(getFragmentName());
        fillScreenData();
    }

    @Override
    public int getFragmentIndex() {
        return 0;
    }

    @Override
    public String getFragmentName() {
        return "פרטים כלליים";
    }


    private void fillScreenData () {
        final ContractDetailsResponse contractDetails = getContractDetails();

        GetCarDetails_Response_Inner _cur = null;
        GetCarDetails_Response details = GetCarDetails_Response.carDetails_response;
        if (details!=null) {
            _cur = details.GetRentCarDetails_res;
            if (_cur==null) {
                _cur.CarModel="";
            }
        }



        if (contractDetails != null){
            txtBrandAndModel.setText(_cur.CarModel);
            txtContractNumber.setText(contractDetails.BContractNo);
            txtDriverName.setText(contractDetails.getFullName());
            txtRateDateOut.setText(contractDetails.DateOut);
            txtRateDateBack.setText(contractDetails.DateIn);
            txtBranchesOut.setText(contractDetails.BranchOut);
            txtBranchesBack.setText(contractDetails.BranchIn);
            txtLicensingNumber.setText(ContractChetchData.currentLicensingNumber);
            MadUtz = String.valueOf(contractDetails.Km);
            etOUZMeter.setText(MadUtz);
            ViewTreeObserver vto = fuelSlide.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    fuelSlide.set_fuel_level(contractDetails.Fuel);
                    fuelSlide.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                }
            });


//            etOUZMeter.requestFocus();
//            try {
//                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                fuelSlide.invalidate();
//            }catch (Exception e){e.printStackTrace();}

        }
    }


    public void updateContractData (){
        if(!etOUZMeter.getText().toString().isEmpty()) {
            getContractDetails().Km = Integer.parseInt(etOUZMeter.getText().toString());
        }
        getContractDetails().Fuel = fuelSlide.get_fuel_level();
        fuelSlide.invalidate();
        getContractDetails().saveToFile();
    }

    @Override
    public boolean onNext() {

        if (validation()){

            updateContractData();

            Fragment nextFragment = DriverDetailseActivity.getNextFragment(getClass().getSimpleName(),getContractDetails());
            if (nextFragment != null){
                FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                        nextFragment,R.id.driverDetailsContainer);
            }else {
                ContractDocActivity_.intent(getActivity())
                        .extra("km",getContractDetails().Km)
                        .extra("fuel", getContractDetails().Fuel)
                        .start();
            }
            return true;
        }

        return false;
    }

    private boolean validation() {

        StringBuilder errorMessage = new StringBuilder();
        boolean hasError = false;

        errorMessage.append("אנא השלם את השדות החסרים:" + "\n\n");
        System.out.println("FUEL X" + etOUZMeter.getText().toString().isEmpty());
        if (etOUZMeter.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append("מד אוץ"+"\n");
        }

        if (fuelSlide.get_fuel_level() < 0){
            hasError = true;
            errorMessage.append("מד דלק"+"\n");
        }

        System.out.println("ss" + fuel_was_alerted);
        System.out.println("sss" + fuel_alert_stop_mode);
        System.out.println("sss" + fuelSlide.get_fuel_level());

        if (!fuel_was_alerted || (fuel_was_alerted && fuel_alert_stop_mode)) {
            GetCarDetails_Response.FuelAlert f_alert;
            if ((f_alert=GetCarDetails_Response.fuel_alert_needed(fuelSlide.get_fuel_level())) != null) {
                errorMessage.append(f_alert.errmsg);
                fuel_was_alerted = true;
                fuel_alert_stop_mode = f_alert.stop;
                hasError = true;
            }
        }

        final String LARGER_MSG  = "הוזן מד אוץ גבוה מהמשוער";
        final String SMALLER_MSG = "הוזן מד אוץ נמוך מהידוע";


        if (GetCarDetails_Response.mad_utz_too_large(etOUZMeter.getText().toString()) && !informUtzUp) {
            errorMessage.append(LARGER_MSG);
            informUtzUp = true;
            hasError = true;
        }

        if (GetCarDetails_Response.mad_utz_too_small(etOUZMeter.getText().toString()) && !informUtzDown) {
            errorMessage.append(SMALLER_MSG);
            informUtzDown = true;
            hasError = true;
        }



        if (hasError){
            toast(errorMessage.toString());
        }

        return !hasError;

    }

    @Override
    public void onPrevious() {

    }

    @Override
    public void onClear() {
        Dialog2Btns.create(getActivity(), "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
            @Override
            public void run() {
                doClear();
            }
        }).show();

    }

    private void doClear (){
        etOUZMeter.setText("");
        fuelSlide.set_fuel_level(-1);
    }
}
