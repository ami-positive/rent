package com.sixt.android.activities;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.sixt.android.activities.base.BaseListActivity;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.PrivatenessOfCar;
import com.sixt.android.app.util.TEST;
import com.sixt.android.ui.DisplayLogic;
import com.sixt.android.ui.SubRejectsAdapter;
import com.sixt.android.ui.Debug_SubrejectAdapter;
import com.sixt.android.ui.Debug_SubrejectRecord;
import com.sixt.rent.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;


@EActivity
@NoTitle
public class SubRejectsActivity extends BaseListActivity { 

	// R.layout.subreject_entry;

	// SubrejectsAdapter 
	
	
	//>> problem in zmiggim if all marked as ok and Bitul is clicked before Ishur;  
	
	//>> add "Simmun" above check;
	
	//"instead of Mispar:" >>> "Mispar Rishuy" + same size as other screens; wsame size as Signature Screenn
	
	
	

	private SubRejectsAdapter the_adapter; 
	
	private static SubRejectsActivity inst; 

	private static final boolean _DEBUG = TEST.debug_mode(false);

	private static final int BOTTOM_BUTTONS_LAYOUT = R.layout.bottom_buttons2;

	private static final boolean IS_IMAGE = false;

	public static GetRejects_Reject current_reject;
	
	private SubReject[] all_available_subrejects;
	
	private static ArrayList<SubReject> oldValsArr;

//    private TextView simun_label;

//	private GetRejects_Reject orig_current_reject;
//	private ArrayList<SubReject> orig_active_subrejects;

	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inst = this;  
		
		oldValsArr = new ArrayList<SubReject>();

		setContentView(R.layout.reject_screen_layout);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View bottomView =  inflater.inflate(BOTTOM_BUTTONS_LAYOUT, null, false);

		
		TextView num = (TextView) findViewById(R.id.car_number);
		num.setText("מספר רישוי  " + CarNumberValue.get());
		
//		simun_label = (TextView) findViewById(R.id.simun_label);

		Button okButton = (Button) bottomView.findViewById(R.id.ok_button);
		okButton.setText("אישור");
		Button cancelButton = (Button) bottomView.findViewById(R.id.clear_button);
		cancelButton.setText("ביטול");

		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handle_ok();
			}
		});
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handle_cancel(true);
			}
		});
		ListView listView = getListView();
		listView.addFooterView(bottomView);

		boolean has_checkbox = false;
		if (_DEBUG) {
			setListAdapter(new Debug_SubrejectAdapter(this, debug_dialogValues));
		}
		else {
			has_checkbox = populate_list();         	
		}
		
//		int eol_visible = has_checkbox ? View.VISIBLE : View.INVISIBLE; 
//		simun_label.setVisibility(eol_visible);
	}

	
	public static void onClick_ListItem(SubReject current, boolean is_first_option, int old_val, boolean old_checked) {
		save_old_value(current, old_val, old_checked);
//		current.value = new_val;
//		current.left_check_selected = new_checked;
	}

	
	private static void save_old_value(SubReject current, int old_val, boolean old_checked) {
		String code = current.SubRejectCode;
		for (SubReject sr: oldValsArr) {
			if (sr.SubRejectCode.equals(code)) {
				return; // orig val already set
			}
		} 
		oldValsArr.add(new SubReject(code, old_val, old_checked));
	}
	
	

	private boolean populate_list() {
		boolean has_checkbox = false;
		if (current_reject==null) {
			return has_checkbox;
		} 
		
//		boolean is_private_car = GetCarDetails_Response.is_p_or_pm();
		boolean is_private = PrivatenessOfCar.is_p_or_pm();
		
//		orig_current_reject = current_reject.duplicate();
		all_available_subrejects = current_reject.filter_SubRejects(is_private);
		if (all_available_subrejects==null || all_available_subrejects.length==0) {
			throw new RuntimeException();
		}
		
//		clear_all_prior_selections(all_available_subrejects);
		
		ArrayList<RejectRecord> subrejects_for_car = //_get_actual_SUBrejects(all_available_subrejects);
				GetCarDetails_Response.get_actual_SUBrejects(all_available_subrejects);
		int val; 
		if (subrejects_for_car != null) {
			for (SubReject sr: all_available_subrejects) {
				if (sr.show_eol_checkbox()) {
					has_checkbox = true;
				}
				int orig_val = sr.value ;
				int jj=234;
				jj++;			
				if ((val=value_for_car(sr, subrejects_for_car)) > -1) {
					sr.value = val;
					jj++;
					jj++;
				}
			} 
		}
		
//		orig_active_subrejects = create_list_reject_arr();		
		setListAdapter((the_adapter=new SubRejectsAdapter(this, all_available_subrejects)));
		 
		return has_checkbox;
	}


//	private ArrayList<RejectRecord> _get_actual_SUBrejects(SubReject[] all_available_SUBrejects) {
//		ArrayList<RejectRecord> res = null;
////		if (CarNumberActivity.lastForm != null) {
////			res = CarNumberActivity.lastForm.get_actual_SUBrejects(all_available_SUBrejects);
////			int jj=234;
////			jj++;
////		}
////		else {
//			res = GetCarDetails_Response.get_actual_SUBrejects(all_available_SUBrejects);
//			int jj=234;
//			jj++;
////		}
//		return res;
//	}
//


//	private static void clear_all_prior_selections(SubReject[] r_arr) {
//		for (SubReject r: r_arr) {
//			r.value = 0;
//		}		
//	}


	private static int value_for_car(SubReject sub_reject, ArrayList<RejectRecord> active_rejects) {
		if (sub_reject==null || sub_reject.SubRejectCode==null) {
			return -1;
		}
		for (RejectRecord srcar: active_rejects) {
			if (sub_reject.SubRejectCode.equals(srcar.Code)) {
				// match
				return srcar.Value;
			}
		}
		return -1;
	}
	
	@Override
	public void onBackPressed() {
		handle_cancel(false);
	}
	

	protected void handle_cancel(boolean call_finish) {
		if (!_DEBUG) {
			revert_to_old_values();
			RejectListActivity.handle_subreject_cancellation();
		}
		if (call_finish) {
			do_finish();
		}
//		handle_ok();		
	}
		
	
	private void revert_to_old_values() {
		int jj=234;
		jj++;
		for (SubReject reject: all_available_subrejects) {
			for (SubReject old_reject: oldValsArr) {
				if (reject.SubRejectCode.equals(old_reject.SubRejectCode)) {
					reject.value = old_reject.value; // revert
					reject.left_check_selected = old_reject.left_check_selected; //
					oldValsArr.remove(old_reject); // optimization
					break;
				}
			}
		}		
	}


	protected void handle_ok() { //do_send()
		if (!_DEBUG) {
			if (items_with_no_value_exist()) { // if rejectCheck==1 >> no default 
				MyToast.makeText(this, " יש לציין ערך לכל אחד מהפריטים ברשימה", Toast.LENGTH_LONG).show(); 
				return;
			}
			if (mok_items_not_cleared()) {
				MyToast.makeText(this, " פריט חובה סומן כלא תקין", Toast.LENGTH_LONG).show(); 
				return;
			}
			ArrayList<SubReject> active_subrejects = create_list_reject_arr();
			RejectListActivity.handle_subreject_selection(current_reject, active_subrejects);
		}
		do_finish();
	}


	private boolean items_with_no_value_exist() {
		if (the_adapter==null) {
			return false;
		}
		int num = the_adapter.getCount();
		for (int i = 0; i < num; i++) {
			SubReject cur = the_adapter.getItem(i);
			if (cur.value < 0) {
				return true;
			}
		}
		return false;
	}
	
	
	
	private boolean mok_items_not_cleared() {
//		cmode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra();
		
		final boolean mok_is_active = DisplayLogic.mok_is_active(); //
		
		if (!mok_is_active) { 
			return false; // MOK is only for Mesirra mode
		}
		
		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
		if (all_available_subrejects==null) {
			return false;
		}
		for (SubReject sreject: all_available_subrejects) {
			if (sreject.value < 1) {
				continue; // not selected
			}
			if (sreject.is_mok()) {
				return true;
			}
		}
		return false;
	}
	
	
	private ArrayList<SubReject> create_list_reject_arr() {
		ArrayList<SubReject> res = new ArrayList<SubReject>();
		for (SubReject reject: all_available_subrejects) { 
			int value = reject.value;
			boolean checked = reject.left_check_selected;
//			if (value > 0) { 
			if (checked || (value >= 0)) { // also keep 'OK' values 
				res.add(reject);
			}
		}
		return res;
	}
	
	
	@UiThread
	void do_finish() {
		finish();
	}

	private static final Debug_SubrejectRecord[] debug_dialogValues = new Debug_SubrejectRecord[] {
		//debug data
		new Debug_SubrejectRecord("תקלות קופסא שחורה", "תקין", "חצי תקין" , "לא תקין","שמיש","הרוס","משומש"), 
		new Debug_SubrejectRecord("תקלות מנוע", /**/"תקין","חצי תקין", "לא תקין","שמיש",null, null),
		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש","הרוס",null),
		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין",null, null, null), 
		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש",null, null),
		new Debug_SubrejectRecord("תקלות קופסא שחורה", "תקין","חצי תקין", "לא תקין","שמיש","הרוס","משומש"), 
		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש","הרוס",null),
		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין",null, null, null),    	
	};

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}


	public static void handle_lesser_reject_set() {
		if (inst != null) {
			MyToast.makeText(inst, "לא ניתן להפחית חומרת תקלה", Toast.LENGTH_LONG).show();
		}
	}
	

}

