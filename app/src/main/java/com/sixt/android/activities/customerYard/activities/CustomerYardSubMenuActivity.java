package com.sixt.android.activities.customerYard.activities;

import android.os.Bundle;
import android.widget.Button;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_customer_yard_sub_menu)
@NoTitle
public class CustomerYardSubMenuActivity extends BaseActivity {


    @ViewById
    Button btnChange;
    @ViewById
    Button btnDelivery;
    @ViewById
    Button btnReturn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_yard_sub_menu);
    }


    @AfterViews
    public void onPostCreate (){

    }



    @Click
    void btnChange (){
        toast("אפשרות לא זמינה");
    }
    @Click
    void btnDelivery () {

        LicensingNumberActivity_.intent(this).start();

    }
    @Click
    void btnReturn (){
        toast("אפשרות לא זמינה");
    }
}
