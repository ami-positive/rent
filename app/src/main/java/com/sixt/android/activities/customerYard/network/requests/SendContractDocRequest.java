package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class SendContractDocRequest extends RequestObject<BaseResponseObject> {


    public int Fuel;
    public int Km;
    public String ContractDoc;
    public int PDASignatureID;


    public SendContractDocRequest(int km,int fuel,String  contractDoc, int PDASignatureID){
        extandTimeOut = true;
        this.ContractDoc = contractDoc;
        this.Km = km;
        this.Fuel = fuel;
        this.PDASignatureID = PDASignatureID;
    }

    @SerializedName("SendContractDoc_Req")
    private SendContractDocRequestInner contractDocRequestInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<BaseResponseObject> callback) {

        initRequestObj ();
        apiInterface.sendContractDoc(contractDocRequestInner, callback);

    }

    private void initRequestObj() {
        System.out.println("");
        contractDocRequestInner = new SendContractDocRequestInner(this.Km,this.Fuel,this.ContractDoc , this.PDASignatureID);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }




    public class SendContractDocRequestInner {


        public SendContractDocRequestInner (int km,int fuel,String  contractDoc, int PDASignatureID){
            this.ContractDoc = contractDoc;
            this.Km = km;
            this.Fuel = fuel;
            this.PDASignatureID = PDASignatureID;
        }

        public int Fuel;
        public int Km;
        public String Action = "O";
        public String App = "R";
        public int PDASignatureID;
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public String ContractDoc;
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "SendContractDoc_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
