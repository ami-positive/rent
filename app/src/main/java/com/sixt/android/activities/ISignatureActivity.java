package com.sixt.android.activities;

public interface ISignatureActivity {
	void onSignatureSet();
	void onSignatureClear();
}
