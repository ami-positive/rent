package com.sixt.android.activities.customerYard.pdf;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Base64;
import android.util.Log;

import com.sixt.android.activities.customerYard.utils.BitmapUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by natiapplications on 24/12/15.
 */
public class PDFUtil {


    private static final int MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory) throws IOException {

        URL url = new URL(fileUrl);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        //urlConnection.setRequestMethod("GET");
        //urlConnection.setDoOutput(true);
        urlConnection.connect();

        InputStream inputStream = urlConnection.getInputStream();
        FileOutputStream fileOutputStream = new FileOutputStream(directory);
        int totalSize = urlConnection.getContentLength();

        byte[] buffer = new byte[MEGABYTE];
        int bufferLength = 0;
        while ((bufferLength = inputStream.read(buffer)) > 0) {
            fileOutputStream.write(buffer, 0, bufferLength);
        }
        fileOutputStream.close();

    }


    public static String writePDFAsBase64ToFile(String fileAsBase64, int flag) throws Exception {
        System.out.println("6789 " + fileAsBase64);
        String destPat = BitmapUtil.createStringPathForStoringMedia("downloadedFDFFile.pdf");

        final File destFile = new File(destPat);

        byte[] pdfAsBytes = Base64.decode(fileAsBase64, Base64.DEFAULT);


        FileOutputStream fileOutputStream = new FileOutputStream(destFile);

        fileOutputStream.write(pdfAsBytes);
        fileOutputStream.flush();
        fileOutputStream.close();




        return destPat;
    }

    /**
     * Converts the given PDF file into an array of bytes
     *
     * @param fileName PDF file
     * @return The PDF file converted into a byte array
     * @throws Exception If the PDF document cannot be opened
     */
    public static byte[] readAssetFile(Context context, String fileName) {
        AssetManager am = context.getResources().getAssets();
        byte[] data = null;
        try {
            InputStream is = am.open(fileName);
            int size = is.available();
            if (size > 0) {
                data = new byte[size];
                is.read(data);
            }
            is.close();
        } catch (Exception ex) {
            Log.e("PlugPDF", "[ERROR] open fail because, ", ex);
        }
        return data;
    }

    public static String convertPDFFileToBase64String(String fileName) throws IOException {
        byte[] bytes = readFile(new File(fileName));
        return Base64.encodeToString(bytes, Base64.NO_WRAP);

    }


    public static byte[] readFile(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }
}
