package com.sixt.android.activities.customerYard.pdf;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Environment;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.utils.BitmapUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by natiapplications on 04/01/16.
 */
public class CopyAssetTask extends AsyncTask<String, Boolean, Boolean> {



    private String destPath;
    private boolean state;
    private String errDesc;
    private AssetCopyCallback callback;

    public CopyAssetTask (AssetCopyCallback callback){
        this.callback = callback;
    }


    @Override
    protected Boolean doInBackground(String... strings) {

        destPath = BitmapUtil.createStringPathForStoringMedia("OriginalPDFFromAsset.pdf");

        copyAssets(strings[0],destPath);


        return state;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (callback != null){
            callback.onCopy(aBoolean.booleanValue(), destPath, errDesc);
        }

    }



    private void copyAssets(String assetFileName , String destinationFileName) {
        AssetManager assetManager = MyApp.appContext.getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(assetFileName);
            out = new FileOutputStream(destinationFileName);
            copyFile(in, out);
            state = true;
        } catch(IOException e) {
            errDesc = "Failed to copy asset file: " + assetFileName;
            state = false;
        }
        finally {
            if (in != null) {
                try {in.close(); } catch (IOException e) {}
            }
            if (out != null) {
                try {out.close();} catch (IOException e) {}
            }
        }

    }


    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    public interface AssetCopyCallback {
        public void onCopy(boolean success,String destFilePat,String errDesc);
    }

}