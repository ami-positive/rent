package com.sixt.android.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseListActivity;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.json.objects.HearotLehachzarraRecord;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.TEST;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.android.ui.HearotLehachzarraAdapter;
import com.sixt.rent.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;


// R.layout.hearot_lehachzarra_layout
@EActivity
@NoTitle
public class HearotLehachzarraActivity extends BaseListActivity {

	// HearotLehachzarraAdapter
	
	
	public static HearotLehachzarraActivity inst; 

	private static final boolean _DEBUG = TEST.debug_mode(false);

	private static final int BOTTOM_BUTTONS_LAYOUT = R.layout.bottom_buttons2;

//	private static final boolean IS_IMAGE = false;

//	public static GetRejects_Reject current_reject;
	
//	private SubReject[] all_available_subrejects;
	
//	private static ArrayList<SubReject> oldValsArr;

    private TextView caption;


    private HearotLehachzarraAdapter cur_adapter;
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}

	
	public static ArrayList<HearotLehachzarraRecord> list_content;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inst = this; 
		
		
//		oldValsArr = new ArrayList<SubReject>();

		setContentView(R.layout.hearot_lehachzarra_layout);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View bottomView =  inflater.inflate(BOTTOM_BUTTONS_LAYOUT, null, false);
		
//		BreadCrumbs.append("הערות");
		final String cur_bcrums = BreadCrumbs.get() + BreadCrumbs.SEP + "הערות";

		caption = (TextView)findViewById(R.id.caption);
		caption.setText(cur_bcrums);

		TextView num = (TextView) findViewById(R.id.car_number);
//		num.setText("מספר: " + CarNumberActivity.number);
		final String _PREF = "מספר רישוי ";
		if (_DEBUG) {
			num.setText(_PREF + "33-245-66");
		}
		else {
			num.setText(_PREF + CarNumberValue.get());
		}

		Button okButton = (Button) bottomView.findViewById(R.id.ok_button);
		okButton.setText("אישור");
		Button cancelButton = (Button) bottomView.findViewById(R.id.clear_button);
		cancelButton.setText("ביטול");

		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handle_ok();
			}
		});
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handle_cancel();
			}
		});
		ListView listView = getListView();
		listView.addFooterView(bottomView);
		
		int e_layout = HearotLehachzarraAdapter.ENTRY_LAYOUT;
		list_content = getFirstRec(); 
		setListAdapter((cur_adapter=new HearotLehachzarraAdapter(this, e_layout, list_content)));  
 
//		populate_list();

	}


	private static ArrayList<HearotLehachzarraRecord> getFirstRec() {
		ArrayList<HearotLehachzarraRecord> arr = new ArrayList<HearotLehachzarraRecord>();
		arr.add(new HearotLehachzarraRecord());
		return arr;
	}

//	public static void add_line(HearotLehachzarraAdapter adapter) {
//		if (inst == null) {
//			return;
//		}
//		update_struct_from_ui();
//		verify_no_same_selection();
//		update_to_adapter(adapter);
//		list_content.add(new HearotLehachzarraRecord());
//		refresh_list();
//	}

	private static HearotLehachzarraRecord[] hearrot_arr = null;
	
	
	public static HearotLehachzarraRecord[] get() {
		return hearrot_arr;
	}
	
	public static void clear_hearrot() {
		hearrot_arr = null;
	}
	

//	public static void remove(HearotLehachzarraRecord to_remove, HearotLehachzarraAdapter adapter) {
//		update_to_adapter(adapter);
//		// delete to_remove
//		ArrayList<HearotLehachzarraRecord> nlist = new ArrayList<HearotLehachzarraRecord>(); 
//		for (HearotLehachzarraRecord c: list_content) {
//			if (c != to_remove) {
//				nlist.add(c);
//			}
//		}
//		list_content = nlist;
//		refresh_list(); 
//	}

	
//	private static void update_to_adapter(HearotLehachzarraAdapter adapter) {
//		list_content = new ArrayList<HearotLehachzarraRecord>();
//		for (int i = 0; i < adapter.getCount(); i++) {
//			list_content.add(adapter.getItem(i)); 
//		}
//	}


//	private static void refresh_list() {
//		int e_layout = HearotLehachzarraAdapter.ENTRY_LAYOUT;
//		inst.setListAdapter((inst.cur_adapter=new HearotLehachzarraAdapter(inst, e_layout, list_content)));
//	}
	
//	public static void onClick_ListItem(SubReject current, int selected_ind, boolean is_first_option, int old_val) {
//		save_old_value(current, old_val);
//		current.value = selected_ind; //!
//	}
//
//	
//	private static void save_old_value(SubReject current, int old_val) {
//		String code = current.SubRejectCode;
//		for (SubReject sr: oldValsArr) {
//			if (sr.SubRejectCode.equals(code)) {
//				return; // orig val already set
//			}
//		} 
//		oldValsArr.add(new SubReject(code, old_val));
//	}
//	

//	private void populate_list() {
//		if (current_reject==null) {
//			return;
//		}
//		
//		boolean is_private = PrivatenessOfCar.is_p_or_pm();
//		
//		all_available_subrejects = current_reject.filter_SubRejects(is_private);
//		if (all_available_subrejects==null || all_available_subrejects.length==0) {
//			throw new RuntimeException();
//		}
//		
//		ArrayList<RejectRecord> actual_SUBrejects = get_actual_SUBrejects(all_available_subrejects);
//		int val; 
//		if (actual_SUBrejects != null) {
//			for (SubReject sr: all_available_subrejects) {
//				if ((val=value_for_car(sr, actual_SUBrejects)) > -1) {
//					sr.value = val;
//				}
//			}
//		}
//		
//		setListAdapter(new CarSubrejectsAdapter(this, all_available_subrejects));		 
//	}

//
//	private ArrayList<RejectRecord> get_actual_SUBrejects(SubReject[] all_available_SUBrejects) {
//		ArrayList<RejectRecord> res = null;
//		if (CarNumberActivity.lastForm != null) {
//			res = CarNumberActivity.lastForm.get_actual_SUBrejects(all_available_SUBrejects);
//			int jj=234;
//			jj++;
//		}
//		else {
//			res = GetCarDetails_Response.get_actual_SUBrejects(all_available_SUBrejects);
//			int jj=234;
//			jj++;
//		}
//		return res;
//	}



//	private static int value_for_car(SubReject sub_reject, ArrayList<RejectRecord> active_rejects) {
//		if (sub_reject==null || sub_reject.SubRejectCode==null) {
//			return -1;
//		}
//		for (RejectRecord srcar: active_rejects) {
//			if (sub_reject.SubRejectCode.equals(srcar.Code)) {
//				// match
//				return srcar.Value;
//			}
//		}
//		return -1;
//	}
	
//	@Override
//	public void onBackPressed() {
//		handle_cancel();
//	}
	

	@UiThread
	protected void handle_cancel() {
		clear_hearrot();
		this.finish();
//		handle_ok(); -- before		
	}
		
  

	protected void handle_ok() { //do_send()
//		if (!_DEBUG) {
//			ArrayList<SubReject> active_subrejects = create_list_reject_arr();
//			RejectListActivity.handle_subreject_selection(current_reject, active_subrejects);
//		} 
		update_struct_from_ui();		
		if (!verify_code_selected_for_all()) {
			return;
		}
		if (!verify_no_same_selection()) {
			return;
		}
		set_hearrot_values();
//		do_finish();
//		SignatureActivity.show_decline_option = false;
		ImageCarActivity_.intent(this).start();
	}

	
	private static boolean verify_code_selected_for_all() {
		if (inst==null) {
			return true;
		}
		
		final int num = inst.cur_adapter.getCount();
		
		// allow if only one line and no text and no selection
		if (num==1 && first_line_empty()) {
			return true;
		}
		
		for (int i = 0; i < num; i++) {
			HearotLehachzarraRecord item = inst.cur_adapter.getItem(i);
//			String cap = item.caption;
			int curcode = item.actual_reject_code;
//			boolean is_last = (i == num-1);
//			if (!is_last && curcode <= 0) {
			if (curcode <= 0) {
				MyToast.makeText(inst, "לא נבחר קוד הערה", 1).show();
				return false;
			}
		}
		int jj=234;
		jj++;
		return true;
	}

	
	private static boolean first_line_empty() {
		HearotLehachzarraRecord first = inst.cur_adapter.getItem(0);
		int curcode = first.actual_reject_code;
		String txt = first.txt_description.trim();
		if (curcode < 1 && txt.length()==0) {
			return true;
		}		
		return false;
	}


	private static boolean verify_no_same_selection() {
		if (inst==null) {
			return true;
		}						
//		ArrayList<String> priors = new ArrayList<String>(); 
		ArrayList<Integer> priors = new ArrayList<Integer>();
		final int _num = inst.cur_adapter.getCount();
		for (int i = 0; i < _num; i++) {
			HearotLehachzarraRecord item = inst.cur_adapter.getItem(i);
//			String cap = item.caption;
			int curcode = item.actual_reject_code;
//			if (cap != null && cap.length() > 0) { 
				if (priors.contains(curcode)) {					
					MyToast.makeText(inst, "קוד הערה כפול", 1).show();
					return false;
				}
				priors.add(curcode); 
//			}
		}
		int jj=234;
		jj++; 
		return true;
	}

	
	private static void update_struct_from_ui() {
		if (inst != null && inst.cur_adapter != null) {
			inst.cur_adapter.update_struct_from_ui();
		}
	}


	private static void set_hearrot_values() {
		clear_hearrot();
		if (inst==null || inst.cur_adapter==null) {
			return;
		}
		final int count = inst.cur_adapter.getCount();
		hearrot_arr = new HearotLehachzarraRecord[count];  
		for (int i = 0; i < count; i++) {
			HearotLehachzarraRecord item = inst.cur_adapter.getItem(i);
//			String ciName = item.caption;
//			item.actual_reject_code = AllDataRepository.getRejectListInCiCode(ciName);
			hearrot_arr[i] = item;
		}		
	}

	
	
//	private ArrayList<SubReject> create_list_reject_arr() {
//		ArrayList<SubReject> res = new ArrayList<SubReject>();
//		for (SubReject reject: all_available_subrejects) { 
//			int value = reject.value;
//			if (value > 0) {
//				res.add(reject);
//			}
//		}
//		return res;
//	}
	
	
//	@UiThread
//	void do_finish() {
//		finish();
//	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}


	public static void cannot_delete_last_toast() {
		if (inst != null) {
			MyToast.makeText(inst, "לא ניתן למחוק שורה זו", 1).show();
		}
	}
	


	

//	private static final Debug_SubrejectRecord[] debug_dialogValues = new Debug_SubrejectRecord[] {
//		//debug data
//		new Debug_SubrejectRecord("תקלות קופסא שחורה", "תקין", "חצי תקין" , "לא תקין","שמיש","הרוס","משומש"), 
//		new Debug_SubrejectRecord("תקלות מנוע", /**/"תקין","חצי תקין", "לא תקין","שמיש",null, null),
//		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש","הרוס",null),
//		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין",null, null, null), 
//		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש",null, null),
//		new Debug_SubrejectRecord("תקלות קופסא שחורה", "תקין","חצי תקין", "לא תקין","שמיש","הרוס","משומש"), 
//		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין","שמיש","הרוס",null),
//		new Debug_SubrejectRecord("תקלות מנוע", "תקין","חצי תקין", "לא תקין",null, null, null),    	
//	};


}

