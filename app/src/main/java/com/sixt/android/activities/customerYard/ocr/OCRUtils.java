package com.sixt.android.activities.customerYard.ocr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.util.Log;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.utils.DateUtil;
import com.sixt.rent.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

/**
 * Created by natiapplications on 23/12/15.
 */
public class OCRUtils {


    public static  void configureLanguageFile (Context context,String fileName,int rawRes){

        File f = context.getFilesDir();
        InputStream in = context.getResources().openRawResource(rawRes);
        FileOutputStream out = null;

        File outFolder = new File(context.getFilesDir().getPath() + "/tessdata");
        File outFile = new File(outFolder.getAbsolutePath() + "/" + fileName );

        Log.e("FileLog", "File path: " + outFile.getAbsolutePath());

        if (outFolder.exists()) {
            outFolder.delete();
            outFolder.mkdir();
        }else{
            outFolder.mkdir();
        }

        // if (!outFile.exists()) {
        try {
            out = new FileOutputStream(outFile);
            byte[] buff = new byte[1024];
            int read = 0;

            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  }
    }



    public static Bitmap drawRectangleOnBitmap(Bitmap tempBitmap){

        Bitmap mutableBitmap = tempBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);

        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setAntiAlias(true);
        p.setFilterBitmap(true);
        p.setDither(true);
        p.setColor(MyApp.appContext.getResources().getColor(R.color.alpha_white));
        p.setStrokeWidth(10);

        //canvas.drawRect(0, 0, tempBitmap.getWidth(), tempBitmap.getHeight(), p);

        Point pTopLeft = new Point();
        Point pBotRight = new Point();

        //TODO:set x,y for points
        pTopLeft.set((tempBitmap.getWidth()/3) + (tempBitmap.getWidth()/20),tempBitmap.getHeight()/5);
        pBotRight.set(tempBitmap.getWidth(),tempBitmap.getHeight() /*- (tempBitmap.getHeight()/5)*/);

        Rect rHole = new Rect(pTopLeft.x, pTopLeft.y, pBotRight.x, pBotRight.y);
        //assume current clip is full canvas
        //put a hole in the current clip
        canvas.clipRect(rHole,  Region.Op.DIFFERENCE);
        //fill with semi-transparent red
        canvas.drawARGB(50, 255, 0, 0);
        //restore full canvas clip for any subsequent operations
        canvas.clipRect(new Rect(0, 0, canvas.getWidth(), canvas.getHeight())
                , Region.Op.REPLACE);

        return mutableBitmap;

    }


    public static boolean baseFieldsValidation(TextView... toValid) {
        boolean result = true;
        for (int i = 0; i < toValid.length; i++) {
            if (toValid[i].getText().toString().isEmpty()) {
                toValid[i].setError("Required field");
                result = false;
            } else {
                try {
                    toValid[i].setError(null);
                } catch (Exception e) {
                }
            }
        }
        return result;
    }

    public static String stringByAddingPercentEscapesUsingEncoding(String input, String charset) throws UnsupportedEncodingException {
        byte[] bytes = input.getBytes(charset);
        StringBuilder sb = new StringBuilder(bytes.length);
        for (int i = 0; i < bytes.length; ++i) {
            int cp = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
            if (cp <= 0x20 || cp >= 0x7F || (
                    cp == 0x22 || cp == 0x25 || cp == 0x3C ||
                            cp == 0x3E || cp == 0x20 || cp == 0x5B ||
                            cp == 0x5C || cp == 0x5D || cp == 0x5E ||
                            cp == 0x60 || cp == 0x7b || cp == 0x7c ||
                            cp == 0x7d
            )) {
                sb.append(String.format("%%%02X", cp));
            } else {
                sb.append((char) cp);
            }
        }
        return sb.toString();
    }

    public static String stringByAddingPercentEscapesUsingEncoding(String input) {
        try {
            return stringByAddingPercentEscapesUsingEncoding(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Java platforms are required to support UTF-8");
            // will never happen
        }
    }

    public static String getNumberFromString(String toParse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);
            if (temp >= '0' && temp <= '9') {
                result += temp;
            }
        }
        return result;
    }


    public static String cleanText(String toParse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);
            if (temp >= '0' && temp <= '9') {
                result += temp;
            }
            if (temp >= 'א' && temp <= 'ת'){
                result += temp;
            }
            if (temp == ' '){
                result += temp;
            }
            if (temp == '.'){
                result += temp;
            }
        }
        return result;
    }


    public static String cleanTextEng(String toParse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);
            if (temp >= '0' && temp <= '9') {
                result += temp;
            }
            Log.e("cleanTextEngLog","char at " + i + " : " + temp);
            if (temp >= 'A' && temp <= 'Z'){
                result += temp;
            }
            if (temp >= 'a' && temp <= 'z'){
                result += temp;
            }
            if (temp == ' '){
                result += temp;
            }
            if (temp == '.'){
                result += temp;
            }
        }
        return result;
    }

    public static String cleanNumber(String toParse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);

            if (temp >= 'א' && temp <= 'ת'){
                result += temp;
            }
            if (temp == ' '){
                result += temp;
            }

        }
        return result;
    }

    public static String cleanNumberEng(String toParse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);

            if (temp >= 'A' && temp <= 'Z'){
                result += temp;
            }
            if (temp >= 'a' && temp <= 'z'){
                result += temp;
            }
            if (temp == ' '){
                result += temp;
            }

        }
        return result;
    }

    public static String getFistSevenNumbersFromString(String toParse,boolean withElse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);
            if (temp >= '0' && temp <= '9') {
                result += temp;
            }else{
                if (!withElse){
                    return result;
                }else{
                    if (result.length() < 5){
                        result = "";
                    }
                }
            }

            if (result.length() == 7){
                return result;
            }
        }
        return result;
    }


    public static String getFistNineNumbersFromString(String toParse,boolean withElse) {
        String result = "";
        for (int i = 0; i < toParse.length(); i++) {
            char temp = toParse.charAt(i);
            if (temp >= '0' && temp <= '9') {
                result += temp;
            }else{
                if (!withElse){
                    return result;
                }else{
                    if (result.length() < 5){
                        result = "";
                    }
                }
            }

            if (result.length() == 9){
                return result;
            }
        }
        return result;
    }

    public static String[] splitDouble(String doubleString) {
        String[] result = new String[2];
        String a = "";
        String b = "";
        boolean isEndSide = false;
        for (int i = 0; i < doubleString.length(); i++) {
            char temp = doubleString.charAt(i);
            if (temp == '.') {
                isEndSide = true;
                continue;
            }
            if (isEndSide) {
                b += temp;
            } else {
                a += temp;
            }

        }
        if (a.isEmpty()) {
            a = "0";
        }
        if (b.isEmpty()) {
            b = "00";
        }
        result[0] = a;
        result[1] = b;
        return result;
    }

    public static int getStartDoubleLength(double value) {
        String priceString = new DecimalFormat("##.##").format(value);
        int counter = 0;
        for (int i = 0; i < priceString.length(); i++) {
            char temp = priceString.charAt(i);
            if (temp != '.') {
                counter++;
            } else {
                break;
            }
        }
        return counter;
    }


    public static boolean isValidValue(String str) {
        if (str != null && !str.trim().isEmpty()) {
            return true;
        }
        return false;
    }

    static int index;
    static int index2;
    static String res1;
    static String res2;
    static String res3;


    public static String[] getBirthDateAndLicenseStartDate (String ocr){

        Log.i("ADVANCED_OCR_LOG","ocr: " + ocr);
        boolean finish = false;

        Log.e("ADVANCED_OCR_LOG","index: " + index);
        if (index < ocr.length() && ocr.length() >= index+10){
            String sub = ocr.substring(index,index+10);

            Log.e("ADVANCED_OCR_LOG","sub: " + sub);

            String temp = findDateInString(sub);

            Log.e("ADVANCED_OCR_LOG","temp: " + temp);

            if (temp != null){

                Log.e("ADVANCED_OCR_LOG","res1: " + res1 + " res2: " + res2);

                if (res1 == null){
                    res1 = temp;
                    index+=10;
                }else if (res2 == null){
                    if(isValidDate(temp)){
                        res2 = temp;
                        finish = true;
                    }else {
                        index++;
                    }

                }
            }else {
                index++;
            }
        }else {
            finish = true;
        }

        if (finish){

            String[] result = new String[2];
            String copyRes1 = "";
            String copyRes2 = "";
            if (res1 != null){
                copyRes1 = new String(res1);
            }
            if (res2 != null){
                copyRes2 = new String(res2);
            }

            result[0] = copyRes1;
            result[1] = copyRes2;

            res1 = null;
            res2 = null;
            index = 0;

            return result;
        }else {
            return getBirthDateAndLicenseStartDate(ocr);
        }

    }

    public static String getLicenseEndDate (String ocr) {

        Log.i("ADVANCED_OCR_LOG","ocr: " + ocr);
        boolean finish = false;

        Log.e("ADVANCED_OCR_LOG","index: " + index2);
        if (index2 < ocr.length() && ocr.length() >= index2+10){
            String sub = ocr.substring(index2,index2+10);

            Log.e("ADVANCED_OCR_LOG","sub: " + sub);

            String temp = findDateInString(sub);

            Log.e("ADVANCED_OCR_LOG","temp: " + temp);

            if (temp != null){


                if (res3 == null){
                    if(isValidDate(temp)){
                        res3 = temp;
                        finish = true;
                    }else{
                        index2++;
                    }
                }else {
                    finish = true;
                }

            }else {
                index2++;
            }
        }else {
            finish = true;
        }

        if (finish){


            String copyRes1 = "";
            if (res3 != null){
                copyRes1 = new String(res3);
            }
            res3 = null;
            index2 = 0;

            return copyRes1;
        }else {
            return getLicenseEndDate(ocr);
        }
    }

    public static boolean isValidDate(String date){
        try{
            String[] split = date.split("/");

            return Integer.parseInt(split[2]) > 1900;
        }catch (Exception e){
            return false;
        }
    }

    public static String findDateInString (String source){

        String result = DateUtil.parseDateStringByFormat("dd.MM.yyyy",source,DateUtil.FORMAT_JUST_DATE);
        if (result != null && !result.isEmpty()){
            return result;
        }
        return null;
    }

    public static String[] findAddressAndCity(String ocr){

        String[] result = new String[2];

        String address = "";
        String city = "";
        boolean startCity = false;
        boolean hasNumber = false;
        boolean endNumber = false;

        for (int i = 0; i < ocr.length() ; i++) {
            char tempChar = ocr.charAt(i);

            if (tempChar >= '0'&& tempChar <= '9'){
                hasNumber = true;
            }else{
                if (hasNumber){
                    endNumber = true;
                    startCity = true;
                }
            }

            if (startCity && endNumber){
                city += tempChar;
            }else {
                address += tempChar;
            }

        }

        result[0] = address;
        result[1] = city;

        return result;

    }

}
