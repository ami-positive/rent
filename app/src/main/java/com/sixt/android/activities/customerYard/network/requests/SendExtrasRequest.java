package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import java.util.ArrayList;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class SendExtrasRequest extends RequestObject<BaseResponseObject> {


    private ArrayList<String> extras;


    public SendExtrasRequest(ArrayList<String> extras){
        this.extras = extras;
    }

    @SerializedName("SendExtras_Req")
    private ExtrasRequestInner  extrasRequestInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<BaseResponseObject> callback) {

        initRequestObj ();
        apiInterface.sendExtras(extrasRequestInner, callback);

    }

    private void initRequestObj() {
        extrasRequestInner = new ExtrasRequestInner(this.extras);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }




    public class ExtrasRequestInner {


        public ExtrasRequestInner (ArrayList<String> extras){
            this.Extras = extras;
        }

        public ArrayList<String> Extras;

        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "SendExtras_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
