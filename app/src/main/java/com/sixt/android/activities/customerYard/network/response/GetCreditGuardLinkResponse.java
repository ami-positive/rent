package com.sixt.android.activities.customerYard.network.response;

/**
 * Created by natiapplications on 26/01/16.
 */
public class GetCreditGuardLinkResponse extends BaseResponseObject {


    private String Url;
    private String Token;


    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

}
