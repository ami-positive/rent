/**
 * 
 */
package com.sixt.android.activities.customerYard.storage;

import android.util.Log;


import com.sixt.android.MyApp;

import java.io.Serializable;


/**
 * @author Nati Gabay
 *
 */
public class StorageManager {
	
	private final String TAG = "storatelog";


	public static final String FILE_NAME_COMPANIES = "fileNameCOMPANIES";
	public static final String FILE_NAME_LOCATIONS_TYPE = "fileLocationTypes";

	public static final String FILE_SELECTED_CURRENCY = "FILE_SELECTED_CURRENCY_NAME";
	public static final String FILE_CURRENT_CONTRACT = "FILE_CURRENT_CONTRACT";
	
	private static StorageManager instance;
	
	private StorageManager () {
		
	}
	
	public static StorageManager getInstance () {
		if (instance == null){
			instance = new StorageManager();
		}
		
		
		return instance;
	}
	
	public static void removeInstance () {
		instance = null;
	}
	
	public void readFromStorage (String fileName, StorageListener callback) {
		Log.d(TAG, fileName);
		new ReadFromStorageThread(MyApp.appContext, fileName, callback).start();
	}
	
	public void writeToStorage (String fileName, Serializable toWrite){
		Log.e(TAG, fileName);
		new WriteToStorageThread(MyApp.appContext, toWrite, fileName).start();
	}

}
