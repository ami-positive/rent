package com.sixt.android.activities.customerYard.dialog;

import android.support.v4.app.FragmentManager;

/**
 * Created by natiapplications on 26/01/16.
 */
public class DialogManager {


    public static void showCreditGuardDialog (String cardNum, FragmentManager fm,String url,DialogCallback callback, CreditGuardDialog.onLinkChanges onLinkChanges){

        CreditGuardDialog creditGuardDialog = new CreditGuardDialog();
        creditGuardDialog.setParams(cardNum, url,callback, onLinkChanges);
        creditGuardDialog.show(fm, CreditGuardDialog.DIALOG_NAME);

    }

    public static void showCreditGuardDialog (String cardNum, FragmentManager fm,String url, CreditGuardDialog.onLinkChanges onLinkChanges){

        CreditGuardDialog creditGuardDialog = new CreditGuardDialog();
        creditGuardDialog.setParams(cardNum, url, onLinkChanges);
        creditGuardDialog.show(fm, CreditGuardDialog.DIALOG_NAME);

    }

}
