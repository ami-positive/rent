package com.sixt.android.activities.customerYard.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.sixt.android.MyApp;
import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.activities.CarNumberMischarriMesirraActivity;
import com.sixt.android.activities.CarNumberMischarriMesirraActivity_;
import com.sixt.android.activities.FuelSgirraActivity_;
import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.RejectListActivity;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.GetContractDataRequest;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.OffSiteMode;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_licensing_number)
@NoTitle
public class LicensingNumberActivity extends BaseActivity {


    private final String DO_INSIDE_CLOSING = "I-C";
    private final String DO_CONTRACT_BODY = "B-O";
    private final String DO_CAR_CHEK_OUT = "B-D";

    // a flag that indicate if the CreditGuard dialog has no need to be shown again | init with false
    public static boolean isCreditProccessOver = false;

    @ViewById
    EditText car_number;
    @ViewById
    Button btn_ok;


    private static CarNumberActivity inst;
    public static boolean car_is_private = true;
    private boolean fromResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {


        super.onResume();



        if (!fromResult) {
            ContractChetchData.clear();
            car_number.setText("1542475");
        } else {
            fromResult = false;
        }

    }

    @Click
    void btn_ok() {

        String carNumber = validateCarNumber();
        ContractChetchData.currentLicensingNumber = carNumber;

        PnimmiValue.set_is_pnimmi();
        BreadCrumbs.set("פנימי");
        MainActionValue.set("I");
        OffSiteMode.val = 1;
        ActionValue.set(ActionValue.Values.Sgirra);

        if (carNumber != null) {
            showProgressDialog();
            bg_send_car_number(carNumber);
        }

    }

    private String validateCarNumber() {
        String num = car_number.getText().toString();

        if (num == null || num.length() != 7) {
            toast("מספר הרכב אינו חוקי!");
            return null;
        }
        //num = CarNumberUtils.format_car_number(num);

        return num;
    }


    @Background
    void bg_send_car_number(final String car_no) {

        final String action = ActionValue.get();
//		boolean goto_next_activity = true;

        try {
            boolean success = get_car_details_from_server(car_no, action);
            if (success) {
                FuelSgirraActivity_.toLicenseNumberActivity = true;
                CarNumberValue.set(car_no);
                   String WTD =  GetCarDetails_Response.getWhatToDo();
//                String WTD = DO_CONTRACT_BODY;
                open_next_activity(WTD, car_no); // move to next activity even if error
            }
        } finally {
            closeDialog();
        }

    }


    @UiThread
    public void closeDialog() {
        dismissProgressDialog();
    }

    private boolean get_car_details_from_server(String car_no, String action) {
        GetCarDetails_Response.clear();
        GetCarDetails_Request req = new GetCarDetails_Request(car_no);
        String req_str = GGson.toJson(req);
        try {
            Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
            // if success - keep "privateness" of car
            if (res instanceof GetCarDetails_Response) {
                car_is_private = GetCarDetails_Response.is_p_or_pm();
                return true; // success
            }
        } catch (ConnectionError e) {
            Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
            e.printStackTrace();
//			do_toast("ארעה תקלה בחיבור  " );
            do_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
        } catch (ActionFailedError e) {
            Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
            do_toast(e.getReasonDesc());
            boolean stay_in_screen = !e.is_recoverable_error(); // == 99
//			goto_next_activity = !stay_in_screen;
        } catch (Exception e) {
            // do not propagate
        }

        return false;
    }


    @UiThread
    void do_toast(String msg) {
        toast(msg);
    }

    @UiThread
    void open_next_activity(String what2do, String num) {
        dismissProgressDialog();
        CarNumberValue.set(num);
        if (what2do != null) {
            ToastUtil.toaster("What to do: " + what2do, false);
            if (what2do.equals(DO_INSIDE_CLOSING)) {
                startActivityForResult(new Intent(this, FuelSgirraActivity_.class), 7000);
            } else if (what2do.equals(DO_CONTRACT_BODY)) {

                getContractDetailsByCarNumber(num);
            } else if (what2do.equals(DO_CAR_CHEK_OUT)) {

                RejectListActivity.showHearot = true; // no need for showing Hearot screen
                MenuMischarriActivity.mode = 0; // enabling the mandatory rejects messages by changing to Mesirra()

                CarNumberMischarriMesirraActivity.fromDocActivity = true; // not from doc activity but the same process
                CarNumberMischarriMesirraActivity.carNumber = ContractChetchData.currentLicensingNumber;
                CarNumberMischarriMesirraActivity_.intent(this).start();            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 7000) {
                String CoNumber = data.getStringExtra("result").replace("-", "");
                fromResult = true;
                ContractChetchData.currentLicensingNumber = CoNumber;
                getContractDetailsByCarNumber(CoNumber);
            }
        }
    }




    public static void call_finish() {
        if (inst != null) {
            inst.finish();
        }
    }


    private void getContractDetailsByCarNumber(String carNumber) {
        showProgressDialog();
        MyApp.networkManager.makeRequest(new GetContractDataRequest(carNumber), new NetworkCallback<ContractDetailsResponse>() {

            @Override
            public void onResponse(boolean success, String errorDesc, ContractDetailsResponse response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ContractChetchData.currentContractNumber = response.getContractNomber();
                    response.saveToFile();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog();
                            DriverDetailseActivity_.intent(LicensingNumberActivity.this).start();
                        }
                    }, 1000);
                } else {
                    dismissProgressDialog();
                    toast(errorDesc);
                }
            }
        });
    }
}
