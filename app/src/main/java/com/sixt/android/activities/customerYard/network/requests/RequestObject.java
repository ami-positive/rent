package com.sixt.android.activities.customerYard.network.requests;


import android.util.Log;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.NetworkManager;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.utils.ToastUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by natiapplications on 16/08/15.
 */
public abstract class RequestObject<T extends BaseResponseObject> implements Callback<T>{


    protected ApiInterface apiInterface;

    protected boolean extandTimeOut;
    protected NetworkCallback<T> networkCallback;
    protected int remoteControllerType;

    protected abstract void execute(ApiInterface apiInterface, Callback<T> callback);
    protected abstract boolean showDefaultToastsOnError();

    public void request (ApiInterface apiInterface, final NetworkCallback<T> networkCallback){
        this.apiInterface = MyApp.networkManager.initRestAdapter(getServerEndPoint(), extandTimeOut);
        this.networkCallback = networkCallback;


        if (NetworkManager.isNetworkAvailable()){
            execute(this.apiInterface, this);
        }else{
            finishOnError("אינטרנט לא זמין");
        }


    }

    @Override
    public void success(T responseObject, Response response) {
        if (responseObject == null){
            finishOnError("שגיאה בקבלת הנתונים מהשרת");
            return;
        }

        if (responseObject.isSuccess()) {
            try {
                String responseString =  new String(((TypedByteArray)response.getBody()).getBytes());
                responseObject.setResponseBody(responseString);
            }catch (Exception e) {
                e.printStackTrace();
            }

            if (networkCallback != null) {
                networkCallback.onResponse(true, null, responseObject);
            }
        } else {
            finishOnError(responseObject.getErrorDesc());
        }
    }


    @Override
    public void failure(RetrofitError error) {
        finishOnError("שגיאה: " + error.getMessage());
        try {
            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            Log.v("failure", json.toString());
        }catch (Exception e){}

    }



    protected  void finishOnError (String error){
        showErrorToast(error);
        if (networkCallback != null) {
            networkCallback.onResponse(false, error,null);
        }
    }


    public void showErrorToast (String errorMessage){
        if (showDefaultToastsOnError()){
            ToastUtil.toaster(errorMessage, false);
        }
    }

    public String getServerEndPoint(){
        return MyApp.serverEndPoint;
    }


}
