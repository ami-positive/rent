/**
 * 
 */
package com.sixt.android.activities.customerYard.fragments;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.Toast;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity_;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.ProgressDialogUtil;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.dialog.DialogError;


/**
 * @author Nati Gabay
 *
 */
public abstract class BaseFragment extends Fragment implements OnGlobalLayoutListener {



	protected String TAG = "lifeCycleLog";

	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected FragmentChangeListener fragmentListener;
	
	public boolean onBackPressed () {
		return false;
	}
	
	public abstract int getFragmentIndex();
	public abstract String getFragmentName();

	public void showProgressDialog(String message) {
		this.progressDialog = 
				ProgressDialogUtil.showProgressDialog(getActivity(), message);
	}
	
	public void showProgressDialog() {
		try {
			this.progressDialog = 
			         ProgressDialogUtil.showProgressDialog(getActivity(),"אנא המתן...");
		} catch (Exception e) {}
	}

	public void setProgressDialogMessage (String message){
		try{
			if (this.progressDialog != null && this.progressDialog.isShowing()){
				this.progressDialog.setMessage(message);
			}

		}catch (Exception e ){} catch (Error e){}
	}
	
	public void dismisProgressDialog() {
		ProgressDialogUtil.dismisDialog(this.progressDialog);
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		//TAG = getClass().getSimpleName() + "Log";
		Log.d(TAG, "ovViewCreated");
		view.getViewTreeObserver().addOnGlobalLayoutListener(this);
		fragIsOn = true;

		hideKeyBoard(getActivity());
		
	}




	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		Log.e(TAG, getClass().getSimpleName() + " onCreateView");

		if (fragmentListener != null){
			try{
				fragmentListener.onFragmentChanged(getFragmentName(), getFragmentIndex());
			}catch (Exception e){}catch (Error r){}
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, getClass().getSimpleName() + " onResume");
	}
	
	
	@Override
	public void onStart() {
		super.onStart();
		Log.i(TAG, getClass().getSimpleName() + " onStart");
	}
	
	
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("onDestroyLog", "onDestroy");
		fragIsOn = false;

	}

	
	
	@SuppressLint("NewApi")
	@Override
	public void onGlobalLayout() {
		try {
			getView().getViewTreeObserver().removeOnGlobalLayoutListener(this); 
			onViewRendered(getView());
		} catch (Exception e) {} catch (Error e){}
		
	}
	
	public void onViewRendered(View view){
		Log.d(TAG, getClass().getSimpleName() +  " onViewRendered");
	}
	
	
	public BaseActivity getBaseActivity() {
		BaseActivity activity = null;
		
		try {
			activity = (BaseActivity) getActivity();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}

		return activity;
	}

	public void onNewIntent(Intent intent){

	}


	public void setFragmentListener (FragmentChangeListener fragmentListener){
		this.fragmentListener = fragmentListener;
	}

	public interface FragmentChangeListener {
		public void onFragmentChanged(String newFragment, int index);
	}


	public static void hideKeyBoard (Activity activity){
		activity.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
		);
	}

	public void toast(String text, boolean success) {
		if (success) {
			MyToast.makeText_success(getActivity(), text, Toast.LENGTH_LONG).show();
		}
		else {
			toast(text);
		}
	}


	public void toast(String text) { // 'error' type of toast
//		MyToast.makeText(this, text, Toast.LENGTH_LONG).show();
		DialogError.show(getActivity(), text); //ggg show 'error' type toasts in popup
	}


	public ContractDetailsResponse getContractDetails () {
		return ((DriverDetailseActivity_)getActivity()).getContractDetails();
	}


}