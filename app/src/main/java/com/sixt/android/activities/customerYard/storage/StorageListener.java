package com.sixt.android.activities.customerYard.storage;



public interface StorageListener {
	
	public void onDataStorageReceived(boolean success,String errDesc, Object object);

}
