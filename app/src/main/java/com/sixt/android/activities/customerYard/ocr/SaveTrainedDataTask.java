package com.sixt.android.activities.customerYard.ocr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.sixt.android.activities.customerYard.utils.ProgressDialogUtil;
import com.sixt.rent.R;

/**
 * Created by natiapplications on 23/12/15.
 */
public class SaveTrainedDataTask extends AsyncTask<Void,Void,Boolean> {


    private ProgressDialog dialog;
    private Activity activity;


    public SaveTrainedDataTask (Activity activity){
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialogUtil.showProgressDialog(activity, "Please wait...");
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OCRUtils.configureLanguageFile(activity,"eng.traineddata", R.raw.eng);
        OCRUtils.configureLanguageFile(activity,"heb.traineddata", R.raw.heb);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        ProgressDialogUtil.dismisDialog(dialog);
    }
}
