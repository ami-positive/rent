package com.sixt.android.activities.customerYard.models;


import com.sixt.android.MyApp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by natiapplications on 22/11/15.
 */
public class ErrorCorrection implements Serializable{


    private String originalWord;

    private String result;

    private boolean isCorrected;

    public ErrorCorrection (String originalWord){
        this.originalWord = originalWord;
    }


    public String findCorrection (){

        result = originalWord;


        if (originalWord != null && !originalWord.isEmpty()){
            ArrayList<WordReport> matches = MyApp.wordsReportManager.getByOriginalWord(this.originalWord);
            if (matches.size() > 0){

                int tempCount = 0;

                for (int i = 0; i < matches.size(); i++) {

                    int count = Collections.frequency(matches,matches.get(i));
                    if (count > tempCount){
                        tempCount = count;
                        result = matches.get(i).getCorrectWord();

                    }

                }
            }
        }


        if (!this.originalWord.equals(result)){
            isCorrected = true;
        }
        return result;
    }

    public boolean isCorrected (){
        return this.isCorrected;
    }

}
