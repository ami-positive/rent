package com.sixt.android.activities.customerYard.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.activities.customerYard.fragments.BaseFragment;
import com.sixt.android.activities.customerYard.fragments.CreditFragment_;
import com.sixt.android.activities.customerYard.fragments.DriverDetailseFragment_;
import com.sixt.android.activities.customerYard.fragments.ExtrasFragment_;
import com.sixt.android.activities.customerYard.fragments.GeneralDetailsFragment_;
import com.sixt.android.activities.customerYard.fragments.PhotosFragment_;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.storage.StorageListener;
import com.sixt.android.activities.customerYard.ui.StepIndicator;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.activities.customerYard.utils.FragmentsUtil;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_driver_details)
@NoTitle
public class DriverDetailseActivity extends BaseActivity implements BaseFragment.FragmentChangeListener {


    @ViewById
    Button btnNext;
    @ViewById
    Button btnPrevious;
    @ViewById
    Button btnClear;
    @ViewById
    StepIndicator stepIndicator;
    @ViewById
    LinearLayout llBaseDetails;
    @ViewById
    TextView txtDriverName;
    @ViewById
    TextView txtLicensingNumber;
    @ViewById
    TextView txtContractNumber;


    private ActionButtonsListener actionButtonsListener;


    private ContractDetailsResponse contractDetails;

    public static Fragment getNextFragment(String currentFragment, ContractDetailsResponse contractDetails) {
        System.out.println("getNextFragment " + currentFragment);
        if (currentFragment.equals(GeneralDetailsFragment_.class.getSimpleName())) {
//            if (contractDetails.isNeedTakeCreditCard()){
            return new CreditFragment_();
////            }
//            else if (contractDetails.isNeedTakePhotos()){
//                return new PhotosFragment_();
//            }else if (contractDetails.isNeedTakeDriverDetails()){
//                return new DriverDetailseFragment_();
//            }else if (contractDetails.isHasExtras()){
//                return new ExtrasFragment_();
//            }else{
//                return null;
//            }
        } else if (currentFragment.equals(CreditFragment_.class.getSimpleName())) {

            if (contractDetails.isNeedTakePhotos()) {
                return new PhotosFragment_();
            } else if (contractDetails.isNeedTakeDriverDetails()) {
                return new DriverDetailseFragment_();
            } else if (contractDetails.isHasExtras()) {
                return new ExtrasFragment_();
            } else {
                return null;
            }

        } else if (currentFragment.equals(PhotosFragment_.class.getSimpleName())) {
            if (contractDetails.isNeedTakeDriverDetails()) {
                return new DriverDetailseFragment_();
            } else if (contractDetails.isHasExtras()) {
                return new ExtrasFragment_();
            } else {
                return null;
            }
        } else if (currentFragment.equals(DriverDetailseFragment_.class.getSimpleName())) {
            if (contractDetails.isHasExtras()) {
                return new ExtrasFragment_();
            } else {
                return null;
            }
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    public void onPostCreate() {
        initContractDetails();
    }

    private void initContractDetails() {

        ContractDetailsResponse.readFromFile(ContractChetchData.currentContractNumber, new StorageListener() {
            @Override
            public void onDataStorageReceived(boolean success, String errDesc, Object object) {

                if (success) {
                    contractDetails = (ContractDetailsResponse) object;
                    txtDriverName.setText(contractDetails.getFullName());
                    txtContractNumber.setText(contractDetails.BContractNo);
                    txtLicensingNumber.setText(ContractChetchData.currentLicensingNumber);
                }
                defineCurrentStep();
            }
        });

    }

    private void defineCurrentStep() {

        if (contractDetails == null) {
            ToastUtil.toaster("שגיאה בניסיון לקרוא את פרטי החוזה !", false);
            finish();
            return;
        }


        stepIndicator.setSteps(0, contractDetails.getSteps());
        openNextStepFragment();

    }

    public ContractDetailsResponse getContractDetails() {

        return contractDetails;

    }

    @Click
    void btnNext() {
        if (actionButtonsListener != null) {
            if (actionButtonsListener.onNext()) {
                stepIndicator.next();
            }
        }
    }

    @Click
    void btnPrevious() {
        if (stepIndicator.getCurrentPosition() <= 0) {
            finish();
        } else {
            stepIndicator.previous();
            getSupportFragmentManager().popBackStack();
        }

        if (actionButtonsListener != null) {
            actionButtonsListener.onPrevious();
        }

    }

    @Click
    public void btnClear() {
        if (actionButtonsListener != null) {
            actionButtonsListener.onClear();
        }
    }

    @Override
    public void onFragmentChanged(String newFragment, int index) {
        stepIndicator.setCurrentPosition(index);
    }

    public void showBaseDetails() {
        this.llBaseDetails.setVisibility(View.VISIBLE);
    }

    public void hideBaseDetails() {
        this.llBaseDetails.setVisibility(View.GONE);
    }

    private void openNextStepFragment() {
        FragmentsUtil.addFragment(getSupportFragmentManager(),
                new GeneralDetailsFragment_(), R.id.driverDetailsContainer);
    }

    public void setActionButtonsListener(ActionButtonsListener listener) {
        this.actionButtonsListener = listener;
    }

    public interface ActionButtonsListener {
        public boolean onNext();

        public void onPrevious();

        public void onClear();
    }

}
