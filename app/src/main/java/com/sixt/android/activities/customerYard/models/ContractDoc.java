package com.sixt.android.activities.customerYard.models;

import android.util.Log;

import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

import java.util.ArrayList;

/**
 * Created by natiapplications on 03/01/16.
 */
public class ContractDoc {


    private ArrayList<PageSignatures> pageSignatures;


    public ContractDoc(ArrayList<PageSignatures> pageSignatures){
        this.pageSignatures = pageSignatures;
    }


    public boolean isPageNeedSignature (int pagePos) {
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                PageSignatures temp = pageSignatures.get(pagePos);
                if (temp.isNeedSignatures() && !temp.isSign()){
                    return true;
                }
            }catch (Exception e){
                return false;
            }
        }

        return false;
    }


    public void addSignatureToPage (int pagePos) {
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                this.pageSignatures.get(pagePos).increment();
            }catch (Exception e){}
        }
    }

    public int getPageLeftSignaturesCount (int pagePos){
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                return this.pageSignatures.get(pagePos).getLeftSignaturesCount();
            }catch (Exception e){}
        }
        return 0;

    }

    public int getPageSignaturesCount (int pagePos){
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                return this.pageSignatures.get(pagePos).getSignaturesCount();
            }catch (Exception e){}
        }
        return 0;

    }

    public double getPageSignaturePositionX (int pagePos){
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                return this.pageSignatures.get(pagePos).getPageSignaturePositionX();
            }catch (Exception e){}
        }

        return 0;
    }


    public double getPageSignaturePositionY (int pagePos){
        if (pagePos > 0){
            pagePos -= 1;
        }
        if (this.pageSignatures != null){
            try{
                return this.pageSignatures.get(pagePos).getPageSignaturePositionY();
            }catch (Exception e){}
        }

        return 0;
    }


    public ArrayList<Integer> isHasAllSignatures () {

        ArrayList<Integer> result = new ArrayList<Integer>();

        if (this.pageSignatures != null){
            for (int i = 0; i < pageSignatures.size() ; i++) {
                if (pageSignatures.get(i).isNeedSignatures() && !pageSignatures.get(i).isSign()){
                    result.add(i+1);
                }
            }
        }

        if (result.size() > 0){
            return result;
        }
        return null;
    }



    public static void createByPDF (final String pdfSrcPath,final OnPageCreated callback){
        final String PDF_TAG = "pdfTag";
        Thread back = new Thread(){

            PdfReader reader;
            ArrayList<PageSignatures> pageSignatures;
            boolean success = true;

            @Override
            public void run() {
                super.run();


                try {
                    try{
                        Log.i(PDF_TAG, "pdfSrcPath1: " + pdfSrcPath);

                        reader = new PdfReader(pdfSrcPath);
                        Log.i(PDF_TAG, "pdfSrcPath2: " + pdfSrcPath);
                        Log.i(PDF_TAG, "reader: " + reader);
                    }catch (InvalidPdfException e){
                        e.printStackTrace();
                        Log.e(PDF_TAG, "InvalidPdfException: " + pdfSrcPath);
                    }

                    pageSignatures = new ArrayList<PageSignatures>();
                    Log.i(PDF_TAG, "NumberOfPages: " + reader.getNumberOfPages());
                    for (int i = 0; i < reader.getNumberOfPages() ; i++) {

                        PageSignatures tempPageSignatures = new PageSignatures(i+1);
                        Log.i(PDF_TAG, "tempPageSignatures: " + tempPageSignatures);
                        RenderListener listener = new MyRenderListener(tempPageSignatures);
                        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
                        PdfDictionary pageDic = reader.getPageN(i+1);
                        PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
                        processor.processContent(ContentByteUtils.getContentBytesForPage(reader, i + 1), resourcesDic);

                        pageSignatures.add(tempPageSignatures);

                    }
                    Log.i(PDF_TAG, "pageSignatures: " + pageSignatures);
                    reader.close();
                } catch (Exception e) {
                    Log.e(PDF_TAG, "Exception: " + e.getMessage());
                    e.printStackTrace();
                    success = false;
                } finally {

                    ContractDoc contractDoc = new ContractDoc(pageSignatures);
                    Log.i(PDF_TAG, "contractDoc: " + contractDoc);
                    if (callback != null){
                        callback.onPageCreated(success,contractDoc);
                    }

                }
            }
        };

        back.setPriority(Thread.MAX_PRIORITY);
        back.start();
    }


    public interface OnPageCreated {
        public void onPageCreated(boolean sucess,ContractDoc contractDoc);
    }

    public static class MyRenderListener implements RenderListener {


        private PageSignatures pageSignatures;
        private String tempBlock;
        private TextRenderInfo renderInfo;


        public MyRenderListener (PageSignatures pageSignatures){
            this.pageSignatures = pageSignatures;
        }


        @Override
        public void beginTextBlock() {
            Log.e("PDF_PARSE_LOG", "begin text block");
            tempBlock = "";

        }

        @Override
        public void renderText(TextRenderInfo renderInfo) {
           // Log.e("PDF_PARSE_LOG", "render text = " + renderInfo.getText().toString());

            tempBlock += renderInfo.getText();
            this.renderInfo = renderInfo;
        }

        @Override
        public void endTextBlock() {
            Log.e("PDF_PARSE_LOG", "end block");

            if (tempBlock.contains("sig")){
                Log.d("PDF_PARSE_LOG", " ************** Add signature *****************");
                pageSignatures.addSignature(new SignatureState(renderInfo));
                Log.d("PDF_PARSE_LOG", " page: " + pageSignatures.getPageNumber() + " counter: " + pageSignatures.getSignaturesCount());
            }

        }

        @Override
        public void renderImage(ImageRenderInfo renderInfo) {
           // Log.e("PDF_PARSE_LOG", "Value = " + renderInfo.toString());

        }


    }
}
