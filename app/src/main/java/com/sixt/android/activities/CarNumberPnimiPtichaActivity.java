package com.sixt.android.activities;


import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.ContractRecord;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActionValue.Values;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CarNumberUtils;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.NetworkState;
import com.sixt.android.app.util.TEST;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.pnimmi_pticha_number_layout)
@NoTitle
public class CarNumberPnimiPtichaActivity extends BaseActivity {

    public static volatile SendForm_Request lastForm; // to be used at Mesirrat_Rechev
 
    
	private static final boolean _DEBUG = TEST.debug_mode(false);
	
	private static final String DEBUG_NO = "5797911";//TEST.NUM_PNIMMI_PTICHA;
	
	

	private static CarNumberPnimiPtichaActivity inst;
	 
//	public static String number;


	public static boolean car_is_private = true;
	
	
	@ViewById
	TextView caption;
			
    @ViewById
    EditText car_number; //==login
    
    @ViewById
    Button btn_ok; // send  void btn_ok() 
    

    @ViewById
	Spinner contract_type_spinner;   //HAS first empty line; NO default 

     
    @ViewById
    TextView txt_caption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	inst = this;
    	lastForm = null;
    	car_is_private = true;
    	GetCarDetails_Response.carDetails_response = null;
//    	number = ""; // clear old value
    }
     
	@AfterViews
	void post_onCreate() {
		CarNumberValue.clear();
		set_caption();		
				
		if (DeviceAndroidId.is_lab_device(this)) {
			car_number.setText(DEBUG_NO);
//			number = CarNumberUtils.format_car_number(DEBUG_NO);
		}
		
		if (_DEBUG) {
			debug_populate_spinner();
		}
		else {
			populate_contract_types();
		}
	} 	

	
	private void populate_contract_types() {
		ArrayAdapter <String> adapter =
				new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter.add( "");
		ContractRecord[] all = AllDataRepository.getAllContracts();
		if (all != null) {
			for (ContractRecord rec: all) {
				if (rec.ForDisplay == 1) {   
					adapter.add(rec.ContractName);
				}
			}
		}
		contract_type_spinner.setAdapter(adapter);
		
	}
	
	
    private void debug_populate_spinner() {
		ArrayAdapter <String> adapter =
				new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter.add( "");
		adapter.add( "דליוורי");					 
		adapter.add( "מכללה");
		adapter.add( "בודד");
		adapter.add( "חוזר");
		contract_type_spinner.setAdapter(adapter);
	}

	private void set_caption() {
//		caption.setText(BreadCrumbs.get());
//		CaptionSetter.set(caption);
//		txt_caption.setText("");
//		String txt = "";
//		if (MainMenuActivity.Mischari()) {
//			if (MischarriMenuActivity.Mesirra()) {
//				txt = "מסחרי - מסירה";
//			}
//			else if (MischarriMenuActivity.Hachzarra()) {
//				txt = "מסחרי - החזרה";
//			}
//			else {
//				txt = "מסחרי  ";
//			}
//		}
//		else if (MainMenuActivity.Pnimmi()) {
//			if (PnimmiMenuActivity.pticha()) {
//				txt = "פנימי - פתיחה";				
//			}
//			else if (PnimmiMenuActivity.sgirra()) {
//				txt = "פנימי - סגירה";
//			}
//			else if (PnimmiMenuActivity.mesirra()) {
//				txt = "פנימי - מסירה";
//			}
//			else if (PnimmiMenuActivity.hachzarra()) {
//				txt = "פנימי - החזרה";
//			}
//			else {
//				txt = "פנימי ";
//			}
//		}
//		else if (MainMenuActivity.ChatzarLakoach()) {
//			txt = "חצר - ";
//		}
//		
//		txt_caption.setText(txt);		
	}

	@Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	inst = null;
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart(); 
//    	caption.setText(MainMenuActivity.caption);
    	String bcrums = BreadCrumbs.get(); 
    	caption.setText(bcrums); 
    }
    

    @Click
    void btn_ok() {
    	GetCarDetails_Response.carDetails_response = null;
//    	number = "";
    	
    	if (contract_type_spinner.getSelectedItemPosition() < 1) {
			toast("יש לציין סוג חוזה");
			return;    		
    	}
    	
    	String type_str = (String) contract_type_spinner.getSelectedItem();    	
    	int contract_type = AllDataRepository.getContractTypeCode(type_str);
    	
    	String num = car_number.getText().toString();
    	CarNumberValue.set_with_contract_type(num, contract_type);
    	    	
    	if (_DEBUG) {
    		open_progress_dialog("מעביר נתונים לשרת...");
    		new Timer().schedule(new TimerTask() {				
				@Override
				public void run() {
					open_next_activity();
				}
			}, 800);
    	}
    	else {
    		if (num == null || num.length() != 7) {
    			toast("מספר הרכב אינו חוקי!");
    			return;    			
    		}
    		num = CarNumberUtils.format_car_number(num);
    		if (!NetworkState.is_connected(this)) {
    			toast("אין חיבור רשת פעיל!");
    			return;
    		}
    		open_progress_dialog("מתחבר לשרת. אנא המתן...");
    		bg_send_car_number(num);
    	}    	    	
    }

    
	@Background
	void bg_send_car_number(final String car_no) {				
		final String action = ActionValue.get();
//		boolean goto_next_activity = true;
				
		try  {
//			boolean skip_server = false;
			final String MESIRRA = Values.Mesirrat_Rechev.value;

//			if (MESIRRA.equals(action)) { //!!
//				LastFormStruct res = SignatureActivity.get_last_form_for_number(car_no); //use last stored form
//				lastForm = res.last_form;
//				boolean match = res.is_match; 
//				if (!match) {
//					do_toast("הפריט לא תואם את האיסוף. אנא בדוק את התפריט" );
//					// and allow user to continue
//				}

//				if (lastForm != null) {
//					car_is_private = lastForm.is_p_or_pm();
//					skip_server = true;
//				}
//			}

//			if (!skip_server) {
				boolean success = get_car_details_from_server(car_no, action);		
//			}
			
			if (success) {
				open_next_activity(); // move to next activity even if error
			}
		}
		finally {
			close_progress_dialog();	
		}

	}



	private boolean get_car_details_from_server(String car_no, String action) {
		GetCarDetails_Response.clear();
		GetCarDetails_Request req = new GetCarDetails_Request(car_no); 
		String req_str = GGson.toJson(req);
    	try {
			Base_Response_JsonMsg res = JsonTransmitter.send_blocking(req_str);
			// if success - keep "privateness" of car
			if (res instanceof GetCarDetails_Response) {
				car_is_private = GetCarDetails_Response.is_p_or_pm();
				return true; //success
			}			
		} 
    	catch (ConnectionError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber connectError " + e + " \n\n\n");
			e.printStackTrace();
//			do_toast("ארעה תקלה בחיבור  " );
			do_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
    	catch (ActionFailedError e) {
			Log.e("SIXTJSON", "\n\n\n carNumber actionFailed " + e + " \n\n\n");
			do_toast(e.getReasonDesc());
			boolean stay_in_screen = !e.is_recoverable_error(); // == 99
//			goto_next_activity = !stay_in_screen; 
		} 
    	catch (Exception e) {
    		// do not propagate
    	}
    	
    	return false;
	}
	

	@UiThread
	void do_toast(String msg) {
		toast(msg);
	}

	@UiThread
	void open_next_activity() { 
		FuelPtichaActivity_.intent(this).start(); 
	}
	

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}

	}
	
}

