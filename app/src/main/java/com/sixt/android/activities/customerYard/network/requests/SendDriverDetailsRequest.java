package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class SendDriverDetailsRequest extends RequestObject<BaseResponseObject> {


    private ContractDetailsResponse contractDetails;


    public SendDriverDetailsRequest(ContractDetailsResponse contractDetails){
        this.contractDetails = contractDetails;
    }

    @SerializedName("SendDriver_Req")
    private DriverDetailsRequestInner driverDetailsRequestInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<BaseResponseObject> callback) {

        initRequestObj ();
        apiInterface.sendDriverDetails(driverDetailsRequestInner, callback);

    }

    private void initRequestObj() {
        driverDetailsRequestInner = new DriverDetailsRequestInner(this.contractDetails);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }





    public class DriverDetailsRequestInner {

        public DriverDetailsRequestInner (ContractDetailsResponse contractDetails){

            DriverFName = contractDetails.getDriverFName();
            DriverLName = contractDetails.getDriverLName();
            DateOfBirth = contractDetails.getDriverBDate().replaceAll("/","");
            Tz = contractDetails.getPrivateID();
            DriverAddress = contractDetails.getDriverAddress();
            DriverCity = contractDetails.getDriverCity();
            DriverPhone = contractDetails.DriverPhone;
            LicenseNo = contractDetails.getDriverLicense();
            LicenseIssue = contractDetails.getLicenseStartDate();
            LicenseValid = contractDetails.getDriverExpire();
            LicenseType = contractDetails.getLicenseType();
            DriverMail = contractDetails.DriverMail;
            DriverAgree = contractDetails.DriverAgree;

        }

        public String DriverFName;
        public String DriverLName;
        public String DateOfBirth;
        public String Tz;
        public String DriverAddress;
        public String DriverCity;
        public String DriverPhone;
        public String LicenseNo;
        public String LicenseIssue;
        public String LicenseValid;
        public String LicenseType;
        public String DriverMail;
        public int DriverAgree;


        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "SendDriver_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
