package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import java.util.ArrayList;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class SendPhotoRequest extends RequestObject<BaseResponseObject> {


    private int photoCode;
    private String photo;


    public SendPhotoRequest(int photoCode,String photo){
        this.photoCode = photoCode;
        this.photo = photo;

    }

    @SerializedName("SendPhoto_Req")
    private PhotoReqeustInner photoRequestInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<BaseResponseObject> callback) {

        initRequestObj ();
        apiInterface.sendPhoto(photoRequestInner, callback);

    }

    private void initRequestObj() {
        photoRequestInner = new PhotoReqeustInner(this.photoCode,this.photo);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }




    public class PhotoReqeustInner {


        public PhotoReqeustInner (int photoCode,String  photo){
            this.PhotoCode = photoCode;
            this.Photo = photo;
        }



        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public int PhotoCode;
        public String Photo;
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "SendPhoto_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
