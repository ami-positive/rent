package com.sixt.android.activities.customerYard.network.requests;



import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 16/08/15.
 */
public class GetContractDataRequest extends RequestObject<ContractDetailsResponse> {


    private String carNumber;


    public GetContractDataRequest(String carNumber){
        this.carNumber = carNumber;
    }

    @SerializedName("GetContractDetails_Req")
    private ContractDetailsRequestInner contractDetailsRequestInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<ContractDetailsResponse> callback) {

        initGetCompaniesRequest();
        apiInterface.getContractDetails(contractDetailsRequestInner, callback);

    }

    private void initGetCompaniesRequest() {
        contractDetailsRequestInner = new ContractDetailsRequestInner(this.carNumber);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }



    public class ContractDetailsRequestInner {


        public ContractDetailsRequestInner (String carNo){
            CarNo = carNo;
        }

        public String Action = "O";
        public String App = "R";
        public String CarNo;
        public String MenuAction = "B";
        public String Login = LoginSixt.get_Worker_No();
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "GetContractDetails_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

}
