package com.sixt.android.activities.base;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import com.sixt.android.ui.DialogUtils;

import org.androidannotations.annotations.EActivity;

@EActivity
public class BaseListActivity extends ListActivity {

	
	@Override
	protected void onPause() {
		super.onPause();
		close_progress_dialog();
	}



	private ProgressDialog progress_dialog;

	protected void open_progress_dialog(String txt) {
		close_progress_dialog();
		progress_dialog = ProgressDialog.show(this, DialogUtils.CAPTION, txt, true, true, 
				new DialogInterface.OnCancelListener() {					
			@Override
			public void onCancel(DialogInterface dialog) {
				// no op
			}
		});
	}



	protected void close_progress_dialog() {
		if (progress_dialog != null && progress_dialog.isShowing()) {
			progress_dialog.dismiss();
			progress_dialog = null;
		}
	}


}
