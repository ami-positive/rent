package com.sixt.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.android.app.util.CarSortUtils;
import com.sixt.rent.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.car_delivery_activity)
@NoTitle
public class KavuaChalufiActivity extends Activity { //abc

	private static KavuaChalufiActivity inst;

	public static String caption_elem = "";
			
//	@ViewById 
//	Button btn_mode_kavua;
	@ViewById
	Button btn_mode_chalufi;
	
	
	@ViewById
	TextView caption;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inst = this;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		MainMenuActivity.caption = MainMenuActivity.caption_elem; 
		caption.setText(MainMenuActivity.caption);
	}
	

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;
	}

//	@Click
	void btn_mode_kavua() {
		MainMenuActivity.rechev_kavua = true;
//		MainMenuActivity.caption = "קבוע";
		caption_elem = " קבוע";
		MainMenuActivity.caption += caption_elem;
		CarSortUtils.set("C");
		LakoachSapakSelectorActivity_.intent(this).start();
	}

	@Click
	void btn_mode_chalufi() {
		MainMenuActivity.rechev_kavua = false;
//		MainMenuActivity.caption = "חלופי";
		caption_elem = " חלופי";
		MainMenuActivity.caption += caption_elem; 
		CarSortUtils.set("R"); 
		LakoachSapakSelectorActivity_.intent(this).start();
	}


	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}

	}

}
