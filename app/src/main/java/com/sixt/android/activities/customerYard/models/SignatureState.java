package com.sixt.android.activities.customerYard.models;

import android.graphics.Point;
import android.util.Log;

import com.itextpdf.text.pdf.parser.TextRenderInfo;

import java.io.Serializable;

/**
 * Created by natiapplications on 31/12/15.
 */
public class SignatureState implements Serializable {

    private final String TAG = "SignatureStateLog";

    private TextRenderInfo renderInfo;


    public SignatureState (TextRenderInfo renderInfo){
        this.renderInfo = renderInfo;
    }

    public TextRenderInfo getRenderInfo() {
        return renderInfo;
    }

    public void setRenderInfo(TextRenderInfo renderInfo) {
        this.renderInfo = renderInfo;
    }

    public double getX (){
        try{
            double x = renderInfo.getDescentLine().getBoundingRectange().getX();
            Log.e(TAG, "X :" + x);
            return  x;
        }catch (Exception e){
            Log.e(TAG,"fail to get X ! reason: " + e.getMessage());
            e.printStackTrace();
        }
        return 0;

    }

    public double getY (){
        try{
            double y = renderInfo.getDescentLine().getBoundingRectange().getY();
            Log.e(TAG,"Y :" + y);
            return y;
        }catch (Exception e){
            Log.e(TAG,"fail to get Y ! reason: " + e.getMessage());
            e.printStackTrace();
        }
        return 0;
    }


}
