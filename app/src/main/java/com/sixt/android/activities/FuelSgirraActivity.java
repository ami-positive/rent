package com.sixt.android.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.dialog.DialogOkCancel;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.BranchesRecord;
import com.sixt.android.app.json.request.GetCarDetails_Request;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.request.SendForm_Request_Inner;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response.FuelAlert;
import com.sixt.android.app.json.response.GetCarDetails_Response_Inner;
import com.sixt.android.app.json.response.SendForm_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.FuelValues;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.TEST;
import com.sixt.android.app.util.TxtUtils;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.android.ui.FuelSlide;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import static com.sixt.android.ui.FuelSlide.FUEL_LEVEL_NOTSET;

@EActivity(R.layout.fuel_sgirra_layout)
@NoTitle
public class FuelSgirraActivity extends BaseActivity { // car_isuff_mesirra_layout

	private static final boolean _DEBUG = TEST.debug_mode(false);

	private static final int DLG_CANCEL = 323;
	private static final int DLG_CLEAR = 442; 

	
//	public static String CurFuel = null;
//	public static String CarCode = "";
//	public static String Mileage = null;

	public static boolean toLicenseNumberActivity;
	public static FuelSgirraActivity inst;

	private boolean fuel_was_alerted;
	private boolean fuel_alert_stop_mode;

	
	@ViewById
	TextView caption;
//	@ViewById
//    TextView txt_caption;


	@ViewById
	Button btn_send;// next ok
	@ViewById
	Button btn_clear;
	@ViewById
	Button btn_cancel;	
		
	@ViewById
	TextView license_no;	
	@ViewById
	TextView car_model;
	@ViewById
	TextView contract_type;	
	@ViewById
	EditText mileage; // Mad Mud Utzz
	@ViewById
	FuelSlide fuel_slide;	
	@ViewById
	TextView txt_driver_name; 
	@ViewById
    TextView txt_musach_label;
	@ViewById
    TextView txt_musach;
	@ViewById
    Spinner snif_chazarra_list; // no empty line, default val = selected branch
	@ViewById
    TextView txt_snif_label;

	@ViewById
    EditText txt_comment;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inst = this;
	}

	@AfterViews
	void post_onCreate() {
//		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);
		
		FuelValues.clear();
		
		set_caption_txt();

//		int CiCoIndex = AllDataRepository.getCiCoIndex();
		final boolean habba_mode =  GetCarDetails_Response.show_next_button();
//		if (CiCoIndex==1) {
		if (habba_mode) {
			//HABBA
			btn_send.setText("הבא");
		}
		else {
			//SHLACHH
			btn_send.setText("שלח");
		}
		
//		if (CurrentAction.get().equals(CurrentAction.Values.Isuf_Rechev.value)) {
//			collected_container.setVisibility(View.GONE);
//			rechev_neesaf_metuchnan_container.setVisibility(View.GONE);
//		}
		

//		BreadCrumbs.append("פרטי חוזה");
		final String cur_bcrums = BreadCrumbs.get() + BreadCrumbs.SEP + "חוזה";
		caption.setText(cur_bcrums);		

		if (!_DEBUG) { 
			populate_screen();
			GetCarDetails_Response_Inner _cur;
			populate_snif_chazarra_list();
			if ((_cur=carDtls()) != null) {
				set_branch_default(_cur);
			} 
		}
		else {
			debug_populate_spinners();
		}

		if (DeviceAndroidId.is_lab_device(this)) {
			setText_id_empty(mileage, "20");
//			setText_id_empty(car_code,"1");
//			setText_id_empty(editText_neesaf_bafoall, "3333333"); 
//			setText_id_empty(num_of_keys, "2");
//			setText_id_empty(num_of_controllers, "3");
		}

	}



	private void set_caption_txt() {
//		CaptionSetter.set(txt_caption); 
//		caption.setText("");
//		String txt = "";
//		if (MainMenuActivity.Mischari()) {
//			if (MischarriMenuActivity.Mesirra()) {
//				txt = "מסחרי - מסירה";
//			}
//			else if (MischarriMenuActivity.Hachzarra()) {
//				txt = "מסחרי - החזרה";
//			}
//			else {
//				txt = "מסחרי  ";
//			}
//		}
//		else if (MainMenuActivity.Pnimmi()) {
//			if (PnimmiMenuActivity.pticha()) {
//				txt = "פנימי - פתיחה";				
//			}
//			else if (PnimmiMenuActivity.sgirra()) {
//				txt = "פנימי - סגירה";
//			}
//			else if (PnimmiMenuActivity.mesirra()) {
//				txt = "פנימי - מסירה";
//			}
//			else if (PnimmiMenuActivity.hachzarra()) {
//				txt = "פנימי - החזרה";
//			}
//			else {
//				txt = "פנימי ";
//			}
//		}
//		else if (MainMenuActivity.ChatzarLakoach()) {
//			txt = "חצר - ";
//		}		
//		caption.setText(txt);		
	}
	

	private void debug_populate_spinners() {
		if (_DEBUG) {
			ArrayAdapter <String> snif_adapter;
			snif_adapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item );
			snif_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			snif_adapter.add("רעננה");					 
			snif_adapter.add("עפולה");
			snif_adapter.add("תל אביב");			
			snif_chazarra_list.setAdapter(snif_adapter);
		}
	}

	
	private void populate_snif_chazarra_list() {
		ArrayAdapter<String> snif_adapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item );
		snif_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		BranchesRecord[] all = AllDataRepository.getAllBranches();
		if (all != null) {
			for (BranchesRecord r: all) {
				snif_adapter.add(r.Branch_Name);
			}
		}
		snif_chazarra_list.setAdapter(snif_adapter);			
	}
	

	private static void setText_id_empty(EditText txt, String s) {
		if (txt.getText().toString().trim().length() == 0) {
			txt.setText(s);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;
	}
	
//	private boolean musach_is_mandatory;

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
//		caption.setText(MainMenuActivity.caption);
//		driver_name_list.setF;
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
//		int garage_def = AllDataRepository.getGarageDefault();
//		musach_is_mandatory = (garage_def == 1);

//		InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//		inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}


	@Click
	void btn_clear() {
//		do_clear();
		showDialog(DLG_CLEAR); //do_clear
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == DLG_CANCEL) {
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {				
				@Override
				public void run() {
					do_cancel();
				}
			});
		}
		else if (id == DLG_CLEAR) {
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {				
				@Override
				public void run() {
					do_clear();
				} 
			});
		}
		else {
			return super.onCreateDialog(id);
		}
	}
	
	@UiThread
	void do_cancel() {
		this.finish();
		ActivityUtils.close_all();
	}


	private void do_clear() {
		mileage.setText("");
	    txt_musach.setText("");
		txt_comment.setText("");
		
//		car_code.setText("");
//		editText_neesaf_bafoall.setText("");
//		num_of_keys.setText("");
//		num_of_controllers.setText("");
		fuel_slide.clear();
	}


	private void populate_screen() {
		
		clear_textViews();
		
		license_no.setText(CarNumberValue.get());
		
//		SendForm_Request last_form = CarNumberActivity.lastForm;
//		if (last_form != null) {
//			populate_from_last_form(last_form);
//		}
//		else { 
			populate_from_last_details_response();
			
			GetCarDetails_Response_Inner _cur;
			if ((_cur=carDtls()) != null) {
				set_branch_default(_cur);
			}
//		}
	}

	
	private void clear_textViews() {
		license_no.setText("");
		car_model.setText("");		
		contract_type.setText("");	
		txt_driver_name.setText("");	
		mileage.setText("");
	    txt_musach.setText("");
	    txt_comment.setText("");
	    
	    snif_chazarra_list.setSelection(0);			
		
		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);
	}


	
	private void populate_from_last_form(SendForm_Request last_form) {
		SendForm_Request_Inner inner = last_form.SendRentForm_req;
		if (inner==null) {
			return;
		}
		car_model.setText(inner.CarModel);
//		company_name.setText(TxtUtils.limit(12, inner.CompanyName));
//		license_expiration_date.setText(inner.DateLicense);
//		insurance_expiration_date.setText(inner.DateInsurance);
	}

	private boolean can_change_branch = true;
	
	private void populate_from_last_details_response() {
		GetCarDetails_Response_Inner _cur;
		if ((_cur=carDtls()) == null) {
			return;
		}
		
		String prior_comments = _cur.ContractRemarks; 
		txt_comment.setText(prior_comments);
		
		
		car_model.setText(TxtUtils.limit(22,_cur.CarModel));		
//		license_no.setText("");
//		contract_type.setText(TxtUtils.limit(13,_cur.ContractType));	
		contract_type.setText("" + _cur.ContractType);
		txt_driver_name.setText(TxtUtils.limit(13,_cur.DriverName));	
//		mileage.setText("");
		final long GarageCode = _cur.GarageCode;
		if (!_DEBUG) {
			if (GarageCode < 1) {
//				txt_musach_label.setVisibility(View.GONE);
//				txt_musach.setVisibility(View.GONE);
				txt_musach_label.setText("   ");
				txt_musach.setText("   ");
			}
			else {
//				txt_musach_label.setVisibility(View.VISIBLE);
//				txt_musach.setVisibility(View.VISIBLE);
				String garage_name = AllDataRepository.getGarageName(GarageCode);
				if (garage_name==null) garage_name = "";
				txt_musach.setText(TxtUtils.limit(13, garage_name));
				txt_musach_label.setText("מוסך");
			}
		}
//	    txt_comment.setText("");
	    

		set_branch_default(_cur);
		
	    
//		int garage_def = AllDataRepository.getGarageDefault();
//		int show_musach = (garage_def == 1) ? View.VISIBLE : View.GONE;
//		if (!_DEBUG) {
//			if (show_musach == View.VISIBLE) {
//				txt_musach_label.setVisibility(show_musach);
//				txt_musach.setVisibility(show_musach);
//			}
//			else {
//				txt_musach_label.setText("   ");
//				txt_musach.setText("   ");
//			}
//		}		
		 
		
		can_change_branch = GetCarDetails_Response.can_change_branch();
		snif_chazarra_list.setEnabled(can_change_branch);
		
//		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);		
//		int fuel_ind = _cur.FuelIndex;
//		fuel_slide.set_fuel_level(fuel_ind);
	}
//		


	private static GetCarDetails_Response_Inner carDtls() {
		GetCarDetails_Response details = GetCarDetails_Response.carDetails_response;
		if (details==null) {
			return null;
		}
		GetCarDetails_Response_Inner _cur = details.GetRentCarDetails_res;
		if (_cur==null) {
			return null;
		}		
		return _cur;
	}

	private void set_branch_default(GetCarDetails_Response_Inner _cur) {
		long branch_no = _cur.ReturnBranchNo;
		int jj=234;
		jj++;
	    String snif_name = AllDataRepository.getBranchName(branch_no);
	    if (snif_name != null) {
	    	if (snif_chazarra_list != null && snif_chazarra_list.getAdapter() != null) {
		    	ArrayAdapter<String> adapter = (ArrayAdapter<String>) snif_chazarra_list.getAdapter();
		    	int pos = adapter.getPosition(snif_name);
		    	if (pos > -1) {
		    		snif_chazarra_list.setSelection(pos);
		    	}
	    	}
	    }
	    
	}
	
	
	ActionValue.Values orig_action;

	private String txt_mileage ;
	private int fuel_level ;

	
	@Click
	void btn_send() {		
//		CarCode = car_code.getText().toString().trim(); 
//		Keys = num_of_keys.getText().toString().trim();
//		Controls = num_of_controllers.getText().toString().trim();
		
//		CollectedCar = editText_neesaf_bafoall.getText().toString().trim();
//		String ncontrols = num_of_controllers.getText().toString().trim();
//		String nkeys = num_of_keys.getText().toString().trim();


		System.out.println("BTN HERO");

		txt_mileage = mileage.getText().toString().trim();
		fuel_level = fuel_slide.get_fuel_level();
		
		
		if (txt_mileage.isEmpty()) {
			toast("יש להזין ערך מד אוץ");
			return;				
		}
		System.out.println("BTN HERO1");
		if (txt_mileage.length() > 6) {
			toast(" קילומטראז לא חוקי");
			return;
		}
		System.out.println("BTN HERO2");
//		if (!DeviceAndroidId.is_lab_device(this)) {
//		    if (!GetCarDetails_Response.mad_utz_ok(txt_mileage)) {
//		    	toast(GetCarDetails_Response.error_msg());
//		    	return;
//		    }
//		}
		if (fuel_level < 0) {
			toast("יש לציין ערך דלק");
			return;								
		}
		System.out.println("BTN HERO3");
//		FuelAlert f_alert;
//	    if ((f_alert=GetCarDetails_Response.fuel_alert_needed(fuel_level)) != null) {
//	    	toast(f_alert.errmsg);
//	    	if (f_alert.stop) {
//	    		return;
//	    	}
//	    }
		System.out.println("BTN HERO4");
		if (!fuel_was_alerted || (fuel_was_alerted && fuel_alert_stop_mode)) {
			FuelAlert f_alert;
		    if ((f_alert=GetCarDetails_Response.fuel_alert_needed(fuel_level)) != null) {
		    	toast(f_alert.errmsg);
		    	fuel_was_alerted = true;
		    	fuel_alert_stop_mode = f_alert.stop;
	    		return;
		    }
		}

		System.out.println("BTN HERO5");
	    if (can_change_branch) {
		    if (snif_chazarra_list.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
				toast("יש לציין סניף החזרה");
				return;									    	
		    }
	    }

	    
		if (DeviceAndroidId.is_lab_device(this)) {
			System.out.println("BTN HERO6");
			perform_actual_action();
		}
		else {
			System.out.println("BTN HERO7");
			DialogOkCancel.handle_mad_utz(this, txt_mileage, mileage, new Runnable() {				
				@Override
				public void run() {
					perform_actual_action();
				}
			});
//			if (!GetCarDetails_Response.mad_utz_ok(txt_mileage)) {
//				toast(GetCarDetails_Response.error_msg());
//				return;
//			}
		}	    
	}

	
	private void perform_actual_action() {
		System.out.println("BTN HERO8");
		MyApp.appSetting.setFuel(fuel_level);
		MyApp.appSetting.setKm(txt_mileage);
		FuelValues.comments = txt_comment.getText().toString().trim(); 
		 
		String sel_branch_name = (String) snif_chazarra_list.getSelectedItem();
//		FuelValues.return_branch_ind = AllDataRepository.getBranchCode(sel_branch_name);
		MyApp.appSetting.setReturnBranch(AllDataRepository.getBranchCode(sel_branch_name));


//		int CiCoIndex = AllDataRepository.getCiCoIndex();
//		final boolean habba_mode = (CiCoIndex==1);
		final boolean habba_mode =  GetCarDetails_Response.show_next_button();
		
		
		open_progress_dialog("מתחבר לשרת");
		orig_action = ActionValue.getObj();
		bg_perform_send_commands(habba_mode);
		
	}


	@Background
	void bg_perform_send_commands(boolean habba_mode) {		
		try { 			
			if (!habba_mode) { // shlach
				System.out.println("BTN HERO9");
				ActionValue.set(ActionValue.Values.Sgirra);
				boolean success = block_sendForm();
				if (!success) {
					return;
				}

				if(toLicenseNumberActivity){
					Intent returnIntent = new Intent();
					returnIntent.putExtra("result", CarNumberValue.get());
					setResult(Activity.RESULT_OK, returnIntent);
					finish();

//					DriverDetailseActivity_.intent(this).start();
//					LicensingNumberActivity_.intent(this).start();
					System.out.println("BTN HERO10");
				}

				else{
					System.out.println("BTN HERO11");
					ActivityUtils.close_all(); // done
				}

			}
			else  { // ==habba_mode
				System.out.println("BTN HERO12");
				ActionValue.set(ActionValue.Values.Hachzarra);  
				boolean success = block_getCarDetails();
				if (!success) {
					return;
				}
				System.out.println("BTN HERO13");
				ui_open_image_activity();
			}			
		}
		finally {
			close_progress_dialog();
			ActionValue.set(orig_action);
		}		
		
	}

	@UiThread
	void ui_open_image_activity() {
		ImageCarActivity_.intent(this).start();		
	}
	
	
	private boolean block_getCarDetails() {
		try { 
			String car_no = CarNumberValue.get();
			GetCarDetails_Request req = new GetCarDetails_Request(car_no);
			String json = GGson.toJson(req);
			Base_Response_JsonMsg response;
			try { 
				response = JsonTransmitter.send_blocking(json);
			}
			catch (ActionFailedError failed_err) {
				call_toast("<" + failed_err.getReasonDesc() + ">");
				return false;
			}
			if (response==null || !(response instanceof GetCarDetails_Response)) {
				call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
				return false;
			}
//			SendForm_Response res = (SendForm_Response) response;
			//call_toast("הטופס  הועבר לשרת בהצלחה");
			return true;
		}
		catch (ConnectionError e) {
			e.printStackTrace();
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		finally {
			//?
		}
		return false;
	}

	private boolean block_sendForm() {
//		SendForm_Request_Inner.Image_remote_filename = null;
		SendForm_Request_Inner.reset_photo_arr();
		try { 
			SendForm_Request req = new SendForm_Request(this, true, null, null);
			if (!req.is_valid_form()) {
				call_toast("חסרים נתונים לבקשה!");
				return false;
			}
			String json = GGson.toJson(req);
			Base_Response_JsonMsg response;
			try { 
				response = JsonTransmitter.send_blocking(json);
			}
			catch (ActionFailedError failed_err) {
				call_toast("<" + failed_err.getReasonDesc() + ">");
				return false;
			}
			if (response==null || !(response instanceof SendForm_Response)) {
				call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
				return false;
			}
			SendForm_Response res = (SendForm_Response) response;
			//call_toast("הטופס  הועבר לשרת בהצלחה" , true);
//			call_toast(SignatureActivity.getSuccessMsg(res), true);//
			MainMenuActivity.success_mst_to_show = SignatureActivity.getSuccessMsg(res);
			return true;
		}
		catch (ConnectionError e) {
			e.printStackTrace();
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		finally {
			//?
		}
		return false;
	}
	

	@UiThread
	void call_toast(String string) {
		toast(string);
	}

	@UiThread
	void call_toast(String string, boolean success) {
		toast(string, success);
	}
	
	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}
	}


	@Click 
	void btn_cancel() {
		showDialog(DLG_CANCEL); //do_clear
//		this.finish();
	}

}
