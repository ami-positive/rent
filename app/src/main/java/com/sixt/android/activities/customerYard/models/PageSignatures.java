package com.sixt.android.activities.customerYard.models;

import java.util.ArrayList;

/**
 * Created by natiapplications on 03/01/16.
 */
public class PageSignatures {


    private int pageNumber;
    private int signaturesCount;
    private ArrayList<SignatureState> signatures = new ArrayList<SignatureState>();

    public PageSignatures (int pageNumber){
        this.pageNumber = pageNumber;
    }

    public void addSignature (SignatureState signatureState){
        signatures.add(signatureState);
    }

    public void increment () {
        if (signaturesCount < signatures.size()){
            signaturesCount ++ ;
        }
    }

    public boolean isSign () {
        return signaturesCount == signatures.size();
    }

    public boolean isNeedSignatures () {
        return signatures.size() > 0;
    }

    public int getSignaturesCount(){
        return signatures.size();
    }

    public int getLeftSignaturesCount () {
        return signatures.size() - signaturesCount;
    }

    public int getPageNumber () {
        return this.pageNumber;
    }

    public ArrayList<SignatureState> getSignatures () {
        return this.signatures;
    }


    public double getPageSignaturePositionX (){


        if (this.signaturesCount < signatures.size()){
            try{
                return this.signatures.get(signaturesCount).getX();
            }catch (Exception e){}
        }

        return 0;
    }


    public double getPageSignaturePositionY (){

        if (this.signaturesCount < signatures.size()){
            try{
                return this.signatures.get(signaturesCount).getY();
            }catch (Exception e){}
        }

        return 0;
    }


}
