/**
 * 
 */
package com.sixt.android.activities.customerYard.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class BaseResponseObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@SerializedName("msg_type")
	private String messageType;

	@SerializedName("Success")
	private int success;

	@SerializedName("ReasonCode")
	private String errorCode;

	@SerializedName("ReasonDesc")
	private String errorDesc;

	private String responseBody;



	public BaseResponseObject() {
		super();

	}


	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public boolean isSuccess (){
		if (success > 0){
			return true;
		}
		return false;
	}

	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}


	public void setResponseBody (String body){
		this.responseBody = body;
	}

	public String getResponseBody() {
		return responseBody;
	}


	@Override
	public String toString() {
		return "BaseResponseObject{" +
				"messageType='" + messageType + '\'' +
				", success=" + success +
				", errorCode='" + errorCode + '\'' +
				", errorDesc='" + errorDesc + '\'' +
				", responseBody='" + responseBody + '\'' +
				'}';
	}
}
