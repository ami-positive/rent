package com.sixt.android.activities.customerYard.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.activities.ContractDocActivity_;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.dialog.DialogCallback;
import com.sixt.android.activities.customerYard.dialog.OCRResultDialog;
import com.sixt.android.activities.customerYard.models.LicenseDrive;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.SendPhotoRequest;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.ocr.OCRUtils;
import com.sixt.android.activities.customerYard.ocr.SaveTrainedDataTask;
import com.sixt.android.activities.customerYard.ui.PhotosView;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.activities.customerYard.utils.FragmentsUtil;
import com.sixt.android.activities.customerYard.utils.ProgressDialogUtil;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.scanovate.LicenceSavedData;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by natiapplications on 18/12/15.
 */
@EFragment(R.layout.fragment_photos)
public class PhotosFragment extends BaseFragment implements DriverDetailseActivity.ActionButtonsListener
        , PhotosView.PhotoChangeListener {


    private final String TAG = "OCR_TEST_LOG";


    @ViewById
    TextView txtFragTitle;

    @ViewById
    PhotosView photosView;


    private ProgressDialog dialog;
    private ArrayList<Integer> photosSent;
    private PhotosView.Photo mTempPhoto; // for deleting photo

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new SaveTrainedDataTask(getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            new SaveTrainedDataTask(getActivity()).execute();

    }


    private boolean checkIfAllMandatoryImagesUploaded(ArrayList<ContractDetailsResponse.Photo> photos) {
        for (int i = 0; i < photos.size(); i++) {
            if (photosSent == null) {
                photosSent = new ArrayList<Integer>();
            }
            if (photos.get(i).getMandatory() == 1) {
                if (!photosSent.contains(photos.get(i).getPhotoCode())) {
                    String name = AllDataRepository.getPhotoByCode(String.valueOf(photos.get(i).getPhotoCode())).PhotoDesc;
                    ToastUtil.toaster("חסרה תמונה: " + name, true);
                    return false;
                }
            }
        }
        return true;
    }

    @AfterViews
    public void onPostCreate() {
        ((DriverDetailseActivity) getActivity()).setActionButtonsListener(this);
        ((DriverDetailseActivity) getActivity()).showBaseDetails();
        txtFragTitle.setText(getFragmentName());

        AppUtil.hideKeyBoard(this);

        doClear();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        photosView.onActivityResult(requestCode, resultCode, data);
        ContractChetchData.photos = photosView.getPhotos();

    }

    private void sendPhotoToServer(final int photoCode, String photoAsBase64) {
        MyApp.networkManager.makeRequest(new SendPhotoRequest(photoCode, photoAsBase64), new NetworkCallback<BaseResponseObject>() {
            @Override
            public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
                super.onResponse(success, errorDesc, response);
                if (success) {
                    ToastUtil.toaster("תמונה נשלחה בהצלחה", false);
                    if (photosSent == null) {
                        photosSent = new ArrayList<Integer>();
                    }
                    photosSent.add(photoCode);
                } else {
                    if (mTempPhoto != null) {
                        photoLog("delete photo process...");
                        doClearPhoto(mTempPhoto);
                    }
                    toast(errorDesc);
                }
            }
        });
    }

    public void photoLog(String log) {
        Log.e("PhotoLog", "" + log);
    }

    @Override
    public int getFragmentIndex() {
        return 2;
    }

    @Override
    public String getFragmentName() {
        return "צילומים";
    }

    @Override
    public boolean onNext() {

        if (validation()) {
            Fragment nextFragment = DriverDetailseActivity.getNextFragment(getClass().getSimpleName(), getContractDetails());
            if (nextFragment != null) {
                FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                        nextFragment, R.id.driverDetailsContainer);
            } else {
                ContractDocActivity_.intent(getActivity())
                        .extra("km", getContractDetails().Km)
                        .extra("fuel", getContractDetails().Fuel)
                        .start();
            }
            return true;
        }

        return false;

    }

    private boolean validation() {
        StringBuilder errorMessage = new StringBuilder();
        boolean hasError = false;

        if (!checkIfAllMandatoryImagesUploaded(getContractDetails().getPhotosArray())) {
            hasError = true;
        }

//        errorMessage.append("חסרות התמונות הבאות: " + "\n\n");
//        if (! photosView.isHasAllPhotos()){
//            hasError = true;
//            errorMessage.append(photosView.getMissingPhotosNames());
//        }
//
//        if (hasError){
//            //toast(errorMessage.toString());
//        }

        return !hasError;
    }

    @Override
    public void onPrevious() {

    }

    @Override
    public void onClear() {
        Dialog2Btns.create(getActivity(), "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
            @Override
            public void run() {
                doClear();
            }
        }).show();
    }


    private void doClear() {

        if (photosView != null) {
            ContractChetchData.photos = null;
            photosView.config(this, this, ContractChetchData.photos, getContractDetails().getPhotos());
        }

        photosView.clear();
        ContractChetchData.photos = photosView.getPhotos();
    }

    private void doClearPhoto(PhotosView.Photo mTempPhoto) {
        photosView.clearPhoto(mTempPhoto);
        ContractChetchData.photos = photosView.getPhotos();
    }

    @Override
    public void onPhotoChanged(PhotosView.Photo photo, Bitmap bitmap) {
        mTempPhoto = photo;
        boolean sendToServer = false;
        int photoCode = 0;
        String photoAsBase64 = "";

        if (photo.name.equals("רשיון נהיגה")) {

//            if (getContractDetails().isNeedTakeDriverDetails()){
            try {
                dialog = ProgressDialogUtil.showProgressDialog(getActivity(), "מנתח תמונה...");
                dialog.setCancelable(true);
                byte[] imageData = handleRequestCameraResult(bitmap);
                handleReturnedImage(imageData);
            } catch (Exception e) {
                dismisProgressDialog();
            } catch (Error e) {
                dismisProgressDialog();
            }

//            }

            if (getContractDetails().isNeedToSendLicenseDrivePhotoToServer()) {
                sendToServer = true;
            }
        } else {
            sendToServer = true;
        }

        if (sendToServer) {
            try {
                photoCode = Integer.parseInt(AllDataRepository.getPhotoByDesc(photo.name).PhotoCode);
                photoAsBase64 = photosView.getPhotoHasBase64ByName(photo.name);
                sendPhotoToServer(photoCode, photoAsBase64);
            } catch (Exception e) {
            }
        }


    }


    private byte[] handleRequestCameraResult(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    @Background
    public void handleReturnedImage(final byte[] imageData) {
        Log.e(TAG, "licenseType1");
        byte[] tempBytesData = imageData;
        Log.e(TAG, "licenseType2");
        if (tempBytesData != null) {
            Log.e(TAG, "licenseType3");
            final Bitmap bitmap = BitmapFactory.decodeByteArray(tempBytesData, 0, tempBytesData.length);
            Log.e(TAG, "licenseType4");
            if (bitmap != null) {
                Log.e(TAG, "licenseType5");
                final Bitmap copy = OCRUtils.drawRectangleOnBitmap(bitmap);
                Log.e(TAG, "licenseType6");
                final Bitmap toScan = copy.copy(Bitmap.Config.ARGB_8888, true);
                Log.e(TAG, "licenseType7");
                final String ocrTestEng = getResultTextEng(toScan);
                Log.e(TAG, "licenseType8");
                final String ocrText = getResultText(toScan);
                Log.e(TAG, "licenseType9");
                final String licenseType = getResultTextLicenseType(bitmap);
                Log.e(TAG, "licenseType: " + licenseType);

                onOCRScanningDone(ocrText, ocrTestEng, licenseType);
                Log.e(TAG, "licenseType10");
            } else {
                Log.e(TAG, "bitmap is null ! + " + tempBytesData.length);
            }
        } else {
            Log.e(TAG, "bytes is null !");
        }

    }

    @UiThread
    public void onOCRScanningDone(String ocrText, String ocrTextEng, String licenseType) {
        if (ocrText != null && ocrTextEng != null) {
            Log.e(TAG, "*******************************************************************");
            Log.d(TAG, "ocr text: " + ocrText + "\n\n" + "ocr text eng: " + ocrTextEng);
            Log.e(TAG, "*******************************************************************");
            ProgressDialogUtil.dismisDialog(dialog);
            showOcrResultDialog(ocrText, ocrTextEng, licenseType);
        }
    }


    private String getResultText(Bitmap bitmap) {
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.init(getActivity().getFilesDir().getPath(), "heb");
        tessBaseAPI.setImage(bitmap);
        setRect(tessBaseAPI, bitmap);
        String text = tessBaseAPI.getUTF8Text();
        return text;
    }


    private String getResultTextEng(Bitmap bitmap) {
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.init(getActivity().getFilesDir().getPath(), "eng");
        tessBaseAPI.setImage(bitmap);
        setRect(tessBaseAPI, bitmap);
        String text = tessBaseAPI.getUTF8Text();
        return text;
    }


    private String getResultTextLicenseType(Bitmap bitmap) {
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.init(getActivity().getFilesDir().getPath(), "eng");
        tessBaseAPI.setImage(bitmap);
        setRectForLicenseType(tessBaseAPI, bitmap);
        String text = tessBaseAPI.getUTF8Text();
        return text;
    }

    private void setRect(TessBaseAPI tessBaseAPI, Bitmap original) {
        Rect rect = new Rect();
        rect.set((original.getWidth() / 3) + (original.getWidth() / 20), original.getHeight() / 5,
                original.getWidth(), original.getHeight()/* - (original.getHeight()/5)*/);
        tessBaseAPI.setRectangle(rect);

    }


    private void setRectForLicenseType(TessBaseAPI tessBaseAPI, Bitmap original) {
        Rect rect = new Rect();
        rect.set(0, original.getHeight() - (original.getHeight() / 5),
                original.getWidth() / 10, original.getHeight()/* - (original.getHeight()/5)*/);
        tessBaseAPI.setRectangle(rect);

    }


    private void showOcrResultDialog(String ocrString, String ocrStringEng, String licenseType) {

        LicenseDrive licenseDrive = new LicenseDrive(ocrString);
        Log.d("OCRR", "step1");
        licenseDrive.buildByOcrStringEng(ocrStringEng);
        Log.d("OCRR", "step2");
        licenseDrive.setLicenseType(licenseType);
        Log.d("OCRR", "step3");
//        if (licenseDrive.isValidResult()){
        Log.d("OCRR", "step4");
        OCRResultDialog ocrResultDialog = new OCRResultDialog();
        ocrResultDialog.setLicenseDriveToShow(licenseDrive);
        ocrResultDialog.setCallback(new DialogCallback() {
            @Override
            protected void onDialogButtonPressed(int buttonID, DialogFragment dialog, Object Extra) {
                super.onDialogButtonPressed(buttonID, dialog, Extra);
                LicenseDrive result = (LicenseDrive) Extra;
                result.addWordReport();
                getContractDetails().setLicenseDrive(result);
                getContractDetails().saveToFile();
            }
        });

        ocrResultDialog.show(getFragmentManager(), OCRResultDialog.DIALOG_NAME);
//        }

        /*else {
            toast("סריקת נתונים נכשלה." + "\n" + "יתכן והרשיון שצולם אינו רישיון ישראלי");
        }*/


    }

    public void initLicenceFields(){
        LicenceSavedData lsd = MyApp.lsdManager;
        lsd.setStrFirstNameHeb("");
        lsd.setStrFirstNameEng("");
        lsd.setStrLastNameHeb("");
        lsd.setStrLastNameEng("");
        lsd.setStrDateOfBirth("");
        lsd.setStrDateOfExpiry("");
        lsd.setStrIdNumber("");
        lsd.setStrAddress("");
        lsd.setStrDateOfIssue("");
        lsd.setStrDrivinLicenseNumber("");
        lsd.setFaceImageBase64("");
        lsd.save();
    }

}
