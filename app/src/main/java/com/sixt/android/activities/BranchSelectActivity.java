package com.sixt.android.activities;

import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.db.JsonsTable;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.AvailableBranchesRecord;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.RejectCodeDesc;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.objects.SubRejectValue;
import com.sixt.android.app.json.request.Authentication_Request;
import com.sixt.android.app.json.request.GetRejects_Request;
import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.json.response.GetAllData_Response_Inner;
import com.sixt.android.app.json.response.GetRejects_Response;
import com.sixt.android.app.json.response.GetRejects_Response_Inner;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.BranchNoValue;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.GGson;
import com.sixt.android.app.util.TEST;
import com.sixt.android.httpClient.ConnectionError;
import com.sixt.android.httpClient.JsonTransmitter;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.branch_select)
@NoTitle
public class BranchSelectActivity extends BaseActivity {
 
	private static final boolean _DEBUG = TEST.debug_mode(false); 

	protected static final int _MINUTE = 1000*60;

//	public static String the_VersionName = null;

	public static BranchSelectActivity inst; 

	private final String TRANSPORT_USER = "112233"; 	 

	

	@ViewById
    TextView txt_hello_username; 

	@ViewById
	Spinner branch_list; //NO first empty line; use default == branch returning from Auth


	@ViewById
	Button okButton; //send
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		inst = this;		
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (DeviceAndroidId.is_sharon_device(this) || DeviceAndroidId.is_lab_device(this)) {
			getMenuInflater().inflate(R.menu.select_server_mode, menu);
			return true;
		}
		else {
			return super.onCreateOptionsMenu(menu);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		int i = item.getItemId();
		switch (i)
		{
		//		case R.id.menu_server_prod_csp:
		//			JsonTransmitter.setUrl(1);
		//			return true;

		case R.id.menu_server_prod_http:
			JsonTransmitter.setUrl(2);
			return true;

		case R.id.menu_server_dev:
			JsonTransmitter.setUrl(0);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}



//	private void set_ver_code() {
//		if (the_VersionName != null) {
//			return;
//		}
//		try { 
//			String name = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//			the_VersionName = name; 
//		} catch (NameNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			the_VersionName = "unknown";
//		}	
//	}

	@AfterViews
	void post_onCreate() {

		//gggggggggggg autoComplete Spinner
//		final AutoCompleteTextView textView;
//	    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
//	            getActivity(), android.R.layout.simple_dropdown_item_1line,
//	            getResources().getStringArray(R.array.names));
//
//	    textView = (AutoCompleteTextView) v.findViewById(R.id.txtViewNames);
//	    textView.setAdapter(arrayAdapter);
//	    textView.setOnClickListener(new View.OnClickListener() {
//	        @Override
//	        public void onClick(final View arg0) {
//	            textView.showDropDown();
//	        }
//	    });
	    
		ArrayAdapter<String> adapter =
				new ArrayAdapter <String> (this, android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		if (_DEBUG) {
			adapter.add("נתבג");					 
			adapter.add("עפולה");
			adapter.add("נתניה");
			adapter.add("רחובות");
			adapter.add("בת-ים");
			adapter.add("מעלות");
			adapter.add("חולון");
			adapter.add("חולון 1");
			adapter.add("חולון 2");
			adapter.add("חולון 3");
			adapter.add("חולון 4");
			adapter.add("חולון 5");
			adapter.add("חולון 6");
			adapter.add("חולון 7");
			adapter.add("אילת");
		}
		else {
			GetAllData_Response_Inner allData = AllDataRepository.get();
			if (allData==null) {
				//cannot bla bla msg to user
				return;
			}			 
			String username = get_username();
			txt_hello_username.setText("שלום " + username); 
			
			AvailableBranchesRecord[] branches = Authentication_Response.getAvailableBranches();
	
			if (branches==null || branches.length==0) {
				// no branches to show..
			}
			else {
				// populate adapter  
				for (AvailableBranchesRecord r: branches) {
					int b_num = r.BranchNo;
					String b_name = allData.getBranchName(b_num);
					if (b_name != null && b_name.length() > 0) {
						adapter.add(b_name);
					}
				}
			}
		}
		branch_list.setAdapter(adapter);
		
		branch_list.setSelection(0);// first is always default here!
		
//		int selpos = Authentication_Response.get_current_branch_position(adapter); 
//		if (selpos > -1) {
//			branch_list.setSelection(selpos);
//		}		
	}

	
	private String get_username() {
		String n = Authentication_Response.get_WorkerName();
		if (n==null) {
			return "";
		}
		return n;
	}


	@Override
	protected void onResume() { 
		super.onResume();
		//		debug_show_device_id();
	}


	//	private void debug_show_device_id() {
	//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//		builder.setMessage(DeviceAndroidId.getId(this))
	//		       .setCancelable(false)
	//		       .setPositiveButton("", new DialogInterface.OnClickListener() {
	//		           public void onClick(DialogInterface dialog, int id) {
	//		                //do things
	//		           }
	//		       });
	//		AlertDialog alert = builder.create();
	//		alert.show();				
	//	}



	@Override 
	protected void onDestroy() {
		super.onDestroy();
		inst = null;
	}
	

	@Click(R.id.okButton)
	public void startMainMenu(){
		//		login = null;
		final String selected_name = (String) branch_list.getSelectedItem();
		if (selected_name==null || selected_name.length()==0) {
			toast("יש לבחור סניף");
			return;			
		}
		int selected_BranchNo =  AllDataRepository.getBranchCode(selected_name);
		BranchNoValue.set(selected_BranchNo, selected_name);  
		if (_DEBUG) {
			open_progress_dialog("מתחבר לשרת. אנא המתן...");
			new Timer().schedule(new TimerTask() {				
				@Override
				public void run() {
					open_menu_activity();
				}
			}, 600);
		}
		else {
//			do_connect(); 
			BranchNoValue.set(selected_BranchNo, selected_name);
			open_menu_activity();
		}
	}



//	private void do_connect() {    	
//		if (!NetworkState.is_connected(this)) {
//			toast("אין חיבור רשת!");
//			clear_password();
//			return;
//		}
//		String username_txt = username.getText().toString(); 
//		String password_txt = password.getText().toString(); 
//		if (username_txt==null || username_txt.length()==0) {
//			toast("יש להזין שם משתמש");
//			clear_password();
//			return;    			
//		}
//		if (!username_txt.equals(password_txt)) { // must be identical
//			toast("פרטי המשתמש לא חוקיים!");
//			clear_password();
//			return;    			
//		}
//		login = username_txt; 
//
//		open_progress_dialog("מתחבר לשרת. אנא המתן...");
//		bg_attempt_connection(login);		    		
//	}


	@Background
	void bg_attempt_connection(final String _login) {		
		Authentication_Request req = new Authentication_Request(_login, LoginActivity.the_pwd); 
		String req_str = GGson.toJson(req);
		try {
			Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
			if (response==null || !(response instanceof Authentication_Response)) {
				throw new ConnectionError("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
			}
			Authentication_Response res = (Authentication_Response) response;

			start_loadRejects_thread();

			start_json_sender_thread();


			open_menu_activity();
		}
		catch (ActionFailedError err) {
			call_toast(err.getReasonDesc());
		} 
		catch (ConnectionError e) {
			e.printStackTrace();
			call_toast("לא ניתן להתחבר לשרת כעת. אנא נסה שנית מאוחר יותר");
		} 
		finally {
			clear_password();
			close_progress_dialog();
		}
	}

	@UiThread
	void clear_password() {
		if (!DeviceAndroidId.is_lab_device(this)) {
//			password.setText("");
		}
	}


	private static boolean start_json_sender_thread_started = false;


	private void start_json_sender_thread() {
		if (start_json_sender_thread_started) {
			return; 
		}
		new Thread() {
			public void run() {
				start_json_sender_thread_started = true;
				setPriority(Thread.NORM_PRIORITY-2); 
								
				for (;;) {      
					try { Thread.sleep(3*_MINUTE); } catch(Exception e) {} //return 
					//					try { Thread.sleep(4*1000); } catch(Exception e) {} //

					try { JsonsTable.get().send_all_pending_jsons(); } catch(Exception e) {}
				}
			}
		}.start();

	}

	@UiThread
	void call_toast(String msg) {
		toast(msg);		
	}


	private void start_loadRejects_thread() {
		new Thread() {
			public void run() {
				final int MAX_ATTEMPTS = 5;
				for (int ii = 0; ii < MAX_ATTEMPTS; ii++) {
					if (ii > 0 && has_stored_rejects()) {
						break;
					}
					GetRejects_Request req = new GetRejects_Request(); 
					String req_str = GGson.toJson(req);
					try {
						Base_Response_JsonMsg response = JsonTransmitter.send_blocking(req_str);
						if (response != null && response instanceof GetRejects_Response) {
							//							correct_rejects_strings((GetRejects_Response)response); //
						}
						return; //done 
					}  
					catch (Exception e) {
						int jj=435; 
						jj++;
						try { Thread.sleep(5000);} catch (InterruptedException e1) {}
						// and go on
					} 
				}				
			}
		}.start();
	}  



	protected static boolean has_stored_rejects() {
		GetRejects_Reject[] rej_arr = GetRejects_Response.get_all_available_rejects(true);
		if (rej_arr != null && rej_arr.length > 0) {
			return true;
		}
		rej_arr = GetRejects_Response.get_all_available_rejects(false);
		if (rej_arr != null && rej_arr.length > 0) {
			return true;
		}
		return false;
	}

	protected void correct_rejects_strings(GetRejects_Response response) {
		GetRejects_Response_Inner inner = response.GetRentRejects_res;
		GetRejects_Reject[] reject_arr = inner.Rejects;
		if (reject_arr==null) { 
			int jj=243;
			jj++;
			return;
		}
		for (GetRejects_Reject r: reject_arr) {
			String orig = r.RejectDesc;
			if (!is_legal_hebrew(orig)) {
				int jj=234;
				jj++;
			}
			RejectCodeDesc[] vals = r.RejectValues;
			if (vals != null) {
				for (RejectCodeDesc v: vals) {
					String desc = v.RejectValueDesc;
					if (!is_legal_hebrew(desc)) {
						int jj=234;
						jj++;
					}
				}

				SubReject[] sr_arr = r.SubReject;
				if (sr_arr != null) {
					for (SubReject sr: sr_arr) {
						String desc = sr.SubRejectDesc;
						if (!is_legal_hebrew(desc)) {
							int jj=234;
							jj++;
						}
						SubRejectValue[] val_arr = sr.SubRejectValues;
						if (val_arr != null) {
							for (SubRejectValue srv: val_arr) {
								String d = srv.SubRejectValueDesc;						
								if (!is_legal_hebrew(d)) {
									int jj=234;
									jj++;
								}
							}
						}
					}
				}
			}
		}  
		int jjj=243; 
		jjj++;

	}


	public static String fixEncoding(String Hebrew1) {
		try {
			byte[] bytes = Hebrew1.getBytes("ISO-8859-1");
			if (!validUTF8(bytes)) {
				return Hebrew1;  
			}
			return new String(bytes, "UTF-8"); 
		} catch (UnsupportedEncodingException e) {
			// Impossible, throw unchecked
			throw new IllegalStateException("No Hebrew1 or UTF-8: " + e.getMessage());
		}

	}

	public static boolean validUTF8(byte[] input) {
		int i = 0;
		// Check for BOM
		if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
				&& (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
			i = 3;
		}

		int end;
		for (int j = input.length; i < j; ++i) {
			int octet = input[i];
			if ((octet & 0x80) == 0) {
				continue; // ASCII
			}

			// Check for UTF-8 leading byte
			if ((octet & 0xE0) == 0xC0) {
				end = i + 1;
			} else if ((octet & 0xF0) == 0xE0) {
				end = i + 2;
			} else if ((octet & 0xF8) == 0xF0) {
				end = i + 3;
			} else {
				// Java only supports BMP so 3 is max
				return false;
			}

			while (i < end) {
				i++;
				octet = input[i];
				if ((octet & 0xC0) != 0x80) {
					// Not a valid trailing byte
					return false;
				}
			}
		}
		return true;
	}





	//		String all = "";
	//		for (GetRejects_Reject r: reject_arr) {
	//			String desc = r.RejectDesc;
	//			all += " "  + desc;
	////			if (!legal_hebrew(desc)) {
	////				int place_breakpoint_here = 2;
	////				place_breakpoint_here++;
	////			}
	//		} 
	//		all = all;
	//		int place_breakpoint_here=0;
	//		place_breakpoint_here++;		
	//	}

	private static boolean is_legal_hebrew(String desc) {
		if (desc==null) {
			return true;
		}
		for (int i = 0; i < desc.length(); i++) {
			char c = desc.charAt(i);
			if (c != ' ' && c != '\'' && (c > 'ת' || c < 'א')) {

				//U+FFFD � replacement character used to replace an unknown or unrepresentable character
				if (c == '\uFFFD') {
					int jjj=2342;
					jjj++;
				}

				//				if (desc.contains("\uFFFD")) {
				//					int jj = 423;
				//					jj++;
				//				}

				return false;
			}
		}
		return true;
	}


	@UiThread
	void open_menu_activity() {
		MainMenuActivity_.intent(this).start();
	}


	public static void call_finish() {
		if (inst != null) {
			inst.finish(); 
		}		
	}

}
