package com.sixt.android.activities.customerYard.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.activities.CardReaderActivity;
import com.sixt.android.activities.customerYard.activities.ContractDocActivity_;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.activities.LicensingNumberActivity;
import com.sixt.android.activities.customerYard.dialog.CreditGuardDialog;
import com.sixt.android.activities.customerYard.dialog.DialogManager;
import com.sixt.android.activities.customerYard.models.CreditCard;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.CreditGuardQueryRequest;
import com.sixt.android.activities.customerYard.network.requests.GetCreditGuardLink;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.GetCreditGuardLinkResponse;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.activities.customerYard.utils.FragmentsUtil;
import com.sixt.android.activities.customerYard.utils.ToastUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.dialog.DialogError;
import com.sixt.android.app.json.objects.CurrencyRecord;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by natiapplications on 18/12/15.
 */
@EFragment(R.layout.fragment_credit_card)
public class CreditFragment extends BaseFragment implements DriverDetailseActivity.ActionButtonsListener{

    private final int MODE_EDIT = 0;
    private final int MODE_ONLY_DISPLAY = 1;

    @ViewById
    TextView txtFragTitle;
    @ViewById
    TextView skip;
    @ViewById
    EditText etAmount;
    @ViewById
    EditText etID;
    @ViewById
    EditText etUserName;
    @ViewById
    EditText etFirstName;
    @ViewById
    EditText etLastName;
    @ViewById
    Spinner spinnerCurrency;
    @ViewById
    EditText etCVV;
    //@ViewById
    //EditText etCardNumber;
    //@ViewById
    //TextView etValidityMonth;
    //@ViewById
    //TextView etValidityYear;


    private int currentMode;

    private CreditGuardDialog.onLinkChanges onLinkChanges;
    private CurrencyRecord selectedCurrency;

    @Override
    public int getFragmentIndex() {
        return 1;
    }

    @Override
    public String getFragmentName() {
        return "שיריון אשראי";
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @AfterViews
    public void onPostCreate () {

        AppUtil.hideKeyBoard(this);

        ((DriverDetailseActivity)getActivity()).setActionButtonsListener(this);
        ((DriverDetailseActivity)getActivity()).showBaseDetails();
        txtFragTitle.setText(getFragmentName());
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNextFragment();
            }
        });
        populateCurrencySpinner();
        fillScreenData();
    }



    private void populateCurrencySpinner() {


        final ArrayAdapter<String> currenciesAdapter = new ArrayAdapter<String> (getActivity(), android.R.layout.simple_spinner_item );
        currenciesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        String[] currencies = {"No currencies"};
        if(getContractDetails() != null && getContractDetails().getCurrencies() != null){
            currencies = getContractDetails().getCurrencies();
        }
        currenciesAdapter.clear();
        for (String r: currencies) {
            currenciesAdapter.add(r);
        }

        spinnerCurrency.setAdapter(currenciesAdapter);

        spinnerCurrency.setSelection(0);

        spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    selectedCurrency = AllDataRepository.getCurrencyByDesc(currenciesAdapter.getItem(position));
                    if (selectedCurrency != null) {
                        selectedCurrency.saveToStorage();

                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }




    private void fillScreenData() {

        // if credit guard process is over change mode
        if (LicensingNumberActivity.isCreditProccessOver){
            currentMode = MODE_ONLY_DISPLAY;
            applyOnlyDisplayMode();
            try{
                int currencyPosition = AllDataRepository.getCurrencyPositionByCode(getContractDetails().getCurrencyCode());
                if (currencyPosition >=0 && currencyPosition < spinnerCurrency.getAdapter().getCount()){
                    spinnerCurrency.setSelection(currencyPosition);
                }
            }catch (Exception e){}

        }else {
            currentMode = MODE_EDIT;
            etAmount.setText(String.valueOf(getContractDetails().getAmount()));
            etID.setText(getContractDetails().getPrivateID());
            etFirstName.setText(getContractDetails().getDriverFName());
            etLastName.setText(getContractDetails().getDriverLName());
            etCVV.setText(getContractDetails().getCVV());
//            fillTest();
        }



    }

    private void fillTest() {
        etAmount.setText("1");
        etCVV.setText("123");
        etID.setText("035898733");
    }


    private void applyOnlyDisplayMode() {

        etAmount.setKeyListener(null);
        etID.setKeyListener(null);
        etUserName.setOnClickListener(null);
        etCVV.setKeyListener(null);

        //etCardNumber.setKeyListener(null);
        //etValidityYear.setOnClickListener(null);
        //etValidityMonth.setOnClickListener(null);

    }





    @Override
    public boolean onNext() {
        System.out.println("PRE-PASS " +currentMode );
        if (currentMode == MODE_EDIT){
            if (validation()){
                System.out.println("PASS");
                updateContractDetails();

                //sendCreditCardToServer();
                startActivityForResult(new Intent(getActivity(), CardReaderActivity.class), 1000);
                return true;
            }
        }else {
            openNextFragment();
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode !=  Activity.RESULT_OK){return;}
        if(requestCode == 1000){
            String result = data.getStringExtra("result");
            if(result.matches("close")){
                openNextFragment();
            }else {
                result = result.replace(";", "");
                result = result.replace("?", "");
                loadCreditGuardLink(result);
            }
        }
    }

    @Override
    public void onPrevious() {

    }

    @Override
    public void onClear() {
        Dialog2Btns.create(getActivity(), MyApp.appContext.getResources().getString(R.string.are_you_sure_you_want_to_clean_out_the_form), new Runnable() {
            @Override
            public void run() {
                doClear();
            }
        }).show();
    }





    String token = "";
    private void loadCreditGuardLink (final String cardNum){


        if(etAmount.getText().toString().isEmpty()){
            openNextFragment();
            return;
        }
        showProgressDialog();
        int sum = Integer.parseInt(etAmount.getText().toString())*100;

        MyApp.networkManager.makeRequest(new GetCreditGuardLink(sum, getContractDetails().getCurrencyCode(),
                getContractDetails().Usercg, getContractDetails().requestId), new NetworkCallback<GetCreditGuardLinkResponse>() {

            @Override
            public void onResponse(boolean success, String errorDesc, GetCreditGuardLinkResponse response) {
                super.onResponse(success, errorDesc, response);
                dismisProgressDialog();
                if (success) {
                    token = response.getToken();
                    DialogManager.showCreditGuardDialog(cardNum, getFragmentManager(),
                            response.getUrl(), new CreditGuardDialog.onLinkChanges(){
                                @Override
                                public void linkChanges(CreditGuardDialog dialog, String url) {
                                    System.out.println("url - > " + url);
                                    ToastUtil.toaster(url, false);
                                    boolean sent = false;
                                    if(url.contains("merchantPages")){
                                        if(!sent) {
                                            sendCreditGuardQuery(dialog);
                                            sent = true;
                                        }
                                    }
                                }
                            });
                } else {
                    toast(errorDesc);
                }

            }
        });

    }


//    private void sendCreditCardToServer() {
//        showProgressDialog();
//
//        MyApp.networkManager.makeRequest(new SendCreditCardRequest(getContractDetails().getCreditCard()),
//                new NetworkCallback<BaseResponseObject>() {
//                    @Override
//                    public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
//                        super.onResponse(success, errorDesc, response);
//                        dismisProgressDialog();
//                        if (success) {
//                            openNextFragment();
//                }else {
//                    toast(errorDesc);
//                    openNextFragment();
//                }
//            }
//        });
//    }

    final String URL_TAG = "URL_TAG";
    private void sendCreditGuardQuery(final CreditGuardDialog dialog) {
        showProgressDialog();

        MyApp.networkManager.makeRequest(new CreditGuardQueryRequest(getContractDetails().getCreditCard(), getContractDetails().requestId, token),
                new NetworkCallback<BaseResponseObject>() {
                    @Override
                    public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
                        super.onResponse(success, errorDesc, response);

                        if(response != null) {
                            Log.i(URL_TAG, "onResponse: obj " + response.toString());
                            Log.i(URL_TAG, "onResponse: error desc 1 " + response.getErrorDesc());
                            if(MyApp.appContext != null) {
                                Toast.makeText(MyApp.appContext, response.toString(), Toast.LENGTH_LONG).show();
                            }
                        }else{
                            if(MyApp.appContext != null) {
                                Toast.makeText(MyApp.appContext, "response is null", Toast.LENGTH_LONG).show();
                            }
                        }

                        Log.i(URL_TAG, "onResponse: error desc 2 " + errorDesc);

                        dismisProgressDialog();

                        /*
                            change isCreditProssesOver to true after receiving indication that
                            the CreditGaurd proccess was successfully finished. and needs no more too show
                            the credit dialog
                             */
                            LicensingNumberActivity.isCreditProccessOver = success;

                        // show dialog presenting process description
                        String textToShow = response != null ? response.getErrorDesc() : errorDesc;
                        DialogError.show(getActivity(), textToShow, new Runnable() {
                            @Override
                            public void run() {
                                if(dialog != null){dialog.dismiss();}
                                openNextFragment();
                            }
                        });
                    }
                });
    }






    private boolean validation () {
        StringBuilder errorMessage = new StringBuilder();
        boolean hasError = false;

        if(etAmount.getText().toString().isEmpty()
                && etFirstName.getText().toString().isEmpty()
                && etLastName.getText().toString().isEmpty()
                && etID.getText().toString().isEmpty()
                && etCVV.getText().toString().isEmpty()){
                errorMessage.append(MyApp.appContext.getResources().getString(R.string.complete_the_details_or_click_skip)+ "\n\n");
                hasError = true;
        }

        errorMessage.append(MyApp.appContext.getResources().getString(R.string.please_complete_the_missing_fields)+ "\n\n");
        if (etAmount.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append(MyApp.appContext.getResources().getString(R.string.amount)+"\n");
        }

        if (etFirstName.getText().toString().isEmpty() || etLastName.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append(MyApp.appContext.getResources().getString(R.string.cardholders_name)+"\n");
        }

        if (etID.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append(MyApp.appContext.getResources().getString(R.string.id)+"\n");
        }else {
            if (!IDValidator(etID.getText().toString())){
                hasError = true;
                errorMessage.append(MyApp.appContext.getResources().getString(R.string.id__an_invalid_value)+ "\n");
            }
        }



        /*if (etCardNumber.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append("מספר כרטיס"+"\n");
        }


        if (etValidityMonth.getText().toString().isEmpty()||etValidityYear.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append("תוקף"+"\n");
        }else {
            if (!isValidValidity()){
                errorMessage.append("תוקף - ערך לא חוקי"+"\n");
            }
        }*/

        if (etCVV.getText().toString().isEmpty()){
            hasError = true;
            errorMessage.append(MyApp.appContext.getResources().getString(R.string.cvv)+"\n");
        }

        if (hasError){
            toast(errorMessage.toString());
        }

        return !hasError;
    }



    private void updateContractDetails () {
        CreditCard creditCard = new CreditCard();
        creditCard.setAmount(etAmount.getText().toString());
        creditCard.setUserID(etID.getText().toString());
        creditCard.setUserName(etFirstName.getText().toString() + " " + etLastName.getText().toString());
        creditCard.setFirstName(etFirstName.getText().toString());
        creditCard.setLastName(etLastName.getText().toString());
        try{
            String currencyCode = selectedCurrency.CurrencyCode;
            creditCard.setCurrencyCode(currencyCode.trim());
        }catch (Exception e){}

        //creditCard.setCardNumber(etCardNumber.getText().toString());
        creditCard.setCvv(etCVV.getText().toString());
        //creditCard.setMonthValidity(Integer.parseInt(etValidityMonth.getText().toString()));
        //creditCard.setYearValidity(Integer.parseInt(etValidityYear.getText().toString()));


        getContractDetails().setCreditCard(creditCard);
        getContractDetails().saveToFile();
    }

    private void doClear (){
        String EMPTY = "";

        etID.setText(EMPTY);
        etUserName.setText(EMPTY);
        //etCardNumber.setText(EMPTY);
        //etValidityMonth.setText(EMPTY);
        //etValidityYear.setText(EMPTY);
        etCVV.setText(EMPTY);
    }


    private void openNextFragment () {
        dismisProgressDialog();
        Fragment nextFragment = DriverDetailseActivity.getNextFragment(getClass().getSimpleName(),getContractDetails());
        if (nextFragment != null){
            FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                    nextFragment,R.id.driverDetailsContainer);
        }else {
            ContractDocActivity_.intent(getActivity())
                    .extra("km",getContractDetails().Km)
                    .extra("fuel",getContractDetails().Fuel)
                    .start();
        }
    }


    // check if id number is valid
    private boolean IDValidator(String id) {
        if(id == null){
            return false;
        }
        int idLenght = id.length();

        /*if (id.length() != 9) {
            return false;
        }*/
        int counter = 0, incNum;

        for (int i = 0; i < id.length(); i++) {
            incNum = Integer.parseInt(String.valueOf(id.charAt(i))) * ((i % 2) + 1);//multiply digit by 1 or 2
            counter += (incNum > idLenght) ? incNum - idLenght : incNum;//sum the digits up and add to counter
        }
        return (counter % 10 == 0);
    }

     /* @Click
    public void etValidityMonth (){
        showDatePickerDialog();
    }

    @Click
    public void etValidityYear () {
        showDatePickerDialog();
    }


    @EditorAction(R.id.etCardNumber)
    void onEditorActionsOnetCardNumber(TextView tv, int actionId, KeyEvent keyEvent) {

        if (actionId == EditorInfo.IME_ACTION_NEXT){
            showDatePickerDialog();
        }

    }*/



    /*private boolean isValidValidity() {

        try {
            int month = Integer.parseInt(etValidityMonth.getText().toString());
            int year = Integer.parseInt(etValidityYear.getText().toString());

            Calendar enteredDate = Calendar.getInstance();
            enteredDate.set(Calendar.MONTH,month);
            enteredDate.set(Calendar.YEAR,year);

            if (System.currentTimeMillis() < enteredDate.getTimeInMillis()){
                return true;
            }

        }catch (Exception e){}

        return false;
    }*/


    /*private void showDatePickerDialog () {

        int years = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH)+1;

        try{
            years = Integer.parseInt(etValidityYear.getText().toString());
        }catch (Exception e){}
        try{
            month = Integer.parseInt(etValidityMonth.getText().toString());
        }catch (Exception e){}


        DatePickerDialog datePickerDialog = new DatePickerDialog
                (getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        etValidityYear.setText(String.valueOf(year));
                        etValidityMonth.setText(String.valueOf(monthOfYear+1));

                        etCVV.requestFocus();
                        AppUtil.showKeyboard(etCVV);

                    }
                }, years, month, 0);

        try {
            java.lang.reflect.Field[] datePickerDialogFields = datePickerDialog.getClass().getDeclaredFields();
            for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField.get(datePickerDialog);
                    java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
                    for (java.lang.reflect.Field datePickerField : datePickerFields) {
                        Log.i("test", datePickerField.getName());
                        if ("mDaySpinner".equals(datePickerField.getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }
            }
        }
        catch (Exception ex) {}

        datePickerDialog.show();
    }*/



}
