package com.sixt.android.activities.customerYard.utils;

import com.sixt.android.activities.customerYard.ui.PhotosView;

import java.util.ArrayList;

/**
 * Created by natiapplications on 27/12/15.
 */
public class ContractChetchData {


    public static String currentLicensingNumber;
    public static ArrayList<PhotosView.Photo> photos;
    public static String currentContractNumber;

    public static void clear () {
        currentLicensingNumber = null;
        photos = null;
        currentContractNumber = null;
    }
}
