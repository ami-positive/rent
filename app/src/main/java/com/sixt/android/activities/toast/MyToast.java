package com.sixt.android.activities.toast;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

public class MyToast {
	
	private final Activity caller; 
	private final String text; 
	
	private MyToast(Activity caller, String text) {
		this.caller = caller; 
		this.text = text; 	
	}

	public static MyToast makeText_success(final Activity caller, final String text, int notused) { 
		return makeText(caller, text, notused);
	}

	public static MyToast makeText(final Activity caller, final String text, int notused) {
		return new MyToast(caller, text);
	}
	
	
	public void show() {
		final Context ac = caller.getApplicationContext();
		show_toast(ac); // 3.5 secs
		new Timer().schedule(new TimerTask() {			
			@Override
			public void run() {
				Looper.prepare();  
				caller.runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						show_toast(ac);
					}
				});
			}
		}, 3000);
		
	}
		
	private void show_toast(Context ac) {
		try { 
			Toast.makeText(ac, text, Toast.LENGTH_LONG).show(); // 3.5 secs
		}
		catch (Exception e) {
			// no op
		}
	}
	
}
