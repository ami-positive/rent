package com.sixt.android.activities.customerYard.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.activities.ContractDocActivity_;
import com.sixt.android.activities.customerYard.activities.DriverDetailseActivity;
import com.sixt.android.activities.customerYard.adapters.ExtrasAdapter;
import com.sixt.android.activities.customerYard.network.NetworkCallback;
import com.sixt.android.activities.customerYard.network.requests.SendExtrasRequest;
import com.sixt.android.activities.customerYard.network.response.BaseResponseObject;
import com.sixt.android.activities.customerYard.network.response.ContractDetailsResponse;
import com.sixt.android.activities.customerYard.utils.AppUtil;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by natiapplications on 18/12/15.
 */
@EFragment(R.layout.fragment_extras)
public class ExtrasFragment extends BaseFragment implements DriverDetailseActivity.ActionButtonsListener
                ,AdapterView.OnItemClickListener{




    @ViewById
    TextView txtFragTitle;
    @ViewById
    ListView extrasListView;

    ExtrasAdapter extrasAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @AfterViews
    public void onPostCreate () {

        AppUtil.hideKeyBoard(this);

        ((DriverDetailseActivity) getActivity()).setActionButtonsListener(this);
        ((DriverDetailseActivity)getActivity()).showBaseDetails();
        txtFragTitle.setText(getFragmentName());
        fillScreenData();
    }

    private void fillScreenData() {
        ArrayList<ContractDetailsResponse.Extra> array = new ArrayList<ContractDetailsResponse.Extra>();
        if(getContractDetails().Extras != null){
            array = getContractDetails().Extras;
        }
        extrasAdapter = new ExtrasAdapter(getActivity(),0,array);
        extrasListView.setAdapter(extrasAdapter);
        extrasListView.setOnItemClickListener(this);
    }

    @Override
    public int getFragmentIndex() {
        return 4;
    }

    @Override
    public String getFragmentName() {
        return "אקסטרות";
    }

    @Override
    public boolean onNext() {

        // save even if array of extras is empty
        sendExtrasToServer();

/*        if (getContractDetails().isHasCheckedExtras()){
            sendExtrasToServer();
        }else {
            ContractDocActivity_.intent(getActivity()).start();
        }*/

        return true;
    }

    @Override
    public void onPrevious() {

    }

    @Override
    public void onClear() {
        Dialog2Btns.create(getActivity(), "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
            @Override
            public void run() {
                doClear();
            }
        }).show();

    }


    private void sendExtrasToServer(){
        showProgressDialog();

        MyApp.networkManager.makeRequest(new SendExtrasRequest(getContractDetails().getCheckedExtras()),new NetworkCallback<BaseResponseObject>(){
            @Override
            public void onResponse(boolean success, String errorDesc, BaseResponseObject response) {
                super.onResponse(success, errorDesc, response);
                dismisProgressDialog();
                if (success){
                    ContractDocActivity_.intent(getActivity())
                            .extra("km",getContractDetails().Km)
                            .extra("fuel",getContractDetails().Fuel)
                            .start();
                }else {
                    toast(errorDesc);
                }
            }
        });
    }


    private void doClear (){
        for (int i = 0; i < getContractDetails().Extras.size(); i++) {
            getContractDetails().Extras.get(i).checked = 0;
        }
        getContractDetails().saveToFile();
        extrasAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        int newValue = getContractDetails().Extras.get(position).checked > 0 ? 0:1;

        getContractDetails().Extras.get(position).checked = newValue;
        getContractDetails().saveToFile();
        extrasAdapter.notifyDataSetChanged();
    }
}
