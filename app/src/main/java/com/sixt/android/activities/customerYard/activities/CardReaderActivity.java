package com.sixt.android.activities.customerYard.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.rent.R;

import kr.co.bluebird.android.bbapi.client.BBAPI_Callback;
import kr.co.bluebird.android.bbapi.client.BBAPI_Client;
import kr.co.bluebird.android.bbapi.client.BBAPI_ServiceListener;
import kr.co.bluebird.android.bbapi.client.func.BBAPI_Barcode;
import kr.co.bluebird.android.bbapi.client.func.BBAPI_Device;
import kr.co.bluebird.android.bbapi.client.func.BBAPI_ICReader;
import kr.co.bluebird.android.bbapi.client.func.BBAPI_MSR;

/**
 * Created by Izakos on 13/04/2016.
 */

public class CardReaderActivity extends FragmentActivity {

    public static final String  ACTION_DESTROY_SETTING_FROM_HOMEKEY = "kr.co.bluebird.android.action.ACTION_DESTROY_SETTING_FROM_HOMEKEY";
    public static final String ACTION_DEVICE_RECONNECT = "kr.co.bluebird.android.action.DEVICE_RECONNECT";

    StringBuilder stringBuilder;
    private BroadcastReceiver mScreenReceiver;
    private BBAPI_Callback mRemoteCallback;
    private BBAPI_MSR mMSR;
    private BBAPI_Client mClient;
    private BBAPI_Device mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Make us non-modal, so that others can receive touch events.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setContentView(R.layout.main);

        put("--Start Program---");
        put("setVolumeControlStream -> AudioManager.STREAM_MUSIC");
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        put("Create remote callback");
        mRemoteCallback = new BBAPI_Callback() {
            @Override
            public void onMSRGetStatus(int i, int i1, int i2, int i3) throws RemoteException {
                put("onMSRGetStatus");
            }

            @Override
            public void onDeviceConnected(int deviceType, int handle, boolean hasBarcode, boolean hasICReader, boolean hasMSR) throws RemoteException {
                put("onDeviceConnected");
                mMSR.setEnableTrackSound(handle, false, true, false);
                put("onDeviceConnected open MSR");
                mMSR.open(handle, 1, 0);
            }

            @Override
            public void onDeviceDisconnected(int i, int i1) throws RemoteException {
                put("onDeviceDisconnected");
            }

            @Override
            public void onDevicePermissionDenied(int i, int i1) throws RemoteException {
                put("onDevicePermissionDenied");
            }

            @Override
            public void onDeviceNotFound(int i, int i1) throws RemoteException {
                put("onDeviceNotFound");
            }

            @Override
            public void onSuccess(int deviceType, int handle, int no) throws RemoteException {
                put("onSuccess");


            }

            @Override
            public void onError(int i, int i1, int i2, int i3) throws RemoteException {
                put("onError " + i + " | " + i1 + " | " + i2 + " | " + i3);

            }

            @Override
            public void onUpgrade(int i, int i1, int i2) throws RemoteException {
                put("onUpgrade " + i + " | " + i1 + " | " + i2);
            }

            @Override
            public void onUpgradeProgress(int i, int i1, int i2) throws RemoteException {
                put("onUpgradeProgress " + i + " | " + i1 + " | " + i2);
            }

            @Override
            public void onDeviceKeyEvent(int i, int i1, int i2) throws RemoteException {
                put("onDeviceKeyEvent");
            }

            @Override
            public void onGetDeviceInfo(int i, int i1, int i2, String s, boolean b, boolean b1, boolean b2, String s1, String s2, String s3, String s4, String s5, String s6, String s7) throws RemoteException {
                put("onGetDeviceInfo");
            }

            @Override
            public void onBarcodeDecodingData(int i, int i1, int i2, String[] strings) throws RemoteException {
                put("onBarcodeDecodingData");
            }


            @Override
            public void onBarcodeSetDataComplete(int i, int i1, int i2) throws RemoteException {
                put("onBarcodeSetDataComplete");
            }

            @Override
            public void onBarcodeDecodingTimeout(int i, int i1) throws RemoteException {
                put("onBarcodeDecodingTimeout");
            }

            @Override
            public void onBarcodeGetStatus(int i, int i1, int i2, int i3) throws RemoteException {
                put("onBarcodeGetStatus");
            }

            @Override
            public void onBarcodeGetSymbology(int i, int i1, int i2, int i3, int i4) throws RemoteException {
                put("onBarcodeGetSymbology");
            }

            @Override
            public void onBarcodeGetSymbologyExt(int i, int i1, int i2, int i3, int i4, int i5, int i6) throws RemoteException {
                put("onBarcodeGetSymbologyExt");
            }

            @Override
            public void onBarcodeGetMode(int i, int i1, int i2, int i3, int i4) throws RemoteException {
                put("onBarcodeGetMode");
            }

            @Override
            public void onBarcodeGetModeExt(int i, int i1, int i2, int i3, int i4, int[] ints) throws RemoteException {
                put("onBarcodeGetModeExt");
            }

            @Override
            public void onBarcodeGetSoundMode(int i, int i1, int i2, int i3) throws RemoteException {
                put("onBarcodeGetSoundMode");
            }

            @Override
            public void onBarcodeGetTriggerMode(int i, int i1, int i2, int i3) throws RemoteException {
                put("onBarcodeGetTriggerMode");
            }

            @Override
            public void onICReaderDetectCard(int i, int i1, int i2) throws RemoteException {
                put("onICReaderDetectCard");
            }

            @Override
            public void onICReaderOpen(int i, int i1, byte[] bytes) throws RemoteException {
                put("onICReaderOpen");
            }

            @Override
            public void onICReaderClose(int i, int i1, int i2) throws RemoteException {
                put("onICReaderClose");
            }

            @Override
            public void onICReaderGetStatus(int i, int i1, int i2) throws RemoteException {
                put("onICReaderGetStatus");
            }

            @Override
            public void onICReaderSendCommand(int i, int i1, byte[] bytes) throws RemoteException {
                put("onICReaderSendCommand");
            }

            @Override
            public void onMSRData(int deviceType, int handle, int mode, byte[] track1, byte[] track2, byte[] track3) throws RemoteException {
                put("onMSRData" + deviceType + ":" + handle + " mode=" + mode);
                put("[track1]" + track1);
                put("[track2]" + track2);
                put("[track3]" + track3);

                onMSRDataIn(deviceType, handle, mode, track1, track2, track3);
            }
        };

        ((Button) findViewById(R.id.continuation)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "close");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        mScreenReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(ACTION_DESTROY_SETTING_FROM_HOMEKEY)) {
                    put("from Receiver ACTION_DESTROY_SETTING_FROM_HOMEKEY");
                } else if (action.equals(ACTION_DEVICE_RECONNECT)) {
                    put("from Receiver ACTION_DEVICE_RECONNECT");
                }
            }
        };

        put("registerReceiver");
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_DEVICE_RECONNECT);
            filter.addAction(ACTION_DESTROY_SETTING_FROM_HOMEKEY);
            this.registerReceiver(mScreenReceiver, filter);
        } catch(Exception e) {
            put("registerReceiver Exception " + e.getMessage());
        }

        mClient = new BBAPI_Client(this, new BBAPI_ServiceListener() {

            @Override
            public void onServiceConnected(BBAPI_Device bbapi_device, BBAPI_Barcode bbapi_barcode, BBAPI_ICReader bbapi_icReader, BBAPI_MSR bbapi_msr) {
                put("onServiceConnected");
                try {
                    mMSR = bbapi_msr;
                    mDevice = bbapi_device;
                    mDevice.powerOn(0);
                    mDevice.open(65536, mRemoteCallback, 0);
                } catch (RemoteException e) {
                    put("onServiceConnected RemoteException " + e.getMessage());
                }
            }

            @Override
            public void onServiceDisconnected() {
                put("onServiceDisconnected");
            }
        });

        try {
            mClient.startService();
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void onMSRDataIn(int deviceType, int handle, int mode, byte[] track1, byte[] track2, byte[] track3) {
        try {
            int track1Length = 0;
            int track2Length = 0;
            int track3Length = 0;
            if (track1 != null) { track1Length = track1.length; }
            if (track2 != null) { track2Length = track2.length; }
            if (track3 != null) { track3Length = track3.length; }

            String track1String = "";
            String track2String = "";
            String track3String = "";

            if (mode == 1 || mode == 3) {
                track1String = getCharString(track1);
                track2String = getCharString(track2);
                track3String = getCharString(track3);
            } else if (mode == 2) {
                track1String = getHexString(track1);
                track2String = getHexString(track2);
                track3String = getHexString(track3);
            }

            String track1Text = "";
            String track2Text = "";
            String track3Text = "";

            track1Text = "[TRACK1:" + track1Length + "]\n" + track1String + "\n\n";
            track2Text = "[TRACK2:" + track2Length + "]\n" + track2String + "\n\n";
            track3Text = "[TRACK3:" + track3Length + "]\n" + track3String;
            put("MSR result:");
            put(track2Text);

            if(!track2Text.trim().isEmpty()) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", track2String);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
            put("onMSRData Error");
        }
    }

    private String getCharString(byte[] track) {
        StringBuilder sb = new StringBuilder("");
        try {
            int length = track.length;
            for (int index = 0; index < length; index++) {
                sb.append((char)track[index]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private String getHexString(byte[] track) {
        StringBuilder sb = new StringBuilder("");
        try {
            int length = track.length;
            byte b;
            String s;
            for (int index = 0; index < length; index++) {
                b = track[index];
                s = String.format("%02X", b);
                sb.append(s);
                if (index < length - 1) {
                    sb.append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        put("onStart");

        mClient.startService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        put("unregisterReceiver");
        try {
            this.unregisterReceiver(mScreenReceiver);
        } catch(Exception e) {
            put("unregisterReceiver Exception " + e.getMessage());
        }
    }

    public void put(final String log) {
        if(log == null){
            return;
        }
        Log.i("MSR_TAG", log);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (stringBuilder == null) {
                    stringBuilder = new StringBuilder();
                }
                stringBuilder.append(log + "\n");
                try {
                    TextView textView = (TextView) findViewById(R.id.text);
                    textView.setText(log);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }






}
