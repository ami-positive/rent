package com.sixt.android.activities.customerYard.network.requests;

import com.google.gson.annotations.SerializedName;
import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.network.ApiInterface;
import com.sixt.android.activities.customerYard.network.NetworkManager;
import com.sixt.android.activities.customerYard.network.response.GetCreditGuardLinkResponse;
import com.sixt.android.activities.customerYard.utils.ContractChetchData;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.LoginSixt;

import retrofit.Callback;

/**
 * Created by natiapplications on 04/01/16.
 */
public class GetCreditGuardLink extends RequestObject<GetCreditGuardLinkResponse> {


    private int sum;
    private String currencyCode;
    private int userCG;
    private int requestId;


    public GetCreditGuardLink(int sum, String currencyCode, int userCG,int requestId) {
        this.sum = sum;
        this.currencyCode = currencyCode;
        this.userCG = userCG;
        this.requestId = requestId;
    }

    @SerializedName("GetCreditGuardUrl_Req")
    private CreditGuardLinkReqeustInner  creditGuardLinkReqeustInner;

    @Override
    protected void execute(ApiInterface apiInterface, Callback<GetCreditGuardLinkResponse> callback) {

        initRequestObj ();
        apiInterface.getCreditGuardLink(creditGuardLinkReqeustInner, callback);

    }

    private void initRequestObj() {
        creditGuardLinkReqeustInner = new CreditGuardLinkReqeustInner(userCG,currencyCode,sum,requestId);
    }

    @Override
    protected boolean showDefaultToastsOnError() {
        return true;
    }




    public class CreditGuardLinkReqeustInner {


        public CreditGuardLinkReqeustInner(int usercg, String currencyCode, int sum, int requestId) {
            Usercg = usercg;
            CurrencyCode = currencyCode;
            Sum = sum;
            this.requestId = requestId;
        }

        public int requestId;
        public int Sum;
        public int Reader = 1;
        public int Usercg;
        public String CurrencyCode;
        public String Action = "O";
        public String App = "R";
        public String CarNo = ContractChetchData.currentLicensingNumber;
        public String MenuAction = "B";
        public int Login = Integer.parseInt(LoginSixt.get_Worker_No());
        public int OffSite = 1;
        public String debugVersion = MyApp.appVersion;
        public String imei = DeviceAndroidId.get_imei();
        public String msg_type = "GetCreditGuardUrl_Request";
        public String simcard_serial = DeviceAndroidId.get_Sim_serial();

    }

    @Override
    public String getServerEndPoint() {
        return NetworkManager.CREDIT_GUARD;
    }
}