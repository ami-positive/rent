package com.sixt.android.activities;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.sixt.android.MyApp;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.FuelValues;

/**
 * Created by Izakos on 17/06/2016.
 */
public class ActivityLifecycle implements android.app.Application.ActivityLifecycleCallbacks{

    private long mLogTime;
    public static Activity mCurrentActivity;

    public static ActivityLifecycle activate(Application application){
        ActivityLifecycle activityLifecycle = new ActivityLifecycle(application);
        return activityLifecycle;
    }

    public ActivityLifecycle(Application application) {
        application.registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        activityLifecycleLog("onActivityCreated: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        activityLifecycleLog("onActivityStarted: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        activityLifecycleLog("onActivityResumed: " + activity.getClass().getSimpleName());
        MyApp.appSetting.setActivity(activity.getClass().getSimpleName());
        mCurrentActivity = activity;

         if(activity instanceof LoginActivity_
                 || activity instanceof BranchSelectActivity_
                 || activity instanceof MainMenuActivity_
                 || activity instanceof MenuMischarriActivity_
                 || activity instanceof CarNumberMischarriHachzarraActivity_
                 || activity instanceof MenuPnimmiActivity_){
             CarNumberValue.clear();
             FuelValues.clear();
             MyApp.appSetting.showImageLoadingProgressBar = false;
             MyApp.appSetting.restImageEvent();
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        activityLifecycleLog("onActivityPaused: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
        MyApp.appSetting.save();


    }

    @Override
    public void onActivityStopped(Activity activity) {
        activityLifecycleLog("onActivityStopped: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        activityLifecycleLog("onActivitySaveInstanceState: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityLifecycleLog("onActivityDestroyed: " + activity.getClass().getSimpleName());
        mCurrentActivity = activity;
    }

    public void activityLifecycleLog(String log){
        Log.i("activityLifecycleLog", " {" + (System.currentTimeMillis() - mLogTime) + "} " + log);
        mLogTime = System.currentTimeMillis();
    }
}
