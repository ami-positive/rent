package com.sixt.android.activities;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sixt.android.activities.base.BaseListActivity;
import com.sixt.android.activities.toast.MyToast;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetRejects_Response;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.PrivatenessOfCar;
import com.sixt.android.app.util.TEST;
import com.sixt.android.ui.Debug_CarRejectAdapter;
import com.sixt.android.ui.Debug_RejectRecord;
import com.sixt.android.ui.DisplayLogic;
import com.sixt.android.ui.RejectListAdapter;
import com.sixt.android.ui.RejectListAdapter.ViewHolder;
import com.sixt.rent.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

// setContentView(R.layout.reject_screen_layout);
@EActivity
@NoTitle
public class RejectListActivity extends BaseListActivity {

	// select onClick of item: onClick_ListItem

	// CarRejectsAdapter
	
	// R.layout.reject_entry;
	
	//clear should keep GetCarDetails reject on!;   

	static final String MANDATORY_SET_ERROR = "שים לב: פריט חובה סומן כשגוי"; 

	private static final boolean IS_IMAGE = false;

	private int last_clicked_pos = -1;
	
	private static final boolean _DEBUG = TEST.debug_mode(false);
	private static final int SUBREJECT_CODE = 33433;
	
	private static final int DLG_CLEAR = 4344;
	private static final int DLG_CANCEL = 433;

	private static RejectListActivity inst;
	
	private static RejectListAdapter list_adapter;
	private static ViewHolder clicked_reject_holder;
	private static int clicked_reject_position;
	private static volatile int clicked_reject_orig_val = -1;
	private static GetRejects_Reject clicked_reject_current ;

	private Button ok_button;
	private Button clear_button;
	private Button cancel_button;

	private static GetRejects_Reject[] all_available_list_rejects;


	@ViewById
	ListView list;

	// flag indicate if Hearot screen need to be shown
	public static boolean showHearot;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		inst = null;		
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		ok_button.setText("הבא");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SignatureActivity.list_reject_arr = null; 
		
		inst = this;

		setContentView(R.layout.reject_screen_layout);

		TextView num = (TextView) findViewById(R.id.car_number);
//		num.setText("מספר: " + CarNumberActivity.number); 
		num.setText("מספר: " + CarNumberValue.get());


		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View bottomView =  inflater.inflate(R.layout.bottom_btns_three, null, false);

		ok_button = (Button) bottomView.findViewById(R.id.btn_send);
		clear_button = (Button) bottomView.findViewById(R.id.btn_clear);
		cancel_button = (Button) bottomView.findViewById(R.id.btn_cancel);

		ok_button.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				handle_ok();
			}
		});

		clear_button.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				showDialog(DLG_CLEAR); //do_clear
			}
		});
		
		cancel_button.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				showDialog(DLG_CANCEL); //do_clear
			}
		});

		ListView listView = getListView();
		listView.addFooterView(bottomView);

		reset_data();
	} 

	
	private void reset_data() {
		last_clicked_pos = -1; 
		if (_DEBUG) {
			setListAdapter(new Debug_CarRejectAdapter(this, debug_listValues));
		}
		else {
			populate_list();
			if (all_available_list_rejects==null) {
				int jj=234;
				jj++;
				return; 
			}
			clear_all_prior_selections(all_available_list_rejects);
			
			// lastForm used below:
			GetCarDetails_Response.add_actual_rejects_type_list(all_available_list_rejects); //actual car rejects 
			
			setListAdapter((list_adapter=new RejectListAdapter(this, all_available_list_rejects))); //SubRejects inside
		}
	}
	

	protected Dialog onCreateDialog(int id) {
		if (id == DLG_CLEAR) {
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לנקות את כל התקלות?", new Runnable() {				
				@Override
				public void run() {
					clear_all_rejects();
				}
			});
		}
		else if (id == DLG_CANCEL) {
			return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {				
				@Override
				public void run() {
					RejectListActivity.this.finish(); 
				}
			});
		}

		else {
			return super.onCreateDialog(id);
		}
	}
 
	
	
	protected void clear_all_rejects() {
//		clear_all_prior_selections(all_available_list_rejects);
//		setListAdapter((list_adapter=new RejectListAdapter(this, all_available_list_rejects)));
		reset_data();
		refresh_display(); 
	}

		
	public static void handle_lesser_reject_set() {
		if (inst != null) {
			MyToast.makeText(inst, "לא ניתן להפחית חומרת תקלה", Toast.LENGTH_LONG).show();
		}
	}
	
	
	public static void onClick_ListItem(GetRejects_Reject current, int orig_val, int new_val, boolean is_first_option, ViewHolder _holder, final int _position) {
		clicked_reject_holder = _holder;
		clicked_reject_position = _position;
		clicked_reject_orig_val = -1;
		clicked_reject_current = current;
		
		if (inst == null) {
			return;
		}
		
		boolean reject_mode_mesirra = DisplayLogic.reject_mode_mesirra();
		if (reject_mode_mesirra) { 
			boolean isMandatory = current.is_mok();
			if (new_val > 0 && isMandatory) {
				MyToast.makeText(inst, MANDATORY_SET_ERROR, Toast.LENGTH_LONG).show();
			}
		}
		
		if (current.has_subrejects()) {
			inst.last_clicked_pos = _position;
			clicked_reject_orig_val = orig_val;
        	SubRejectsActivity.current_reject = current;
        	SubRejectsActivity_.intent(inst).startForResult(SUBREJECT_CODE); 
        }
        else {
			if (is_first_option) {
				// should clear - no op
			}
			else { 
				current.value = new_val; 
			}
        }
	} 
	   
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
//		if (requestCode==SUBREJECT_CODE && clicked_reject_position > 0) {
//			list.smoothScrollToPosition(clicked_reject_position);
//		}
	}
 

	public static void handle_subreject_cancellation() {
		if (inst==null) {
			return;
		}
		if (clicked_reject_current==null) {
			return;
		}
//		int cur_val = clicked_reject_current.value;
//		if (cur_val == clicked_reject_orig_val) {
//			return;
//		}  
		
		// revert to old value 
		clicked_reject_current.value = clicked_reject_orig_val; //
		if (clicked_reject_current.value == 0 && has_active_subrejects(clicked_reject_current)) {
			clicked_reject_current.value = 1;
		}
		inst.refresh_display();		
	}

	
	private static boolean has_active_subrejects(GetRejects_Reject reject) {
		boolean is_private = PrivatenessOfCar.is_p_or_pm();
		SubReject[] all_subrejects = reject.filter_SubRejects(is_private);
		if (all_subrejects==null) {
			return false;
		}
		for (SubReject sr: all_subrejects) {
			if (sr.value > 0) {
				return true;
			}
		}
		return false;
	}

	public static void handle_subreject_selection(GetRejects_Reject current_reject,
			ArrayList<SubReject> active_subrejects) {
		// active_subrejects will include (1) no-default SRs now set to OK
		// and (2) no-default/OK SRs with left checkBox selected, as well as (3) 'real' rejects
		
		final int num_subrejects = active_subrejects.size();
		SubReject[] active_arr = active_subrejects.toArray(new SubReject[num_subrejects]);
		_set_from_arr(current_reject.SubReject, active_arr);
				
		if (no_real_errors(active_subrejects)) {
			// mark parent reject as OK
			clear_single(all_available_list_rejects, current_reject); 
			current_reject.value = 0;
			if (list_adapter != null) {
				list_adapter.set_to_noerror(clicked_reject_holder);
			}
		} 
		else {
			if (list_adapter != null) {
				// mark middle btn as a default error;
				int ind = list_adapter.get_selected_btn_ind(clicked_reject_holder);
				if (ind < 1) {
					current_reject.value = 1;
//					list_adapter.set_selected(clicked_reject_holder, 1);
				}
			}
		}
 		inst.refresh_display();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		if (_DEBUG) {
//	 		inst.refresh_display();			
//		}
		if (last_clicked_pos > -1/* && list_adapter != null*/) {
//			int last = list_adapter.getCount()-1;
//			int center_pos = Math.min(last_clicked_pos + 6, last);
//			list.setSelection(center_pos); 
			list.setSelection(last_clicked_pos);
			last_clicked_pos = -1;
		}
	}

	
	private static boolean no_real_errors(ArrayList<SubReject> active_subrejects) {
		int num_subrejects = active_subrejects.size();
		if (num_subrejects==0) {
			return true;
		}
		for (SubReject sr: active_subrejects) { 
			if (sr.value > 0) {
				return false; // REAL error found
			}
		}
		return true;
	}

	private static void _set_from_arr(SubReject[] subRejects, SubReject[] active_arr) {
		if (active_arr==null) {
			return;
		}
		for (SubReject sr: active_arr) {
			final String code = sr.SubRejectCode;
			final int new_value = sr.value;
			final boolean new_checked = sr.left_check_selected;
//			if (new_value < 1) {
			if (new_value < 0 && !new_checked) {
				continue;
			}
			
			boolean found = false;
			for (SubReject orig: subRejects) {
				if (orig.SubRejectCode.equals(code)) {
					// match
					orig.value = new_value;
					orig.left_check_selected = new_checked;
					found = true;
					break;
				}				
			}
			
			if (!found) {
				int jjj=234;
				jjj++;
				String codd = code;;
				jjj++;
				jjj++;
			}
		}		
	}

	private void refresh_display() {
		if (_DEBUG) {
			// no op
			Debug_CarRejectAdapter dad = new Debug_CarRejectAdapter(this, debug_listValues);
			setListAdapter(dad);
		}
		else {
			setListAdapter((list_adapter=new RejectListAdapter(this, all_available_list_rejects))); //SubRejects inside
		}
	} 

	
	private void populate_list() { 
		boolean is_private = PrivatenessOfCar.is_p_or_pm();		
		all_available_list_rejects = 
				GetRejects_Response.get_all_available_rejects(is_private);		
		if (all_available_list_rejects==null) {
			MyToast.makeText(this, "לא נתקבלה רשימת תקלות מהשרת", Toast.LENGTH_LONG).show();
		}
	}

	private static void clear_single(GetRejects_Reject[] arr, GetRejects_Reject current_reject) {
		if (arr==null || current_reject==null) {
			return;
		}
		for (GetRejects_Reject r: arr) {
			if (r.RejectCode.equals(current_reject.RejectCode)) {
				r.value = 0;
				return;
			}
		}				
	}
 
	
	private static void clear_all_prior_selections(GetRejects_Reject[] arr) { 
		if (arr != null) {
			for (GetRejects_Reject r: arr) {
				r.init_value_for_reject_and_subs_according_to_default_flag();
			}
		}
	}


	private static String mok_items_msg = "";
	
	private void handle_ok() { // do_send();
		if (!_DEBUG) { 
			mok_items_msg = "";
			if (mok_items_not_cleared()) {
				//MyToast.makeText(this, " פריט חובה סומן כלא תקין", Toast.LENGTH_LONG).show(); 
				MyToast.makeText(this, mok_items_msg, Toast.LENGTH_LONG).show();
				return;
			}
			if (items_with_no_value_exist()) { // if rejectCheck==1 >> no default 
				MyToast.makeText(this, " יש לציין ערך לכל אחד מהפריטים ברשימה", Toast.LENGTH_LONG).show(); 
				return;
			}
			SignatureActivity.list_reject_arr = create_list_reject_arr(); 
		}
		
//		boolean is_hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();
//		boolean pnimmi_sgirra =  PnimmiValue.Pnimmi() && ActionValue.Sgirra();
//		if (is_hachzarra || pnimmi_sgirra) {


		// when the root is customer yard no need for showing Hearot screen
		if (showHearot) { // added by Itsik 20/03/16
			ImageCarActivity_.intent(this).start();
			showHearot = false;
		}else if (DisplayLogic.should_show_hearot_screen()) { //
			HearotLehachzarraActivity_.intent(this).start();
		}
		else { 
			ImageCarActivity_.intent(this).start();
		}
	}  


	private static boolean items_with_no_value_exist() {
		if (list_adapter==null) {
			return false;
		}
		int num = list_adapter.getCount();
		for (int i = 0; i < num; i++) {
			GetRejects_Reject cur = list_adapter.getItem(i);
			if (cur.value < 0) {
				return true;
			}
		}
		return false;
//		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
//		if (all_available_list_rejects==null) {
//			return false;
//		}
//		for (GetRejects_Reject reject: all_available_list_rejects) {
//			if (reject.value  < 0) { // no value was
//				return true;
//			}
//		}
//		return false;
	}

	private static boolean mok_items_not_cleared() {
//		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra();
		mok_items_msg = "יש לסמן פריטי חובה כתקינים: ";
		final boolean mok_is_active = DisplayLogic.mok_is_active(); //
		
//		if (!mode_mesirra) {
		if (!mok_is_active) {
			return false; // MOK is only for Mesirra mode
		}
		
		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
		if (all_available_list_rejects==null) {
			return false;
		}
		
		
		boolean result = false;
		
		for (GetRejects_Reject reject: all_available_list_rejects) {
			if (reject.value < 1) {
				continue; // not selected
			}
			SubReject[] sr_arr = reject.SubReject;
			if (sr_arr != null && sr_arr.length > 0) {
				// pass only subrejects!
				for (SubReject sr: sr_arr) {
					String code = sr.SubRejectCode;
					int value = sr.value;
					String _desc = sr.SubRejectDesc;
					if (value > 0) {
//						res.add(new RejectRecord(code, value, IS_IMAGE));
						if (sr.is_mok()) {
							mok_items_msg += (", " + _desc);
							result = true;
						}
					}					
				}
			}
			else {
				String code = reject.RejectCode;
				int value = reject.value;			
				String _desc = reject.RejectDesc;
				if (value < 0) {
					throw new RuntimeException();
				}
				if (value > 0) {
//					res.add(new RejectRecord(code, value, IS_IMAGE));
					if (reject.is_mok()) {
						mok_items_msg += (", " + _desc);
						result = true;
					}
				}
			}
		}
		return result;
	}
	

	private ArrayList<RejectRecord> create_list_reject_arr() { 
		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
		if (all_available_list_rejects==null) {
			return res;
		}
		for (GetRejects_Reject reject: all_available_list_rejects) {
			if (!value_was_set(reject) && !has_checked_subrejects(reject)) {
				continue; // not selected
			}
			SubReject[] sr_arr = reject.SubReject;
			if (sr_arr != null && sr_arr.length > 0) {
				// pass only subrejects!
				for (SubReject sr: sr_arr) {
					String code = sr.SubRejectCode;
					int value = sr.value;
					int mark = sr.left_check_selected ? 1 : 0;
//					if (value > 0) {
					if (subreject_should_be_sent(sr)) { 
						res.add(new RejectRecord(code, value, mark, IS_IMAGE));
					}					
				}
			}
			else { 
				// no SR -- just send 1st level reject
				String code = reject.RejectCode;
				int value = reject.value;
				int mark = 0; // mark is non-zero only for subRejects!
				res.add(new RejectRecord(code, value, mark, IS_IMAGE));
			}
		}
		return res;
	}
	

	
	private static boolean subreject_should_be_sent(SubReject sr) {
		if (sr.value > 0) {
			return true;
		}
		if (sr.value == 0 && sr.no_default_value()) {
			return true;
		}
		if (sr.left_check_selected) {
			return true;
		}
		return false;
	}

	private static boolean value_was_set(GetRejects_Reject reject) {
		if (reject.value > 0) {
			return true;
		}
		if (reject.value==0 && reject.no_default_value()) {
			return true;
		}
		return false;
	}

	private static boolean has_checked_subrejects(GetRejects_Reject reject) {
		if (reject.SubReject==null) {
			return false;
		}
		SubReject[] arr = reject.SubReject;
		for (SubReject r: arr) {
			if (r.left_check_selected) {
				return true;
			}
		}
		return false;
	}

	public static void call_finish() {
		if (inst != null) {
			inst.finish();
		}

	}


	private static final Debug_RejectRecord [] debug_listValues = new Debug_RejectRecord[] {
		//debug data
		new Debug_RejectRecord("מכסה רכב", "חסר","לא תקין","תקין", true), // גכדעגכ
		new Debug_RejectRecord("מאפרות ואביזרי נוי", "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים", "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("שימשיות וציוד עזר", "חסר","לא תקין","תקין", false), //s
		new Debug_RejectRecord("חלונות ומראות", "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("אחר", "חסר","לא תקין","תקין",/**/ true),
		new Debug_RejectRecord("מכסה רכב", "חסר","לא תקין","תקין", true),
		new Debug_RejectRecord("מאפרות ואביזרי נוי", "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("מחוונים",/*reject record*/ "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("שימשיות וציוד עזר", "חסר","לא תקין","תקין", false),
		new Debug_RejectRecord("חלונות ומראות", "חסר","לא תקין","תקין", true),
	};


}