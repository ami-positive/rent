package com.sixt.android.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sixt.android.MyApp;
import com.sixt.android.activities.base.BaseActivity;
import com.sixt.android.app.dialog.Dialog2Btns;
import com.sixt.android.app.dialog.DialogOkCancel;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.json.request.SendForm_Request_Inner;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response.FuelAlert;
import com.sixt.android.app.json.response.GetCarDetails_Response_Inner;
import com.sixt.android.app.util.ActivityUtils;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.DateFormat2;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.FuelValues;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.app.util.TEST;
import com.sixt.android.app.util.TxtUtils;
import com.sixt.android.ui.BreadCrumbs;
import com.sixt.android.ui.FuelSlide;
import com.sixt.rent.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import static com.sixt.android.ui.FuelSlide.FUEL_LEVEL_NOTSET;

@EActivity(R.layout.car_isuff_mesirra_layout)
@NoTitle
public class FuelmesirraHachzarraActivity extends BaseActivity {

    private static final boolean _DEBUG = TEST.debug_mode(false);

    private static final int DLG_CANCEL = 323;
    private static final int DLG_CLEAR = 442;
    public static boolean isTheLastScreen;

//	private static final int DEFAULT_FUEL_VALUE = 3; 

//	public static String CollectedCar = null;
//	public static String CurFuel = null;
//	public static String CarCode = "";
//	public static String Keys = null;
//	public static String Controls = null;
//	public static String Mileage = null;


    public static FuelmesirraHachzarraActivity inst;
    @ViewById
    TextView caption;
    @ViewById
    Button btn_send;// next ok
    @ViewById
    Button btn_clear;
    @ViewById
    Button btn_cancel;
    @ViewById
    LinearLayout customer_name_container;
    @ViewById
    TextView license_no;
    @ViewById
    TextView car_model;


    //	@ViewById
//	LinearLayout collected_container;
//	@ViewById
//	LinearLayout rechev_neesaf_metuchnan_container;
    //	@ViewById
//	TextView company_name;
    @ViewById
    TextView license_expiration_date;
    @ViewById
    TextView insurance_expiration_date;
    @ViewById
    TextView contract_number;
    @ViewById
    TextView contract_type;
    @ViewById
    TextView customer_name;
    @ViewById
    TextView driver_name;
    @ViewById
    EditText mileage;
    @ViewById
    FuelSlide fuel_slide;
    private boolean fuel_was_alerted;
    private boolean fuel_alert_stop_mode;

//	@ViewById
//    TextView txtView_rechev_neesaf_metuchnan;

//	@ViewById
//	EditText editText_neesaf_bafoall;

    //	@ViewById
//	public EditText car_code;
//	@ViewById
//	public EditText num_of_keys;
//	@ViewById
//	public EditText num_of_controllers;
    private int fuel_level;
    private String txt_mileage;

    private static void setText_id_empty(EditText txt, String s) {
        if (txt.getText().toString().trim().length() == 0) {
            txt.setText(s);
        }
    }

    public static void call_finish() {
        if (inst != null) {
            inst.finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        inst = this;
    }

    @AfterViews
    void post_onCreate() {

        FuelValues.clear();

        fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);

        if (PnimmiValue.Pnimmi()) { // no customer name for Pnimmi!
            customer_name_container.setVisibility(View.GONE);
        } else {
            customer_name_container.setVisibility(View.VISIBLE);
        }


//		BreadCrumbs.append("פרטי חוזה");
        final String cur_bcrums = BreadCrumbs.get() + BreadCrumbs.SEP + "חוזה";
        caption.setText(cur_bcrums);


//		if (CurrentAction.get().equals(CurrentAction.Values.Isuf_Rechev.value)) {
//			collected_container.setVisibility(View.GONE);
//			rechev_neesaf_metuchnan_container.setVisibility(View.GONE);
//		}

        if (!_DEBUG) {
            populate_screen();
        }

        if (DeviceAndroidId.is_lab_device(this)) {
            int min = GetCarDetails_Response.min_mad_utz_val();
            if (min == -1) {
                setText_id_empty(mileage, "20");
            } else {
                setText_id_empty(mileage, "" + min);
            }
//			setText_id_empty(car_code,"1");
//			setText_id_empty(editText_neesaf_bafoall, "3333333");
//			setText_id_empty(num_of_keys, "2");
//			setText_id_empty(num_of_controllers, "3");
        }

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        inst = null;
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
//		caption.setText(MainMenuActivity.caption);
    }

    @Click
    void btn_clear() {
//		do_clear();
        showDialog(DLG_CLEAR); //do_clear
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DLG_CANCEL) {
            return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לצאת מהטופס?", new Runnable() {
                @Override
                public void run() {
                    do_cancel();
                }
            });
        } else if (id == DLG_CLEAR) {
            return Dialog2Btns.create(this, "האם אתה בטוח כי ברצונך לנקות את הטופס?", new Runnable() {
                @Override
                public void run() {
                    do_clear();
                }
            });
        } else {
            return super.onCreateDialog(id);
        }
    }

    @UiThread
    void do_cancel() {
        this.finish();
        ActivityUtils.close_all();
    }

    private void do_clear() {
        mileage.setText("");
//		car_code.setText("");
//		editText_neesaf_bafoall.setText("");
//		num_of_keys.setText("");
//		num_of_controllers.setText("");
        fuel_slide.clear();
    }

    private void populate_screen() {

//		GetCarDetails_Response_Inner dtls = GetCarDetails_Response.get_last_car_details();


        clear_textViews();

        license_no.setText(CarNumberValue.get());

//		SendForm_Request last_form = CarNumberActivity.lastForm;
//		if (last_form != null) {
//			populate_from_last_form(last_form);
//		}
//		else {
        populate_from_last_details_response();
//		}
    }
//		

    private void clear_textViews() {
        license_no.setText("");
        car_model.setText("");
//		company_name.setText("");
        license_expiration_date.setText("");
        insurance_expiration_date.setText("");
        contract_number.setText("");
        contract_type.setText("");
        customer_name.setText("");
        driver_name.setText("");
        mileage.setText("");
        fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);
    }

    private void populate_from_last_form(SendForm_Request last_form) {
        SendForm_Request_Inner inner = last_form.SendRentForm_req;
        if (inner == null) {
            return;
        }
        car_model.setText(inner.CarModel);
//		car_color.setText(inner.CarColor);
//		company_name.setText(TxtUtils.limit(12, inner.CompanyName));
//		license_expiration_date.setText(DateFormat2.format(_cur.DateLicense));
//		insurance_expiration_date.setText(DateFormat2.format(_cur.DateInsurance));
        license_expiration_date.setText(inner.DateLicense);
        insurance_expiration_date.setText(inner.DateInsurance);
    }

    private void populate_from_last_details_response() {
        GetCarDetails_Response details = GetCarDetails_Response.carDetails_response;
        if (details == null) {
            return;
        }
        GetCarDetails_Response_Inner _cur = details.GetRentCarDetails_res;
        if (_cur == null) {
            return;
        }

        car_model.setText(TxtUtils.limit(22, _cur.CarModel));
//		company_name.setText(TxtUtils.limit(12,_cur.CompanyName));
        license_expiration_date.setText(DateFormat2.format(_cur.LicenseDate));
        insurance_expiration_date.setText(DateFormat2.format(_cur.InsurenceDate));
//		contract_number.setText(DateFormat2.format(_cur.ContractNo));
        contract_number.setText("" + _cur.ContractNo + "/" + _cur.SubContract);
//		contract_type.setText(DateFormat2.format(_cur.ContractType));
        contract_type.setText("" + _cur.ContractType);
        customer_name.setText(_cur.CustomerName);

        driver_name.setText(_cur.DriverName);
//		mileage.setText(DateFormat2.format(???));
//		fuel_slide.set_fuel_level(FUEL_LEVEL_NOTSET);
//		int fuel_ind = _cur.FuelIndex;
//		fuel_slide.set_fuel_level(fuel_ind);
    }

    @Click
    void btn_send() {
//		CarCode = car_code.getText().toString().trim();
//		Keys = num_of_keys.getText().toString().trim();
//		Controls = num_of_controllers.getText().toString().trim();
//		Mileage = mileage.getText().toString().trim();
//		CollectedCar = editText_neesaf_bafoall.getText().toString().trim();
//		String ncontrols = num_of_controllers.getText().toString().trim();
//		String nkeys = num_of_keys.getText().toString().trim();


        fuel_level = fuel_slide.get_fuel_level();
        txt_mileage = mileage.getText().toString();

        //		if (!_DEBUG) {
        if (txt_mileage.isEmpty()) {
            toast("יש להזין ערך מד אוץ");
            return;
        }
        if (txt_mileage.length() > 6) {
            toast(" קילומטראז לא חוקי");
            return;
        }
//		if (!DeviceAndroidId.is_lab_device(this)) {
//		    if (!GetCarDetails_Response.mad_utz_ok(txt_mileage)) {
//		    	toast(GetCarDetails_Response.error_msg());
//		    	return;
//		    }
//		}
        if (fuel_level < 0) {
            toast("יש לציין ערך דלק");
            return;
        }
        //
        if (!fuel_was_alerted || (fuel_was_alerted && fuel_alert_stop_mode)) {
            FuelAlert f_alert;
            if ((f_alert = GetCarDetails_Response.fuel_alert_needed(fuel_level)) != null) {
                toast(f_alert.errmsg);
                fuel_was_alerted = true;
                fuel_alert_stop_mode = f_alert.stop;
                return;
            }
        }


        if (DeviceAndroidId.is_lab_device(this)) {
            perform_actual_action();
        } else {
            DialogOkCancel.handle_mad_utz(this, txt_mileage, mileage, new Runnable() {
                @Override
                public void run() {
                    perform_actual_action();
                }
            });
//			if (!GetCarDetails_Response.mad_utz_ok(txt_mileage)) {
//				toast(GetCarDetails_Response.error_msg());
//				return;
//			}
        }


    }

    private void perform_actual_action() {
        MyApp.appSetting.setFuel(fuel_level);
        MyApp.appSetting.setKm(txt_mileage);
        RejectListActivity_.intent(this).start();

    }

    @Click
    void btn_cancel() {
        if (isTheLastScreen) {
            showDialog();
        } else {
            showDialog(DLG_CANCEL); //do_clear
        }
//		this.finish();
    }


    @Override
    public void onBackPressed() {

        if (isTheLastScreen) {
            showDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void showDialog() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.are_you_sure))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(FuelmesirraHachzarraActivity.this, LoginActivity_.class));
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
