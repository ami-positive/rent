package com.sixt.android.activities.customerYard.network.response;

import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

/**
 * Created by natiapplications on 04/10/15.
 */
public class GetCompaniesResponse extends BaseResponseObject {

    @SerializedName("GetCompanies_res")
    private GetCompaniesInner getCompaniesInner;

    public GetCompaniesInner getGetCompaniesInner() {
        return getCompaniesInner;
    }

    public void setGetCompaniesInner(GetCompaniesInner getCompaniesInner) {
        this.getCompaniesInner = getCompaniesInner;
    }

    public class GetCompaniesInner {
        @SerializedName("Companies")
        private ArrayList<Company> companies;

        public ArrayList<Company> getCompanies() {
            return companies;
        }

        public void setCompanies(ArrayList<Company> companies) {
            this.companies = companies;
        }
    }

    public class Company {}
}
