/**
 * 
 */
package com.sixt.android.activities.customerYard.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sixt.rent.R;


/**
 * @author natiapplications
 *
 */
public class CreditGuardDialog extends BaseDialogFragment implements OnClickListener{

	public static final String DIALOG_NAME = "CreditGuardDialog";
	private onLinkChanges onLinkChanges;


	private String url;

	public WebView webView;
	public Button btnDone;
	public ProgressBar progressBar;



	private DialogCallback callback;
	private String cardNum;
	private TextView debugText;


	public CreditGuardDialog(){}

	public void setParams (String cardNum, String url,DialogCallback callback, onLinkChanges onLinkChanges){
		this.url = url;
		this.cardNum = cardNum;
		this.callback = callback;
		this.onLinkChanges = onLinkChanges;
	}

	public void setParams (String cardNum, String url, onLinkChanges onLinkChanges){
		this.url = url;
		this.cardNum = cardNum;
		this.onLinkChanges = onLinkChanges;
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_credit_gaurd, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
		btnDone = (Button)view.findViewById(R.id.btnDone);
		webView = (WebView)view.findViewById(R.id.webView);
		debugText = (TextView)view.findViewById(R.id.debugText);

		Button cancelDialog = (Button) view.findViewById(R.id.cancel_credit_dialog);

		cancelDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(this != null) {

					dismissDialog();
				}
			}
		});

		setupWebView();
        btnDone.setOnClickListener(this);

        return view;
    }

	private void setupWebView() {

		webView.getSettings().setBuiltInZoomControls(true);
		webView.requestFocus();
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.setWebViewClient(new MyBrowser());

		//webView.setWebChromeClient(new MyWebChromeClient());
		webView.loadUrl(url);
	}


	private void dismissDialog(){
    	
    	this.dismiss();
    }

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if (callback != null){
			callback.onDismiss(this,null);
		}
	}

	@Override
	public void onClick(View v) {
				
    	dismissDialog();

		if (v.getId() == R.id.btnDone){

			if(callback != null){
				callback.onDialogButtonPressed(v.getId(), this);
			}
		}
	}



	int counter = 0;
	private class MyBrowser extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			webView.loadUrl("javascript:(function(){document.getElementById('Track2CardNo').value='" + cardNum + "';})()");
			webView.loadUrl("javascript:(function(){document.getElementById('submitBtn').click();})()");
			progressBar.setVisibility(View.GONE);

			if(debugText != null){
				debugText.setText("" + url);
			}

			onLinkChanges.linkChanges(CreditGuardDialog.this, url);

		}
	}


	public  interface onLinkChanges{
		public void linkChanges(CreditGuardDialog dialog, String url);
	}


	@Override
	public void onStart() {
		super.onStart();

		Dialog dialog = getDialog();
		if (dialog != null) {
			dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		}
	}
	
}

