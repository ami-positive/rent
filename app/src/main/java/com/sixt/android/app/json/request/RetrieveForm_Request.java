package com.sixt.android.app.json.request;

import com.sixt.android.app.json.response.RetrieveForm_Response;

public class RetrieveForm_Request {
	final RetrieveForm_Request_Inner RetrieveForm_req;

	public RetrieveForm_Request() {
		RetrieveForm_Response.localPdfFile = null;
		RetrieveForm_req = new RetrieveForm_Request_Inner();
	}

}
