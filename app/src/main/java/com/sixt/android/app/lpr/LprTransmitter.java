package com.sixt.android.app.lpr;

import java.io.File;
import java.io.FileInputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.Environment;

import com.sixt.android.httpClient.JsonTransmitter;

public class LprTransmitter {

	private static final int LPR_CONNECTION_TIMEOUT = 55000;
	private static final int LPR_SOCKET_TIMEOUT = 55000;
	public static String stat_debug_result = "";


	private LprTransmitter() {} 


//	public static void perform_lpr(final Context c, final File file) {
//		new Thread() {
//			public void run() {
//				perform_lpr_blocking(c, file);
//			};
//		}.start();
//	}
//
	
	public static class LprRes {
		public String number;
		public String errMsg;
	}

	public static LprRes perform_lpr_blocking(Context c, File file) {
		LprRes res = new LprRes();
		stat_debug_result = "כניסה..";
		try {
            HttpParams connParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(connParams, LPR_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(connParams, LPR_SOCKET_TIMEOUT);

			HttpClient httpclient = new DefaultHttpClient(connParams); 

			HttpPost httppost = new HttpPost(JsonTransmitter.LPR_URL);

			// InputStream inputStream = c.getResources().openRawResource(R.raw.debug_lpr1);
			// File file = get_from_sdcard();
			FileInputStream fis = new FileInputStream(file); 
			
			InputStreamEntity reqEntity = new InputStreamEntity(fis, -1);
			reqEntity.setContentType("binary/octet-stream");
			reqEntity.setChunked(true); // Send in multiple parts if needed
			httppost.setEntity(reqEntity);
			
			long fsize = file.length();
			
			HttpResponse response = httpclient.execute(httppost);					

			String result = EntityUtils.toString(response.getEntity());
			if (result==null) {
				stat_debug_result = "null result returned"; 
				on_error();
				res.number = null;
				res.errMsg = "חזר ערך ריק!";
				return res;
			}
			result = result.trim();

			if (result.length() != 7 || !is_numeric(result)) { // e.g. "5356933"
				// error
				stat_debug_result = "bad result: " + result;
				on_error();
				res.number = null;
				res.errMsg = "תשובה מהשרת - " + result;
				return res;
			}
 
			on_success(result);
			stat_debug_result = "הצלחה!";
			res.number = result;
			res.errMsg = null;
			return res;

		} 
		catch (Exception e) {
			on_error();
			stat_debug_result = "Exceptione: " + e;
			res.number = null;
			res.errMsg = "תקלת חיבור - " + e;
			return res;
		}

	}


	private static boolean is_numeric(String str) {
		char[] chars = str.toCharArray();
		for (char c : chars) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}	



	protected static void on_success(String result) {
		// TODO Auto-generated method stub

	}


	private static void on_error() {
		// TODO Auto-generated method stub

	}


	private static File get_from_sdcard() {
		File sdcard = Environment.getExternalStorageDirectory();
		if (!sdcard.exists()) {
			int jj=234;
			jj++;
			return null;
		}


		int jjj=3242;
		jjj++; 

		for (File f: sdcard.listFiles()) {
			if (!f.isDirectory() && f.length() > 4000) {
				String n = f.getName().toLowerCase(); 
				if (n.endsWith("jpg") || n.endsWith("jpeg") || n.endsWith("png")) {
					return f;
				}
			}
		}
		return null;
	}


	private static File get_last_from_sdcard() {
		// TODO Auto-generated method stub
		return null;
	}

}
