//package com.sixt.android.app.util;
//
//import android.content.Context;
//import android.location.Location;
//import android.location.LocationManager;
//
//public class MyLocation {
//	private MyLocation() {}
//	
//
//	public static String getLatitude(Context c) {
//		Location last = get_last_location(c);
//		if (last != null) {
//			return last.getLatitude() + "";
//		}
//		return "";
//	}
//
//	public static String getLongitude(Context c) {
//		Location last = get_last_location(c);
//		if (last != null) {
//			return last.getLongitude() + "";
//		}
//		return "";
//	}
//	
//	private static Location get_last_location(Context c) {
//		LocationManager lm = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
//		if (lm==null) {
//			return null;
//		}
//		Location gps = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//		Location net = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//		if (gps==null) {
//			return net;
//		}
//		else if (net ==null) {
//			return gps;
//		}
//		// return newer fix
//		if (net.getTime() < gps.getTime()) {
//			return net; 
//		}
//		return gps;
//	}
//
//}
