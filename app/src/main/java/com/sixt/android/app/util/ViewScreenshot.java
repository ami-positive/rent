package com.sixt.android.app.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;

public class ViewScreenshot {
	private ViewScreenshot() {}

	public static Bitmap get(View view) {
		int w = view.getWidth();
		int h = view.getHeight();
		Bitmap bmp = Bitmap.createBitmap(w, h,Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bmp);
		canvas.drawColor(Color.WHITE);
		Drawable bgDrawable = view.getBackground();
		if (bgDrawable!=null)  {
			bgDrawable.draw(canvas);
		}
		else {
			canvas.drawColor(Color.WHITE);
		}
		view.draw(canvas);
		return bmp;
	}


}
