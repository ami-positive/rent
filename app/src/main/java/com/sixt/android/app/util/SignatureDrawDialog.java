package com.sixt.android.app.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sixt.android.activities.ISignatureActivity;
import com.sixt.android.activities.customerYard.utils.BitmapUtil;
import com.sixt.android.activities.customerYard.utils.SignatureDialogListener;
import com.sixt.android.ui.SignatureView;
import com.sixt.rent.R;

public class SignatureDrawDialog {
	private SignatureDrawDialog() {}
	
	// see dimension size width height in R.layout.signature_layout
	
	private static SignatureView signatureView;
	private static String orig_text;
	private static ISignatureActivity caller;
	private static TextView inner_txt;
	private static ImageView signature_img;
	private static boolean was_set = false;
	
	public static AlertDialog create(ISignatureActivity _caller, 
			final TextView _inner_txt, final ImageView _signature_img) {
		was_set = false;
		inner_txt = _inner_txt;
		signature_img = _signature_img;		
		caller = _caller;
		Activity caller_activity = (Activity)caller;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(caller_activity);
		
		orig_text = inner_txt.getText().toString();

		LinearLayout lt = (LinearLayout) caller_activity.getLayoutInflater().inflate(R.layout.signature_layout, null);
		Button ok = (Button) lt.findViewById(R.id.sig_ok);
		Button clear = (Button) lt.findViewById(R.id.sig_clear);
		Button cancel = (Button) lt.findViewById(R.id.sig_cancel);

		signatureView = (SignatureView) lt.findViewById(R.id.signatureZone);


		alertDialogBuilder
		.setView(lt)
		.setCancelable(false);

		final AlertDialog dlg = alertDialogBuilder.create();

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				was_set = true;
				inner_txt.setVisibility(View.INVISIBLE);
				signature_img.setImageBitmap(BitmapUtil.takeScreenShot(signature_img));
				signature_img.invalidate();
				dlg.cancel();
				signatureView.clearSignature();
				caller.onSignatureSet();
			}
		});
		clear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clear_popup_signature();
				was_set = false;
			}
			
		});

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				signatureView.clearSignature();
				dlg.cancel();
			}
		});

		return dlg;
	}


	public static AlertDialog create(Activity activity, final SignatureDialogListener listener) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);


		LinearLayout lt = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.signature_layout, null);
		Button ok = (Button) lt.findViewById(R.id.sig_ok);
		Button clear = (Button) lt.findViewById(R.id.sig_clear);
		Button cancel = (Button) lt.findViewById(R.id.sig_cancel);
		final FrameLayout sigFrame = (FrameLayout)lt.findViewById(R.id.sig_frame);
		final  SignatureView signatureView = (SignatureView) lt.findViewById(R.id.signatureZone);

		alertDialogBuilder
				.setView(lt)
				.setCancelable(false);

		final AlertDialog dlg = alertDialogBuilder.create();

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				System.out.println("MONKEY1");
				Bitmap sing = ViewScreenshot.get(sigFrame);
				System.out.println("MONKEY2");
				dlg.cancel();
				System.out.println("MONKEY3");
				signatureView.clearSignature();
				System.out.println("MONKEY4");
				listener.onSign(sing);
			}
		});
		clear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				signatureView.clearSignature();
			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				signatureView.clearSignature();
				dlg.cancel();
			}
		});

		return dlg;
	}

	private static void clear_popup_signature() {
		signatureView.clearSignature();
	}	
	
	public static void clear_signature() {
		if (!was_set) {
			return;
		} 
		if (signatureView==null) {
			return;
		}
		signatureView.clearSignature();
		signature_img.setImageBitmap(ViewScreenshot.get(signatureView));
		signature_img.invalidate();
		
        if (inner_txt != null) {
        	inner_txt.setVisibility(View.VISIBLE);
        }
        if (caller != null) {
        	caller.onSignatureClear();
        }
	}
 

	public static void clear(TextView signatureText) {
		if (signatureView != null) {
			signatureView.clearSignature();
			signatureView = null;
		}
		if (orig_text != null) {
			signatureText.setText(orig_text);
		}
		
	}


}
