package com.sixt.android.app.json.response;

import com.sixt.android.app.json.Base_Response_JsonMsg;

public class SendMultiForm_Response extends Base_Response_JsonMsg {
	
	public static SendMultiForm_Response last_response;
	
	public String FormId; 
	
	@Override
	public void postLoad() {
		super.postLoad();
		last_response = this; 
	}
	
}	
 
