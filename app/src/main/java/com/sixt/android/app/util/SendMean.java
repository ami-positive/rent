package com.sixt.android.app.util;

public class SendMean {
	private SendMean() {}

	public static String get(boolean mode_email) {
		return mode_email ? "M" : "F";				
	}

}
