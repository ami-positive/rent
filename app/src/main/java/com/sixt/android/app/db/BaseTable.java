package com.sixt.android.app.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class BaseTable {

    protected void deleteRecords(String whereClause) {
        SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
        try {
            // all
            if (db != null) {
                int _break_pt = db.delete(getTableName(), whereClause, null);
            }
        } finally {
            if (db != null) db.close();
        }
    }

    
    public void delete_all_records() {
        SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
        try {
            // all
            if (db != null) {
                int _break_pt = db.delete(getTableName(), null, null);
            }
        } finally {
            if (db != null) db.close();
        }
    }


    protected abstract String getTableName();

    public abstract void createTable(SQLiteDatabase db);

    public abstract void dropTable(SQLiteDatabase db);

    
    
    protected final Cursor readAllTable(SQLiteDatabase db) {
        if (!db.isOpen()) {
            db = null;
            db = DatabaseManager.get().getWritableDatabase();
        }
        String selectQuery = "SELECT  * FROM " + getTableName();
        Cursor c = null;
        if (db != null) c = db.rawQuery(selectQuery, null);
        return c;
    }

    protected final void dropTable() {
        SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
        try {
            dropTable(db);
        } finally {
            if (db != null) db.close();
        }
    }

    public final int getRecordCount() {
        String countQuery = "SELECT  * FROM " + getTableName();
        SQLiteDatabase db = DatabaseManager.get().getReadableDatabase();
        try {
            Cursor cursor = null;
            if (db != null) cursor = db.rawQuery(countQuery, null);
            int count = 0;
            if (cursor != null) {
                count = cursor.getCount();
                cursor.close();
            }
            return count;
        } finally {
            if (db != null) db.close();
        }
    }

}
