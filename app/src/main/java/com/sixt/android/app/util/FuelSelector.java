//package com.sixt.android.app.util;
//
//import android.content.Context;
//import android.widget.ArrayAdapter;
//
//public class FuelSelector {
//	private FuelSelector() {}
//	
//	private static final String[] values = { 
//		"E" , 
//		"1/8", 
//		"1/4",  
//		"3/8", 
//		"1/2", 
//		"5/8", 
//		"3/4", 
//		"7/8", 
//		"F" , };
//
//	public static ArrayAdapter<String> get_adapter(Context context) {
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
//		        android.R.layout.simple_list_item_1, values);		
//		return adapter;
//	}
//
//}
