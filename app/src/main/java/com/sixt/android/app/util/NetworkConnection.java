package com.sixt.android.app.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {

	private NetworkConnection() {}

	public static boolean is_connected(Activity a) {
		boolean status = false;
		try{
			ConnectivityManager cm = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(0);
			if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
				status = true;
			}
			else {
				netInfo = cm.getNetworkInfo(1);
				if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
					status = true;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();  
			return false;
		}
		return status;	
	}

}
