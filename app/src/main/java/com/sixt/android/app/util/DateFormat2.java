package com.sixt.android.app.util;

public class DateFormat2 {
	private DateFormat2() {}

	public static String format(String date) {
		if (date==null || date.length() != 8) {
			return date;
		}
		String yr = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		String fmt = day + "/" + month + "/" + yr;
		return fmt;
	}

}
