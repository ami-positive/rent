package com.sixt.android.app.json.request;

import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.LoginSixt;

public class CheckCarExists_Request_Inner extends BaseJsonMsg {
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No();	
	public final String CarNO;// = CarNumberActivity.number; //"11-111-11"

	public CheckCarExists_Request_Inner(String _CarNO) {
		super(CheckCarExistsRequest);
		CarNO = CarNumberValue.remove_dashes(_CarNO); 
	}
}
