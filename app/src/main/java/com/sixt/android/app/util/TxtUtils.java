package com.sixt.android.app.util;

public class TxtUtils {

	public static String limit(int max, String name) {
		if (name==null || name.length() < max) {
			return name;
		}
		name = name.substring(0, max) + "...";
		return name; 
	}


}
