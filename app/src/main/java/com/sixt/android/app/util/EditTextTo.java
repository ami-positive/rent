package com.sixt.android.app.util;

import android.widget.EditText;

public class EditTextTo {
	private EditTextTo() {}

	public static String str(EditText editText) {
		return editText.getText().toString().trim();
	}

}
