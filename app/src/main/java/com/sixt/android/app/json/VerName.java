package com.sixt.android.app.json;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

public class VerName {
	public static String the_VersionName;
	
	public static void set_ver_code(Context c) {
		if (the_VersionName != null) {
			return;
		}
		try { 
			String name = c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName;
			the_VersionName = name; 
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			the_VersionName = "unknown";
		}	
	}
	
}
