package com.sixt.android.app.json.request;

import com.sixt.android.app.json.BaseJsonMsg;

public class LprCarNumber_Request_Inner extends BaseJsonMsg {	
	public final String request_id;
	public final String remote_filename;
	
	public LprCarNumber_Request_Inner(String request_id, String remote_filename) { 
		super(LprRequest); 
		this.request_id = request_id;
		this.remote_filename = remote_filename;
	}

}
