package com.sixt.android.app.util;

import com.sixt.android.activities.BranchSelectActivity;
import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.activities.CarNumberMischarriHachzarraActivity;
import com.sixt.android.activities.CarNumberMischarriMesirraActivity;
import com.sixt.android.activities.CarNumberPnimiPtichaActivity;
import com.sixt.android.activities.FuelPtichaActivity;
import com.sixt.android.activities.FuelSgirraActivity;
import com.sixt.android.activities.FuelmesirraHachzarraActivity;
import com.sixt.android.activities.HearotLehachzarraActivity;
import com.sixt.android.activities.ImageCarActivity;
import com.sixt.android.activities.KavuaChalufiActivity;
import com.sixt.android.activities.LakoachSapakSelectorActivity;
import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.activities.RejectListActivity;
import com.sixt.android.activities.SignatureActivity;
import com.sixt.android.activities.SubRejectsActivity;

public class ActivityUtils {
	private ActivityUtils() {}

	
	public static void close_all() {
		// not MainMenuActivity !!!


		LakoachSapakSelectorActivity.call_finish();
		FuelmesirraHachzarraActivity.call_finish();
		LakoachSapakSelectorActivity.call_finish();
		KavuaChalufiActivity.call_finish();
		CarNumberActivity.call_finish();
		SignatureActivity.call_finish();
		RejectListActivity.call_finish();
		BranchSelectActivity.call_finish();
		MenuMischarriActivity.call_finish();
		SubRejectsActivity.call_finish();
		CarNumberMischarriMesirraActivity.call_finish();
		CarNumberMischarriHachzarraActivity.call_finish();
		MenuPnimmiActivity.call_finish();
		CarNumberPnimiPtichaActivity.call_finish();
		FuelPtichaActivity.call_finish();
		FuelSgirraActivity.call_finish();
		HearotLehachzarraActivity.call_finish();
		CarNumberActivity.call_finish();
		KavuaChalufiActivity.call_finish();
		ImageCarActivity.call_finish();
		RejectListActivity.call_finish();
		FuelmesirraHachzarraActivity.call_finish();

	}

}
