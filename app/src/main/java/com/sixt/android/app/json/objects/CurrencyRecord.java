package com.sixt.android.app.json.objects;

import com.sixt.android.MyApp;
import com.sixt.android.activities.customerYard.storage.StorageListener;
import com.sixt.android.activities.customerYard.storage.StorageManager;

import java.io.Serializable;

/**
 * Created by natiapplications on 24/12/15.
 */
public class CurrencyRecord implements Serializable{

    public String CurrencyCode;
    public String CurrencyDesc;


    public void saveToStorage () {
        MyApp.storageManager.writeToStorage(StorageManager.FILE_SELECTED_CURRENCY,this);
    }

    public static void readFromStorage (StorageListener listener){
        MyApp.storageManager.readFromStorage(StorageManager.FILE_SELECTED_CURRENCY,listener);
    }


}
