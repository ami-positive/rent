package com.sixt.android.app.db;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sixt.android.activities.LoginActivity;
import com.sixt.android.app.util.AppContext;


public final class DatabaseManager extends SQLiteOpenHelper {

    // represents entire app's database

    private static final String DATABASE_NAME = "MyDB16";  
    private static final int DATABASE_VERSION = 12;   
    private volatile static DatabaseManager inst;

    private DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized void init(Context context) {
        //Assert.that(inst==null);
        if (inst == null) {
            inst = new DatabaseManager(context);
        }
    }
    
    public static void delete_entire_db() {
    	try { closeDatabaseConnection(); } catch(Exception e) {}
    	Context c = LoginActivity.inst; 
    	try { c.deleteDatabase(DATABASE_NAME); } catch(Exception e) {}
//        db.delete("TABLE_NAME", null, null);
    }
    

    public static void closeDatabaseConnection() {
        // start with tables
        try {
            JsonsTable.close();
        } catch (Exception e) {
        }

        synchronized (DatabaseManager.class) {
            if (inst != null) {
                try {
                    inst.close();
                } catch (Exception e) {
                }
                inst = null;
            }
        }
    }

    
    public static DatabaseManager get() {
        init(AppContext.appContext);
        return inst;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        JsonsTable.get().createTable(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // remove old ver
        JsonsTable.get().dropTable(db);
        // and recreate
        onCreate(db);

        // (or "ALTER TABLE" so not to loose data)
    }

    
      
	public static void write_form_to_db(String json, String unique_id, int has_ref, ArrayList<String> file_arr, Context c) {
		init(c.getApplicationContext());
		JsonsTable.get().add_unsent_json(json, unique_id, has_ref, file_arr);
		
	}


}

