package com.sixt.android.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.rent.R;

public class DialogOkCancel {
	
	private DialogOkCancel() {}
	
	public static void handle_mad_utz(Activity caller, String lineage_value, final EditText mileage, final Runnable ok_handler) {
		final String LARGER_MSG  = "הוזן מד אוץ גבוה מהמשוער";
		final String SMALLER_MSG = "הוזן מד אוץ נמוך מהידוע";
		
		String msg;
		
		if (GetCarDetails_Response.mad_utz_too_large(lineage_value)) {
			msg = LARGER_MSG;
		}
		else if (GetCarDetails_Response.mad_utz_too_small(lineage_value)) {
			msg = SMALLER_MSG;
		}
		else {
			//ok
			ok_handler.run();
			return;
		}
		
		final Dialog dialog = new Dialog(caller); 
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_2_btns); 
		Button cancel = (Button)dialog.findViewById(R.id.btn_cancel);
		Button ok = (Button)dialog.findViewById(R.id.btn_confirm);
	    TextView txt = (TextView)dialog.findViewById(R.id.textView1);
	    
	    cancel.setText("בטל");
	    ok.setText("המשך");		
	    
	    txt.setText(msg);


		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				// cancel handler
				mileage.post(new Runnable() {					
					@Override
					public void run() {
						mileage.setText("");						
					}
				});				
			}
		});

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (ok_handler != null) {
					ok_handler.run();
				}
			}
		});
				
		dialog.show();
	}
	

	public static void old__handle_mad_utz(Activity caller, String lineage_value, final EditText mileage, Runnable ok_handler) {
		
		final String LARGER_MSG  = "הוזן מד אוץ גבוה מהמשוער";
		final String SMALLER_MSG = "הוזן מד אוץ נמוך מהידוע";
		
		String msg;
		
		if (GetCarDetails_Response.mad_utz_too_large(lineage_value)) {
			msg = LARGER_MSG;
		}
		else if (GetCarDetails_Response.mad_utz_too_small(lineage_value)) {
			msg = SMALLER_MSG;
		}
		else {
			//ok
			ok_handler.run();
			return;
		}
		
		show(caller, msg, ok_handler, new Runnable() {			
			@Override
			public void run() {
				// cancel handler
				mileage.post(new Runnable() {					
					@Override
					public void run() {
						mileage.setText("");						
					}
				});				
			}
		});
	}

	
	public static void show(Context c, String msg, 
			final Runnable ok_callback, final Runnable cancel_callback) {
		final Dialog dialog = new Dialog(c); 
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_2_btn_sixt); 
		Button cancel = (Button)dialog.findViewById(R.id.btn_cancel);
		Button ok = (Button)dialog.findViewById(R.id.btn_confirm);
	    TextView txt = (TextView)dialog.findViewById(R.id.textView1);
	    
	    txt.setText(msg);


		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (cancel_callback != null) {
					cancel_callback.run();
				}
			}
		});

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (ok_callback != null) {
					ok_callback.run();
				}
			}
		});
		dialog.show();
	}


}
