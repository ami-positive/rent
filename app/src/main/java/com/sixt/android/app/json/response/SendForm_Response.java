package com.sixt.android.app.json.response;

import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.request.SendForm_Request_Inner;

public class SendForm_Response extends Base_Response_JsonMsg {
	
	public static SendForm_Response last_response;
	
	public SendForm_Response_Inner SendRentForm_res;	
	
	@Override
	public void postLoad() {
		super.postLoad();
		last_response = this;
		SendForm_Request_Inner.delete_images(); // sendForm succeeded - delete image files
	}
	
	public String getSuccessMsg() {
		if (SendRentForm_res==null || SendRentForm_res.successMsg==null) {
			return null;
		}
		String msg = SendRentForm_res.successMsg.trim();
		if (msg.length()==0) {
			return null;
		}
		return msg;
	}
	
}	
 
