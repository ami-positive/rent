package com.sixt.android.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.rent.R;

public class Dialog2Btns {
	private Dialog2Btns() {}
	
	public static Dialog create(Context c, String msg, final Runnable ok_callback) {
		final Dialog dialog = new Dialog(c); 
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_2_btns); 
		Button cancel = (Button)dialog.findViewById(R.id.btn_cancel);
		Button ok = (Button)dialog.findViewById(R.id.btn_confirm);
	    TextView txt = (TextView)dialog.findViewById(R.id.textView1);
	    
	    txt.setText(msg);


		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				ok_callback.run(); 
			}
		});
		return dialog;
	}


	
}
