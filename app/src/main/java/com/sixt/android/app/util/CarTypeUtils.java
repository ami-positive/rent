package com.sixt.android.app.util;

public class CarTypeUtils {
	private CarTypeUtils() {}

	public static boolean is_p_or_pm(String carType) {
		if (carType==null || carType.length()==0) {
			return true;
		}
		carType = carType.toUpperCase();		
		return carType.contains("P");
	}
	
	public static boolean is_m_or_pm(String carType) {
		if (carType==null || carType.length()==0) {
			return true;
		}
		carType = carType.toUpperCase();		
		return carType.contains("M");
	}


}
