package com.sixt.android.app.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.sixt.android.activities.toast.MyToast;
import com.sixt.rent.R;

public class PdfViewer {

	private static final String DEBUG_PDF_FILE_ON_SDCARD = "/mnt/sdcard/dada23424.pdf";


	private PdfViewer() {}



	public static void open(File pdfDoc, Activity caller) {
		//	File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/example.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(pdfDoc), "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		try { 
			caller.startActivity(intent);
		}
		catch (Exception e) {
			MyToast.makeText(caller, "לא ניתן להציג את המסמך", -1);
		}

		//		Intent chooserIntent = Intent.createChooser(intent, "Open File");
		//		try {
		//		    c.startActivity(chooserIntent);
		//		} catch (ActivityNotFoundException e) {
		//		    // Instruct the user to install a PDF reader here, or something
		//		}   
	}

	private static int debug_ctr;
	
	public static File copy_to_sdcard_if_needed(Context c) {
		if (!DeviceAndroidId.is_lab_device(c) && !DeviceAndroidId.is_sharon_device(c)) {
			return null;
		}
		
		final File outFile = new File(DEBUG_PDF_FILE_ON_SDCARD);
		if (outFile.exists() && outFile.length() > 10) {
			return outFile;
		}

		InputStream in = c.getResources().openRawResource(R.raw.prior_form);
		FileOutputStream out;
		try {
			out = new FileOutputStream(outFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
		byte[] buff = new byte[10*1024];
		int read = 0;
		debug_ctr = 0;
		try {
			try {
				while ((read = in.read(buff)) > 0) {
					out.write(buff, 0, read);
					debug_ctr += read;
				}
				int jj=234; 
				jj++;
				return outFile;
			}			
			catch (IOException e) {
				IOException xx = e;
				int ttt = debug_ctr; 
				int JJ=234;
				JJ++;
				e.printStackTrace();
				return null;
			}
		} finally {
			try { in.close(); } catch (Exception e) {}
			try { out.close(); } catch (Exception e) {}
		}		
	}

}
