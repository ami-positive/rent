package com.sixt.android.app.util;

import java.util.UUID;

public class FileUtils {
	
	private FileUtils() {}
	
	public static String get_randon_uuid() {
		return "" + System.currentTimeMillis();
	}
	
	public static String get_randon_filename(String pref, String file_ext) { // e.g. ".jpeg"
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "_");
		String filename = pref + "_" + uuid + file_ext;
		return filename;
	}
	
}
