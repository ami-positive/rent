package com.sixt.android.app.util;

public class JsonDebug {
	
	public static final String DEBUG_FILENAME = "GILAD.txt";

	private JsonDebug() {}
	
	private static volatile String request_str = "";
	private static volatile String response_str = "";
	
	public static void clear() {
		request_str = "";
		response_str = "";		
	}
	
	public static void set_request(String request) {
		request_str = request;
		response_str = "";		
	}

	public static void set_response(String response) {
		response_str = response;		
	}
	
	public static String get_request() {
		return request_str ;
	}

	public static String get_response() {
		return response_str; 
	}

}
