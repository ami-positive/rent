package com.sixt.android.app.json.request;

import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.CurrentSixtApp;

public class Authentication_Request_Inner extends BaseJsonMsg {
	
	public final String App = CurrentSixtApp.type();
	public final String Login; 
	public final String ADLogin;
//	public final String Debug = "חג 'שמח' לגבי ולשרון " + "\"" + "ולכל " + "\"" + "הצוות" + "\n" + "בלבלב";
	
	public Authentication_Request_Inner(String username, String pwd) { 
		super(AuthenticationRequest); 
		Login = pwd; 
		ADLogin = username; 				
	}
}
