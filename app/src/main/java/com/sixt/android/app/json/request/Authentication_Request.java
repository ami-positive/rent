package com.sixt.android.app.json.request;

import com.sixt.android.app.util.LoginSixt;

public class Authentication_Request {	
	final Authentication_Request_Inner AuthenticationRent_req; 
	
	public Authentication_Request(String username, String pwd) {
		LoginSixt.set_password(pwd);
		AuthenticationRent_req = new Authentication_Request_Inner(username, pwd);
	}
}

