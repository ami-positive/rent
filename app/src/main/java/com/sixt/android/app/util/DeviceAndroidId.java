package com.sixt.android.app.util;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import com.sixt.android.MyApp;

public class DeviceAndroidId {
	
	private static String device_id; 
	 
	private static final String SHARON_DEVICE = "f9401ab4af0a6292"; //"97290bc9b8437c71";
	private static final String GABI_DEVICE = "a2c65b71b873f032"; // gabi gaby

	
	private DeviceAndroidId() {}
	
	private static String DeviceId;
	private static String SimSerial;
	
	
	public static String get_Sim_serial() {
		if (SimSerial==null) {	
			Context c = AppContext.appContext;
			if(c == null){
				c = MyApp.appContext;
			}
			TelephonyManager telemamanger = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
			SimSerial = telemamanger.getSimSerialNumber();
//			String getSimNumber = telemamanger.getLine1Number();
		}
		return SimSerial;
	}
	
	public static String get_imei() {
		if (DeviceId==null) {
			Context c = AppContext.appContext;
			if(c == null){
				c = MyApp.appContext;
			}
			System.out.println("DeviceId - Context " + c);
			TelephonyManager telephonyManager = (TelephonyManager)c.getSystemService(Context.TELEPHONY_SERVICE);
			System.out.println("DeviceId - telephonyManager " + telephonyManager);
			DeviceId = telephonyManager.getDeviceId();
			System.out.println("DeviceId -  " + DeviceId);
		}
		return DeviceId;
	}
	
	public static boolean is_sharon_device(Context c) {
    	String id = getId(c);    	
    	return id.equals(SHARON_DEVICE) || id.equals(GABI_DEVICE);   
	}
	 
	public static boolean is_lab_device(Context c) { 
    	String id = getId(c);    	
    	if (id.equals("46baa06727f7909d") || 
    		id.equals("82df159248622650") ||
    		id.equals("b0c1f2839a08854f") ||
    		id.equals("113366839e66ac5b") ||
    		id.equals("4eff728b7f2aeb32") || 
    		id.equals("7dd788308c0218c")  ||
    		id.equals("797339c5ca269575") || // yaniv 
    		id.equals("a5f8edac1f39150d")) {  //gilad 
    		return true; 
    	}
		return false; 
	}


	public static String getId(Context c) {
		if (device_id == null) {
			device_id = Secure.getString(c.getContentResolver(),Secure.ANDROID_ID);
		}
    	return device_id;
	}

}
