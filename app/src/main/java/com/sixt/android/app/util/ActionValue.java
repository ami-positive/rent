package com.sixt.android.app.util;


public class ActionValue {
	
	private ActionValue() {}
	
	public static enum Values {
		Knissa_Lemigrash("I"),
		Yetzia_Memigrash("O"),
		Isuf_Rechev("C"),
		Mesirrat_Rechev("D"), 
		Hachzarra("R"), 
		Sgirra("C"), //  
		Pticha("O");
		
		public final String value;
		
		private Values(String val) { 
			this.value = val; 
		}
	};

	private static volatile Values _cur = Values.Knissa_Lemigrash;
	
	
	public static void set(Values newVal) {  
		_cur = newVal;
		
	}
	
	public static Values getObj() {
		return _cur;
	}
	
	
	public static String get() {
		return _cur.value;
//		Knissa_Lemigrash("I"),
//		Yetzia_Memigrash("O"),
//		Isuf_Rechev("C"),
//		Mesirrat_Rechev("D");
	}

	public static boolean Yetzia_Memigrash() {
		return _cur == Values.Yetzia_Memigrash; 
	}

	public static boolean Knissa_Lemigrash() {
		return _cur == Values.Knissa_Lemigrash;
	}
	
	public static boolean Isuff() {
		return _cur == Values.Isuf_Rechev;
	}

	public static boolean Sgirra() {
		return _cur == Values.Sgirra;
	}

	public static boolean Messira() {
		return _cur == Values.Mesirrat_Rechev;
	}

	public static boolean should_pass_image() {
//		return _cur == Values.Isuf_Rechev || _cur == Values.Mesirrat_Rechev;
		return true;
	}

	public static boolean Hachzarra() {
		return _cur == Values.Hachzarra;
	}

	public static boolean Pticha() {
		return _cur == Values.Pticha;
	}



}
