package com.sixt.android.app.util;

public class ActionFailedError extends Exception {

	private static final long serialVersionUID = 1L;

	public static final int IS_RECOVERABLE = 99;
	public static final int NOT_RECOVERABLE = IS_RECOVERABLE+1;

	private final int ReasonCode;
	private String ReasonDesc;
	// code==1 missing mandatory fields
	// code==2 illegal format (app = T/M/L)
	
	@Override
	public String toString() {
		return "ActionFailedError desc=" + ReasonDesc + ",  code=" + ReasonCode;  
	}
	
	public ActionFailedError(int c, String d) { 
		ReasonCode = c;
		ReasonDesc = d;		
	}
	
	public int getReasonCode() {
		return ReasonCode;
	}

	public String getReasonDesc() { 
		return "<" + ReasonDesc + ">";
	}
	
	public void setReasonDesc(String desc) { 
		ReasonDesc = desc;
	}

	public boolean is_recoverable_error() {
		return ReasonCode == IS_RECOVERABLE; // if 99 save and transmit 
//		return !(ReasonCode==1 || ReasonCode==2 || 
//			   ReasonCode==4 || ReasonCode==5);
	}


}
