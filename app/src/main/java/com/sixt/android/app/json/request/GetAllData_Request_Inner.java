package com.sixt.android.app.json.request;

import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.LoginSixt;

public class GetAllData_Request_Inner extends BaseJsonMsg {
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No();
//	public final String CarNO;
//	public final String Action;
	
	
	public GetAllData_Request_Inner() {
		super(Get_All_Data_Request);
	}
	
}