package com.sixt.android.app.json;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.json.response.CheckCarExists_Response;
import com.sixt.android.app.json.response.GetAllData_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.json.response.GetRejects_Response;
import com.sixt.android.app.json.response.RetrieveForm_Response;
import com.sixt.android.app.json.response.SendForm_Response;
import com.sixt.android.app.json.response.SendMultiForm_Response;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.app.util.GGson;

public class Base_Response_JsonMsg extends BaseJsonMsg implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 2265996278579091931L;


	// ctor
	public Base_Response_JsonMsg(String type) {		
		msg_type = type;
	}
 
	// def ctor
	public Base_Response_JsonMsg() {
	}



	public static Base_Response_JsonMsg load(String json) throws ActionFailedError {
		org.json.JSONObject j_obj;
		try { 
			j_obj = new org.json.JSONObject(json);
		} catch (JSONException e1) { 
			e1.printStackTrace();
			throw new RuntimeException();
		} 
 

		if (action_has_failed(j_obj)) { //= Succeeded
			int ReasonCode = getReasonCode(j_obj);
			String ReasonDesc = getReasonDesc(j_obj);
			throw new ActionFailedError(ReasonCode, ReasonDesc);
		}
		
		String type = get_msg_type(j_obj);   
		String xx = type;
		Object obj = null;
		if (type.equals(AuthenticationResponse)) { 
			obj = GGson.fromJson(json, Authentication_Response.class);
		}
		else if (type.equals(GetRejectsResponse)) {
			obj = GGson.fromJson(json, GetRejects_Response.class);
		}
		else if (type.equals(GetCarDetailsResponse)) {
			obj = GGson.fromJson(json, GetCarDetails_Response.class); 
		}
		else if (type.equals(SendFormResponse)) { 
			obj = GGson.fromJson(json, SendForm_Response.class); 
		}
		else if (type.equals(CheckCarExistsResponse)) {  
			obj = GGson.fromJson(json, CheckCarExists_Response.class); 
		}
		else if (type.equals(SendMultiFormResponse)) {  
			obj = GGson.fromJson(json, SendMultiForm_Response.class); 
		}
		else if (type.equals(RetrieveFormResponse)) {  
			obj = GGson.fromJson(json, RetrieveForm_Response.class);  
		}
		else if (type.equals(Get_All_Data_Response)) {  
			obj = GGson.fromJson(json, GetAllData_Response.class);   
		}
		else {
			int jj=234;
			jj++;
			throw new RuntimeException("Unknown msg type: " + type);
		} 
		Base_Response_JsonMsg msg = (Base_Response_JsonMsg) obj;
		return msg;		
	} 


	private static int getReasonCode(JSONObject j_obj) {
		int ReasonCode;
		try {
			ReasonCode = j_obj.getInt("ReasonCode");
		} catch (JSONException e) {
			e.printStackTrace();
//			throw new RuntimeException();
			return 0;
		}

		return ReasonCode;
	}

	
	private static String getReasonDesc(JSONObject j_obj) {
		String ReasonDesc;
		try {
			ReasonDesc = j_obj.getString("ReasonDesc");
		} catch (JSONException e) {
			e.printStackTrace();
//			throw new RuntimeException();
			return "";
		}
		return ReasonDesc;
	}

	private static boolean action_has_failed(JSONObject j_obj) {
		int Success;
		try {
			Success = j_obj.getInt("Success");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		if (Success == 0) {
			return true;
		}
		return false;
	}

	private static String get_msg_type(org.json.JSONObject j_obj) {
		String type;
		try {
			type = j_obj.getString("msg_type");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		if (type == null | type.length()==0) {
			throw new RuntimeException();
		}		
		return type;
	}


	public void postLoad() {

	}


}
