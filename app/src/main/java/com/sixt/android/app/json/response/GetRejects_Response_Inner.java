package com.sixt.android.app.json.response;

import java.util.ArrayList;

import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.MandatoryImgRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;

public class GetRejects_Response_Inner extends ResponseHeader {

	public GetRejects_Reject[] Rejects;
	public MandatoryImgRecord[] MandatoryImg; //only for image >>  all values here MUST BE OK USED ONLY IN MESSIRA (pnimmi + lakoach);

	public GetRejects_Reject[] get_Rejects(boolean car_is_private) {
		return get_Rejects(car_is_private, false);
	}
	
	public void postLoad() {
		if (Rejects==null) {
			return;
		}
		for (GetRejects_Reject r: Rejects) {
			r.init_value_for_reject_and_subs_according_to_default_flag();
		}
		
	}


	public GetRejects_Reject[] get_Rejects(boolean car_is_private, boolean filter_RD) {
		if (Rejects==null) {
			int jj=234;
			jj++;
			return null;
		}

		int car_reject_val;

		ArrayList<GetRejects_Reject> filtered = new ArrayList<GetRejects_Reject>(); 
		for (GetRejects_Reject elem: Rejects) {
			String code = elem.RejectCode;
			
			if (elem.SubReject != null && elem.SubReject.length > 0) {
				int jj=345;
				jj++;
			}
			
			if ((car_reject_val=is_car_reject(code)) > -1) { // appearing in GetCarDetails.Rejects
				elem.value = car_reject_val;
				filtered.add(GetRejects_Reject.filter(elem));
				continue;
			}
			
			if (code.equals("FRONTRUG") || code.equals("GF") ||   // debug code
					code.equals("INSURANCE") || code.equals("TF")) {
				int jj=234;
				jj++;
				jj--;
			}
			
			if (car_is_private) {
				if (elem.is_p_or_pm() && was_filtered(filter_RD, elem)) {
					filtered.add(GetRejects_Reject.filter(elem));
				}
			}
			else {
				if (elem.is_m_or_pm() && was_filtered(filter_RD, elem)) {
					filtered.add(GetRejects_Reject.filter(elem));
				}

			}
		}

		GetRejects_Reject[] res = filtered.toArray(new GetRejects_Reject[filtered.size()]);
		return res;

	}

	
	private static int is_car_reject(String code) {
		if (code==null) {
			return -1;
		}
		RejectRecord[] car_rejects = GetCarDetails_Response.get_Rejects();
		if (car_rejects==null) {
			return -1;
		}
		for (RejectRecord creject: car_rejects) {
			if (code.equals(creject.Code)) {
				int val = creject.Value;
				return val;
			}
		}		
		return -1;
	}
	
	
	private static boolean was_filtered(boolean filter_RD, GetRejects_Reject elem) {
		if (!filter_RD) {
			return true;
		}
		return elem.should_show();
//		String typ = elem.RejectType.toUpperCase();
//		boolean is_hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();
//		if (is_hachzarra) {
//			boolean ok = typ.contains("R"); // return
//			int jj = 345;
//			jj++;
//			return ok;
//		}
//		else {
//			boolean ok = typ.contains("D"); //delivery
//			int jj = 345;
//			jj++;
//			return ok;
//		}
	}

	private static String debug_all_codes;

	public void debug_print() { 
		if (Rejects==null) {
			return;
		}
		
		if (debug_all_codes == null) {
			debug_all_codes = debug_print_all_codes(Rejects);
		}
		 
		int jjj=234;
		jjj++; 
		
		for (GetRejects_Reject cur: Rejects) {
			if (cur.SubReject != null) {
				SubReject[] kk = cur.SubReject;
				int jj=234;
				jj++;
			}
			String code = cur.RejectCode;
			if (code.contains("INSURANCE") ||
				code.contains("GF") ||
				code.contains("FRONTRUG") ||
				code.contains("TF") ) {
				int jj=324; 
				jj++;
			}
		}
		
	}

	private static String debug_print_all_codes(GetRejects_Reject[] all_arr) {
		if (all_arr==null) {
			return " ";
		}
		String res = "";
		for (GetRejects_Reject rej: all_arr) {
			res += ( " " + rej.RejectCode);			
		}
		return res; 
	}

}