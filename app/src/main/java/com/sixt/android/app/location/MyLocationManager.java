package com.sixt.android.app.location;

import java.util.LinkedList;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.sixt.android.app.util.AppContext;
import com.sixt.android.app.util.DeviceAndroidId;
import com.sixt.android.app.util.TEST;

public class MyLocationManager {

	static MyLocationManager inst;

	private static final long TIME_THRESHOLD = 3*60*1000;

	private static final int ACCURACY_THRESHOLD = 0; // meters
	private static final int MIN_TIME_SECS = 10;

	private static final int MAX_QUEUED_LOCATIONS = 5;

	private static final LinkedList<Location> location_queue = new LinkedList<Location>();

	



	private static LocationManager locationManager;

	private static Location curLocation;

	private static MyLocationListener gpsListener;
	private static MyLocationListener networkListener;

	private static boolean gpsTurnedOn = false;


	@SuppressWarnings("unused")
	private static volatile boolean isInitialied = false;

	private static volatile boolean firstLocationSentToCmc = false;


	
	public static void setILocationConfigurer() {
//		locationClient = _locationConfigurer;
	}
	

	private MyLocationManager() {

	}




	public void close() {
		//		if (!isInitialied) {
		//			return;
		//		}

		inst = null;

//		if (gpsTurnedOn) {
//			try { turnOffGps(); } catch(Exception e) {}
//		}

		if (locationManager != null) {

			if (gpsListener != null) {
				try { locationManager.removeUpdates(gpsListener); } catch(Exception e) {}
				gpsListener = null;
			}

			if (networkListener != null) {
				try { locationManager.removeUpdates(networkListener); } catch(Exception e) {}
				networkListener = null;
			}

			locationManager = null;
		}

	}



	private static int debug_sign = +1; 

	public void debug_set_mock_location() {
		Location mock = new Location("MOCK_GPS_PROVIDER");
		mock.setLatitude(curLocation.getLatitude() + debug_sign*0.005);
		mock.setLongitude(curLocation.getLongitude() + 0.008);
		mock.setTime(System.currentTimeMillis());
		debug_sign *= -1;
		onLocationChanged(mock);
	}


	public static class AddressRec {
		public final String address;
		public final String city;
		public final String country;

		public AddressRec(String address, String city, String country) {
			super();
			this.address = address;
			this.city = city;
			this.country = country;
		}

	};


//
//	public AddressRec getAddress(Location location) {
//
//		final int MAX_ADDR_RESULTS = 10;
//
//		final AddressRec no_results = new AddressRec("","",""); 
//
//		if (location==null) {
//			return no_results;
//		}
//
//		String address = "";
//		String city = "";
//		String country = "";
//
//		Geocoder gc = new Geocoder(AppContext.appContext, Locale.getDefault());
//
//		double lat = location.getLatitude();
//		double lon = location.getLongitude();
//
//		//          for (int i = 0; i < MAX_ADDR_ATTEMPTS; i++) {
//
//		List<Address> address_arr;
//		try {
//			address_arr = gc.getFromLocation(lat, lon, MAX_ADDR_RESULTS);
//		}
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return no_results;
//		}
//
//		if (address_arr != null) {
//			for (Address addr: address_arr) {
//				address = set_if_empty(address, addr.getAddressLine(0));
//				country = set_if_empty(country, addr.getAddressLine(2));
//				// String country = addresses.get(0).getAddressLine(2);
//				// String city = addr.getAddressLine(1);
//				city = set_if_empty(city, addr.getLocality());
//				//				if (not_empty(city)) {
//				//					return city;   // 1st priority
//				//				}
//
////				if (StrUtils.notEmpty(address) && StrUtils.notEmpty(city) && StrUtils.notEmpty(country)) {
////					break;					
////				}
//
//			}
//		}
//		//          }
//
//		return new AddressRec(address, city, country);
//
//	}
	
//	public AddressRec getAddress(double lon,double lat) {
//
//		final int MAX_ADDR_RESULTS = 10;
//
//		final AddressRec no_results = new AddressRec("","",""); 
//
//		if (lon<1) {
//			return no_results;
//		}
//
//		String address = "";
//		String city = "";
//		String country = "";
//
//		Geocoder gc = new Geocoder(locationClient.getAppContext(), Locale.getDefault());
//		List<Address> address_arr;
//		try {
//			address_arr = gc.getFromLocation(lat, lon, MAX_ADDR_RESULTS);
//		}
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return no_results;
//		}
//
//		if (address_arr != null) {
//			for (Address addr: address_arr) {
//				address = set_if_empty(address, addr.getAddressLine(0));
//				country = set_if_empty(country, addr.getAddressLine(2));
//				// String country = addresses.get(0).getAddressLine(2);
//				// String city = addr.getAddressLine(1);
//				city = set_if_empty(city, addr.getLocality());
//				//				if (not_empty(city)) {
//				//					return city;   // 1st priority
//				//				}
//
//				if (StrUtils.notEmpty(address) && StrUtils.notEmpty(city) && StrUtils.notEmpty(country)) {
//					break;					
//				}
//
//			}
//		}
//		//          }
//
//		return new AddressRec(address, city, country);
//
//	}

//	private static String set_if_empty(String old_value, String new_value) {
//		if (not_empty(old_value)) {
//			return old_value;
//		}
//		return new_value;
//	}
//
//	private static boolean not_empty(String s) {
//		return s != null && s.length() > 1;
//	}		
//

	public void onLocationChanged(Location newLocation) {

		if (newLocation==null) {
			return;
		}

//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(locationClient.getAppContext());
//		final int min_dist = IdaqPreferencesManager.get().map_get_min_dist(sp);
//		final int min_dist = locationClient.getMapMinDist();

//		// 'smoothen' location reports
//		if (curLocation != null && curLocation.distanceTo(newLocation) < ACCURACY_THRESHOLD) {  
//			return;
//		}

		Location best_match = chooseBest(curLocation, newLocation); //!

		if (best_match==null || best_match.equals(curLocation)) {
			return; // keep current location
		}

		curLocation = newLocation;
		String res = "" + curLocation.getLatitude();
		String res2 = "" + curLocation.getLongitude();
		int jj=5435;
		jj++;


//		sendLocationReport(curLocation);

		//		if (IdaqMapActivity.inst != null) {
		//			IdaqMapActivity.inst.runOnUiThread(new Runnable() {				
		//				@Override
		//				public void run() {
		//					//TODO - ori , removed this for now 
		//					//MyToast.makeText(TrackingMapActivity.inst, "NEW LOC: !!!!", Toast.LENGTH_LONG).show(); 
		//					
		//				}
		//			});  
		//		}

	}

//	public Location getCurrentPosition() {  
//		return curLocation; // possibly null
//	}

	private static Location chooseBest(Location loc_a, Location loc_b) {

		if (loc_a == loc_b) {
			return loc_a;
		}

		if (loc_a == null || loc_b == null) {
			return (loc_a == null) ? loc_b : loc_a; 
		}



		// Check whether the new location fix is newer or older
		final long time_a = loc_a.getTime();
		final long time_b = loc_b.getTime();
		final long deltaTime = Math.abs(time_a - time_b);

		if (deltaTime > TIME_THRESHOLD) {
			// return newer
			return (time_a > time_b) ? loc_a : loc_b;  
		}

		if (loc_a.hasAccuracy() && loc_b.hasAccuracy()) {
			final float acc_a = loc_a.getAccuracy();
			final float acc_b = loc_b.getAccuracy();
			final int deltaAccuracy = (int) Math.abs(acc_a - acc_b);


			if (deltaAccuracy > ACCURACY_THRESHOLD) {
				// lesser Accuracy() value =>> MORE accurate
				return (acc_a > acc_b) ? loc_b : loc_a; 
			}
		}

		// just return newer
		return (time_a > time_b) ? loc_a : loc_b;  

	}


	@SuppressWarnings("unused")
	private boolean turnOnGps() {
		Context context = AppContext.appContext;
		String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		if (provider.contains("gps")) {
			// gps already on
			return false;
		}
		final Intent poke = new Intent();
		poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
		poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
		poke.setData(Uri.parse("3")); 
		context.sendBroadcast(poke);
		return true;
	}

//
//	private void turnOffGps(){
//		Context context = locationClient.getAppContext();
//		String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//
//		if (!provider.contains("gps")) { 
//			// gps is off
//			return;
//		}
//		final Intent poke = new Intent();
//		poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
//		poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//		poke.setData(Uri.parse("3")); 
//		context.sendBroadcast(poke);
//	}
//
//	
//	
//	public static GeoPoint from_locationToGeoPoint(Location location) {
//		int jj=234;
//		jj++;
//		jj--; 
//		if (location==null) {
//			return null; 
//		}
//		int lat = (int) (location.getLatitude() * 1E6);
//		int lng = (int) (location.getLongitude() * 1E6);
//		GeoPoint point = new GeoPoint(lat, lng);
//		return point;
//	}

	public static synchronized MyLocationManager get() {
		init();
		return inst;
	}


	private static synchronized void init() {

		if (inst != null) {
			return;
		}

		//Assert.that(inst==null);
		MyLocationManager tmp = new MyLocationManager();

		isInitialied = false;

		firstLocationSentToCmc = false;

		// Location services must be initialized from UI thread!
		//		Assert.that(UIThread.isUIThread()); -- unexplained exception when called from non ui thread 


		try {


			// gpsTurnedOn = turnOnGps(); -- do we want it?

			Context context = AppContext.appContext;

			locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

			/**
					Criteria finest = new Criteria();
			        finest.setAccuracy(Criteria.ACCURACY_FINE);
			        String best = locationManager.getBestProvider(finest, false);
			        locationManager.requestLocationUpdates(best, 0, 1, locationListener);
			 */

//			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(IdaqApplication.getAppContext());
			//			final int min_dist = 1000*IdaqPreferencesManager.get().map_get_min_dist(sp); -- gile probably an error
//			final int min_dist = IdaqPreferencesManager.get().map_get_min_dist(sp);
			final int min_dist = 50;
//			final int min_time = 1000*IdaqPreferencesManager.get().map_get_min_time(sp); // seconds to millisecs
			final int min_time = 1000*50; // 5 secs


			// zero optimizations for now:  minTime = minDistance = 0
			final String network = LocationManager.NETWORK_PROVIDER;
//			networkListener = new ILocationListener(network, context);
			networkListener = new MyLocationListener(network);
			locationManager.requestLocationUpdates(network, min_time, min_dist, networkListener);	

			final String gps = LocationManager.GPS_PROVIDER;
			gpsListener = new MyLocationListener(gps);
			locationManager.requestLocationUpdates(gps, min_time, min_dist, gpsListener);


			// get initial values until first reads are obtained
			Location net_loc = locationManager.getLastKnownLocation(network);
			Location gps_loc = locationManager.getLastKnownLocation(gps);

			curLocation = chooseBest(net_loc, gps_loc);

//			if (curLocation != null) {
//				sendLocationReport(curLocation);
//			}

			isInitialied = true;

			inst = tmp;
		}
		catch (Exception e) {
			// probably recoverable
			String errmsg = "Error initializing location manager: " + e;
			Log.e("Init", "\n\n\n" + errmsg + "\n\n\n");
			//throw new RTException(errmsg);

		}				
	}

//	@SuppressWarnings("unused")
//	public static void sendLocationReport(Location newLocation) {
//		if (newLocation == null) {
//			return;
//		}
//
////		MissionManager.get().addToLocationHistory(newLocation);
////		locationClient.addToLocationHistory(newLocation);
//
//		double Latitude = newLocation.getLatitude();
//		double Longitude = newLocation.getLongitude();
//		double Accuracy = newLocation.getAccuracy();
//
//		boolean wasSent;
//		try {
////			DeviceLocationUpdateMsg msg = new DeviceLocationUpdateMsg(newLocation);
////			wasSent = MqClientManager.get().mq_PublishMessage("LocationReport", msg);
//			
////			wasSent = locationClient.sendLocationUpdateMsg(newLocation);
////			if (wasSent) {
////				firstLocationSentToCmc = true;
////				synchronized (location_queue) {
////					location_queue.clear();
////				}
////			}
////			else if (!firstLocationSentToCmc) {
////				synchronized (location_queue) {
////					location_queue.add(newLocation);
////				}
////			}
//		} 
//		catch (Exception e) {
//			Log.e("MQ", "Failed to send location to MQ server. Error: " + e);
//			// and go on
//		}
//	}
//
//

//	public static void sendAllQueuedLocations() {
//		LinkedList<Location> queue;
//		synchronized (location_queue) {
//			queue = (LinkedList<Location>) location_queue.clone();
//		}
//
//		boolean wasSent;
//		for (Location loc: queue) {
//			try {
////				DeviceLocationUpdateMsg msg = new DeviceLocationUpdateMsg(loc);
////				wasSent = MqClientManager.get().mq_PublishMessage("LocationReport", msg);
//				wasSent = locationClient.sendLocationUpdateMsg(loc);
//				if (wasSent) {
//					firstLocationSentToCmc = true;
//				}
//			}
//			catch (Exception ee) {
//				// log error
//				// do not propagate exception
//			}
//		}
//		
//		if (firstLocationSentToCmc) {
//			synchronized (location_queue) {
//				location_queue.clear();
//			}
//		}
//
//	}
//	


//	public Location getCurrentLocationRecord() {
//		Location loc = getCurrentPosition();
//		if (loc==null) {
//			return null;
//		}
////		LocationRecord res = new LocationRecord(loc);
//		return loc;
//	}

	public static String getLatitude(Context c) {
		if (DeviceAndroidId.is_lab_device(c)) {
			return "32.1233837"; // always return a value!
		}
		
		if (curLocation==null) {
			return "";
		}
		String res = "" + curLocation.getLatitude();
		return res;
	}


	public static String getLongitude(Context c) {
		if (DeviceAndroidId.is_lab_device(c)) {
			return "34.8339847"; // always return a value! 
		}
		if (curLocation==null) {
			return "";
		}
		String res = "" + curLocation.getLongitude();
		return res;
	}


	public static void debug_runTest() {
		if (!TEST.MODE) {
			throw new RuntimeException();
		}
		new Thread() {
			public void run() {
				for (;;) {
					try {
						Thread.sleep(777);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String la = MyLocationManager.getLatitude(AppContext.appContext); 
					String lo = MyLocationManager.getLongitude(AppContext.appContext);
					int jj=234;
					jj++;
				}
			};
		}.start();

	}



}

class MyLocationListener implements LocationListener
{
	public final String type;
	
	public MyLocationListener(String _type) {
		type = _type;
	}

    public void onLocationChanged(final Location loc)
    {
    	MyLocationManager.inst.onLocationChanged(loc);
    }

    public void onProviderDisabled(String provider)
    {
//        MyToast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
    }


    public void onProviderEnabled(String provider)
    {
//        MyToast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
    }


    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

}