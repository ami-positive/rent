package com.sixt.android.app.json.response;

import com.sixt.android.app.json.objects.AvailableBranchesRecord;

public class Authentication_Response_Inner extends ResponseHeader {
//	public String RepName;		
//	public String Debug;
    public long DriverNo; 
    public long WorkerNo; 
    public String WorkerName;
    public AvailableBranchesRecord[] AvailableBranches;
	
}
