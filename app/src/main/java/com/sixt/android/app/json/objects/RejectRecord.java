package com.sixt.android.app.json.objects;


public class RejectRecord {

// Note: received Code can belong to a subreject!
//	      e.g.: be {"Code":"Wheel1","Value":3,"image":0},
// treatment: find 'parent' Reject of subreject and set its value to 1 (hard coded)  

	
	public String Code; // always unique e.g. "INSURANCE" , "Wheel1" , "HLF"
	public int Value;   // to be matched against RejectValue.RejectValueCode
	public int mark;   // 1 or 0
	public int image;   // 0 or 1


	public RejectRecord(String code, int value, int _mark, boolean is_image) {
		Code = code;
		Value = value;
		mark = _mark;
		image = is_image ? 1 : 0;
	}


	public RejectRecord(RejectMarkRecord reject) {
		this(reject.Code, reject.Value, 0, reject.Image != 0);
	}

}
