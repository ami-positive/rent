package com.sixt.android.app.util;

public class MainActionValue {
	private static String val = "B"; // B==mischari, I==pnimmi
	
	private MainActionValue() {}
	
	public static void set(String s) {
		val = s;
	}

	public static String get() {
		return val;
	}
}
