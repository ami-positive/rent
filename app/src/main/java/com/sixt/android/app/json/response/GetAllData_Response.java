package com.sixt.android.app.json.response;

import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.BranchesRecord;
import com.sixt.android.app.json.objects.ContractRecord;
import com.sixt.android.app.json.objects.DriverRecord;
import com.sixt.android.app.json.objects.GarageRecord;
import com.sixt.android.app.json.objects.RejectListInCiRecord;
import com.sixt.android.app.json.objects.ReturnReasonRecord;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.CarNumberValue;

public class GetAllData_Response extends Base_Response_JsonMsg {
 
	public static GetAllData_Response all_data;
		
	private GetAllData_Response_Inner GetAllData_res;	


	@Override 
	public void postLoad() {
		AllDataRepository.write_to_shared_prefs(this);
		int jj=234;
		jj++;
	}


	public GetAllData_Response_Inner get_inner() {
		return GetAllData_res;
	}
	
}
