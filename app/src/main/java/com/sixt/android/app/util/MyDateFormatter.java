package com.sixt.android.app.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MyDateFormatter {
	private MyDateFormatter() {}
	
	public static String format_now() {
		Date lm = new Date();
		String res = new SimpleDateFormat("yyyyMMddHHmm").format(lm);
		return res;
//		new SimpleDateFormat("yyyyMMdd").format(lm);
//		return null;
	}

	public static String format_now_secs() {
		Date lm = new Date();
		String res = new SimpleDateFormat("yyyyMMddHHmmss").format(lm);
		return res;
//		new SimpleDateFormat("yyyyMMdd").format(lm);
//		return null;
	}

}
