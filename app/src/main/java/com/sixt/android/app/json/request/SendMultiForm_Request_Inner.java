//package com.sixt.android.app.json.request;
//
//import java.util.ArrayList;
//
//import android.content.Context;
//
//import com.sixt.android.activities.CarNumberActivity;
//import com.sixt.android.activities.MesiraMerukezzetActivity;
//import com.sixt.android.activities.SignatureActivity;
//import com.sixt.android.app.json.BaseJsonMsg;
//import com.sixt.android.app.json.objects.CarModelNumber;
//import com.sixt.android.app.json.objects.CarRecord;
//import com.sixt.android.app.location.MyLocationManager;
//import com.sixt.android.app.util.CurrentAction;
//import com.sixt.android.app.util.CurrentSixtApp;
//import com.sixt.android.app.util.DebugMode;
//import com.sixt.android.app.util.LoginSixt;
//import com.sixt.android.app.util.MyDateFormatter;
//import com.sixt.android.app.util.SendMean;
//import com.sixt.android.app.util.TEST;
//import com.sixt.android.ftp.FtpUploader;
//
//public class SendMultiForm_Request_Inner extends BaseJsonMsg {
//
//	private static final boolean _DEBUG = TEST.debug_mode(false); 
//
//	public final String App = CurrentSixtApp.type();
//	public final String Login = LoginSixt.get_WorkerNo();
//	public String MovilitNo;// = "12-345-67"; 
//	public String Action = CurrentAction.get();
//	public String SignerName = "Sam H. Spade";	
//	public String SignerId = "123";
//	public String LicenseNo = CarNumberActivity.number;
//	public String Phone = "03-5667788";
//	public String Mobile = "055446677";
//	public String FormComment = "from ... until";
//	public String SendingMean = "M"; //"M", 
//	public String Address = "address from ... address until"; //":"dan@clalit.co.il",
//	public String Latitude ;// = "33.44";//= MyLocation.getLatitude();
//	public String Longitude ;// = "55.66";//= MyLocation.getLongitude();
//	public String FormDate = "201208071144";//":"201208071144",
//	public String SignatureFile; // = JsonDebug.DEBUG_FILENAME; //":"signature.jpg",
//	public boolean silent_transmit = false;
//	public CarRecord[] Cars;// = null; // {"Carno":"11-111-11"}
//
//
//	public SendMultiForm_Request_Inner(Context c, String _SignatureFile) { 
//		super(SendMultiFormRequest); 
//		SignerId = "";
//		LicenseNo = "";
//		SignatureFile = _SignatureFile;
//		Latitude = MyLocationManager.getLatitude(c);
//		Longitude = MyLocationManager.getLongitude(c);
//		FormDate = MyDateFormatter.format_now();
//
//		if (_DEBUG) {
//			String local_name = FtpUploader.upload_blocking(DebugMode.get_img_file_from_sdcard(3));
//			SignatureFile = local_name;
//			debug_set_car_numbers();
//		}
//		else {
//			SignerName = SignatureActivity.sSignerName;
////			SignerId = SignatureActivity.sSignerId;
////			LicenseNo = SignatureActivity.sLicenseNo;
//			Phone = SignatureActivity.sPhone;
//			Mobile = SignatureActivity.sMobile;
//			FormComment = SignatureActivity.sFormComment;
//			Address = SignatureActivity.sAddress;
//			SendingMean = SendMean.get(SignatureActivity.sEmailMode);
//			
//			ArrayList<CarModelNumber> carList;
////			carList = MesiraMerukezzetActivity.car_record_arr;
//			final int size = carList.size();
//			Cars = new CarRecord[size];
//			for (int i = 0; i < size; i++) {
//				String car_number = carList.get(i).number;
//				Cars[i] = new CarRecord(car_number);
//			}
//		}
//	}
//
//
//	private void debug_set_car_numbers() {
//		Cars = new CarRecord[5]; 
//		Cars[0] = new CarRecord("10-329-76"); 
//		Cars[1] = new CarRecord("10-330-76");
//		Cars[2] = new CarRecord("10-331-76");
//		Cars[3] = new CarRecord("10-332-76");
//		Cars[4] = new CarRecord("10-334-76");		
//	}
//}
