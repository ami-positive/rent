package com.sixt.android.app.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.os.Environment;

import com.sixt.android.app.uniqueid.UniqueId;

public class SdcardImageSaver {
	
	private SdcardImageSaver() {}

	public static void save(Bitmap bmp, String file_name) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

		File _sdcard  = Environment.getExternalStorageDirectory();//
		String sdcard = _sdcard.getAbsolutePath();
		if (!UniqueId.id_exists()) {
			throw new RuntimeException();
		}
		String _id = UniqueId.current_transaction_id;		
		String dir = sdcard + "/" + UniqueId.idToFolderName(_id);
		new File(dir).mkdirs();
		File f = new File(dir + "/" + file_name);

		FileOutputStream fo = null;
		try {
			f.createNewFile();
			fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();
		}
		finally {
			if (fo != null) {
				try { fo.close(); } catch(Exception e) {}
			}
		}
	}
	
//	public static void delete(String file_name) { 
//		File _sdcard  = Environment.getExternalStorageDirectory();
//		String sdcard = _sdcard.getAbsolutePath();
//		File f = new File(sdcard + "/" + file_name);
//		try { f.delete(); } catch(Exception e) {}
//	}
	

}
