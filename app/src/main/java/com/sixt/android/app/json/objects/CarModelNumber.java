package com.sixt.android.app.json.objects;

import com.sixt.android.app.util.TxtUtils;

public class CarModelNumber {
	private static final int LIMIT_CHARS = 12;
	public String number;
	private String model = ""; //deggem

	public CarModelNumber(String number) {
		this(number, "");
	}
	
	public CarModelNumber(String number, String _model) {
		this.number = number;
		this.model = TxtUtils.limit(LIMIT_CHARS, _model);
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setModel(String _model) {
		this.model = TxtUtils.limit(LIMIT_CHARS, _model);
	}
	
	public String getModel() {
		return this.model;
	}

	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CarModelNumber)) {
			return false;
		}
		CarModelNumber other = (CarModelNumber) o;
		return other.number != null && other.number.equals(number); 
	}
}
	
