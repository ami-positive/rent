package com.sixt.android.app.json.response;

public class ImageRejects {
	
	private ImageRejects() {}

	public static Object[] get_descriptions_for_type(String reject_Type) {
		RejectValue[] _res = get_for_type(reject_Type);
		String[] res2 = new String[_res.length];
		for (int i = 0; i < _res.length; i++)  {
			res2[i] = _res[i].desc;
		}
		return res2;
	}


	public static int position_to_value(int position, String reject_Type) {
		RejectValue[] arr = get_for_type(reject_Type);
		return arr[position].code; 
	}

	
	public static int value_to_position(int value, String reject_Type) {
		RejectValue[] arr = get_for_type(reject_Type);
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].code == value) {
				return i;
			}
		}
		return 0; // default
	}


	public static final RejectValue[] get_for_type(String reject_Type) {
		if (reject_Type==null) {
			throw new RuntimeException();
		}
		if (reject_Type.equals("B")) {
			return B_Rejects;
		}
		else if (reject_Type.equals("S")) {
			return S_Rejects;
		}
		else if (reject_Type.equals("T")) {
			return T_Rejects;
		}
		else if (reject_Type.equals("W")) {
			return W_Rejects;
		}
		throw new RuntimeException();
	}
	
	public static class RejectValue {
		public final int code;
		public final String desc;
		RejectValue(int _code, String _desc) {
			code = _code;
			desc = _desc;
		}
	};
	
	private static final RejectValue[] B_Rejects = {	
		new RejectValue( 0, "תקין"),
		new RejectValue( 1, "שריטה"),
		new RejectValue( 2, "שפשוף"),
		new RejectValue( 3, "לחיצה"),
		new RejectValue( 4, "מכה"),
		new RejectValue( 5, "מכה קשה"),
	};
	
	
	private static final RejectValue[] S_Rejects = {
		new RejectValue(  0, "תקין"),
		new RejectValue(  1, ""),
	};

	private static final RejectValue[] T_Rejects = {
		new RejectValue( 0, "תקין"),
		new RejectValue( 1, "שריטה"),
		new RejectValue( 4, "שבור"), 
	};
	
	private static final RejectValue[] W_Rejects = {
		new RejectValue(  0, "תקין"),
		new RejectValue(  1, "נקודה"),
		new RejectValue(  2, "סדק"),
		new RejectValue(  3, "מנופץ"),
		new RejectValue(  4, "שבור"),
	};

	
}

