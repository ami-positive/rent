package com.sixt.android.app.json.objects;

public class RejectMarkRecord {
	public final String Code;
	public final int Value;
	public final int Image;
	public final String Mark; // 0 1 
	
	public RejectMarkRecord(String code, int val, int img, String mark) {
		Code = code;
		Value = val;
		Image = img;
		Mark = mark;		
	}

	public RejectMarkRecord(RejectRecord rr) {
		Code = rr.Code;
		Value = rr.Value;
		Image = rr.image;
		Mark = (rr.mark==1 ? "1" : "0");
	}

}
