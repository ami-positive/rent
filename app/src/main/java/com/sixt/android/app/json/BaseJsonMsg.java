package com.sixt.android.app.json;

import com.sixt.android.activities.LoginActivity;
import com.sixt.android.app.util.DeviceAndroidId;

public class BaseJsonMsg {

	public final String debugVersion = "Gilad_" + VerName.the_VersionName;
	public String msg_type;
	public final String imei = DeviceAndroidId.get_imei();
	final public String simcard_serial = DeviceAndroidId.get_Sim_serial();


	public BaseJsonMsg() {
		// TODO Auto-generated constructor stub
	} 
	
	public BaseJsonMsg(String type) { 
		msg_type = type; 
	}
	
	public static final String AuthenticationRequest = "AuthenticationRent_Request";
	public static final String AuthenticationResponse = "AuthenticationRent_res";

	public static final String GetRejectsRequest = "GetRentRejects_Request";
	public static final String GetRejectsResponse = "GetRentRejects_response";

	public static final String LprRequest = "LprCarNumber_Request";
	public static final String LprResponse = "LprCarNumber_Response";
	
	public static final String GetCarDetailsRequest = "GetRentCarDetails_Rquest";
	public static final String GetCarDetailsResponse = "GetRentCarDetails_Response";
	
	public static final String SendFormRequest = "SendRentForm_request";
	public static final String SendFormResponse = "SendRentForm_Response";
	
	public static final String CheckCarExistsRequest = "CheckCarExists_Request";
	public static final String CheckCarExistsResponse = "CheckCarExists_Response";
	public static final String SendMultiFormRequest = "SendMultiForm_request";
	public static final String SendMultiFormResponse = "SendMultiForm_response";
	public static final String RetrieveFormRequest = "RetrieveForm_Request";
	public static final String RetrieveFormResponse = "RetrieveForm_Response";
	
	public static final String Get_All_Data_Request = "Get_All_Data_Request";
	public static final String Get_All_Data_Response = "Get_All_Data_Response";
	
	
	


}
