package com.sixt.android.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.sixt.android.app.json.objects.BranchesRecord;
import com.sixt.android.app.json.objects.ContractRecord;
import com.sixt.android.app.json.objects.CurrencyRecord;
import com.sixt.android.app.json.objects.DriverRecord;
import com.sixt.android.app.json.objects.GarageRecord;
import com.sixt.android.app.json.objects.PhotoRecord;
import com.sixt.android.app.json.objects.RefusalReasonRecord;
import com.sixt.android.app.json.objects.RejectListInCiRecord;
import com.sixt.android.app.json.objects.ReturnReasonRecord;
import com.sixt.android.app.json.response.GetAllData_Response;
import com.sixt.android.app.json.response.GetAllData_Response_Inner;
import com.sixt.android.app.json.response.GetCarDetails_Response;

import java.util.Currency;

public class AllDataRepository {
	
	private AllDataRepository() {}
	
	private static final String ALL_DATA_KEY = "ALL_DATA_KEY";
	private static final String ALL_DATA_NAME = "ALL_DATA_NAME";

	private static final String LAST_TIME_KEY = "LAST_TIME_KEY2";
	private static final String LAST_TIME_NAME = "LAST_TIME_NAME2";
	
	private static GetAllData_Response_Inner _cur;


	public static void write_to_shared_prefs(GetAllData_Response last_response) {
		if (last_response==null || last_response.get_inner()==null) {
			return;
		}
		_cur = last_response.get_inner();
		try { 
			Context c = AppContext.appContext; 
			SharedPreferences preferences = c.getSharedPreferences(ALL_DATA_NAME, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();		
			String txt = GGson.toJson(last_response);
			editor.putString(ALL_DATA_KEY, txt);
			editor.commit();
			
			
			preferences = c.getSharedPreferences(LAST_TIME_NAME, Context.MODE_PRIVATE);
			editor = preferences.edit();		
			txt = "" + System.currentTimeMillis();
			editor.putString(LAST_TIME_KEY, txt);
			editor.commit();
			
		}
		catch (Exception e) {
			// no op
			int jj=324; 
			jj++; 
		}
	}
	
	private static GetAllData_Response_Inner load_from_shared_prefs() { 
		try { 
//			Context c = LoginActivity.inst;
			Context c = AppContext.appContext; 
			SharedPreferences preferences = c.getSharedPreferences(ALL_DATA_NAME, Context.MODE_PRIVATE);
			String val = preferences.getString(ALL_DATA_KEY, null);
			if (val==null) {
				return null;
			}
			GetAllData_Response obj = 
					(GetAllData_Response) GGson.fromJson(val, GetAllData_Response.class);
			if (obj != null) {
				return obj.get_inner();
			}
			return null;
		}
		catch (Exception e) {
			int jj=234;
			jj++;
			// no op
		}
		return null; 
	}

		
	public static GetAllData_Response_Inner get() {
		if (_cur==null) {
			_cur = load_from_shared_prefs();
		}
		return _cur;
	}

	//////////////////////////////


	public static CurrencyRecord[] getCurrencies (){
		GetAllData_Response_Inner inner = get();
		if (inner == null){
			return  null;
		}
		return inner.Currency;
	}

	public static CurrencyRecord getCurrencyByCode (String code){
		CurrencyRecord[] currencies = getCurrencies();
		if (currencies != null){
			for (int i = 0; i < currencies.length; i++) {

				Log.i("CurrencyLog","code: " + code + " iter: " + currencies[i].CurrencyCode.trim() );

				if (currencies[i].CurrencyCode.trim().equalsIgnoreCase(code)){
					return currencies[i];
				}
			}
		}

		return null;
	}

	public static CurrencyRecord getCurrencyByDesc (String desc){
		CurrencyRecord[] currencies = getCurrencies();
		if (currencies != null){
			for (int i = 0; i < currencies.length; i++) {
				if (currencies[i].CurrencyDesc.equals(desc)){
					return currencies[i];
				}
			}
		}

		return null;
	}

	public static int getCurrencyPositionByCode (String code){
		CurrencyRecord[] currencies = getCurrencies();
		if (currencies != null){
			for (int i = 0; i < currencies.length; i++) {
				if (currencies[i].CurrencyCode.equals(code)){
					return i;
				}
			}
		}

		return 0;
	}


	public static int getCurrencyPositionByDesc (String desc){
		CurrencyRecord[] currencies = getCurrencies();
		if (currencies != null){
			for (int i = 0; i < currencies.length; i++) {
				if (currencies[i].CurrencyDesc.equals(desc)){
					return i;
				}
			}
		}

		return 0;
	}







	public static PhotoRecord[] getPhotos (){
		GetAllData_Response_Inner inner = get();
		if (inner == null){
			return  null;
		}
		return inner.Photo;
	}

	public static PhotoRecord getPhotoByCode (String code){
		PhotoRecord[] photos = getPhotos();
		if (photos != null){
			for (int i = 0; i < photos.length; i++) {
				if (photos[i].PhotoCode.equalsIgnoreCase(code)){
					return photos[i];
				}
			}
		}

		return null;
	}

	public static PhotoRecord getPhotoByDesc (String desc){
		PhotoRecord[] photos = getPhotos();
		if (photos != null){
			for (int i = 0; i < photos.length; i++) {
				if (photos[i].PhotoDesc.equals(desc)){
					return photos[i];
				}
			}
		}

		return null;
	}

	public static int getPhotoPositionByCode (String code){
		PhotoRecord[] photos = getPhotos();
		if (photos != null){
			for (int i = 0; i < photos.length; i++) {
				if (photos[i].PhotoCode.equals(code)){
					return i;
				}
			}
		}

		return 0;
	}

	public static int getPhotoPositionByDesc (String desc){
		PhotoRecord[] photos = getPhotos();
		if (photos != null){
			for (int i = 0; i < photos.length; i++) {
				if (photos[i].PhotoDesc.equals(desc)){
					return  i;
				}
			}
		}

		return 0;
	}





	public static String[] getPhotosAsStringArray () {
		PhotoRecord[] records = getPhotos();

		String[] result = new String[records.length];

		for (int i = 0; i < records.length; i++) {
			result[i] = records[i].PhotoDesc;
		}

		return result;
	}



	public static ReturnReasonRecord[] getReturnReasons() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.ReturnReason;
	}
	
	public static int getReturnReasonFromName(String return_reason_str) {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		ReturnReasonRecord[] reasons = inner.ReturnReason;
		if (reasons==null) {
			return -1;
		}
		for (ReturnReasonRecord r: reasons) {
			if (r.ReasonName.equals(return_reason_str)) {
				return r.ReturnCode;
			}
		}
		return -1;
	}

	public static int getContractTypeCode(String type_str) {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		ContractRecord[] contracts = inner.Contract;
		if (contracts==null) {
			return -1;
		}
		for (ContractRecord cr: contracts) {
			if (cr.ContractName.equals(type_str)) {
				return cr.ContractCode;
			}
		}
		return -1;
	}


	
//	public static int getCiCoIndex() {
//		GetAllData_Response_Inner inner = get();
//		if (inner==null) {
//			return -1;
//		}
//		ContractRecord[] _Contract = inner.Contract;
//		if (_Contract==null) {
//			return -1;
//		}
//		int selected_type = GetCarDetails_Response.get_IntContractTypeCode();
//		return getCiCoIndex(selected_type);
//	}
	
	
//	public static int getCiCoIndex(int selected_type) {
//		if (selected_type < 0) {
//			return -1;
//		}
//		
//		GetAllData_Response_Inner inner = get();
//		if (inner==null) {
//			return -1;
//		}
//		ContractRecord[] _Contract = inner.Contract;
//		if (_Contract==null) {
//			return -1;
//		}
//		for (ContractRecord rec: _Contract) {
//			if (rec.ContractCode == selected_type) {
//				int CoIndex = rec.CiCoIndex;
//				return CoIndex;
//			}
//		}
//		return -1;		
//	}

	
	public static String getContractName(int contract_code) {
		if (contract_code < 0) {
			return "";
		}
		
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return "";
		}
		ContractRecord[] _Contract = inner.Contract;
		if (_Contract==null) {
			return "";
		}
		for (ContractRecord rec: _Contract) {
			if (rec.ContractCode == contract_code) {
				return rec.ContractName;
			}
		}
		return "";
		
	}

	public static int getGarageDefault() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		ContractRecord[] _Contract = inner.Contract;
		if (_Contract==null) {
			return -1;
		}
		
		int selected_type = CarNumberValue.get_contract_type();
		
		for (ContractRecord rec: _Contract) {
			if (rec.ContractCode == selected_type) {
				int garage_def = rec.GarageDefault;
				return garage_def;
			}
		}
		return -1;
	}
	
	public static DriverRecord[] getAllDrivers() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.Driver; 
	}
	
	public static ContractRecord[] getAllContracts() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.Contract; 
	}


	
	public static GarageRecord[] getAllGarages() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.Garage; 
	}
	
	public static BranchesRecord[] getAllBranches() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.Branches; 
	}
	
	
	public static RejectListInCiRecord[] getRejectList() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		return inner.RejectListInCi;
	}

	public static int getBranchCode(String snif_name) {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		BranchesRecord[] all = inner.Branches;
		if (all==null) {
			return -1;
		}
		for (BranchesRecord r: all) {
			if (r.Branch_Name.equals(snif_name)) {
				return r.Branch_No;
			}
		}
		return -1;
	}

	
	
	public static int getSnifChazarraValue(String snif_name) {
		return getBranchCode(snif_name);
	}
	
	public static int getDriverValue(String driver_name) { 
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		DriverRecord[] all = inner.Driver;
		if (all==null) {
			return -1;
		}
		for (DriverRecord r: all) {
			if (driver_name.equals(r.DriverName)) {
				return r.DriverNo;
			}
		}
		return -1;
	}
	
	public static String getDriverName(long ext_driver_no) {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		DriverRecord[] all = inner.Driver;
		if (all==null) {
			return null;
		}
		for (DriverRecord r: all) {
			if (ext_driver_no == r.DriverNo) {
				return r.DriverName;
			}
		}
		return null;
	}
	
	
	
	public static int getMusachValue(String garage_name) { 
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		GarageRecord[] all = inner.Garage;
		if (all==null) {
			return -1;
		}
		for (GarageRecord r: all) {
			if (garage_name.equals(r.GarageName)) {
				return r.GarageCode;
			}
		}
		return -1;
	}

	public static String getGarageName(long garage_code) {
//		int sel_code;
//		try { 
//			sel_code = Integer.parseInt(gcode_str);
//		}
//		catch (Exception e) {
//			return null;
//		}
		
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		GarageRecord[] all = inner.Garage;
		if (all==null) {
			return null;
		}
		for (GarageRecord r: all) {
			if (r.GarageCode == garage_code) {
				return r.GarageName;
			}
		}
		return null;
	}

	public static String getBranchName(long branch_no) {
//		int branch_no;
//		try { 
//			branch_no = Integer.parseInt(branch_no_str);
//		}
//		catch (Exception e) {
//			return null;
//		}
		
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		BranchesRecord[] all = inner.Branches;
		if (all==null) {
			return null;
		}
		for (BranchesRecord r: all) {
			if (r.Branch_No == branch_no) {
				return r.Branch_Name;
			}
		}
		return null;
	}

	public static RefusalReasonRecord[] getRefusalReasons() {
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return null;
		}
		RefusalReasonRecord[] all = inner.RefusalReasons;
		return all;
	}

	
	public static long time_since_last_allData() {		
		try { 
			Context c = AppContext.appContext; 
			SharedPreferences preferences = c.getSharedPreferences(LAST_TIME_NAME, Context.MODE_PRIVATE);
			String val = preferences.getString(LAST_TIME_KEY, null);
			if (val==null) {
				return -1;
			}
			long last_time = Long.parseLong(val);
			if (last_time < 10000) {
				return -1;
			}
			long interval = System.currentTimeMillis() - last_time; 
			return interval;
		}
		catch (Exception e) {
			int jj=234;
			jj++;
			// no op
		}
		return -1;
	}

	
	public static int get_hearrot_code(String caption) {
		if (_cur==null) {
			return -1;
		}
		RejectListInCiRecord[] arr = _cur.RejectListInCi;
		if (caption==null || arr==null) { 
			return -1;
		}
		for (RejectListInCiRecord r: arr) {
			String n = r.RejectName;
			if (n.equals(caption)) {
				return r.RejectCiNo;
			}			
		}
		return -1;
	}

	public static int getRejectListInCiCode(String ciName) {
		if (ciName==null) {
			return -1;
		}
		GetAllData_Response_Inner inner = get();
		if (inner==null) {
			return -1;
		}
		RejectListInCiRecord[] ciRejects = inner.RejectListInCi;
		if (ciRejects==null) {
			return -1;
		}
		for (RejectListInCiRecord r: ciRejects) {
			if (ciName.equals(r.RejectName)) {
				int code = r.RejectCiNo;
				return code;
			}
		}
		return -1;
	}


}
