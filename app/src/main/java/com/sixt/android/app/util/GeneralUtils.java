package com.sixt.android.app.util;

import com.sixt.android.app.json.objects.SubReject;

public class GeneralUtils {
	
	private GeneralUtils() {}

	public static boolean code_is_in(String code, SubReject[] all_available_subrejects) {
		for (SubReject sr: all_available_subrejects) {
			if (code.equals(sr.SubRejectCode)) {
				return true;
			}
		}
		return false;
	}
}


