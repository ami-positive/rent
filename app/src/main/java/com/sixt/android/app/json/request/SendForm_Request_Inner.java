package com.sixt.android.app.json.request;

import android.content.Context;

import com.sixt.android.MyApp;
import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.activities.HearotLehachzarraActivity;
import com.sixt.android.activities.SignatureActivity;
import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.json.objects.HearotLehachzarraRecord;
import com.sixt.android.app.json.objects.PictureRecord;
import com.sixt.android.app.json.objects.RejectListItem;
import com.sixt.android.app.json.objects.RejectMarkRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.response.Authentication_Response;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.location.MyLocationManager;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.BranchNoValue;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.FuelValues;
import com.sixt.android.app.util.GeneralUtils;
import com.sixt.android.app.util.LoginSixt;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.MyDateFormatter;
import com.sixt.android.app.util.OffSiteMode;
import com.sixt.android.app.util.SignerTypeValue;
import com.sixt.android.app.util.TEST;

import java.util.ArrayList;
//import com.sixt.android.activities.CarDetailsActivity;

public class SendForm_Request_Inner extends BaseJsonMsg {

	private static final boolean _DEBUG = TEST.debug_mode(false);
	
//	private static final String DEFAULT_FUEL = "";	
//	public static String Image_local_filename;  
//	public static String Image_remote_filename;  //CarImage
	
	
	private static final ArrayList<String> photo_arr = new ArrayList<String>(); //remote names of photos
	private static final ArrayList<String> all_images_arr = new ArrayList<String>(); // carImage, signature, photos
	
//	private transient final boolean called_by_signature;
	
	public final transient boolean car_is_private = CarNumberActivity.car_is_private;

	private static String debug_photo_name;
	
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No();
	
	public String CarNo = "1122233"; //= CarNumberActivity.number; //"11-222-33";
	public String MenuAction = "B"; //
	public String Action = "D"; ////= CurrentAction.get();
	public final int OffSite = OffSiteMode.val;
	public int FuelStatus = 8; //
	public String Km =  "55800"; 
	public String ContractNo =  "100214895";  //from getRentCarDetails
	public String SubContractNo = "6"; //from getRentCarDetails
	public String DriverNo = "33"; 
	public String FormComment ; //= "";  
	public String DepartureBranchNo = "9";  
	public String ReturnBranchNo = "3";  
	public String GarageCode = ""; //
	public String Address ; // //= "dan@clalit.co.il";
	public String ReturnCode = "1"; //  
	public String RefusalReason = ""; //
	public String Latitude ; //= MyLocation.getLatitude(); 
	public String Longitude ; //= MyLocation.getLongitude();	
	public String FormDate ; //= "201208071144";
	public String SignatureFile ; // = "signature.jpg";	
	public String ContractType = "B-6 משרד הבטחון B6"; // 
	
	public RejectListItem[] RejectListInCi; // Hearrot Lehachzarra
	public RejectMarkRecord[] Rejects; // {"Code":"INSURANCE","Value":1,"image":0}, 
	public PictureRecord[] Pictures; // {"PicFile":"pic1.jpg"},
	
	public String SendingMean; //= "M" or "F"

	
	
	/////////////////////  NOT USED ////////////////////////
	public transient String CarType = "P"; // defaut==prati
//	public transient String CarSort = "?"; 		
	public transient String SignerType = SignerTypeValue.get(); 	
	public transient String CarModel ; //= GetCarDetails_Response.get_last_car_details().CarModel;//
	public transient String CarColor ; //= GetCarDetails_Response.get_last_car_details().CarColor;
	public transient String CompanyName ; //= GetCarDetails_Response.get_last_car_details().CompanyName;
	public transient String DateLicense ; //= GetCarDetails_Response.get_last_car_details().DateLicense;
	public transient String DateInsurance ; //= GetCarDetails_Response.get_last_car_details().DateInsurance;
	public transient String Mileage ; //= "112233";
//	public transient String Fuel = DEFAULT_FUEL; //= "0.8"; //":6,
	public transient String CarCode ; //= "1234"; //:"1235*",
	public transient String Keys ; //= "abc"; //:1,
	public transient String Controls ; //= "1"; 
	public transient String CollectedCar ; //= GetCarDetails_Response.get_last_car_details().CollectionCar;
	public transient String SignerName ; //= "";
	public transient String SignerId ; //= "487822457";
	public transient String LicenseNo ; //= "";
	public transient String Phone ; //= "";
	public transient String Mobile ; //= "";
		
	public transient boolean silent_transmit = false; 
	
	public transient String ImageFile ; //= "image.png";
	

			
	public SendForm_Request_Inner(Context c, boolean by_signature, 
					String _SignatureFile, ArrayList<RejectRecord> active_rejects) { 
		super(SendFormRequest);
		
//		final boolean pticha = MenuPnimmiActivity.Pticha();
//		final boolean sgirra = MenuPnimmiActivity.Sgirra();
//		final boolean hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();  
//		final boolean mesirra = MenuPnimmiActivity.Mesirra() || MenuMischarriActivity.Mesirra();

		final boolean pticha = ActionValue.Pticha();
		final boolean mesirra = ActionValue.Messira();
		final boolean sgirra = ActionValue.Sgirra();
		final boolean hachzarra = ActionValue.Hachzarra();
		
		
		if (_DEBUG) {
			debug_set_reject_arr();
		}
		else {
			SignatureFile = null;
		}
		
//		called_by_signature = by_signature;
				
			
		Latitude = null;
		Longitude = null;
		if (!pticha && !sgirra) {
			Latitude = MyLocationManager.getLatitude(c); 
			Longitude = MyLocationManager.getLongitude(c);
		}
		FormDate = MyDateFormatter.format_now_secs();
		
		if (sgirra) {
			SignatureFile = null;
			RejectListInCi = null;
			Rejects = null; 
			Pictures = null;
		}
		else { 
			SignatureFile = _SignatureFile;
		}
		

		if (!_DEBUG) {
			set_form_values(active_rejects);
		}
	}


	private void debug_set_reject_arr() {
		if (_DEBUG) {
			RejectListInCi = new RejectListItem[3] ;
			RejectListInCi[0] = new RejectListItem(1,  "רעש בבלימה");
			RejectListInCi[1] = new RejectListItem(2,  "בעצירה");
			RejectListInCi[2] = new RejectListItem(3,  "פלגים");
			
			Rejects = new RejectMarkRecord[3];
			Rejects[0] = new RejectMarkRecord("INSURANCE", 1, 0, "");
			Rejects[1] = new RejectMarkRecord( "Wheel1", 3, 0, "0");
			Rejects[2] = new RejectMarkRecord(  "HLF", 5, 0, "1");
		}
	}

	

	private void set_form_values(ArrayList<RejectRecord> active_rejects) {
		
//		final boolean _pticha = MenuPnimmiActivity.Pticha();
//		final boolean sgirra = MenuPnimmiActivity.Sgirra();
//		final boolean hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();  
//		final boolean mesirra = MenuPnimmiActivity.Mesirra() || MenuMischarriActivity.Mesirra();

		final boolean pticha = ActionValue.Pticha();
		final boolean mesirra = ActionValue.Messira();
		final boolean sgirra = ActionValue.Sgirra();
		final boolean hachzarra = ActionValue.Hachzarra();
		
	    FuelStatus = MyApp.appSetting.getFuel();
		Km = MyApp.appSetting.getKm();
		ContractNo = null;
		SubContractNo = null;
		if (!pticha) {
//			if (CarNumberValue.get_contract_type() > -1) {
//				ContractNo = "" + CarNumberValue.get_contract_type();
//			}
			/*else */if (GetCarDetails_Response.get_contract_no() > -1) { 
				int jj=24;
				jj++;
				ContractNo = "" + GetCarDetails_Response.get_contract_no();
			}
			else {
				int jj=24;
				jj++;
			}
		
			if (GetCarDetails_Response.get_suncontract_no() > -1) {
				SubContractNo = "" + GetCarDetails_Response.get_suncontract_no();
			}
		}
		
		FormComment = null;
		if (pticha || sgirra) {
			FormComment =  FuelValues.comments;
		}
		
		DepartureBranchNo = set_DepartureBranchNo();		
		ReturnBranchNo = set_ReturnBranchNo();
		
		
		GarageCode = null;
		if (pticha && FuelValues.musach_ind > -1) {
			GarageCode = "" + FuelValues.musach_ind;
		}
		Address = SignatureActivity.sAddress;
		SendingMean = SignatureActivity.sSendingMean;
	    ReturnCode = get_ReturnCode();
		System.out.println("-----------1> " +SignatureActivity.sRefusalReasonInd);
	    RefusalReason = null;
	    if (hachzarra) {
		    if (SignatureActivity.sRefusalReasonInd > -1) {
		    	RefusalReason = "" + SignatureActivity.sRefusalReasonInd;
		    }
	    }
		System.out.println("-----------2> " + RefusalReason);
	    // from Auhtenticate -- AllData Drivers + or user values if changed by user
		DriverNo = null;
		if (pticha) {
			if (FuelValues.driver_name_ind > -1) {
				DriverNo = "" + FuelValues.driver_name_ind;
			}
			else if (Authentication_Response.get_this_driver_no() > -1) {
				DriverNo = "" + Authentication_Response.get_this_driver_no();
			}
		}
		
		ContractType = null;
		if (pticha && CarNumberValue.get_contract_type() > -1) {
			ContractType = "" + CarNumberValue.get_contract_type();
		}		
		
		RejectListInCi = get_hearrot_lehachzarra(); 
		
		String catNoVal = CarNumberValue.get();
		CarNo = CarNumberValue.remove_dashes(catNoVal); 
		Action = ActionValue.get(); 
		MenuAction = MainActionValue.get();  			
		
		if (sgirra) {
			Rejects = null;	
		}
		else {
			Rejects = to_Array(active_rejects);
		}
//		CarSort = CarSortUtils.get(); 								
		
		Pictures = null;
		int _len = photo_arr.size(); 
		if (_len > 0) {
			Pictures = new PictureRecord[_len];
			for (int i = 0; i < _len; i++) {
				String remoteName = photo_arr.get(i);
				Pictures[i] = new PictureRecord(remoteName);
				debug_photo_name = remoteName;
				int jj=234;
				jj++;
			}
		}
		
		if (pticha || sgirra) {
			Pictures = null; //!
		}
		
//		if (ActionValue.should_pass_image()) {
//			ImageFile = Image_remote_filename;
//			if (ImageFile==null || ImageFile.length() < 3) {
////				throw new RuntimeException(); its ok	
//			}
//		}
		
//		if (FuelStatus==null || FuelStatus.length() != 1) {
//			FuelStatus = DEFAULT_FUEL; // default; cannot be empty
//		}
		
		if (Address.isEmpty()) {
			SendingMean = ""; //!
		}
			
		populate_image_file();		
	}



	private static String set_ReturnBranchNo() {
		
//		final boolean pticha = MenuPnimmiActivity.Pticha();
//		final boolean sgirra = MenuPnimmiActivity.Sgirra();
//		final boolean hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();  
//		final boolean mesirra = MenuPnimmiActivity.Mesirra() || MenuMischarriActivity.Mesirra();
		
		final boolean pticha = ActionValue.Pticha();
		final boolean mesirra = ActionValue.Messira();
		boolean sgirra = ActionValue.Sgirra();
		boolean hachzarra = ActionValue.Hachzarra();
		
		if (mesirra) {
			return null;
		}
		else if (hachzarra) {
			return get_authentication_or_userset_branch(); 
		}
		else if (sgirra || pticha) {
			int val = get_returnBranch_from_fuelScreen();
			if (val > -1) {
				return "" + val;
			}
			else {
				val = get_returnBranch_from_CarDetails();
				if (val > -1) {
					return "" + val;
				}
			}
		}
		else  {
//			return null;
		}   
		return null;
		
//		last comment text sometimes lost
		
	}
	

	private static int get_returnBranch_from_CarDetails() {
		int val = GetCarDetails_Response.get_ReturnBranchNo();
		return val;
	}


	private static int get_returnBranch_from_fuelScreen() {
		int val = MyApp.appSetting.getReturnBranch();
		return val;
	}


	private static String get_authentication_or_userset_branch() {
		int usersel_branch = BranchNoValue.get();	
		if (usersel_branch > -1) {
			return "" + usersel_branch;
		}
		return null;
	}


	private static RejectListItem[] get_hearrot_lehachzarra() {
		final boolean sgirra = ActionValue.Sgirra();
		if (sgirra) {
			return null;
		}
		HearotLehachzarraRecord[] _comments = HearotLehachzarraActivity.get();
		ArrayList<HearotLehachzarraRecord> real_comments = filter_real_comments(_comments);
		if (real_comments.size() == 0) {
			return null;
		}
		final int size = real_comments.size();
		RejectListItem[] res = new RejectListItem[size];		
		for (int i = 0; i < size; i++) {
			HearotLehachzarraRecord orig = real_comments.get(i);
//			int reject_code = orig.reject_code;
			int reject_code = orig.actual_reject_code;
			String desc = orig.txt_description;
			res[i] = new RejectListItem(reject_code, desc);
		}		
		return res;
	}


	private static ArrayList<HearotLehachzarraRecord> filter_real_comments(
			HearotLehachzarraRecord[] comments) {
		ArrayList<HearotLehachzarraRecord> filtered = new ArrayList<HearotLehachzarraRecord>(); 
		if (comments==null || comments.length==0) {
			return filtered;
		}
		for (HearotLehachzarraRecord r: comments) {
			if (r.actual_reject_code > 0) { // real comment
				filtered.add(r);
			}
		}
		return filtered;
	}


//	private static boolean no_hearrot(HearotLehachzarraRecord[] comments) {
//		if (comments==null || comments.length==0) {
//			return true;
//		}
//		for (HearotLehachzarraRecord r: comments) {
//			if (r.actual_reject_code > 0) { // real comment
//				return false;
//			}
//		}
//		return true;
//	}


	private void populate_image_file() {
		if (_notEmpty(ImageFile)) {
			all_images_arr.add(ImageFile);
		}
		if (_notEmpty(SignatureFile)) {
			all_images_arr.add(SignatureFile);
		}
		if (Pictures != null) {
			for (PictureRecord p: Pictures) {
				if (_notEmpty(p.PicFile)) {
					all_images_arr.add(p.PicFile);
				}					
			}
		}		
	}


	private static String set_DepartureBranchNo() {
//		boolean pticha = MenuPnimmiActivity.Pticha();
//		boolean mesirra = MenuPnimmiActivity.Mesirra() || MenuMischarriActivity.Mesirra();

		final boolean pticha = ActionValue.Pticha();
		final boolean mesirra = ActionValue.Messira();
		final boolean sgirra = ActionValue.Sgirra();
		final boolean hachzarra = ActionValue.Hachzarra();
		
//		only in pticha and mesirra {
		if (pticha) { //O
		   int val = GetCarDetails_Response.get_PickupBranchNo();
		   if (val > -1) {
			   return "" + val;
		   }
		}
		else if (mesirra) {  //D
			String sval = get_authentication_or_userset_branch();
			return sval;
		}
		return null;
	}

	
	private String get_ReturnCode() { // == sibat hachzarra
//		boolean sgirra = MenuPnimmiActivity.Sgirra();
//		boolean hachzarra = MenuPnimmiActivity.Hachzarra() || MenuMischarriActivity.Hachzarra();
		
		boolean sgirra = ActionValue.Sgirra();
		boolean hachzarra = ActionValue.Hachzarra();
		
		if (!sgirra && !hachzarra) {
			return "";
		}
		int reason = CarNumberValue.get_return_reason();
		if (reason < 0) {
			return "";
		}
		return "" + reason;
	}




	private static final RejectMarkRecord[] to_Array(ArrayList<RejectRecord> _rejects) {
		if (_rejects==null) {
			return null;
		}
		int len = _rejects.size();
		RejectMarkRecord[] res = new RejectMarkRecord[len];
		for (int i = 0; i < len; i++) {
			res[i] = new RejectMarkRecord(_rejects.get(i));
		}
		return res;
	}


	private static boolean _notEmpty(String str) {
		return str != null && str.length() > 2;
	}



	public static void delete_images() { 
//		for (String f: all_images_arr) {
//			try { SdcardImageSaver.delete(f); } catch(Exception e) {}			
//		}
//		all_images_arr.clear();		
	}

	
	public static void reset_photo_arr() {
		photo_arr.clear();
	}


	public static void add_to_photo_arr(String remote_photoName) {
		photo_arr.add(remote_photoName);
	}
	
	public boolean is_valid_form() {
		// verify mandatory fields exist
		if (Login==null || Login.length() < 2) {
			return false;
		}
//		if (SignerName==null || SignerName.length() < 1) {
//			return false;
//		} 
		return true;
		
	}


	public ArrayList<RejectRecord> get_actual_SUBrejects_inner(SubReject[] all_available_SUBrejects) {
		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
		if (Rejects==null) {
			return res;
		}
		
		for (RejectMarkRecord reject: Rejects) {   
			int value = reject.Value;
			String code = reject.Code;
			if ((value > 0) && GeneralUtils.code_is_in(code, all_available_SUBrejects)) {
				res.add(new RejectRecord(reject));
			}
		}
		return res;
	}

}
