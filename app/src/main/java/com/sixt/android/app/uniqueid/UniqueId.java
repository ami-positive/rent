package com.sixt.android.app.uniqueid;

import java.io.File;
import java.util.ArrayList;

import android.os.Environment;

import com.sixt.android.app.util.FileUtils;

public class UniqueId {
	public static String current_transaction_id;
	
	private static final String IMAGE_FOLDER_PREFIX = "RENT5477_"; //gggggfix2 change for each app!!!

	public static boolean id_exists() {
		return id_exists(current_transaction_id);
	}

//	public static boolean id_exists(String id) {
//		return id != null && id.length() > 2;
//	}

	public static boolean id_exists(String id) {
		if(id==null)
			return false;
		if (!id.startsWith(IMAGE_FOLDER_PREFIX)) {
			return id != null && id.length() > 2;
		}
		return (id != null) && (id.length() - IMAGE_FOLDER_PREFIX.length()) > 2;
	}
	
//	public static void delete_unique_folder() {
//		delete_unique_folder(current_transaction_id);
//	}

	public static void delete_unique_folderWithId(String transId) {
		delete_unique_folder(UniqueId.idToFolderName(transId));
	}

	public static void delete_all_image_folders_accept(ArrayList<String> pendingForms) { //gggggfix
		File sdcard = Environment.getExternalStorageDirectory();

		File[] childs = sdcard.listFiles();

		for (File f: childs) {
			String name = f.getName();
			if (f.isDirectory() && is_image_folder(name)) {
				if (!pendingForms.contains(name)) {
					delete_single_image_folder(f);
				}
			}
		}
	}

	private static boolean is_image_folder(String name) {
		return name != null && name.startsWith(IMAGE_FOLDER_PREFIX);
//		if (name.length() < 4 || name.length() > 20) {
//			return false;
//		}
//		try {
//			long num = Long.parseLong(name);
//			return true;
//		}
//		catch (Exception e) {
//			return false;
//		}
	}
	
	public static String idToFolderName(String name) { //gggfix
		if (name==null) {
			return "";
		}
		if (name.startsWith(IMAGE_FOLDER_PREFIX)) {
			return name;
		}
		return IMAGE_FOLDER_PREFIX + name;
	}
	
	public static void delete_unique_folder(String folderName) {
		try { 
			if (!id_exists(folderName)) {
	//			throw new RuntimeException();
				int jj = 4353;
				jj++;
				return;
			}						
			File sdcard = Environment.getExternalStorageDirectory();
			File dir = new File(sdcard, UniqueId.idToFolderName(folderName));
			if (!dir.exists()) {
				int jj=234;
				jj++;
				return;
			}
			
			delete_single_image_folder(dir); //ggggfix
			
		}
		catch (Exception ee) {
			int jj=234;
			jj++;
		}
		
	}

	private static void delete_single_image_folder(File dir) {
		for (File f: dir.listFiles()) {
			if (!f.isDirectory()) {
				try {
					f.delete();
				}
				catch (Exception eee) {
					int jj=3242;
					jj++;
				}
			}
		}
		
		try { 
			dir.delete();
		}
		catch (Exception eee) {
			int jj=3242;
			jj++;
		}
		 
	}

	
	public static void set_only_if_notset() {
		if (current_transaction_id == null) {
			set_new();
		}
	}

	public static void set_new() {
		current_transaction_id = FileUtils.get_randon_uuid();
	}

	public static void clear() {
		current_transaction_id = null;
	}
	
}
