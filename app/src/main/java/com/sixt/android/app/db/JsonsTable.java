package com.sixt.android.app.db;


import java.util.ArrayList;
import java.util.StringTokenizer;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sixt.android.app.uniqueid.UniqueId;
import com.sixt.android.app.util.ActionFailedError;
import com.sixt.android.ftp.FtpUploader;
import com.sixt.android.httpClient.JsonTransmitter;

 
public class JsonsTable extends BaseTable { 

	static final String KEY_TIME = "KEY_TIME";
	static final String KEY_FILE_LIST = "KEY_FILE_LIST";
	static final String KEY_UNIQUE_ID = "KEY_UNIQUE_ID";
	static final String KEY_JSON = "KEY_JSON";
	static final String KEY_HAS_REF = "has_ref";
 
	private static final String TABLE_NAME = "JsonsTable8"; 
	private static final long _WEEK = 1000*60*60*24*7; 

	private static JsonsTable inst;

	private JsonsTable() {
	}

	public static JsonsTable get() {
		if (inst == null) {
			inst = new JsonsTable();
		}
		return inst;
	}

	public static void close() {
		inst = null;
	}

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	public synchronized void add_unsent_json(String json, String uniuqe_id,int has_ref, ArrayList<String> files) {
		if (json == null) {
			return;
		}		
		DatabaseManager mngr = DatabaseManager.get();
		int jj=243;
		jj++;
		SQLiteDatabase db = mngr.getWritableDatabase();
		int jjj=243;
		jjj++;
		try {
			add_unsent_json(db, json, uniuqe_id,has_ref ,files);
		} finally {
			if (null != db) db.close();
		}
	}
	
	
	private synchronized void add_unsent_json(SQLiteDatabase db, String json, String uniuqe_id ,int has_ref, ArrayList<String> files) {
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_FILE_LIST, filelistToStr(files));
			values.put(KEY_UNIQUE_ID, uniuqe_id);
			values.put(KEY_JSON, json);
			values.put(KEY_TIME, "" + System.currentTimeMillis());
			values.put(KEY_HAS_REF, has_ref);
			db.insert(TABLE_NAME, null, values);
			int jj=234;
			jj++;
			
			Cursor cursor = readAllTable(db);			
			int num_records = cursor.getCount();
			int jjj=324;
			jjj++;
			
		} finally {
			db.close();
		}
	}

	
	private static String filelistToStr(ArrayList<String> files) {
		String res = "";
		if (files==null) {
			return res;
		}
		for (String f: files) {
			res += (f + "|");
		}
		return res;
	}

	
	// SilentMode
	public synchronized void send_all_pending_jsons() { //silent
		SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
		Cursor cursor = null;
		try {
			cursor = readAllTable(db);			
			int num_records = cursor.getCount();
			if (num_records > 0) {
				int jj=24; 
				jj++;
			}
			while (cursor.moveToNext()) {
				send_single_json(cursor);
			}
			int jjj=234;
			jjj++;
		}
		catch(Exception e){
			Log.e("JsonsTable", "getBrief" + e.toString());
		}
		finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception e) {
					Log.e("JsonsTable", "getBrief" + e.toString());
				}
			}
			try {
				if (db != null) db.close();
			} catch (Exception e) {
				Log.e("JsonsTable", "getBrief" + e.toString());
			}
		}
	}

	// SilentMode return sending status
		public synchronized boolean sendPendingJsonRequest() { //silent
			boolean all_prior_were_sent = true;
			SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
			Cursor cursor = null;
			try {
				cursor = readAllTable(db);		
				while (cursor.moveToNext()) {
					boolean ok = send_single_json(cursor); 
					if (!ok) {
						all_prior_were_sent = false;
						break;
					}				
				}
				
			}
			catch(Exception e) {
				all_prior_were_sent = false;
				Log.e("JsonsTable", "getBrief" + e.toString());
			}
			finally {
				if (cursor != null) {
					try {
						cursor.close();
					} catch (Exception e) {
						Log.e("JsonsTable", "getBrief" + e.toString());
					}
				}
				try {
					if (db != null) db.close();
				} catch (Exception e) {
					Log.e("JsonsTable", "getBrief" + e.toString());
				}
			}
			return all_prior_were_sent;
		}

	private boolean send_single_json(Cursor cursor) {
		boolean ok = true;
		final String unique_id = cursor.getString(cursor.getColumnIndex(JsonsTable.KEY_UNIQUE_ID));
		final String time = cursor.getString(cursor.getColumnIndex(JsonsTable.KEY_TIME));
		final String file_list = cursor.getString(cursor.getColumnIndex(JsonsTable.KEY_FILE_LIST));
		final String json = cursor.getString(cursor.getColumnIndex(JsonsTable.KEY_JSON));
		final boolean has_ref=cursor.getInt(cursor.getColumnIndex(JsonsTable.KEY_HAS_REF))==0?false:true;
		if (json==null || json.length() < 10) {
			delete_record(time, unique_id);
			return ok;
		}
		
		if (msg_too_young(time)) { 
			return ok; // not yet
		}
		
		boolean is_reacoverable_error = true;
		try { 
			ok = transmit_json_and_files(file_list, json, unique_id);
		}
		catch (ActionFailedError failed_err) {
			is_reacoverable_error = failed_err.is_recoverable_error();
		}
		
		if (ok && !has_ref) {// if json has further reference, don't delete
			UniqueId.delete_unique_folder(unique_id);
		} 

		if (ok || !is_reacoverable_error) {
			delete_record(time, unique_id);
		}
		else if (msg_too_old(time)) {
			delete_record(time, unique_id);
			ok=true;
		}
		else {
			// no op
		}
		return ok;
	}

	private static boolean msg_too_young(String time) {
		if (time==null || time.length() < 4) {
			return false;
		}
		long now = System.currentTimeMillis();
		try {
			long when_placed = Long.parseLong(time);
			if (now - when_placed < 30*1000) {
				return true;
			}
		}
		catch (Exception e) {
			int jj=234;
			jj++;
			return false;
		}
		return false;
	}

	private void delete_record(String time, String unique_id) {
		deleteRecords(" KEY_TIME = " + time);
		UniqueId.delete_unique_folder(unique_id);
	}

	private static boolean msg_too_old(String time) {
		if (time==null || time.length() < 4) {
			// bad time; just delete it..
			return true;
		}
		
		long creation_time;		
		try { 
			creation_time = Long.parseLong(time);
		}
		catch (Exception e) {
			// bad time; just delete it..
			return true;			
		}
		
		long diff = System.currentTimeMillis() - creation_time;
		
		if (diff > 4*_WEEK) {
			// if > 2 weeks -- remove it
			return true;
		}
		return false;
	}

	//silent send
	private boolean transmit_json_and_files(String file_list, String json,
			String unique_id) throws ActionFailedError { //
		StringTokenizer tok = new StringTokenizer(file_list, "|");
		ArrayList<String> local = new ArrayList<String>();
		ArrayList<String> remote = new ArrayList<String>();
		String remote_filename;
		try { 
			while (tok.hasMoreTokens()) {
				String file_name = tok.nextToken();
				if (file_name != null && file_name.length() > 3) {
					remote_filename = FtpUploader.upload_blocking(file_name, unique_id); 
					local.add(file_name);
					remote.add(remote_filename);
				}
			}
		}
		catch (Exception e) {
			return false;
		}
		
		json = DbUtils.convert_filenames(json, local, remote);

		
		// if here -- all files were uploaded
		try {
			JsonTransmitter.send_blocking(json);
		}
		catch (ActionFailedError err) {
			throw err;
		}
		catch (Exception  e) {
			e.printStackTrace();
			return false;
		} 
		
		return true; // went ok
	}

	@Override
	public void createTable(SQLiteDatabase db) {
		String createCmd = "CREATE TABLE " + TABLE_NAME + "("
				+ JsonsTable.KEY_UNIQUE_ID + " TEXT, "   
				+ JsonsTable.KEY_TIME + " TEXT, " 
				+ JsonsTable.KEY_FILE_LIST + " TEXT, "
				+ JsonsTable.KEY_JSON + " TEXT, "   
				+ JsonsTable.KEY_HAS_REF+" integer default 0)"; 

		db.execSQL(createCmd);
	}


	@Override
	public void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	}

	public void delete_old_image_folders() {
		SQLiteDatabase db = DatabaseManager.get().getWritableDatabase();
		Cursor cursor = null;
		ArrayList<String> pendingForms = new ArrayList<String>(); 
		try {
			cursor = readAllTable(db);			
			int num_records = cursor.getCount();
			if (num_records > 0) {
				int jj=24; 
				jj++;
			}
			while (cursor.moveToNext()) {
				int ctr = 0; 
				String unique_id = cursor.getString(ctr++);
				String folder_name = UniqueId.idToFolderName(unique_id); //gggfix
				pendingForms.add(folder_name);
			}
			int jjj=234;
			jjj++;
		}
		catch (Exception e) {
			return;
		}
		
		UniqueId.delete_all_image_folders_accept(pendingForms);

	}

	
}
