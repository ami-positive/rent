package com.sixt.android.app.util;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

public class CommonFunctions {
	private CommonFunctions() {}
	public static HttpPost getPostObjHebrew(String json, String url)  {
        HttpPost request = new HttpPost(url);
        request.addHeader("Content-type","application/json");
        StringEntity entity;
		try {
			entity = new StringEntity(json, HTTP.UTF_8);      
		} catch (UnsupportedEncodingException e) { 
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		entity.setContentType("text/plain;charset=UTF-8");
		entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"text/plain;charset=UTF-8")); 
        request.setEntity(entity);
        return request;
	}
	
	

}
