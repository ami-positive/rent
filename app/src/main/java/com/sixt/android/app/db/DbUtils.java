package com.sixt.android.app.db;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class DbUtils {
	private DbUtils() {}

	public static String wrap_filename(String fname) {
//		return "##$$" + fname + "$$##";
		return fname;
	} 

	public static String convert_filenames(String json,
			ArrayList<String> local_arr, ArrayList<String> remote_arr) {		
		if (json==null || json.length() < 3) {
			return json;
		}
		if (local_arr.size() != remote_arr.size()) {
			throw new RuntimeException();
		}
		if (local_arr.size()==0) {
			return json;
		}
		
		final int len = local_arr.size(); 
		for (int i = 0; i < len; i++) {
			String local_file = local_arr.get(i);
			String remote_file = remote_arr.get(i); 
			String local_file_w = wrap_filename(local_file);
			if (!json.contains(local_file_w)) {
				throw new RuntimeException();
			}
			json = json.replaceAll(Pattern.quote(local_file_w), remote_file);
			int jj=234;
			jj++;
		}
		int jj=234;
		jj++;
		return json;
	}

}
