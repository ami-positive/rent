package com.sixt.android.app.json.request;

import com.sixt.android.MyApp;
import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.BranchNoValue;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.LoginSixt;
import com.sixt.android.app.util.MainActionValue;
import com.sixt.android.app.util.OffSiteMode;
import com.sixt.android.app.util.PnimmiValue;

public class GetCarDetails_Request_Inner extends BaseJsonMsg {
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No();
	public String BranchNo = "64";// + BranchNoValue.get(); 
	public final String CarNo;
	public final String MenuAction = MainActionValue.get();
	public int ContractType;
	public final String Action = ActionValue.get();
	public final int OffSite = OffSiteMode.val;
	

	public GetCarDetails_Request_Inner(String car_no) { 
		super(GetCarDetailsRequest);
		
		ContractType = get_ContractType();
		CarNo = CarNumberValue.remove_dashes(car_no);
		int bno = BranchNoValue.get();
		if (bno >= 0) {
			BranchNo = "" + bno;
		}
//		Action = action;	
		int jj=234;
		jj++;
	}


	private int get_ContractType() {


		if (MyApp.generalSettings.isInCustomerYardMode()){
			return AllDataRepository.getContractTypeCode("delivery");
		}
		boolean pnimmi = PnimmiValue.Pnimmi();
//		boolean pticha = MenuPnimmiActivity.Pticha();		
		final boolean pticha = ActionValue.Pticha();
		
		if (pnimmi && pticha) {
			int type = CarNumberValue.get_contract_type();
			return type;
		}
		return -1;
	}
}