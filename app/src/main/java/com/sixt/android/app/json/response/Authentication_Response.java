package com.sixt.android.app.json.response;

import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.AvailableBranchesRecord;
import com.sixt.android.app.util.AllDataRepository;
import com.sixt.android.app.util.LoginSixt;

public class Authentication_Response extends Base_Response_JsonMsg {
		
	public Authentication_Response_Inner AuthenticationRent_res;
	
	public static AvailableBranchesRecord[] last_AvailableBranches;

	private static Authentication_Response last_response;
	
	@Override
	public void postLoad() {
		last_response = this;
		last_AvailableBranches = last_response.AuthenticationRent_res.AvailableBranches;
		long worker_no = AuthenticationRent_res.WorkerNo;
		LoginSixt.set_Worker_No(worker_no);
		int jj=234;
		jj++;
	}
	
	public AvailableBranchesRecord[] AvailableBranches; 
		// display THIS at branch selector and -- first record is always the default; 
	
	
	
	public static String get_WorkerName() {
		if (last_response==null || last_response.AuthenticationRent_res==null) {
			return null;
		}
		String name = last_response.AuthenticationRent_res.WorkerName;
		return name;
	}

	public static String get_this_driver_name() {
//		no first empty line; use default == DriverNo from Auth service + match against Drivers[] AllData
		if (last_response==null || last_response.AuthenticationRent_res==null) {
			return null;
		}
		long driver_no = last_response.AuthenticationRent_res.DriverNo;
		String dname = AllDataRepository.getDriverName(driver_no);
		return dname;
	}

	public static int get_this_driver_no() {
		if (last_response==null || last_response.AuthenticationRent_res==null) {
			return -1;
		}
		int driver_no = (int)last_response.AuthenticationRent_res.DriverNo;
		return driver_no;
	}
	

	public static AvailableBranchesRecord[] getAvailableBranches() {
		if (last_response==null || last_response.AuthenticationRent_res==null) {
			return null;
		}
		return last_response.AuthenticationRent_res.AvailableBranches;
	}


}	
 
