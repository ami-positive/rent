package com.sixt.android.app.util;

import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.app.json.response.GetCarDetails_Response;

public class PrivatenessOfCar {
	private PrivatenessOfCar() {}

	public static boolean is_p_or_pm() {
		if (CarNumberActivity.lastForm != null) {
			return CarNumberActivity.lastForm.is_p_or_pm();
		}
		else { 
			return GetCarDetails_Response.is_p_or_pm();
		}
	}

}
