package com.sixt.android.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.sixt.rent.R;

public class DialogError {

	private DialogError() {}
	
	public static void show(Context c, String msg) {
		show(c, msg, null);
	}

	public static void show(Context c, String msg, final Runnable btn_handler) {		
		final Dialog dialog = new Dialog(c); 
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_1_btns_simple);  
		Button ok = (Button)dialog.findViewById(R.id.btn_ok);
		TextView txt = (TextView)dialog.findViewById(R.id.textView1);

		ok.setText("סגור");
		txt.setText(msg);

		dialog.setCancelable(false);

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (btn_handler != null) {
					btn_handler.run();
				}
			}
		});

		dialog.show();
	}


	public static void old__show(Context c, String msg) { 
		final Dialog dialog = new Dialog(c); 
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_1_btn); 
		Button cancel = (Button)dialog.findViewById(R.id.btn_cancel);
		TextView txt = (TextView)dialog.findViewById(R.id.textView1);

		txt.setText(msg);


		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		dialog.show();
	}



}
