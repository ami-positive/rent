package com.sixt.android.app.util;

import android.widget.EditText;
import android.widget.TextView;

import com.sixt.android.activities.MainMenuActivity;
import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;

public class CaptionSetter {
	private CaptionSetter() {} 

	public static void set(TextView txt_caption) {
		txt_caption.setText("");

		String txt = "";
		if (PnimmiValue.Mischari()) {
			if (MenuMischarriActivity.Mesirra()) {
				txt = "מסחרי - מסירה";
			}
			else if (MenuMischarriActivity.Hachzarra()) {
				txt = "מסחרי - החזרה";
			}
			else {
				txt = "מסחרי  ";
			}
		}
		else if (PnimmiValue.Pnimmi()) {
			if (MenuPnimmiActivity.Pticha()) {
				txt = "פנימי - פתיחה";				
			}
			else if (MenuPnimmiActivity.Sgirra()) {
				txt = "פנימי - סגירה";
			}
			else if (MenuPnimmiActivity.Mesirra()) {
				txt = "פנימי - מסירה";
			}
			else if (MenuPnimmiActivity.Hachzarra()) {
				txt = "פנימי - החזרה";
			}
			else {
				txt = "פנימי ";
			}
		}
		else if (MainMenuActivity.ChatzarLakoach()) {
			txt = "חצר - ";
		}

		txt_caption.setText(txt);
	}

}
