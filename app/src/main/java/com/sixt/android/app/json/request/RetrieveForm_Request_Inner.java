package com.sixt.android.app.json.request;

import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.activities.LakoachSapakSelectorActivity;
import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.CarNumberValue;
import com.sixt.android.app.util.CarSortUtils;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.LoginSixt;

public class RetrieveForm_Request_Inner extends BaseJsonMsg {
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No();	
	public final String CarNO = CarNumberValue.get();
	public final String Action = ActionValue.get(); //fix
	public final String CarSort = CarSortUtils.get();
	public final String SignerType = LakoachSapakSelectorActivity.signer_type; 

	public RetrieveForm_Request_Inner() {
		super(RetrieveFormRequest);		
	}
}
