package com.sixt.android.app.util;

public class PnimmiValue {
	
	private PnimmiValue() {}
	
	private static volatile boolean is_pnimmi = true;
	
	public static void set_is_pnimmi() {
		is_pnimmi = true;
	}

	public static void set_is_mischarri() {
		is_pnimmi = false;
	}
	
	public static boolean Pnimmi() {
		return is_pnimmi;
	}
	
	
	public static boolean Mischari() {
		return !is_pnimmi;
	}
	

}
