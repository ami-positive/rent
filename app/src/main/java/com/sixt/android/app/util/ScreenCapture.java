package com.sixt.android.app.util;
//package com.sixt.android.app;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//
//import android.app.Activity;
//import android.content.res.Resources.Theme;
//import android.content.res.TypedArray;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.drawable.Drawable;
//import android.os.Environment;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.View;
//
//public class ScreenCapture {
//
//	private ScreenCapture() {}
//	
//	
//	private static final int IMAGE_QUALITY = 60; //0 - 100
//	
//	
//	public static File take(Activity activity, String filename) {
//		
//		DisplayMetrics metrics = new DisplayMetrics();
//		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
//
//		final int screenHeight = metrics.heightPixels;
//		final int screenWidth = metrics.widthPixels;
//		
//		final String SCREENSHOTS_LOCATIONS = Environment.getExternalStorageDirectory().toString() + "/screenshots/";
//		// Get root view
//		View view = activity.getWindow().getDecorView().getRootView();
//		// Create the bitmap to use to draw the screenshot
//		final Bitmap bitmap = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
//		final Canvas canvas = new Canvas(bitmap);
//
//		// Get current theme to know which background to use
//		final Theme theme = activity.getTheme();
//		final TypedArray ta = theme.obtainStyledAttributes(new int[] { android.R.attr.windowBackground });
//		final int res = ta.getResourceId(0, 0);
//		final Drawable background = activity.getResources().getDrawable(res);
//
//		// Draw background
//		background.draw(canvas);
//
//		// Draw views
//		view.draw(canvas);
//
//		File out_file = null;
//		
//		// Save the screenshot to the file system
//		FileOutputStream fos = null;
//		try {
//		    final File sddir = new File(SCREENSHOTS_LOCATIONS);
//		    if (!sddir.exists()) {
//		        sddir.mkdirs();
//		    }
//		    final String file_path = SCREENSHOTS_LOCATIONS + filename + ".jpg";
//		    fos = new FileOutputStream(file_path);
//		    if (fos != null) {
//		        if (!bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, fos)) {
//		            Log.d("ScreenShot", "Compress/Write failed");
//		        }
//		        else {
//		        	out_file = new File(file_path); // success
//		        }
//		        try { fos.flush(); } catch(Exception e) {}
//		        try { fos.close(); } catch(Exception e) {}
//		    }
//
//		} catch (FileNotFoundException e) {
//		    // TODO Auto-generated catch block
//		    e.printStackTrace();
//		} catch (IOException e) {
//		    // TODO Auto-generated catch block
//		    e.printStackTrace();
//		}		
//		return out_file;
//	}
//
//}
