package com.sixt.android.app.json.response;

import java.util.ArrayList;

import com.sixt.android.app.json.objects.BranchesRecord;
import com.sixt.android.app.json.objects.ContractRecord;
import com.sixt.android.app.json.objects.CurrencyRecord;
import com.sixt.android.app.json.objects.DriverRecord;
import com.sixt.android.app.json.objects.GarageRecord;
import com.sixt.android.app.json.objects.PhotoRecord;
import com.sixt.android.app.json.objects.ReasonsInternalContractRecord;
import com.sixt.android.app.json.objects.RefusalReasonRecord;
import com.sixt.android.app.json.objects.RejectListInCiRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.ReturnReasonRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.util.CarTypeUtils;
import com.sixt.android.app.util.GeneralUtils;


public class GetAllData_Response_Inner extends ResponseHeader {

//	public String CarType; // Sug rechev
//	public String CarModel; // Yatzran Ve-Deggem 
//	public String CarColor;
//	public String CompanyName;
//	public String DateLicense;
//	public String DateInsurance;
//	public String CollectionCar; //  Rechev Le-Issuf
//	private RejectRecord[] Rejects; // access via filter_list_rejects|filter_image_rejects
//	public int DriverExist;
	
	public ReturnReasonRecord[] ReturnReason; // sibat Hachzarra
	public BranchesRecord[] Branches; // all branches
	public DriverRecord[] Driver; // all drivers
	public RejectListInCiRecord[] RejectListInCi; // reject lists
	public RefusalReasonRecord[] RefusalReasons; 
	public ContractRecord[] Contract; // sug choze
	public GarageRecord[] Garage; // musachim
	public CurrencyRecord[] Currency;
	public PhotoRecord[] Photo;



	public String getBranchName(int the_num) {
		if (Branches==null) {
			return null;
		}

		for (BranchesRecord brec: Branches) {
			if (brec.Branch_No==the_num) {
				return brec.Branch_Name; 
			}			
		}
		return null;
	}



//	public void validate() {
//		if (CarType==null ||  (!CarType.equals("P") && !CarType.equals("M"))) {
//			throw new RuntimeException("Bad CarType: " + CarType);
//		}
//	}


}

