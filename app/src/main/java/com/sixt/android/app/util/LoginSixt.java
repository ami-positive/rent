package com.sixt.android.app.util;

public class LoginSixt {
	private static volatile String _login;  // ==password!
	private static volatile long WorkerNo;

	private LoginSixt() {}
	
	public static void set_password(String pwd) {
		if (pwd==null || pwd.length() < 2) {
			throw new RuntimeException("Bad login value!"); 
		}
		_login = pwd;
	}

	public static String get() {
		return _login ;
	}

	public static void set_Worker_No(long worker_no) {
		WorkerNo = worker_no;
	}
	
	public static String get_Worker_No() {
		return "" + WorkerNo;
	}

}
