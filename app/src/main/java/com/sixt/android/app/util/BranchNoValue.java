package com.sixt.android.app.util;

public class BranchNoValue {
	private BranchNoValue() {}
	
	private static int val = -1;
	private static String name = "";
	
	public static void set(int BranchNo, String branchName) {
		val = BranchNo;
		name = branchName;
	}

	public static int get() {
		return val;
	}

	public static String getName() {
		return name;
	}
	
}
