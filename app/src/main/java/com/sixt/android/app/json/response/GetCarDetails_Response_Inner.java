package com.sixt.android.app.json.response;

import java.util.ArrayList;

import com.sixt.android.app.json.objects.OptionalItemsRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.util.CarTypeUtils;
import com.sixt.android.app.util.GeneralUtils;


// car-image logic below
// ---------------------------------
//  special point:  001 + 002 -- outside of image; and are the coordinates of a button
//    (again - below and right of image) that when opening it should display 2/3
//    suggested values


public class GetCarDetails_Response_Inner extends ResponseHeader {		
    public String CarNo; 
	public String CarType;
	
	public String CarModel; // Yatzran Ve-Deggem
	public String LicenseDate; // tokef rishayon
	public String InsurenceDate; // tokef bituach
	
    public int FuelStatus;
    public int FuelIndex; // delek
    public long KmMax; // utz uz max
    public long KmMin; // utz uz min
    public long KmAlert; // utz uz alert val
    public int ContractNo; // mispar choze
    public int SubContract;
    public String ContractType; // sug chozze
    public String CustomerName; // shem lakoach
    public String DriverName; // shem nahag
    public String IgnCode;
    public String CustMail; // address ktovet
    public long GarageCode; // musach
    public long PickupBranchNo;
    public long ReturnBranchNo;// snif hachzarra
    public int ReturnBranchOpt;    
    public String ContractRemarks;
    public int IntContractTypeCode;
    public String BContractNo;
    public String BContractType; 
    public String BCustomerName;
    public String BDriverName;    
    public int CiCoFlag; // 0 or 1 
    
    public RejectRecord[] Rejects; // access via filter_list_rejects|filter_image_rejects
	public OptionalItemsRecord[] OptionalItems;
			
	public transient String CompanyName = "";
	public transient String CarColor = "";
	public transient String CollectionCar = ""; //  Rechev Le-Issuf
	public transient int DriverExist = 1;

	public String What2Do;
	
	
//	public transient String CarModel = ""; // Yatzran Ve-Deggem
//	public transient String DateLicense = "";
//	public transient String DateInsurance = "";

	
	public RejectRecord[] get_list_only_rejects() {
		if (Rejects==null) {
			return null;
		}
		ArrayList<RejectRecord> filtered = new ArrayList<RejectRecord>(); 
		for (RejectRecord cur: Rejects) {
			if (cur.image == 0) { // list reject
				filtered.add(cur);
			}
		}
		
		int len = filtered.size();
		RejectRecord[] res = filtered.toArray(new RejectRecord[len]);
		return res;
	}
	
	
	public RejectRecord[] get_image_only_rejects() {
		if (Rejects==null) {
			return null;
		}
		ArrayList<RejectRecord> filtered = new ArrayList<RejectRecord>(); 
		for (RejectRecord cur: Rejects) {
			if (cur.image != 0) { // image reject
				filtered.add(cur);
			}
		}
		
		int len = filtered.size();
		RejectRecord[] res = filtered.toArray(new RejectRecord[len]);
		return res;
		
	}

	public boolean is_p_or_pm() { // "P" or "PM" 
		return CarTypeUtils.is_p_or_pm(CarType);
	}


	public void validate() {
		if (CarType==null ||  (!CarType.equals("P") && !CarType.equals("M"))) {
			throw new RuntimeException("Bad CarType: " + CarType);
		}
	}


	public ArrayList<RejectRecord> get_actual_SUBrejects_inner(
			SubReject[] all_available_subrejects) {
		ArrayList<RejectRecord> res = new ArrayList<RejectRecord>();
		if (Rejects==null) {
			return res;
		}
			  
		for (RejectRecord reject: Rejects) {
			int value = reject.Value;
			String code = reject.Code; 
			if ((value > 0) && GeneralUtils.code_is_in(code, all_available_subrejects)) {
				res.add(reject);
			}
		}
		return res;
	}


}

