package com.sixt.android.app.json.objects;

import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.ui.DisplayLogic;

public class SubReject {
	public String SubRejectCode; // e.g. "WHEEL1"
	public String SubRejectDesc;
	public String RejectType; // e.g. "RD"
	public String CarType; // "P". "PM"..
	public SubRejectValue[] SubRejectValues; // size <= 5
	public int Mark;
	public int RejectCheck;
	public int RejectMOk;
	public int Optional;
	
	
	public transient int value;
	public transient boolean left_check_selected = false;

	
	private boolean is_mesirra() {
		if (RejectType==null) {
			return false;
		}
		String tmp = RejectType.toUpperCase();
		return tmp.contains("D");
	}
		
	boolean is_hachzarra() {
		if (RejectType==null) {
			return false;
		}
		String tmp = RejectType.toUpperCase();
		return tmp.contains("R");
	}

	public boolean should_show() { 
//		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra(); 
		boolean mode_hachzarra = MenuMischarriActivity.Hachzarra() || MenuPnimmiActivity.Hachzarra();
		
		final boolean reject_mode_mesirra = DisplayLogic.reject_mode_mesirra(); //
		
		if (reject_mode_mesirra && !is_mesirra()) { 
			return false;
		}
		
		if (mode_hachzarra && !is_hachzarra()) {
			return false;
		}
		
		if (Optional != 0) {			
//			if (!GetCarDetails_Response.in_reject_arr(RejectCode)) {
			if (!GetCarDetails_Response.in_Optional_arr(SubRejectCode)) { 
				return false;
			}			
		}
		return true;
	}
		
	
	public boolean no_default_value() { // and user MUST do his selection 
		return RejectCheck == 1;        // also - values returned in GetCarDetails are ADIFF of this field
	}
	
	public boolean is_mok() { // if not OK -- cannot leave reject screen!
//		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra();
		final boolean mok_is_active = DisplayLogic.mok_is_active();  						
		return mok_is_active && RejectMOk == 1;
	}
	
	public boolean show_eol_checkbox() { //
		return Mark != 0;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this==o) {
			return true;
		}
		if (!(o instanceof SubReject)) {
			return false;
		}
		SubReject other = (SubReject) o;		
		return other.SubRejectCode.equals(this.SubRejectCode);
	}

	public SubReject(String code, int val, boolean checked) {
		SubRejectCode = code;
		value = val;
		left_check_selected = checked;
	}

	public void clear_value() {
		value = 0;
		left_check_selected = false;
	}

	public boolean is_p_or_pm() {
		return CarType.toUpperCase().contains("P"); // PRATI
	}

	public boolean is_m_or_pm() {
		return CarType.toUpperCase().contains("M"); // PRATI
	}


	public String first() { //rightmost
		return get_at_pos(0);
	}


	public String place_1() {
		return get_at_pos(1);
	}


	public String place_2() {
		return get_at_pos(2);
	}


	public String place_3() {
		return get_at_pos(3);
	}

	public String place_4() {
		return get_at_pos(4);
	}


	public String last() { // leftmost
		return get_at_pos(5); 
	}
	

	private String get_at_pos(int pos) {
		if (SubRejectValues==null || SubRejectValues.length < (pos+1)) {
			return null;
		}
		String val = SubRejectValues[pos].SubRejectValueDesc;
		return val;
	}

	public void init_value_according_to_default_flag() {
		left_check_selected = false;
		if (no_default_value()) {
			value = -1;			
		}
		else {
			value = 0;
		}
//		if (value >= 0) {
//			return;
//		}
//		if (!no_default_value()) {
//			value = 0; // force 0 as default
//		}
	}
	

//	private transient boolean value_was_set = false;
//	
//	public void mark_value_set_by_user() {
//		first_time = false;
//	}
//
//	public boolean value_was_set_by_user() {
//		return !first_time;
//	}

	
}

