package com.sixt.android.app.json.request;

import java.util.ArrayList;

import android.content.Context;

import com.sixt.android.app.json.objects.RejectMarkRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;


public class SendForm_Request {
	public final SendForm_Request_Inner SendRentForm_req;

	private transient final boolean called_by_signature;

	public SendForm_Request(Context c, boolean by_signature, String _SignatureFile, ArrayList<RejectRecord> rejects) {
		called_by_signature = by_signature;
		SendRentForm_req = new SendForm_Request_Inner(c, called_by_signature, _SignatureFile, rejects);
	}

	public boolean is_valid_form() {
		if (!SendRentForm_req.is_valid_form()) {
			return false;
		}
//		String fuell = SendRentForm_req.Fuel;
//		String carsortt = SendRentForm_req.CarSort;
		int jj=2345;
		jj++;
		return true;

	}


	public RejectRecord[] get_list_only_rejects() {
		if (SendRentForm_req==null || SendRentForm_req.Rejects==null) {
			return new RejectRecord[0];
		}
		RejectMarkRecord[] reject_arr = SendRentForm_req.Rejects;
		ArrayList<RejectRecord> filtered = new ArrayList<RejectRecord>();
		if (reject_arr != null) {
			for (RejectMarkRecord cur: reject_arr) {
				if (cur.Image == 0) { // list reject
					filtered.add(new RejectRecord(cur));
				}
			}
		}

		int len = filtered.size();
		RejectRecord[] res = filtered.toArray(new RejectRecord[len]);
		return res;
	}
	
	
	public RejectRecord[] get_image_only_rejects() {
		if (SendRentForm_req==null || SendRentForm_req.Rejects==null) {
			return new RejectRecord[0];
		}
		RejectMarkRecord[] reject_arr = SendRentForm_req.Rejects;
		ArrayList<RejectRecord> filtered = new ArrayList<RejectRecord>();
		if (reject_arr != null) {
			for (RejectMarkRecord cur: reject_arr) {
				if (cur.Image != 0) { // image reject
					filtered.add(new RejectRecord(cur));
				}
			}
		}

		int len = filtered.size();
		RejectRecord[] res = filtered.toArray(new RejectRecord[len]);
		return res;
	}

	
	public boolean is_p_or_pm() { 
		if (SendRentForm_req==null) {
			return true; // default
		}
		return SendRentForm_req.car_is_private;
	}

//	public ArrayList<RejectRecord> get_actual_SUBrejects(
//			SubReject[] all_available_SUBrejects) {
//		return SendRentForm_req.get_actual_SUBrejects_inner(all_available_SUBrejects);
//	}

//	public String getCarSort() { 
//		if (SendRentForm_req==null) {
//			return null;
//		}
//		return SendRentForm_req.CarSort;
//	}

	public String getSignerType() {
		if (SendRentForm_req==null) {
			return null;
		}
		return SendRentForm_req.SignerType;
	}
	
}

