package com.sixt.android.app.util;

import com.sixt.android.MyApp;

import java.util.regex.Pattern;

public class CarNumberValue {
	
//	private static String number = MyApp.appSetting.getCarNumber();  // 33-222-33


	public static void set(String num) {
		MyApp.appSetting.setCarNumber(_format(num));
	}

	public static void set_with_return_reason(String num, int _return_reason) {
		MyApp.appSetting.setCarNumber(_format(num));
		MyApp.appSetting.setReturnReason(_return_reason);
	}

	public static void clear() {
		MyApp.appSetting.setCarNumber("");
		MyApp.appSetting.setReturnReason(-1);
		MyApp.appSetting.setContractType(-1);
	}
 
	public static void set_with_contract_type(String num, int _contract_type) {
		MyApp.appSetting.setCarNumber(_format(num));
		MyApp.appSetting.setContractType(_contract_type);
	}
	
	public static String get() {
		return MyApp.appSetting.getCarNumber() ;
	}

	public static int get_return_reason() {
		return MyApp.appSetting.getReturnReason();
	}

	public static int get_contract_type() {
		return MyApp.appSetting.getContractType();
	}


	private static String _format(String num) {
		if (!num.contains("-")) { 
			num = CarNumberUtils.format_car_number(num);
		}
		return num;
	}

	public static String remove_dashes(String string) {
		if (string==null) {
			return "";
		}		
		String nn = string.replaceAll(Pattern.quote("-"), ""); 
		return nn; 
	}

}
