package com.sixt.android.app.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.sixt.android.MyApp;
import com.sixt.android.activities.ImageCarActivity;
import com.sixt.android.app.uniqueid.UniqueId;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class CameraUtils {

	private CameraUtils() {}

	private static final int TAKE_PICTURE = 22;
	private static final int MAX_IMG_SIZE_PIX = 30000; // pixels

	public static File handle_picture_taken(int requestCode, int resultCode, Intent data, File photo_file, Activity caller) {
		ImageCarActivity.setErrorString("handle_picture_taken:");
		if (photo_file==null) {
			MyApp.appSetting.setCameraError("שגיאת תמונה");
			ImageCarActivity.setErrorString("handle_picture_taken: photo_file==null");
			return null;
		}
		if (requestCode != TAKE_PICTURE) {
			MyApp.appSetting.setCameraError("תמונה לא שייכת");
			ImageCarActivity.setErrorString("handle_picture_taken: requestCode != TAKE_PICTURE");
			return null;
		}
		if (resultCode != Activity.RESULT_OK) {
			MyApp.appSetting.setCameraError("צילום תמונה בוטל");
			ImageCarActivity.setErrorString("handle_picture_taken: resultCode != Activity.RESULT_OK");
			return null;
		}



		if(!photo_file.exists()){
			MyApp.appSetting.setCameraError("תמונה לא נשמרה בגלריה");
		}

		if(photo_file.length() < 300){
			MyApp.appSetting.setCameraError("תמונה קטנה מדי");
		}

		if (!photo_file.exists() || photo_file.length() < 300) {
			ImageCarActivity.setErrorString("handle_picture_taken: A photo_file.exists() " + photo_file.exists());
			ImageCarActivity.setErrorString("handle_picture_taken: B photo_file.length() " + photo_file.length());
			ImageCarActivity.setErrorString("handle_picture_taken: problem A or B");
			return null;
		}
		return photo_file; // all is well
	}



	public static File start_camera_app(Activity caller) {
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		String file_name = "Pic_" + System.currentTimeMillis() + ".jpg";
		File sdcard = Environment.getExternalStorageDirectory();

		if (TEST.MODE) {
			if (!UniqueId.id_exists()) {
				UniqueId.set_new();
			}			
		} 

		if (!UniqueId.id_exists()) {
			throw new RuntimeException();
		}
		String _id = UniqueId.current_transaction_id;
		File dir = new File(sdcard, UniqueId.idToFolderName(_id));		
		dir.mkdirs();
		File photo = new File(dir,  file_name);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo)); 
		caller.startActivityForResult(intent, TAKE_PICTURE);
		return photo;
	}


	public static void debug_write_image_to_sdcard(File photo_file, Activity caller) {
		//		if (!TEST.MODE) {
		//			throw new RuntimeException();
		//		}
		//		Bitmap bmp = load_image_limit_size(photo_file, MAX_IMG_SIZE_PIX);
		//		if (bmp != null) {
		//			String fname = "testtest_" + System.currentTimeMillis() + ".jpg";
		//			write_bitmap_to_sdcard(bmp, fname);
		//		}

	}

//
//	public static Bitmap load_image_limit_size(File img_file, int max_size_bytes) { 
//		if (img_file==null || !img_file.exists() || img_file.length() < 200) {
//			return null;
//		}
//
//		final String path = img_file.getAbsolutePath();
//		BitmapFactory.Options options = new BitmapFactory.Options();
//		options.inJustDecodeBounds = true;
//		BitmapFactory.decodeFile(path, options);
//
//		// Calculate inSampleSize, Raw height and width of image
//		final int height = options.outHeight;
//		final int width = options.outWidth;
//		options.inPreferredConfig = Bitmap.Config.RGB_565;
//
//		if (height < 10 || width < 10) {
//			return null;
//		}
//
//
//		int scale = 1;
//		while ((width * height) * (1 / Math.pow(scale, 2)) > max_size_bytes) {
//			scale++;
//		}
//
//		Uri uri = Uri.fromFile(img_file);
//
//		Bitmap b = null;
//		InputStream in;		
//		Context c = AppContext.appContext; 
//		try {
//			in = c.getContentResolver().openInputStream(uri);
//		} catch (FileNotFoundException e1) {
//			e1.printStackTrace();
//			return null;
//		}
//
//		if (scale > 1) { //scale factor
//			scale--;
//			// scale to max possible inSampleSize that still yields 
//			//   an image larger than target
//			options = new BitmapFactory.Options();
//			options.inSampleSize = scale;
//			b = BitmapFactory.decodeStream(in, null, options);
//
//			// resize to desired dimensions
//			int height2 = b.getHeight();
//			int width2 = b.getWidth();
//			Log.d("", "1th scale operation dimenions - width: " + width2 + ", height: " + height2);
//
//			double y = Math.sqrt(max_size_bytes
//					/ (((double) width2) / height2));
//			double x = (y / height2) * width2;
//
//			Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, 
//					(int) y, true);
//			b.recycle();
//			b = scaledBitmap;
//
//			System.gc();
//		} else {
//			b = BitmapFactory.decodeStream(in);
//		}
//
//		try { in.close(); } catch (IOException e) {}
//		return b;
//	}
//

	public static Bitmap load_image_limit_size(File img_file, int max_size_bytes, boolean do_not_resize) {
		if (img_file==null) {
			return null;
		}
		if (!img_file.exists()) {
			return null;
		}
		if (img_file.length() < 200) {
			return null;
		}

		final String path = img_file.getAbsolutePath();
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize, Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		options.inPreferredConfig = Bitmap.Config.RGB_565;

		if (height < 10 || width < 10) {
			return null;
		}


		int scale = 1;
		if (!do_not_resize) { 
			while ((width * height) * (1 / Math.pow(scale, 2)) > max_size_bytes) {
				scale++;
			}
		}

		Uri uri = Uri.fromFile(img_file);

		Bitmap b = null;
		InputStream in;		
		Context c = AppContext.appContext; 
		try {
			in = c.getContentResolver().openInputStream(uri);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return null;
		}

		if (scale > 1) { //scale factor
			scale--;
			// scale to max possible inSampleSize that still yields 
			//   an image larger than target
			options = new BitmapFactory.Options();
			options.inSampleSize = scale;
			b = BitmapFactory.decodeStream(in, null, options);

			// resize to desired dimensions
			int height2 = b.getHeight();
			int width2 = b.getWidth();
			Log.d("", "1th scale operation dimenions - width: " + width2 + ", height: " + height2);

			double y = Math.sqrt(max_size_bytes
					/ (((double) width2) / height2));
			double x = (y / height2) * width2;

			Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, 
					(int) y, true);
			b.recycle();
			b = scaledBitmap;

			System.gc();
		} else {
			b = BitmapFactory.decodeStream(in);
		}

		try { in.close(); } catch (IOException e) {}
		return b;
	}

	public static File create_resized_photo(File orig_file, String new_Filename, int max_size) {
		
		final boolean do_not_resize = (orig_file.length() < max_size); 

		Bitmap resized = CameraUtils.load_image_limit_size(orig_file, max_size, do_not_resize);

		File dir = orig_file.getParentFile();

		File resized_file = new File(dir, new_Filename);
		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(resized_file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
		resized.compress(Bitmap.CompressFormat.PNG, 100, fOut);
		try {
			fOut.flush();
			fOut.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resized_file;
	}




}
