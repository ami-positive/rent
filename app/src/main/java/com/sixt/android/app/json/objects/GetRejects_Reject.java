package com.sixt.android.app.json.objects;

import java.util.ArrayList;

import android.preference.PreferenceManager;

import com.sixt.android.activities.MenuMischarriActivity;
import com.sixt.android.activities.MenuPnimmiActivity;
import com.sixt.android.activities.RejectListActivity;
import com.sixt.android.app.json.response.GetCarDetails_Response;
import com.sixt.android.app.util.ActionValue;
import com.sixt.android.app.util.CarTypeUtils;
import com.sixt.android.app.util.PnimmiValue;
import com.sixt.android.ui.DisplayLogic;

public class GetRejects_Reject {

	public String RejectCode; // e.g. "INSURANCE", "Wheels"...
	public String RejectDesc; // show when error msg is displayed to user
	public String RejectType; // e.g. "RD"

	public transient int value = -1; //: before == 0
	
	public String CarType; // "P" or "PM" Prati Mischari
	
	public int RejectPage;
	public int RejectCheck; 
	public int RejectMOk;
	public int Optional; // show ONLY if returned by GetRentCarDetails
 
	public RejectCodeDesc[] RejectValues; // always length==3; first always selected by default 

	public SubReject[] SubReject; // optional subcategories of rejects;
	


	
	public void init_value_for_reject_and_subs_according_to_default_flag() {
		init_value_according_to_default_flag();		
		if (SubReject==null) {
			return;
		}
		for (SubReject sr: SubReject) {
			sr.init_value_according_to_default_flag();
		}
	}
	
	
	private void init_value_according_to_default_flag() {
		if ("WHEELS".equals(RejectCode)) {
			int jj=2354;
			jj++;
		}  
		boolean nodef = no_default_value();
		if (nodef) {
			value = -1;
		}
		else {
			value = 0;
		}
	}


	private boolean is_mesirra() {
		if (RejectType==null) {
			return false;
		}
		String tmp = RejectType.toUpperCase();
		return tmp.contains("D");
	}
		
	boolean is_hachzarra() {
		if (RejectType==null) {
			return false;
		}
		String tmp = RejectType.toUpperCase();
		return tmp.contains("R");
	}
	
	
	public boolean no_default_value() { //>> and user MUST do his selection
		if (RejectCode.toUpperCase().contains("WHEEL")) {
			int jj=234;
			jj++;
		}
		return RejectCheck == 1; //also - values returned in GetCarDetails are ADIFF of this field
	}
	
	
	public boolean is_mok() { 
//		boolean pnimmi_pticha =  PnimmiValue.Pnimmi() && ActionValue.Pticha();
//		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra();
//		mode_mesirra = pnimmi_pticha || mode_mesirra;  
//		return mode_mesirra && RejectMOk == 1;
		return DisplayLogic.mok_is_active() && RejectMOk == 1; // 
	}
	    				
	
	public boolean should_show() { //
//		boolean mode_mesirra = MenuMischarriActivity.Mesirra() || MenuPnimmiActivity.Mesirra(); 
		boolean mode_hachzarra = MenuMischarriActivity.Hachzarra() || MenuPnimmiActivity.Hachzarra();
//		boolean pnimmi_pticha =  PnimmiValue.Pnimmi() && ActionValue.Pticha();
				
//		mode_mesirra = pnimmi_pticha || mode_mesirra;
		
		boolean reject_mode_mesirra = DisplayLogic.reject_mode_mesirra();
		
		if (reject_mode_mesirra && !is_mesirra()) {
			return false;
		}
		
		if (mode_hachzarra && !is_hachzarra()) {
			return false;
		}
		
		if (Optional != 0) {			
//			if (!GetCarDetails_Response.in_reject_arr(RejectCode)) {
			if (!GetCarDetails_Response.in_Optional_arr(RejectCode)) {
				return false;
			}			
		}
		return true;
	}
		
	
//	public void clear_value_and_subrejects() {
//		value = 0; 
//		if (SubReject != null) {
//			for (SubReject sr: SubReject) {
//				sr.clear_value();
//			}
//		}
//	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GetRejects_Reject)) {
			return false;
		}
		GetRejects_Reject other = (GetRejects_Reject) o;
		return this.RejectCode.equals(other.RejectCode); 
	}

	// copy ctor; deep copy
	public GetRejects_Reject(GetRejects_Reject o) {
		value = o.value;
		RejectCode = o.RejectCode;
		RejectDesc = o.RejectDesc; 
		RejectCheck = o.RejectCheck;
		CarType = o.CarType;
		RejectValues = (o.RejectValues==null ? null : o.RejectValues.clone());
		SubReject = (o.SubReject==null ? null : o.SubReject.clone());
	}


	public GetRejects_Reject(String RejectCode2, String RejectDesc2,
			int value2, String carType2, RejectCodeDesc[] rejectValues2,
			SubReject[] subRejects2, int _RejectCheck) {
		value = value2;
		RejectCode = RejectCode2;
		RejectDesc = RejectDesc2; 
		CarType = carType2;
		RejectCheck = _RejectCheck;
		RejectValues = (rejectValues2==null ? null : rejectValues2.clone());
		SubReject = (subRejects2==null ? null : subRejects2.clone());
	}

	public static GetRejects_Reject filter(GetRejects_Reject src) {
		if (src.SubReject==null) {
			return src;
		}
		GetRejects_Reject dest = new GetRejects_Reject(src);
		ArrayList<SubReject> ff = new ArrayList<SubReject>(); 
		for (SubReject sr: dest.SubReject) {
			if (dest.is_p_or_pm()) {
				if (sr.is_p_or_pm()) {
					ff.add(sr);
				}
			}
			else {
				if (sr.is_m_or_pm()) {
					ff.add(sr);
				}
			}
		}

		SubReject[] res2 = ff.toArray(new SubReject[ff.size()]);
		dest.SubReject = res2;
		return dest;
	}

	public boolean is_p_or_pm() {
		return CarTypeUtils.is_p_or_pm(CarType);
	}


	public boolean is_m_or_pm() {
		return CarTypeUtils.is_m_or_pm(CarType);
	}



	public String left_str() {
//		RejectCodeDesc cur = RejectValues[0];
		if (RejectValues==null) {
			return "";
		}
		if (RejectValues.length < 3) {
			return "";
		}
		RejectCodeDesc cur = RejectValues[2];
		return cur.RejectValueDesc;
	}

	public String center_str() {
		if (RejectValues==null) {
			return "";
		}
		if (RejectValues.length < 2) {
			return "";
		}
		RejectCodeDesc cur = RejectValues[1];
		return cur.RejectValueDesc;
	}

	
	public String right_str() {
//		RejectCodeDesc cur = RejectValues[2];
		if (RejectValues==null) {
			return "";
		}
		if (RejectValues.length < 1) {
			return "";
		}
		RejectCodeDesc cur = RejectValues[0]; 
		return cur.RejectValueDesc;
	}



	public boolean has_subrejects() {
		boolean show = (SubReject != null && SubReject.length > 0); 
		return show;
	}

	public SubReject[] filter_SubRejects(boolean is_private_car) {
		if (SubReject==null) {
			return null;
		}
		
		ArrayList<SubReject> res = new ArrayList<SubReject>(); 
		for (SubReject sr: SubReject) {
			if (!sr.should_show()) {
				continue;
			}
			
			if (is_private_car) {
				if (sr.is_p_or_pm()) {
					res.add(sr);
				}
			}
			else {
				if (sr.is_m_or_pm()) {
					res.add(sr);
				}				
			}
		}
		int len = res.size();
		SubReject[] res_arr = res.toArray(new SubReject[len]);
		return res_arr;
	}

	public GetRejects_Reject duplicate() {
		GetRejects_Reject other = new GetRejects_Reject(
				this.RejectCode, this.RejectDesc,
				this.value, this.CarType, 
				this.RejectValues, this.SubReject, this.RejectCheck);
		return other;
	}



}


