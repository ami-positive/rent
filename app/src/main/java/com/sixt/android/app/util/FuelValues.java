package com.sixt.android.app.util;

import com.sixt.android.MyApp;

public class FuelValues {
	
	private FuelValues() {}

	public static String comments = "";
	public static int driver_name_ind = -1;
	public static int musach_ind = -1;


	public String getMad_utz() {
		return MyApp.appSetting.getKm();
	}

	public void setMad_utz(String mad_utz) {
		MyApp.appSetting.setKm(mad_utz);
	}

	public int getFuel_value() {
		return MyApp.appSetting.getFuel();
	}

	public void setFuel_value(int fuel_value) {
		MyApp.appSetting.setFuel(fuel_value);
	}

	public int getReturn_branch_ind() {
		return MyApp.appSetting.getReturnReason();
	}

	public void setReturn_branch_ind(int return_branch_ind) {
		MyApp.appSetting.setReturnReason(return_branch_ind);
	}


	
	public static void clear() {
		MyApp.appSetting.setFuel(-1);
		MyApp.appSetting.setKm("0");
		MyApp.appSetting.setReturnBranch(-1);
		driver_name_ind = -1;
		musach_ind = -1;
		comments = "";
	}

}
