package com.sixt.android.app.json.objects;

public class ContractRecord {
    public int ContractCode;
    public String ContractName;
    public int CiCoIndex;    
    public int GarageDefault;
    public int ForDisplay;
    public int BContractNo;
}
