package com.sixt.android.app.json.request;

import com.sixt.android.app.json.response.CheckCarExists_Response;

public class CheckCarExists_Request {
	final CheckCarExists_Request_Inner CheckCarExists_req;
	
	public CheckCarExists_Request(String car_no, String action) {
		CheckCarExists_req = new CheckCarExists_Request_Inner(car_no);
		CheckCarExists_Response.carExists_last_response = null;
	}
}

