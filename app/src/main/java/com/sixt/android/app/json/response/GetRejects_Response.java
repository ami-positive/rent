package com.sixt.android.app.json.response;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;

import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.MandatoryImgRecord;
import com.sixt.android.app.json.objects.OptionalItemsRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.app.util.AppContext;
import com.sixt.android.app.util.GGson;

public class GetRejects_Response extends Base_Response_JsonMsg { //== carDefects

	private static final String REJECT_LIST_KEY = "REJECT_LIST_KEY";

	private static final String REJECT_LIST_NAME = "REJECT_LIST_NAME";

	private static final HashMap<String, ArrayList<RejectRecord>>
	secondlevel_rejects = new HashMap<String, ArrayList<RejectRecord>>();


	private static GetRejects_Response last_response;

	public GetRejects_Response_Inner GetRentRejects_res;

	@Override
	public void postLoad() {
		super.postLoad();
		last_response = this;
		if (last_response.GetRentRejects_res  != null) {
			last_response.GetRentRejects_res.postLoad();
			last_response.GetRentRejects_res.debug_print();			
			write_to_shared_prefs();
		}
	}


	private void write_to_shared_prefs() {
		try { 
			//			Context c = LoginActivity.inst;
			Context c = AppContext.appContext; 
			SharedPreferences preferences = c.getSharedPreferences(REJECT_LIST_NAME, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();		
			String txt = GGson.toJson(last_response);
			editor.putString(REJECT_LIST_KEY, txt);
			editor.commit();
		}
		catch (Exception e) {
			// no op
			int jj=324; 
			jj++; 
		}
	}

	private static GetRejects_Response get_stored_response() { 
		try { 
			//			Context c = LoginActivity.inst;
			Context c = AppContext.appContext; 
			SharedPreferences preferences = c.getSharedPreferences(REJECT_LIST_NAME, Context.MODE_PRIVATE);
			String val = preferences.getString(REJECT_LIST_KEY, null);
			if (val==null) {
				return null;
			}
			GetRejects_Response obj = 
					(GetRejects_Response) GGson.fromJson(val, GetRejects_Response.class);
			return obj;
		}
		catch (Exception e) {
			int jj=234;
			jj++;
			// no op
		}
		return null; 
	}

	
	public static GetRejects_Reject[] get_all_available_rejects(boolean is_private) { 
		clear_subreject_map(); //!
		GetRejects_Response reject_list = get_last_response();
		if (reject_list==null) {
			return null;
		}
		if (reject_list.GetRentRejects_res==null) {
			return null;
		}
		GetRejects_Reject[] available_rejects = 
				reject_list.GetRentRejects_res.get_Rejects(is_private, true);
		
//		if (from_all_sources) {
//			available_rejects = complete_from_all_sources(available_rejects);
//		}
		
		set_selected_values_to_default(available_rejects); 

		set_actual_reject_values_for_car(available_rejects);

		return available_rejects;
	} 


	private static GetRejects_Reject[] complete_from_all_sources(
			GetRejects_Reject[] available_rejects) {
		// add optionals from GetCarDetails
		
		return available_rejects;
				
//		read below
//		   filterred by Merisa/Hachzarra and (if Optijonal) if Appearing
//		   in GetCarDetails.Optional array (otional is only for list);		   
//		   
//		   (MOK only for MESSIRA)
//		
//		   also filter by car type (prati/mischarri) as before
//		
//		   GetCarDetails.Rejects:  mandatory >> LIST AND Image; FORCE ADD ALL IN how do we merge in GetCarDetails.Rejects ?
//				
//		   subRejects -- exactly same filter!; 
//				
//		   image behavior -- same AS old, uses CarDetails.Rejects; accept for 'no-lesser' logic + ALSO for List;
//				
//		   RejectCheck==1 -- for LIST only; BOTH Rejects and subRejects 
//																
//		return merged;
	}


	private static GetRejects_Reject get_reject_from_code(final String _code) {
		if (_code==null) {
			return null;
		}
		GetRejects_Response reject_list = get_last_response();
		if (reject_list==null) {
			return null;
		}
		if (reject_list.GetRentRejects_res==null) {
			return null;
		}
		GetRejects_Reject[] marr = reject_list.GetRentRejects_res.Rejects;
		if (marr==null) {
			return null;
		}
		
		for (GetRejects_Reject r: marr) {
			if (_code.equals(r.RejectCode)) {
				return r;
			}
		}
		return null;
	}


	private static GetRejects_Response get_last_response() {
		if (last_response != null) {
			return last_response;
		} 
		GetRejects_Response old = get_stored_response();
		if (old != null) {
			return old;
		}
		// write error;
		return null;

	}


	private static void set_actual_reject_values_for_car(GetRejects_Reject[] available_rejects) {
		if (available_rejects==null) {
			return;
		}
		RejectRecord[] actual = null;
		SendForm_Request last_form = CarNumberActivity.lastForm;
		if (last_form != null) {
			actual = gset_actual_from_last_form(last_form);
		}
		else {
			actual = get_actual_from_last_cat_details_response();
		}

		if (actual==null) {
			return;
		}

		HashMap<String, RejectRecord> actual_car_rejects = create_map(actual);

		// rejects from car_rejects which does not appear in available_rejects are always irrelevant 

		// 1st level reject loop e.g. {"Code":"Wheel","Value":3,"image":0},
		for (GetRejects_Reject cur: available_rejects) {
			String cur_code = cur.RejectCode;
			RejectRecord actual_reject = actual_car_rejects.get(cur_code);
			if (actual_reject != null) { 
				// set initial selection 
				cur.value = actual_reject.Value;
			}
		}


		// subreject loop e.g. {"Code":"Wheel-1","Value":3,"image":0},
		for (GetRejects_Reject cur_1st_level: available_rejects) {
			SubReject[] sub = cur_1st_level.SubReject;
			if (sub==null) {
				continue;
			}			
			for (SubReject cur_subreject: sub) {
				String cur_code = cur_subreject.SubRejectCode;
				RejectRecord car_subreject = actual_car_rejects.get(cur_code); 
				if (car_subreject != null) { 
					cur_1st_level.value = 1; // hard coded!
					// set initial selection
					add_to_subreject_map(cur_1st_level.RejectCode, car_subreject);
				}

			}
		}		

	}


	private static RejectRecord[] gset_actual_from_last_form(SendForm_Request last_form) {
		return last_form.get_list_only_rejects();
	}


	private static RejectRecord[] get_actual_from_last_cat_details_response() {
		GetCarDetails_Response carDtls = GetCarDetails_Response.carDetails_response;
		if (carDtls==null || carDtls.GetRentCarDetails_res==null) {
			return null;
		}
		RejectRecord[] actual_rejects = carDtls.GetRentCarDetails_res.get_list_only_rejects();		
		return actual_rejects;
	}

	private static void clear_subreject_map() {
		secondlevel_rejects.clear();
	}

	public static ArrayList<RejectRecord> get_subrejects_for_reject(GetRejects_Reject reject) {
		if (reject==null) {
			return null;
		}
		String code = reject.RejectCode;
		ArrayList<RejectRecord> sub_rejects = secondlevel_rejects.get(code);
		return sub_rejects;
	}


	private static void add_to_subreject_map(String rejectCode, RejectRecord car_subreject) {
		ArrayList<RejectRecord> val = secondlevel_rejects.get(rejectCode);
		if (val==null) {
			secondlevel_rejects.put(rejectCode, new ArrayList<RejectRecord>());
			val = secondlevel_rejects.get(rejectCode);
		}
		val.add(car_subreject);
	}


	private static HashMap<String, RejectRecord> create_map(RejectRecord[] arr) {
		HashMap<String, RejectRecord> map = new HashMap<String, RejectRecord>();
		if (arr==null) {
			return map;
		}
		for (RejectRecord cur: arr) {
			map.put(cur.Code, cur); // "INSURANCE" , "Wheel1" , "HLF" etc
		}
		return map;
	}

	private static void set_selected_values_to_default(GetRejects_Reject[] rejects) {
		if (rejects != null) {
			for (GetRejects_Reject cur: rejects) {
				cur.init_value_for_reject_and_subs_according_to_default_flag();
			}
		}
	}

	public static void clear_last_response() { 
		last_response = null;
	}


	public static MandatoryImgRecord[] get_mandatory_img() {
		clear_subreject_map(); //!
		GetRejects_Response reject_list = get_last_response();
		if (reject_list==null) {
			return null;
		}
		if (reject_list.GetRentRejects_res==null) {
			return null;
		}
		MandatoryImgRecord[] marr = reject_list.GetRentRejects_res.MandatoryImg;
		return marr;
	}

}

