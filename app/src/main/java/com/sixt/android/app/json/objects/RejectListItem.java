package com.sixt.android.app.json.objects;

public class RejectListItem {
	public final int RejectCiNo;
	public String RejectRemarkes = "";
	
	public RejectListItem(int num, String remarks) {
		RejectCiNo = num;
		RejectRemarkes = remarks;		
	}
}
