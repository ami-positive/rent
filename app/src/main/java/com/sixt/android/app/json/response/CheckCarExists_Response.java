package com.sixt.android.app.json.response;

import com.sixt.android.app.json.Base_Response_JsonMsg;

public class CheckCarExists_Response extends Base_Response_JsonMsg {
		
	public CheckCarExists_Response_Inner CheckCarExists_res;

	public static CheckCarExists_Response carExists_last_response;

	public static String get_car_model() { 
		if (carExists_last_response==null || carExists_last_response.CheckCarExists_res==null) {
			return "";
		}
		return carExists_last_response.CheckCarExists_res.CarModel;
	}
		
	@Override
	public void postLoad() {
		// TODO Auto-generated method stub
		super.postLoad();
		carExists_last_response = this;
	}

	public String getCarModel() {
		return CheckCarExists_res.CarModel;
	}

		
}	
 
