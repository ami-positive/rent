package com.sixt.android.app.util;

import com.google.gson.Gson;

public class GGson {
	private GGson() {}
	
	public static String toJson(Object src) {
		return new Gson().toJson(src);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object fromJson(String json, Class classOfT) {
		return new Gson().fromJson(json, classOfT);
	}

}
