package com.sixt.android.app.json.request;

import com.sixt.android.app.json.response.GetRejects_Response;

public class GetRejects_Request {
	final GetRejects_Request_Inner GetRejects_req;
	
	public GetRejects_Request() {
		GetRejects_req = new GetRejects_Request_Inner();
		GetRejects_Response.clear_last_response();
	}
}

