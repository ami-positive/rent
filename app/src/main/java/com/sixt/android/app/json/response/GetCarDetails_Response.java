package com.sixt.android.app.json.response;

import com.sixt.android.activities.CarNumberActivity;
import com.sixt.android.app.json.Base_Response_JsonMsg;
import com.sixt.android.app.json.objects.GetRejects_Reject;
import com.sixt.android.app.json.objects.OptionalItemsRecord;
import com.sixt.android.app.json.objects.RejectRecord;
import com.sixt.android.app.json.objects.SubReject;
import com.sixt.android.app.json.request.SendForm_Request;
import com.sixt.android.data.RejectPoint;

import java.util.ArrayList;

public class GetCarDetails_Response extends Base_Response_JsonMsg {

	private static final int DRIVER_EXIST_DEFAULT = 1; // driver does exist

	public static GetCarDetails_Response carDetails_response;
	
	private static int is_DriverExist = DRIVER_EXIST_DEFAULT;
	
	public GetCarDetails_Response_Inner GetRentCarDetails_res;	

	
	public static boolean show_next_button() {
		System.out.println("BTN HERO200 " + carDetails_response);
		System.out.println("BTN HERO201 " + carDetails_response.GetRentCarDetails_res);
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return true;
		}
		int val = carDetails_response.GetRentCarDetails_res.CiCoFlag;
		System.out.println("BTN HERO202 " + carDetails_response.GetRentCarDetails_res.CiCoFlag);
		boolean show_next = (val==1);
		return show_next;
	}
	
	public static int get_IntContractTypeCode() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		return carDetails_response.GetRentCarDetails_res.IntContractTypeCode;
	}
	
	public static RejectRecord[] get_Rejects() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return null;
		}
		return carDetails_response.GetRentCarDetails_res.Rejects;
	}

	public static OptionalItemsRecord[] get_OptionalItems() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return null;
		}
		return carDetails_response.GetRentCarDetails_res.OptionalItems;
	}

	
	public static class FuelAlert {
		public final String errmsg;
		public final boolean stop;
		public FuelAlert(String  msg, boolean _stop) {
			errmsg = msg;
			stop = _stop;
		}
	}
	
	
	public static String getCustMail() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return "";
		}
		return carDetails_response.GetRentCarDetails_res.CustMail;		
	}
	
	public static int get_DriverExist() {
		return is_DriverExist ;
	}
	


	public static GetCarDetails_Response_Inner get_last_car_details() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return null;
		}
		return carDetails_response.GetRentCarDetails_res;
	}
	
		
	public static boolean in_Optional_arr(String RejectCode) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return false;
		}
		OptionalItemsRecord[] reject_arr = carDetails_response.GetRentCarDetails_res.OptionalItems;
		if (reject_arr==null) {
			return false;
		}
		
		for (OptionalItemsRecord rr: reject_arr) {
			if (rr.Code != null && rr.Code.equals(RejectCode)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean in_reject_arr(String RejectCode) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return false;
		}
		RejectRecord[] reject_arr = carDetails_response.GetRentCarDetails_res.Rejects;
		if (reject_arr==null) {
			return false;
		}
		
		for (RejectRecord rr: reject_arr) {
			if (rr.Code != null && rr.Code.equals(RejectCode)) {
				return true;
			}
		}
		return false;
	}

	
	public static int get_orig_car_reject_val(String RejectCode) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		RejectRecord[] reject_arr = carDetails_response.GetRentCarDetails_res.Rejects;
		if (reject_arr==null) {
			return -1;
		}
		
		for (RejectRecord car_reject: reject_arr) {
			if (car_reject.Code != null && car_reject.Code.equals(RejectCode)) {
				int val = car_reject.Value;
				return val;
			}
		}
		return -1;
	}
	
	
	public static RejectRecord get_reject(String _RejectCode) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return null;
		}
		RejectRecord[] reject_arr = carDetails_response.GetRentCarDetails_res.Rejects;
		if (reject_arr==null) {
			return null;
		}		
		for (RejectRecord rr: reject_arr) {
			if (rr.Code != null && rr.Code.equals(_RejectCode)) {
				return rr;
			}
		}
		return null;		
	}




	public static void add_actual_rejects_type_list(GetRejects_Reject[] all_available_list_rejects) {
		RejectRecord[] list_only_rejects = null;		
		SendForm_Request last_form = CarNumberActivity.lastForm; 
		if (last_form != null) {
			list_only_rejects = last_form.get_list_only_rejects();
		}
		else {
			if (carDetails_response != null && carDetails_response.GetRentCarDetails_res != null) {
				GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
				list_only_rejects = inner.get_list_only_rejects();
			}
		}
		
		
		if (list_only_rejects==null) {
			return;
		}
		for (RejectRecord r_rec: list_only_rejects) {
			String code = r_rec.Code;
			int value = r_rec.Value;
			GetRejects_Reject reject = get_list_reject_by_code(all_available_list_rejects, code);
			if (reject != null) {
				reject.value = value;
//				active_reject_arr.add(reject);
			}
			else {
				if ((reject=find_in_subrejects(all_available_list_rejects, code)) != null) {
					reject.value = 1; // always the value when subrejects are set
				}
			}
		}
	}


	private static GetRejects_Reject find_in_subrejects(GetRejects_Reject[] all_available_list_rejects, String code) {
		for (GetRejects_Reject reject: all_available_list_rejects) {
			if (reject.SubReject != null) {
				for (SubReject sr: reject.SubReject) {
					if (sr.SubRejectCode.equals(code)) {
						return reject;
					}
				}
			}
		}
		return null;
	}


	private static String debug_available_codes;
	
	private static GetRejects_Reject get_list_reject_by_code(GetRejects_Reject[] all_available_list_rejects, String code) {
		
		if (debug_available_codes==null) {
			debug_available_codes = debug_get_all_codes(all_available_list_rejects);
		}
		String cur_code = code;
		int jj=234;
		jj++;
		
		for (GetRejects_Reject reject: all_available_list_rejects) {
			if (reject.RejectCode.equals(code)) {
				return reject;
			}
		}
		return null;
	}


	private static String debug_get_all_codes(GetRejects_Reject[] all_available_list_rejects) {
		String res = "";
		if (all_available_list_rejects==null) {
			return " ";
		}
		for (GetRejects_Reject r: all_available_list_rejects) {
			res += (" "  + r.RejectCode);
		}
		return res;
	}


	public static void add_actual_rejects_type_image(ArrayList<RejectPoint> active_reject_arr, RejectPoint[] all_available_rejectPoints) {
		RejectRecord[] image_only_rejects = null;		
		SendForm_Request last_form = CarNumberActivity.lastForm;
		if (last_form != null) {
			image_only_rejects = last_form.get_image_only_rejects();
		}
		else {
			if (carDetails_response != null && carDetails_response.GetRentCarDetails_res != null) {
				GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
				image_only_rejects = inner.get_image_only_rejects();
			}
		}

		if (image_only_rejects==null) {
			return;
		}
		for (RejectRecord r_rec: image_only_rejects) {
			String code = r_rec.Code;
			int value = r_rec.Value;
			if (code.contains("00")) {
				int jj=234;
				jj++;
			}
			RejectPoint pt = get_image_reject_by_code(all_available_rejectPoints, code);
			if (pt != null) {
				pt.reject_code_position = pt.get_position_from_value(value); 
				active_reject_arr.add(pt);
			}
			else {
				// ok, its a LIST reject
			}
		}
	}


	private static RejectPoint get_image_reject_by_code(RejectPoint[] all_available_rejectPoints, String code) {
		for (RejectPoint pt: all_available_rejectPoints) {
			if (pt.Reject_Code.equals(code)) {
				return pt;
			}
		}
		return null;
	}


	public static boolean is_p_or_pm() {
		if (carDetails_response==null) {
			return true; // = DEFAULT
		}
		if (carDetails_response.GetRentCarDetails_res==null) {
			return true; // = DEFAULT
		}
		return carDetails_response.GetRentCarDetails_res.is_p_or_pm();
	}

	public static String getWhatToDo (){
		if (carDetails_response == null){
			return null;
		}
		try{
			return carDetails_response.GetRentCarDetails_res.What2Do;
		}catch (Exception e){
			return null;
		}

	}

//	public RejectRecord[] filter_list_rejects() {
//		if (GetRentCarDetails_res==null) {
//			return null;
//		}
//		return GetRentCarDetails_res.get_list_only_rejects();
//	}

	public String getCarModel() {
		if (GetRentCarDetails_res==null) {
			return "";
		}
		return GetRentCarDetails_res.CarModel;
	}
 

	@Override
	public void postLoad() {
		super.postLoad();				
		carDetails_response = this;
		if (GetRentCarDetails_res != null) {
			GetRentCarDetails_res.validate();
			is_DriverExist = GetRentCarDetails_res.DriverExist; 
		}
	}



	public static void clear() {
		carDetails_response = null;
		is_DriverExist = DRIVER_EXIST_DEFAULT;
	}


	public static ArrayList<RejectRecord> get_actual_SUBrejects(
			SubReject[] all_available_subrejects) {
		if (carDetails_response==null) {
			return null;
		}
		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
		if (inner==null) {
			return null;
		}
		return inner.get_actual_SUBrejects_inner(all_available_subrejects);
	}



	public static String error_msg() {
		return "מד אוץ שגוי";
//		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
//			return "";
//		}		
//		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;		
//		long min_str = inner.KmMin;
//		long max_str = inner.KmMax;
//		String err = "יש לציין ערך אוץ בין " + min_str + " ו- " + max_str;		
//		return err;
	}


 
//	public static boolean mad_utz_ok(String txt_mileage) { //gggg old handling of mad_utz
//		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
//			return true;
//		}
//		
//		long utz_val;		
//		try { 
//			utz_val = Long.parseLong(txt_mileage);
//		}
//		catch (Exception e) {
//			return true;
//		}
//		
//		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
//		
////		long max = inner.KmMax;
//		long min = inner.KmMin;
//
////		if (utz_val > max || utz_val < min) {
//		if (utz_val < min) {
//			return false;
//		}
//		
//		return true;
//	}


	public static boolean mad_utz_too_small(String txt_mileage) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return false;
		}
		
		long utz_val;		
		try { 
			utz_val = Long.parseLong(txt_mileage);
		}
		catch (Exception e) {
			return false;
		}
		
		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
		
//		long max = inner.KmMax;
		long min = inner.KmMin;

//		if (utz_val > max || utz_val < min) {
		if (utz_val < min) {
			return true;
		}
		
		return false;
	}
	
	public static boolean mad_utz_too_large(String txt_mileage) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return false;
		}
		
		long utz_val;		
		try { 
			utz_val = Long.parseLong(txt_mileage);
		}
		catch (Exception e) {
			return false;
		}
		
		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
		
		long max = inner.KmMax;
		if (utz_val > max) {
			return true;
		}
		
		return false;
	}

	
	public static int min_mad_utz_val() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
				
		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
		
//		long max = inner.KmMax;
		long min = inner.KmMin;
		return (int)min;
	}	

//	public static boolean above_alert_value(String txt_mileage) {
//		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
//			return false;
//		}
//		
//		long utz_val;		
//		try { 
//			utz_val = Long.parseLong(txt_mileage);
//		}
//		catch (Exception e) {
//			return false;
//		}
//		
//		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;
//		
//		long alert = inner.KmAlert;
//
//		if (utz_val > alert) {
//			return true;
//		}
//		
//		return false;
//	}



	public static FuelAlert fuel_alert_needed(int fuel_level) {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return null;
		}
		GetCarDetails_Response_Inner inner = carDetails_response.GetRentCarDetails_res;		
		
		
		final int old_value = inner.FuelStatus;
		System.out.println("sssss" + old_value);
		final int ind = inner.FuelIndex;
		System.out.println("sssssa" + ind);
		if (fuel_level == old_value) {
			return null; // same level
		}
		
		if (ind==0) {
			// do nothing
			return null;
		}
		else if (ind==1) {
			// do nothing
			return new FuelAlert("שים לב מצב הדלק שונה מהמוגדר בחוזה", false);
		}
		else if (ind==2) {
			if (fuel_level < old_value) { //
				return new FuelAlert("מצב הדלק נמוך מהמוגדר בחוזה", true);
			}
		}	
		return null;
	}

		
	public static boolean can_change_branch() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return true;
		}
		boolean can_change = carDetails_response.GetRentCarDetails_res.ReturnBranchOpt == 1;
		return can_change;
	}

	public static int get_suncontract_no() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		int val = carDetails_response.GetRentCarDetails_res.SubContract;
		return val;
	}
	
	public static int get_contract_no() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		int val = carDetails_response.GetRentCarDetails_res.ContractNo;
		return val;
	}

	

	public static int get_PickupBranchNo() { // == DepartureBranchNo
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		long val = carDetails_response.GetRentCarDetails_res.PickupBranchNo;
		return (int)val;
	}

	
	public static int get_ReturnBranchNo() {
		if (carDetails_response==null || carDetails_response.GetRentCarDetails_res==null) {
			return -1;
		}
		long val = carDetails_response.GetRentCarDetails_res.ReturnBranchNo;
		return (int)val;
	}


}	

