package com.sixt.android.app.json.request;

import com.sixt.android.app.json.BaseJsonMsg;
import com.sixt.android.app.util.CurrentSixtApp;
import com.sixt.android.app.util.LoginSixt;

public class GetRejects_Request_Inner extends BaseJsonMsg {
	public final String App = CurrentSixtApp.type();
	public final String Login = LoginSixt.get_Worker_No(); 
	
	
	public GetRejects_Request_Inner() {
		super(GetRejectsRequest);
	}
}
